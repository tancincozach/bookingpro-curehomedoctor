<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//http://www.dropbox-php.com/docs
# Include the Dropbox SDK libraries
/*require_once APPPATH."/third_party/dropbox-sdk-php-1.1.5/lib/Dropbox/autoload.php";
use \Dropbox as dbx;*/

class Test extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

	}

	public function get_suburb_available_test($str='')
	{
		$this->load->model('Areamodel', 'areas');

		echo '<pre>';
		print_r($this->areas->get_suburb_available($str, '2015-10-09'));
		echo $this->db->last_query();
		echo '</pre>';
	}
 	
 	public function pdfhtml(){

		$this->load->library("pdfhelper");
		$this->load->helper('file'); 

		 // page info here, db calls, etc.     
		$html = $this->load->view('test/pdf-test', null, true);
		
		$this->pdfhelper->renderPDF($html,"roster.pdf", false);
		/* or
		 $data = pdf_create($html, '', false);
		 write_file('name', $data);*/
 	}

 	public function dropbox($p=1,$id=''){

 		

		$params['key'] = '1urjc7l5cn672ns';
		$params['secret'] = 'x5l38brdo067zml';


		if( $p == 1 ){

			$this->load->library('dropbox', $params);

			$data = $this->dropbox->get_request_token(site_url("test/dropbox/2/45/"));
			
			/*echo '<pre>'; 
			print_r($data);
			echo '</pre>'; */
			
			$this->session->set_userdata('token_secret', $data['token_secret']);

			redirect($data['redirect']);

			//'http://localhost/local/housecalldoctor/test/dropbox/2/45?oauth_token=Ahs5QKy8xO1lonBD&uid=497226702'

			//http://localhost/local/housecalldoctor/ http%3A//localhost/local/housecalldoctor/test/dropbox/2/45?oauth_token=ZjFomwTRHzye6DH2&uid=497226702
		}
		
		if( $p == 2 ){

			$uri = $this->uri->segment(3);

			$this->load->library('dropbox', $params);

			$oauth = $this->dropbox->get_access_token($this->session->userdata('token_secret'));
 
 			$this->session->set_userdata('oauth_token', $oauth['oauth_token']);
			$this->session->set_userdata('oauth_token_secret', $oauth['oauth_token_secret']);

			redirect('test/dropbox/3/'.$this->uri->segment(4));

			/*echo '<pre>'; 
	        print_r($oauth);
	       // print_r($dbobj);
			echo '</pre>'; 		*/	 
		}

		if( $p == 3 ){

			$params['access'] = array('oauth_token'=>urlencode($this->session->userdata('oauth_token')),
									  'oauth_token_secret'=>urlencode($this->session->userdata('oauth_token_secret')));
			
			$this->load->library('dropbox', $params);
			
	        //$dbobj = $this->dropbox->account();
			
			/*echo '<pre>'; 
	        print_r($dbobj);
			echo '</pre>';*/ 

 			 
			$response = $this->dropbox->add('/', 'files/medicare/HCD15120005-02DEC15CARMELORODAMV.pdf');

			echo '<pre>'; 
			print_r($response);
			echo '</pre>'; 

		}
	 

 	}


 	public function dropbox2(){



		$appInfo = dbx\AppInfo::loadFromJsonFile(APPPATH.'config/dropbox.json');
		/*$appInfo = '{
  "key": "1urjc7l5cn672ns",
  "secret": "x5l38brdo067zml"
}';

		$keys['key'] = '1urjc7l5cn672ns';
		$keys['secret'] = 'x5l38brdo067zml';
*/

		$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");

		$authorizeUrl = $webAuth->start();

		echo "1. Go to: " . $authorizeUrl . "\n";
		echo "2. Click \"Allow\" (you might have to log in first).\n";
		echo "3. Copy the authorization code.\n";
		$authCode = \trim(\readline("Enter the authorization code here: "));

		$authCode = 'xCcJnzZSmaAAAAAAAAAADcUdtCCuGaYmdR4LsR6D5cs';

		list($accessToken, $dropboxUserId) = $webAuth->finish($authCode);
		print "Access Token: " . $accessToken . "\n";

		$dbxClient = new dbx\Client($accessToken, "PHP-Example/1.0");
		$accountInfo = $dbxClient->getAccountInfo();

		print_r($accountInfo);

		$f = fopen("files/medicare/HCD15120005-02DEC15CARMELORODAMV.pdf", "rb");
		$result = $dbxClient->uploadFile("./files/medicare/HCD15120005-02DEC15CARMELORODAMV.pdf", dbx\WriteMode::add(), $f);
		fclose($f);
		print_r($result);

		$folderMetadata = $dbxClient->getMetadataWithChildren("/");
		print_r($folderMetadata);

		$f = fopen("working-draft.txt", "w+b");
		$fileMetadata = $dbxClient->getFile("/HCD15120005-02DEC15CARMELORODAMV.pdf", $f);
		fclose($f);
		print_r($fileMetadata);

 	}

 	public function dropbox3(){
 		require_once APPPATH."libraries/DropboxUploader.php";

 		$uploader = new DropboxUploader('carmeloroda.wdi@gmail.com', 'milo*dropbox');
        $uploader->upload('files/medicare/HCD15120005-02DEC15CARMELORODAMV.pdf', '', 'HCD15120005-02DEC15CARMELORODAMV.pdf');

 	}


 	public function foxbox($phoneNumber=''){

 		try {
 			
 			if( $phoneNumber == '' ) throw new Exception("Error Processing Request: Phone Number is empty", 1);
 			

	 		$this->load->library('Foxbox');

	 		
	 		$foxbox = $this->foxbox->sendSms($phoneNumber, 'Test from House Call Doctor');

	 		echo '<pre>';

	 		if( $this->foxbox->success ){
	 			echo 'Test SMS sent successfully to: '.$phoneNumber;
	 		}

	 		echo '<br />';
	 		echo $foxbox;
 			echo '</pre>';

		} catch (Exception $e) {
 			echo $e->getMessage();
 		}

 	}
 	
}
