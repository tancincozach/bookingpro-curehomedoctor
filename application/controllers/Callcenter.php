<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callcenter extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container-fluid';

		$this->Commonmodel->access('0,1,2,3,53,6');
	}

	function build_where($params){

		$params['where_str'] = '';

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = strtolower(trim($params['search']));
			
			$search = addslashes($search);

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		/*$like = " ( lower(`ref_number_formatted`) LIKE '%{$search}%' OR ";
		 		$like .= " lower(`issue_type`) LIKE '%{$search}%' OR ";
		 		$like .= " lower(`form_details`) LIKE '%{$search}%' ) "; */

				$like = " ( lower(`ref_number_formatted`) LIKE '%{$search}%' OR ";
				$like .= " lower(`medicare_number`) LIKE '%{$search}%' OR ";
				$like .= " lower(`firstname`) LIKE '%{$search}%' OR ";
				$like .= " lower(`lastname`) LIKE '%{$search}%' OR ";
				$like .= " lower(concat(`firstname`,' ',`lastname`)) LIKE '%$search%' OR ";
				
				$like .= " lower(`phone`) LIKE '%{$search}%' OR ";
				$like .= " lower(`mobile_no`) LIKE '%{$search}%' OR ";
				$like .= " lower(`street_addr`) LIKE '%{$search}%' OR ";
				$like .= " lower(`suburb`) LIKE '%{$search}%' ) ";
	 		}
	 		$params['where_str'] .= $like;	

	 		unset($params['search']);
		}

		if( isset($params['search2']) AND $params['search2'] != ''){

			$search = strtolower(trim($params['search2']));
			$search = explode('|', $search);

			$like = " ( (lower(`firstname`) LIKE '%{$search[0]}%' AND ";
			$like .= " lower(`lastname`) LIKE '%{$search[1]}%' ) OR ";
			$like .= " lower(`phone`) LIKE '%{$search[2]}%' ) ";

			$params['where_str'] .= $like;	

	 		unset($params['search2']);

	 		if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';

			$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."') ";
		}

		if( isset($params['appt_start']) &&  $params['appt_start'] != '' ) {
			 
			$dt = explode(' - ',$params['appt_start']); 
			
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			
			//echo $dt_f.' ' .$dt_t;
 			
 			if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';

			//$params['where_str'] .= " (DATE_FORMAT(CONVERT_TZ(`tran_created`, '".$this->serv_tz."', '".$this->site_tz."'),'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";
			
			//comment last 20151228
			//$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";

 			//added last 20151228
			$params['where_str'] .= " (	DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}' ) ";
		}
 
 
		/*if( isset($params['book_status']) AND count($params['book_status'])>0 ){ 
			 
			if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
			
			$params['where_str'] .= ' book_status IN ('.implode(',',array_values($params['statuses'])).')';
		}*/

		/*if( isset($params['area_id']) AND count($params['area_id'])>0 ){ 
			 
			if( count($params['area_id']) == 1 ){

				$params['where'] = array('appointment.area_id'=>$params['area_id'][0]);

			}else{

				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`area_id` IN ('.implode(',',array_values($params['area_id'])).')';				
			}
		}*/		

		//get variables
		//$area = Areas
		//$bst = booking status
		//chap = chaprone
		//doc = doctor
		//car = car

		if( isset($params['bst']) AND count($params['bst'])>0 ){ 
			 
			if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
			
			$params['where_str'] .= ' book_status IN ('.implode(',',array_values($params['bst'])).')';
		}

		if( isset($params['area']) AND count($params['area'])>0 ){ 
			 
			if( count($params['area']) == 1 ){
				$params['where'] = array('appointment.area_id'=>$params['area'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`area_id` IN ('.implode(',',array_values($params['area'])).')';				
			}
		}

		if( isset($params['chap']) AND count($params['chap'])>0 ){
			if( count($params['chap']) == 1 ){
				$params['where'] = array('appointment.chaperone_id'=>$params['chap'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`chaperone_id` IN ('.implode(',',array_values($params['chap'])).')';				
			}
		}

		if( isset($params['doc']) AND count($params['doc'])>0 ){
			if( count($params['doc']) == 1 ){
				$params['where'] = array('appointment.doctor_id'=>$params['doc'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`doctor_id` IN ('.implode(',',array_values($params['doc'])).')';				
			}
		}

		if( isset($params['car']) AND count($params['car'])>0 ){
			if( count($params['car']) == 1 ){
				$params['where'] = array('appointment.car_id'=>$params['car'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`car_id` IN ('.implode(',',array_values($params['car'])).')';				
			}
		}

		return $params;
	}

	public function index()	 
	{
		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Areamodel', 'areas');
		$this->load->model('Usermodel', 'user');
		//$this->load->model('Commonmodel');
		$this->load->library("pagination");

		//get variables
		//$area = Areas
		//$bst = booking status
		//chap = chaprone
		//doc = doctor
		//car = car

 		$data = array();

 		//set search variables
 		$search_daterange = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$timezone = $this->default_timezone;
		$timezone_date = $this->Commonmodel->get_server_converted_date($timezone);
		$tz_date_formatted = date('d/m/Y', strtotime($timezone_date));

/*

		Created filter of areas for Client/Admin Access Level

 */


		if($this->user_lvl.$this->user_sub_lvl == 53){

			$data['areas'] 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($data['areas'] as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			


		}else{

			$data['areas'] 	=  $this->areas->get_array(array('area_status'=>1));
			
		}




		if( !isset($params['search2']) ){
			//filter
			//appt_start
			//$search_daterange = (isset($_GET['appt_start']))?$_GET['appt_start']:date('d/m/Y').' - '.date('d/m/Y');
			$search_daterange = (isset($_GET['appt_start']))?$_GET['appt_start']:$tz_date_formatted.' - '.$tz_date_formatted;
			$params['appt_start'] = $search_daterange;
			
			//book_status
			if( isset($_GET['statuses']) AND count($_GET['statuses'])>0 ){
				$params['book_status'] = $_GET['statuses'];
			}

			if( isset($_GET['areas']) AND count($_GET['areas'])>0 ){
				$params['area_id'] = $_GET['areas'];
			}
		}

		//end filter

		$query_params = $this->build_where($params);

		//exit();

		$params_query = @http_build_query($params);



		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;
		//$query_params['where_str'] .= ($query_params['where_str']!=''?' AND ':'').'( `appointment`.`chaperone_id`='.$this->user_id.' OR `appointment`.`chaperone_id` IS NULL) ';
		$query_params['sorting'] = "`appointment`.`book_status` DESC, in_queue DESC";

		$appt_results 				= $this->appt->get_result_pagination($query_params);



		//echo $this->db->last_query();
		//echo $total_rows;
        $p_config["base_url"] 		= base_url() . "callcenter/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
		

		
		$data['cars'] = $this->areas->get_cars('result', 'car_id, car_name');

		$data['user_chaperone'] = $this->user->get_chaperones(array(), '', 'user_id, user_fullname');
		$data['user_doctor'] = $this->user->get_doctors(array(), '', 'user_id, user_fullname');

		$data['search_daterange'] = $search_daterange;

		$data['download_medicard_default_date'] = $tz_date_formatted;

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'callcenter/index';
		$this->view_data['menu_active'] = 'callcenter';

		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/en-au.js"></script>';		
		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/callcenter-list.js"></script>';

		$this->view_data['js_file'] .= '<script src="assets/plugins/boostrap-dropdown-with-checkboxes.js"></script>'.PHP_EOL;
		
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);
 	}

 	public function view($appt_id)
 	{
 		$this->load->model('Appointmentmodel', 'appt');
 		$this->load->model('Areamodel', 'areas');
 		
 		$data = '';

 		$record = $this->appt->get_row_link_full($appt_id);
 		$car 	= $this->areas->get_row_by_car(array('car_id'=>$record->car_id));


		$consult = $this->appt->get_row_consult_notes($appt_id);

 		$data['record'] = $record;
 		$data['car'] 	= $car;
 		$data['consult'] 	= $consult;

 		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'callcenter/view';
		$this->view_data['menu_active'] = 'callcenter';
		$this->load->view('index', $this->view_data);

 	}

 	public function cancel_booking($appt_id)
 	{
 		$this->load->model('Appointmentmodel', 'appt');
 		$this->load->model('Transactionmodel', 'transaction');
 		$this->load->model('Areamodel', 'areas');
 		//$this->load->library('Smsglobal');
 		$this->load->library('Mailer', 'mailer');

 		$data = '';

 		$record = $this->appt->get_row_link_full($appt_id);
 		$car 	= $this->areas->get_row_by_car(array('car_id'=>$record->car_id));

 		if($_POST){

 			$post = $this->input->post();

 			if( $post['form_type'] == 'cancel_booking' ){
 				
 				$set['book_status'] = BOOK_STATUS_CANCELLED;
 				$set['cancelled_reason'] = addslashes($post['cancel_reason']);
 				$set['appt_start_queue'] = date('Y-m-d H:i:s');
 				$set['appt_end_queue'] = date('Y-m-d H:i:s');
 				
 				$set['cancelled_by'] = $this->agent_name;

 				if( $this->appt->set_cancel($appt_id, $set) ){
 					
 					//logs to "Cancellation
	 				$set_tran['tran_type'] = 'Cancellation';
	 				$set_tran['tran_desc'] = '';
	 				 
	 				$set_tran['caller_firstname'] = addslashes($record->firstname);
	 				$set_tran['caller_lastname'] = addslashes($record->lastname);
	 				
	 				$tran_details = 'Reason : '.addslashes($post['cancel_reason']);
	 				$set_tran['tran_details'] = $tran_details;

	 				$set_tran['parent_tran_id'] = $record->tran_id;
	 				$set_tran['appt_patient_name'] = $record->firstname.' '.$record->lastname;

	 				$set_tran['user_id'] = $this->user_id;
	 				$set_tran['agent_name'] = $this->agent_name;

	 				$this->transaction->add($set_tran);

	 				//SEND SMS
					$sms_cars = $this->areas->get_cars_result(array('area_id'=>$record->area_id)); 					
 					$sms_body = 'CANCELLATION - '.$record->ref_number_formatted.' - '.$record->firstname.' '.$record->lastname.' '.stripslashes(@$record->street_addr).' '.stripslashes(@$record->suburb).' '.@$post['cancel_reason'];

 					/*echo '<pre>';
 					print_r($record);
 					print_r($cars);
 					echo '</pre>';*/
 					foreach($sms_cars as $row ){

 						$row->car_mobile = str_replace(' ', '', $row->car_mobile);

 						//if( $row->car_mobile != '' ){
 						if( $row->car_mobile != ''  && intval($row->send_text)==1){
 							
	 						//$smsglobal = $this->smsglobal->sendSms($row->car_mobile, $sms_body);
	 						$send_sms = $this->Commonmodel->sendSms($row->car_mobile, $sms_body);

	 						$audit['tran_id'] 		= $record->tran_id;
							$audit['audit_to'] 		= $row->car_mobile;
							$audit['audit_type'] 	= 'sms';
							$audit['message'] 		= $sms_body;							
							$audit['return_message']= ($send_sms == 'message_sent')?'SMS - SENT':$send_sms;
							$this->Commonmodel->insert_audit_trail($audit);

						}


						if( $row->car_email != ''  && intval($row->send_email)==1){
									
							//SEND EMAIL						
							$body  = '';
							$body .= 'Appt #: '.$record->ref_number_formatted.PHP_EOL;
							$body .= "Name: ".$record->firstname.' '.$record->lastname.PHP_EOL;					
							$body .= $set_tran['tran_details'].PHP_EOL;
							$body .= PHP_EOL;
							$body .= SENT_VIA;

							$subject = 'CANCEL BOOKING - '.$record->ref_number_formatted;

							$this->mailer->send_to = $row->car_email;
							$this->mailer->Subject = $subject;
							$this->mailer->Body = stripslashes(nl2br($body));

							$mail_return = $this->mailer->sendMail();

							$audit['tran_id'] 		= $record->tran_id;
							$audit['audit_to'] 		= $row->car_email;
							$audit['audit_type'] 	= 'email';
							$audit['message'] 		= addslashes($body);	
							$audit['more_info'] 	= $subject;					
							$audit['return_message']= $mail_return;
							$this->Commonmodel->insert_audit_trail($audit);

						}
 					}
 					//END SEND SMS
					

 					//SEND EMAIL
					$to = $this->Commonmodel->get_admin_email('EMAIL_CANCEL_BOOKING');
					$body = '';
					$body .= 'Appt #: '.$record->ref_number_formatted.PHP_EOL;
					$body .= "Name: ".$set_tran['caller_firstname'].' '.$set_tran['caller_lastname'].PHP_EOL;					
					$body .= $set_tran['tran_details'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = 'CANCEL BOOKING - '.$record->ref_number_formatted;

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $record->tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);
					//END SEND EMAIL


	 				//audit_trail
 					$set_audit['target_table'] 	= 'appointment';
					$set_audit['table_ref_col'] = 'appt_id';
					$set_audit['table_ref_id'] 	= $appt_id;
					$set_audit['table_action'] 	= 'callcenter-cancelled';
					$set_audit['message'] 		= json_encode($set);
					$set_audit['user_id'] 		= $this->user_id;
					$set_audit['agent_name'] 	= $this->agent_name;
					$this->db->insert('table_audit_trail', $set_audit);
 					
 					$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully '.$this->booking_statuses[$set['book_status']]);
					redirect(base_url().'callcenter/');
 				}
 			}

 		}

 		$data['record'] = $record;
 		$data['car'] 	= $car;

 		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'callcenter/cancel-booking';
		$this->view_data['menu_active'] = 'callcenter';
		$this->load->view('index', $this->view_data);
 	}

 	public function eta_required($appt_id)
 	{
		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Transactionmodel', 'transaction');
		$this->load->model('Areamodel', 'areas');
		$this->load->library('Mailer');
 		$this->load->library('Smsglobal');

 		$data = '';

 		$record = $this->appt->get_row_link_full($appt_id);
 		$car 	= $this->areas->get_row_by_car(array('car_id'=>$record->car_id));

 		if( $_POST ){

 			$post = $this->input->post();

 			if( $post['form_type'] == 'eta-required' ){

 				//$this->mailer->sendMail();

 				$set['tran_type'] = 'Request ETA';
 				$set['tran_desc'] = ''; 
 				$set['tran_details'] = 'SMS : '.addslashes($post['sms_details']);
 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				if( $this->transaction->add($set) ){
 					$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
 					
 					//flag appt eta
 					$this->appt->flag_eta($appt_id);

 					//send sms   					
 					$sms_cars = $this->areas->get_cars_result(array('area_id'=>$record->area_id));
 					
 					$sms_body = $post['sms_details'];

 					$email_body = $post['sms_details'];

 					$email_subject = 'Request ETA '.$record->ref_number_formatted;

 					foreach($sms_cars as $row ){

 						$row->car_mobile = str_replace(' ', '', $row->car_mobile);

 						//if( $row->car_mobile != '' ){
 						//
 						if( $row->car_mobile != ''  && intval($row->send_text)==1){	
 							
	 						$smsglobal = $this->smsglobal->sendSms($row->car_mobile, $sms_body);

	 						$audit['tran_id'] 		= $record->tran_id;
							$audit['audit_to'] 		= $row->car_mobile;
							$audit['audit_type'] 	= 'sms';
							$audit['message'] 		= $sms_body;							
							$audit['return_message']= ($this->smsglobal->success)?'SMS - SENT':$smsglobal;
							$this->Commonmodel->insert_audit_trail($audit);

						}

						if( $row->car_email != ''  && intval($row->send_email)==1){
									
							//SEND EMAIL						
							$body  = '';
							$body .= 'Appt #: '.$record->ref_number_formatted.PHP_EOL;
	/*						$body .= "Name: ".$record->firstname.' '.$record->lastname.PHP_EOL;	*/											
							$body .= $post['sms_details'].PHP_EOL;							
							$body .= PHP_EOL;
							$body .= SENT_VIA;

							$subject = 'REQUEST ETA - '.$record->ref_number_formatted;

							$this->mailer->send_to = $row->car_email;
							$this->mailer->Subject = $subject;
							$this->mailer->Body = stripslashes(nl2br($body));

							$mail_return = $this->mailer->sendMail();

							$audit['tran_id'] 		= $record->tran_id;
							$audit['audit_to'] 		= $row->car_email;
							$audit['audit_type'] 	= 'email';
							$audit['message'] 		= addslashes($body);	
							$audit['more_info'] 	= $subject;					
							$audit['return_message']= $mail_return;
							$this->Commonmodel->insert_audit_trail($audit);

						}

					//END SEND EMAIL
 					}
 					//END SEND SMS

					//SEND EMAIL
					$to = $this->Commonmodel->get_admin_email('EMAIL_REQUEST_ETA');
					$body = '';
					$body .= 'Appt #: '.$record->ref_number_formatted.PHP_EOL;
					$body .= "Name: ".$record->firstname.' '.$record->lastname.PHP_EOL;					
					$body .= $set_tran['tran_details'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = 'REQUEST ETA - '.$record->ref_number_formatted;

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $record->tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);
					//END SEND EMAIL

			 		 
 					redirect(base_url().'dashboard');
 				}
 			}

 		}

 		$data['record'] = $record;
 		$data['car'] 	= $car;

 		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'callcenter/eta-required';
		$this->view_data['menu_active'] = 'callcenter';
		$this->load->view('index', $this->view_data);
 	}

 	public function send_dr_info($appt_id)
 	{
		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Transactionmodel', 'transaction');
		$this->load->model('Areamodel', 'areas');
		$this->load->library('mailer');

 		$data = '';

 		$record = $this->appt->get_row_link_full($appt_id);
 		$car 	= $this->areas->get_row_by_car(array('car_id'=>$record->car_id));

 		if( $_POST ){

 			$post = $this->input->post();

 			if( $post['form_type'] == 'dr-info-request' ){

 				$set['tran_type'] = 'Request Dr Info';
 				$set['tran_desc'] = ''; 
 				$set['tran_details'] = 'SMS : '.addslashes($post['sms_details']);
 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				if( $this->transaction->add($set) ){
 					$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
 					redirect(base_url().'dashboard');
 				}

 			}

 		}

 		$data['record'] = $record;
 		$data['car'] 	= $car;

 		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'callcenter/send-dr-info';
		$this->view_data['menu_active'] = 'callcenter';
		$this->load->view('index', $this->view_data);
 	}

 	public function download_medicare_form( $appt_id='' ){

 		try { 			
 		
 			if(!isset($appt_id) OR $appt_id=='') throw new Exception("Error Processing Request", 1);
 			

			$this->load->helper('file');
			$this->load->library("pdfhelper");		

			$this->load->model('Appointmentmodel', 'appt'); 		
			$this->load->model('Areamodel', 'area'); 

			$record = $this->appt->get_row_link_full($appt_id);

	 		$datef = date('d.m.y');

			//$filename = strtoupper($record->ref_number_formatted.'-'.$datef.$record->firstname.$record->middleinitial.$record->lastname.'MV');
			
			$fdoc = str_replace(array('DR',' '), '', strtoupper(@$record->doctor_name));
			$fdoc = preg_replace("/\\W|_/", '', $fdoc); //remove non alphanumeric
			
			$fpatient = strtoupper($record->firstname.$record->middleinitial.$record->lastname);
			$fpatient = preg_replace("/\\W|_/", '', $fpatient); //remove non alphanumeric

			$filename = $datef.' '.$fdoc.' - '.$fpatient.'MV';

			$filename = $filename.'.pdf';

			$html = $this->load->view('chaperone/medicare-pdf-tpl', $record, true);

			$this->pdfhelper->renderPDF($html, $filename);

		} catch (Exception $e) {

 			echo $e->getMessage();

 		} 
 	}


 	public function download_consult_note( $appt_id='' ){

 		try { 			
 		
 			if(!isset($appt_id) OR $appt_id=='') throw new Exception("Error Processing Request", 1);
 			
			$this->load->library("pdfhelper");		

			$this->load->model('Appointmentmodel', 'appt'); 		

			
			$record = $this->appt->get_row_link_full($appt_id);	 		


	 		$datef = date('d.m.y');

			//$filename = strtoupper($record->ref_number_formatted.'-'.$datef.$record->firstname.$record->middleinitial.$record->lastname.'MV');
			
			$fdoc = str_replace(array('DR',' '), '', strtoupper(@$record->doctor_name));
			$fdoc = preg_replace("/\\W|_/", '', $fdoc); //remove non alphanumeric
			
			$fpatient = strtoupper($record->firstname.$record->middleinitial.$record->lastname);
			$fpatient = preg_replace("/\\W|_/", '', $fpatient); //remove non alphanumeric

			$filename = $datef.' '.$fdoc.' - '.$fpatient.'_CONSULT';

			$filename = $filename.'.pdf';

			$html = $this->load->view('doctor/consult-notes-export', array('record'=>$record), true);

			$this->pdfhelper->renderPDF($html, $filename);

		} catch (Exception $e) {

 			echo $e->getMessage();

 		} 
 	}

 	public function download_batch_zip()
 	{
 		 try {
				$this->load->helper(array('directory','file'));				

				$param  = $this->input->get();
				


				if( $param['url']=='' )	
	 		 	{
	 		 		throw new Exception("Error: Invalid URL", 1);
	 		 	}

				$batch_dir = 'files/medicare/batch/';

					$unix_dir = directory_map($batch_dir, 1);


					foreach($unix_dir as $unix_time_dir)
					{
						if ($unix_time_dir <= strtotime('-1 hours'))
						{
					    	delete_files($batch_dir.$unix_time_dir, true); 

							rmdir($batch_dir.$unix_time_dir);
						}
					}

			/*	ob_clean();
				header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
				header("Expires:0"); // Date in the past
				header('Content-Type: application/x-download');
				header("Content-Disposition: attachment; filename=".$param['filename']);
				readfile($filename);*/
					ob_clean();
					header("Content-type: application/zip");
					header("Content-Disposition: attachment; filename=".$param['filename']);
					header("Pragma: no-cache");
					header("Expires: 0");
					readfile($param['url']);
					delete_files($batch_dir.$param['unix'], true); 
					rmdir($batch_dir.$param['unix']);					
 		 	
 		 } catch (Exception $e) { 		 		

				$this->session->set_flashdata('fmesg', $e->getMessage());
 		 		redirect(base_url().'callcenter');
 		 }



 	}


	public function download_consult_batch_zip()
 	{
 		 try {
				$this->load->helper(array('directory','file'));				

				$param  = $this->input->get();
				


				if( $param['url']=='' )	
	 		 	{
	 		 		throw new Exception("Error: Invalid URL", 1);
	 		 	}

				$batch_dir = 'files/consult_notes/batch/';

					$unix_dir = directory_map($batch_dir, 1);


					foreach($unix_dir as $unix_time_dir)
					{
						if ($unix_time_dir <= strtotime('-1 hours'))
						{
					    	delete_files($batch_dir.$unix_time_dir, true); 

							rmdir($batch_dir.$unix_time_dir);
						}
					}

					ob_clean();
					header("Content-type: application/zip");
					header("Content-Disposition: attachment; filename=".$param['filename']);
					header("Pragma: no-cache");
					header("Expires: 0");
					readfile($param['url']);
					readfile($param['url']);
					delete_files($batch_dir.$param['unix'], true); 
					rmdir($batch_dir.$param['unix']);
					
 		 	
 		 } catch (Exception $e) { 		 		

				$this->session->set_flashdata('fmesg', $e->getMessage());
 		 		redirect(base_url().'callcenter');
 		 }



 	}
}
