<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container-fluid';
	}

	public function index()
	{
		$this->load->model('Sitesettingsmodel', 'sitesettings');
		$this->load->model('Areamodel', 'areas');

		$data['step'] = 1;
		$booking_view_file = 'dashboard-greeting';

		if($_GET){

			$get = $this->input->get();

			if( isset($get['phone']) )
				$data['phone'] = trim($get['phone']);

			$data = array_merge($data, $get);

			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			

			if( $data['step'] == 2 ){

				/*$check_out_of_schedule = $this->Commonmodel->check_out_of_schedule();
				 		
				
				if( $check_out_of_schedule['status'] AND $data['bookingtype'] != 'No' ){ 
					$new_data = array_merge($data, $check_out_of_schedule);
					
					//$this->out_of_booking_time($new_data);
					$uri = http_build_query($new_data);

					redirect(base_url().'dashboard/out_of_booking_time?'.$uri); 

					//exit();

				}elseif ( isset($data['bookingtype']) ){

					$col = $get['collection'];
					
					if( $data['bookingtype'] == 'No' ){

						$data['search_more'] = $col['First_Name'].'|'.$col['Last_Name'].'|'.$col['Phone'];
					}
					 
				}*/
 
				if( $data['bookingtype'] == 'No' ){

					$col = $get['collection'];
					
					if( $data['bookingtype'] == 'No' ){

						$data['search_more'] = $col['First_Name'].'|'.$col['Last_Name'].'|'.$col['Phone'];
					}

					$booking_view_file = 'booking-dashboard/v2-booking-no';
				}

				if( $data['bookingtype'] == 'Yes' ){

					$booking_view_file = 'booking-dashboard/v2-booking-yes';

				}


 

			}

			// if(isset($data['schedule_bypass'])){
			// 	unset($data['schedule_bypass']);
			// }

			if( $data['step'] == 3 ) {
 
				if( isset($data['collection']['area_avail_id']) AND @$data['collection']['area_avail_id'] != '' ){
					$area_avail = $this->areas->get_areas_availability_row(array('area_avail_id'=>$data['collection']['area_avail_id']));
					$data['advice_caller_custom_script'] = @$area_avail->more_info;
				}
 				 

				if( isset($data['btn-service']) ){


					if( $data['btn-service'] == 1 ){
					
						$uri = http_build_query($data);
						
						redirect(base_url().'dashboard/patient_search/?when=today&'.$uri);					
					}

					if( in_array($data['btn-service'], 
						array(	'Mon - Frid after 4pm',
								'Saturday after 10am',
								'Mon - Frid after 6pm',
								'Saturday after 12pm', 
								'Sunday & public holiday - anytime')) ){
							
						$data['step'] = 31;
						
						$uri = http_build_query($data);

						redirect(base_url().'dashboard/?'.$uri); 
					}					

				}

			}	 

			if( $data['step'] == 4 ) {
				
				$uri = http_build_query($data);

				if( isset($data['btn-book-next-day']) AND $data['btn-book-next-day'] == 'Yes' ){
					redirect(base_url().'dashboard/patient_search/?when=nextday&?'.$uri);
				}

			}

		}
		
		if( isset($get) ){

			if( isset($get['collection'])  ){

				$collection = $get['collection'];
				$tmp['First_Name'] 	= !isset($collection['First_Name'])?@$get['First_Name']:@$collection['First_Name'];
		 		$tmp['Last_Name'] 	= !isset($collection['Last_Name'])?@$get['Last_Name']:@$collection['Last_Name'];		 		 
		 		$tmp['Phone'] 		= @$collection['Phone'];
		 		$tmp['Suburb'] 		= @$collection['Suburb']['suburb'];
		 		$tmp['Postcode'] 	= @$collection['Suburb']['postcode'];

		 		$data['uri_not_complete'] = http_build_query($tmp);

			}
 		}


		$data['greeting'] = $this->sitesettings->get_row(array('set_name'=>'GREETING'));

		$this->view_data['data']['getvars'] = $data;
		$this->view_data['view_file'] = $booking_view_file;
		$this->view_data['menu_active'] = 'dasbhoard';
		$this->view_data['js_file'] = '<script src="assets/js/mcautocomplete.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/suburb_dropdown.js"></script>'.PHP_EOL;
		$this->load->view('index', $this->view_data);
	}

	public function chaperone_switch_area_doctor(){

		$this->load->model('Areamodel', 'areas');
		$this->load->model('Usermodel', 'user');

		if($_POST){

			$post = $this->input->post();

			//print_r($post);

			//$this->session->unset_userdata('user_extra_areas', 'user_extra_car',' user_extra_doctor');


			unset($_SESSION['user_extra_areas']);
			unset($_SESSION['user_extra_car']);
			unset($_SESSION['user_extra_doctor']);

			//sleep(1);

			// $sess['user_extra_areas'] 	= implode(',', $post['areas']);
			// $sess['user_extra_car'] 	= $post['pcar'];
			// $sess['user_extra_doctor'] 	= $post['pdoctor'];


			//$this->session->set_userdata($sess);
			
			$this->session->set_userdata('user_extra_areas', implode(',', $post['areas']));
			$this->session->set_userdata('user_extra_car',  $post['pcar']);
			$this->session->set_userdata('user_extra_doctor', $post['pdoctor']);


			redirect(base_url().'chaperone');
		}

		/*if( $_GET ){
			redirect(base_url().'chaperone');
		}*/

		//get areas
		$chaperone_areas = $this->areas->get_areas_by_chaperone($this->user_id, 'array');

		//echo count($chaperone_areas);
		//print_r($chaperone_areas);
		if( is_array($chaperone_areas) AND count($chaperone_areas) > 0 ){

			$area_id = @array_keys($chaperone_areas);
			$area_id =  $area_id[0];

			//get cars
			$data['cars'] = $this->areas->get_cars('array');

			//get doctors
			$data['doctors'] = $this->user->get_doctors('', 'dropdown');
		}else{
			$data['error'] = 'No Areas has been set for this User, please contact admin';
		}

		

		$data['areas'] = $chaperone_areas; 
		$this->view_data['data'] = $data;

		$this->view_data['view_file'] = 'chaperone/switch-area-doctor';

		$this->view_data['js_file'] .= '<script src="assets/js/page_js/chaperone_switch_area_doctor.js"></script>'.PHP_EOL;

		$this->load->view('index', $this->view_data);

	}

	public function patient_search(){

		$this->load->model('Patientmodel', 'patient');
		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Areamodel', 'areas');
		$this->load->helper('text');

		try {
			

			$data = '';

			if ($_GET) {
				
				if( isset($_GET['when']) ){
					$data['when'] = $_GET['when'];
				}

				if( isset($_GET['collection']) ){
					 

					$data['collection'] = $_GET['collection']; 
					$data['First_Name'] = $_GET['collection']['First_Name'];
					$data['Last_Name'] = $_GET['collection']['Last_Name'];
				}


				//?appt=40&patient=25&when=today&add_more_patient=1
				if( isset($_GET['add_another_patient']) AND  $_GET['add_another_patient'] == '1'){

					 
					$appt_id = $_GET['appt_id'];

					$patient = $this->patient->get_row(array('patient_id'=>$_GET['patient']));
					
				 

					$appointment = $this->appt->get_row_plain($appt_id);

					$area = $this->areas->get_row(array('area_id'=>$appointment->area_id));

					$data['Last_Name'] = @$patient->lastname;

					$data['add_another_patient'] = 1;

					$data['collection']['related_appt_id'] = $_GET['appt_id'];
					$data['collection']['related_patient_id'] = $_GET['patient'];
					$data['collection']['Phone'] = $patient->phone;
					$data['collection']['Suburb']['suburb'] = $patient->suburb;
					$data['collection']['Suburb']['street_addr'] = $patient->street_addr;
					$data['collection']['Suburb']['cross_street'] = $patient->cross_street;
					$data['collection']['Suburb']['postcode'] = $patient->postcode;
					$data['collection']['Suburb']['area_id'] = $appointment->area_id;
					$data['collection']['Suburb']['area'] = $area->area_name;
					
					$data['collection']['medicare_number'] 	= $patient->medicare_number;
					$data['collection']['medicare_ref'] 	= $patient->medicare_ref;
					$data['collection']['medicare_expiry'] 	= $patient->medicare_expiry;
					$data['collection']['dva'] 				= $patient->dva;

					$data['collection']['wdyhau'] 				= $appointment->latest_wdyhau;
					$data['collection']['latest_symptoms'] 		= $appointment->symptoms;
					$data['collection']['abotsi'] 				= $patient->abotsi;
					$data['collection']['regular_practice'] 	= $patient->regular_practice;
					$data['collection']['regular_doctor'] 		= $patient->regular_doctor;
 
				}

			}



			if($_POST){

				$post = $this->input->post();

				$data = $post;

				$firstname = strtoupper(trim($post['First_Name']));
				$lastname = strtoupper(trim($post['Last_Name']));
				$where['where_str'] = "UPPER(`firstname`) LIKE '%$firstname%' AND UPPER(`lastname`) LIKE '%$lastname%'  ";

				$data['First_Name'] = $firstname;
				$data['Last_Name'] = $lastname;

				$data['patients'] = $this->patient->get_result($where);

			 
			}

			$tmp_data = !empty($_GET)?$_GET:$_POST;
 

	 		$tmp['First_Name'] 	= @$tmp_data['collection']['First_Name'];
	 		$tmp['Last_Name'] 	= @$tmp_data['collection']['Last_Name'];		 		 
	 		$tmp['Phone'] 		= @$tmp_data['collection']['Phone'];
	 		$tmp['Suburb'] 		= @$tmp_data['collection']['Suburb']['suburb'];
	 		$tmp['Postcode'] 	= @$tmp_data['collection']['Suburb']['postcode'];

	 		$data['uri_not_complete'] = http_build_query($tmp);

			 
			$this->view_data['data'] = $data;

			$this->view_data['view_file'] = 'booking-patient-search';

			//$this->view_data['js_file'] .= '<script src="assets/js/page_js/chaperone_switch_area_doctor.js"></script>'.PHP_EOL;

			$this->load->view('index', $this->view_data);

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}

 	public function booking($when='today')
 	{
 		
 		$this->load->model('Patientmodel', 'patient');
 		$this->load->model('Areamodel', 'areas'); 		 
 		$this->load->model('Dropdownmodel');
 		$this->load->helper('text');

 		$data = array('patient_type'=>'');
 		
 		$form_type = 'search';


 		$get = $this->input->get();

 		try {
 			
 			

	 		if($_GET){

 				if(!isset($get['collection'])) throw new Exception("collection index is missing", 1);

	 			$data = array_merge($data, $get);

				$like = ' ( (lower(`firstname`) LIKE "%'.trim($data['collection']['First_Name']).'%" AND ';
				$like .= ' lower(`lastname`) LIKE "%'.trim($data['collection']['Last_Name']).'%" AND ';
				$like .= ' lower(`suburb`) LIKE "%'.trim($data['collection']['Suburb']['suburb']).'%") OR ';
				$like .= " lower(`phone`) LIKE '%".$data['collection']['Phone']."%' ) ";

	 			$data['record'] = $this->patient->get_result(array('where_str'=>$like));

	 		}

	 		//after search
	 		if( $_POST ){

	 			$form_type = 'form';

	 			$post = $this->input->post();

	 			/*echo '<pre>';
	 			print_r($post);
	 			echo '</pre>';*/
		 		
	 			if( !isset($post['patient_type']) || $post['patient_type'] == ''  ){
	 				//Exception here;
	 				exit;
	 			}


		 		$collection = $post['collection'];

		 		$data['collection'] = $collection;

		 		if( $post['patient_type'] == 'existing' AND $collection['patient_id'] != ''  ){
		 			//existing
		 					 	 
		 			if( isset($collection['Suburb']['area_id']) ){
		 				$_area_id = $collection['Suburb']['area_id'];
		 			}else{
		 				$_area_id = $this->areas->get_by_suburb_id($collection['Suburb']['id']);
		 			}

		 			$data['collection']['Suburb']['area_id'] = $_area_id;
		 			
		 			$data['patient'] = $this->patient->get_row(array('patient_id'=>$collection['patient_id']));
		 			
		 			$data['patient']->wdyhau = 'Existing';

		 		}else if( $post['patient_type'] == 'new'){
		 	 		//echo 'new';
		 	 		
	 				$patient = new stdClass();
	 				$patient->firstname = $post['First_Name'];
	 				$patient->lastname 	= $post['Last_Name'];
	 				$patient->phone 	= $collection['Phone'];
	 				$patient->suburb 	= $collection['Suburb']['suburb'];
	 				$patient->postcode 	= $collection['Suburb']['postcode'];
	 				$patient->street_addr 	= @$collection['Suburb']['street_addr'];
	 				$patient->cross_street 	= @$collection['Suburb']['cross_street'];


					$patient->medicare_number 	= @$data['collection']['medicare_number'];

					$patient->medicare_ref 		= @$data['collection']['medicare_ref'];
					$patient->medicare_expiry 	= @$data['collection']['medicare_expiry'];
					$patient->dva 				= @$data['collection']['dva'];

					$patient->wdyhau 			= @$data['collection']['wdyhau'];
					$patient->latest_symptoms 	= @$data['collection']['latest_symptoms'];
					$patient->abotsi 			= @$data['collection']['abotsi'];
					$patient->regular_practice 	= @$data['collection']['regular_practice'];
					$patient->regular_doctor 	= @$data['collection']['regular_doctor'];

	 				
	 				$data['patient'] = $patient;

		 		}else{

		 			exit;
		 		}

		 		$data['wyhaulist'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'WYHAU')), 'array');
		 		
		 		if( isset($_GET) ){

			 		$tmp['First_Name'] 	= !isset($collection['First_Name'])?$post['First_Name']:$collection['First_Name'];
			 		$tmp['Last_Name'] 	= !isset($collection['Last_Name'])?$post['Last_Name']:$collection['Last_Name'];		 		 
			 		$tmp['Phone'] 		= $collection['Phone'];
			 		$tmp['Suburb'] 		= $collection['Suburb']['suburb'];
			 		$tmp['Postcode'] 	= $collection['Suburb']['postcode'];

			 		$data['uri_not_complete'] = http_build_query($tmp);
		 		}

	 		}
	 	 

			$this->view_data['data']['when'] = $when;//$when;
			$this->view_data['data']['getvars'] = $data;
			
			$this->view_data['menu_active'] = 'dasbhoard';
			
			if( $form_type== 'search'){

				$this->view_data['view_file'] = 'booking-patient-search';

			}else{
	 
				$this->view_data['view_file'] = 'booking-patient-form';

				//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>'.PHP_EOL;
				//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/en-au.js"></script>'.PHP_EOL;		
				$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>'.PHP_EOL;					

					
							
				// $this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/inputmask.min.js"></script>'.PHP_EOL;
				// $this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/inputmask.date.extensions.min.js"></script>'.PHP_EOL;
				// $this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/jquery.inputmask.min.js"></script>'.PHP_EOL;
				
				$this->view_data['js_file'] .= '<script src="assets/plugins/combodate.js"></script>'.PHP_EOL;

				//$this->view_data['js_file'] .= '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>';			
						

				//$this->view_data['js_file'] .= '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>';
				$this->view_data['js_file'] .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_7sWD-o0bYY8cQviHJFHDTrYxJpI5Nh8&sensor=true"> </script>';
				$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking.js"></script>';
				$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking_practice_autocomplete.js"></script>';
				
				$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

				

			}



			$this->load->view('index', $this->view_data);

 		} catch (Exception $e) {

 			$this->view_data['data']['message'] = $e->getMessage();

 			$this->view_data['view_file'] = 'exception/booking';
 			$this->load->view('index', $this->view_data);

 		}

 	}

 	public function did_not_complete(){
 		$this->load->model('Transactionmodel', 'transaction');
 		$this->load->library('Mailer', 'mailer');
 		try {

 			if( $_POST ){

 				$post = $this->input->post();

 				//print_r($post); 
 			 
 				$set['tran_type'] = 'Appts Available, declined Appt';
 				$set['tran_desc'] = '';


				$set['caller_phone'] 		= @$post['Phone'];	
				$set['caller_firstname'] 	= @$post['First_Name'];
				$set['caller_lastname'] 	= @$post['Last_Name'];
				$set['caller_suburb'] 		= @$post['Suburb'];	

 				/*$details = 'Name : '.@$post['First_Name'].' '.@$post['Last_Name'].PHP_EOL;
 				$details .= 'Phone : '.@$post['Phone'].PHP_EOL;
 				$details .= 'Suburb : '.@$post['Suburb'].PHP_EOL;
 				$details .= 'Reason : '.stripslashes($post['reason']);
 				*/
 			
 				$details = 'Reason : '.addslashes($post['reason']);

 				$set['tran_details'] = $details;

 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				if( $this->transaction->add($set) ){
 					
					$to = $this->Commonmodel->get_admin_email('EMAIL_SUBURB_NOT_ON_LIST');

					$body = ' '.PHP_EOL;
					$body .= "Name: ".$set['caller_firstname'].' '.$set['caller_lastname'].PHP_EOL;
					$body .= "Phone: ".$set['caller_phone'].PHP_EOL;
					//$body .= "Suburb: ".$set['caller_suburb'].PHP_EOL;
					$body .= "Reason: ".$post['reason'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = "Appts available, declined appt ";

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);


 					$this->session->set_flashdata('fmesg', 'Call Appts Available, declined Appt Successfully submitted');
 					redirect(base_url().'dashboard');
 				} 		

 			}

			$get = $this->input->get();
 			

 			$data['collection'] = $get;

 			$this->view_data['data']= $data;
 			$this->view_data['menu_active'] = 'dasbhoard';
 			$this->view_data['view_file'] = 'booking-did-not-complete';
 			$this->load->view('index', $this->view_data);

 		}catch(Exception $e){

 		}


 	}

 	public function callcancelled(){

		$this->load->model('Transactionmodel', 'transaction');

		if($_POST){

			$post = $this->input->post();

			$set['tran_type'] = $post['calltype'];
			$set['tran_desc'] = '';

			$set['user_id'] = $this->user_id;
			$set['agent_name'] = $this->agent_name;

			if( $this->transaction->add($set) ){
				$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
				redirect(base_url().'dashboard/call_register');
			}
		}

 	}

 	public function out_of_booking_time(){

 		$data = $this->input->get();

 		 
		$this->view_data['data']= $data;
		$this->view_data['menu_active'] = 'dasbhoard';
		$this->view_data['view_file'] = 'out-of-booking-time';
		$this->load->view('index', $this->view_data);

 	}

 	public function custom_submit() {

 		$this->load->model('Transactionmodel', 'transaction');

 		if( $_POST ){

 			$post = $this->input->post();

 			if( @$post['custom_submit_type'] ==  'booking-out-call' ){

 				try {

	 				//print_r($post); 
	 			 
	 				$set['tran_type'] = $post['tran_type'];
	 				$set['tran_desc'] = '';

	 				$collection = $post['collection'];

					$set['caller_phone'] 		= @$collection['Phone'];
					$set['caller_firstname'] 	= @$collection['First_Name'];
					$set['caller_lastname'] 	= @$collection['Last_Name'];
					$set['caller_suburb'] 		= @$collection['Suburb']['suburb'];	 
	 			
					if(isset($post['advice_caller'])){
	 					$details = addslashes($post['advice_caller']);
					}else{
	 					$details = addslashes('Advised to call back tomorrow');
					}


	 				$set['tran_details'] = $details;

	 				$set['user_id'] = $this->user_id;
	 				$set['agent_name'] = $this->agent_name;

	 				if( $this->transaction->add($set) ){

	 					$this->session->set_flashdata('fmesg', $post['tran_type']);
 						
	 					if( $post['btn-service'] == 'Sunday & public holiday - anytime' ){
	 						redirect(base_url().'dashboard/?step=5&btn-service=advice-caller-bookout');
	 					}else{
 							redirect(base_url().'dashboard/call_register');
	 					}

 					}

 				} catch (Exception $e) {
 					
 				}



 			}
 		}

 	}

 	public function booking_submit(){

		try {

			$this->load->model('Patientmodel', 'patient');
 			$this->load->model('Appointmentmodel', 'appointment');
 			$this->load->model('Areamodel', 'areas');
			$this->load->library('Mailer', 'mailer');
			//$this->load->library('Smsglobal');

			if( !$_POST ) throw new Exception("this function accept on post", 1);

			//CHECK area_id
			if( !isset($_POST['collection']['Suburb']['area_id']) OR trim($_POST['collection']['Suburb']['area_id']) == '' ) throw new Exception("Area is missing, something went wrong on the process.", 1);
			
			$post = $this->input->post();


			$patient_id = $post['patient_id'];
			$area_id 	= $post['collection']['Suburb']['area_id'];

			$patient = '';

			if( $patient_id > 0 ){

				$this->patient->get_row(array('patient_id'=>$patient_id));
			}

			//updating or creating a patient
			$patient_set['firstname'] 		= addslashes(strtoupper($post['First_Name']));
			$patient_set['lastname'] 		= addslashes(strtoupper($post['Last_Name']));
			$patient_set['phone'] 			= $post['Landline_Phone'];
			$patient_set['mobile_no'] 		= $post['Mobile_Phone'];

			$patient_set['em_contact_name'] = @$post['Emergency_Contact_Name'];
			$patient_set['em_relationship']	= @$post['Emergency_Relationship'];
			$patient_set['em_contact_phone']= @$post['Emergency_Contact_Phone'];

			$patient_set['next_kin']		= @$post['Next_of_Kin'];
			$patient_set['next_kin_contact_phone']= @$post['Next_Of_Kin_Contact_Phone'];

			$patient_set['email'] 			= $post['Email'];
			$patient_set['street_addr'] 	= strtoupper($post['Address']);
			$patient_set['cross_street'] 	= addslashes(strtoupper($post['Cross_Street']));
			$patient_set['suburb'] 			= addslashes(strtoupper($post['Suburb']));
			$patient_set['postcode'] 		= $post['Post_Code'];

			$dob = explode('/', $post['DOB']);
			$patient_set['dob'] 			= (count($dob)==3)?$dob[2].'-'.$dob[1].'-'.$dob[0]:'';
			$patient_set['gender'] 			= $post['Gender'];
			$patient_set['medicare_number'] = $post['Medical_Number'];
			$patient_set['medicare_ref'] 	= $post['Medical_Reference'];
			$patient_set['medicare_expiry'] = $post['Medical_Expirey'];
			$patient_set['dva'] 			= isset($post['dva_card_gold'])?$post['dva_card_gold']:0;
			
			$patient_set['latest_symptoms'] = addslashes($post['Symptoms']);
			$patient_set['abotsi'] 			= $post['abotsi'];
			$patient_set['cultural_ethnic_bg'] = addslashes($post['cultural_ethnic_bg']);
			$patient_set['regular_practice']= strtoupper($post['search_practice']);
			$patient_set['regular_doctor'] 	= strtoupper($post['Regular_Doctor']);
			
			$patient_set['patient_notes'] 	= strtoupper($post['Patient_Notes']);
			$patient_set['health_insurance_provider'] = $post['Health_Insurance_Provider'];
			$patient_set['health_fund_number'] = $post['Health_Fund_Number'];
			$patient_set['patient_has_health_card'] = $post['health_care_card_question'];

			if(isset($post['expiry_date'])){
				$exp_split = explode('/', $post['expiry_date']);
				$patient_set['health_card_expiry_date'] = $exp_split[2].'-'.$exp_split[1].'-'.$exp_split[0];
			}
			

			//print_r($patient_set);exit;

			//add or update patient
			if( $patient_id > 0 ){				
				$patient_set['patient_updated'] = date('Y-m-d');
				$patient_id = $this->patient->update($patient_id, $patient_set);

				$appt_set['latest_wdyhau'] 	= 'Existing';

			}else{

				$patient_set['wdyhau'] 		= $post['wdyhau']; //add if first time

				$patient_id = $this->patient->add($patient_set);

				$appt_set['latest_wdyhau'] 	= addslashes($post['wdyhau']); //add to appointment
			}
 
			//setting an appointment
			$appt_set['appt_start'] = date('Y-m-d');

			if( $post['booking_when'] == 'nextday' ){

				$appt_set['appt_start'] = date('Y-m-d', strtotime('+1 day'));
			}	

			$sched = $this->Commonmodel->get_schedules('','',$area_id);

			$appt_set['appt_created'] 		= $sched['appt_created'];
			$appt_set['prev_app_created'] 	= date('Y-m-d H:i:s');

			$appt_set['area_id'] 		= $area_id;
			$appt_set['patient_id'] 	= $patient_id;
			$appt_set['symptoms'] 		= addslashes($post['Symptoms']);
			
			$appt_set['book_by'] 		= $this->agent_name;
			$appt_set['book_by_user_id'] = $this->user_id;
			$appt_set['booking_when'] 	= $post['booking_when']; //unset after inserted to transaction table
			$appt_set['tran_type'] 		= @$post['collection']['tran_type']; //mostly this coming from midnight button
			
			$appt_set['firstname'] 	= $post['collection']['First_Name'];
			$appt_set['lastname'] 	= $post['collection']['Last_Name'];
			$appt_set['phone'] 		= $post['collection']['Phone'];
			$appt_set['suburb'] 	= $post['collection']['Suburb']['suburb'];


			
			
			//some filter here
			 
			if( isset($post['collection']['related_appt_id']) AND $post['collection']['related_appt_id'] != '' ){
				
				$appt_set['related_appt_id'] 	= $post['collection']['related_appt_id'];

				$patient_id = $post['collection']['related_patient_id'];

			}


			$appt_id = $this->appointment->add($appt_set);


			//parent
			//set parent thier own appt_id so that we can detect right away rather doing query
			if( isset($post['add_another_patient']) AND  !isset($post['collection']['related_appt_id'])){				

				$this->appointment->set_parent_related_appt_id($appt_id);
			}

			
			$tran = $this->appointment->get_row_plain($appt_id);

			//email HCD for new practice
			if( trim($post['email_to_hcd']) != '' ){
				//echo HCD HERE;

				/*$to = $this->Commonmodel->get_admin_email();

				$body = "The Regular Practice below does not exist on current list".PHP_EOL.PHP_EOL;
				$body .= strtoupper($post['email_to_hcd']).PHP_EOL;
				$body .= PHP_EOL;
				$body .= SENT_VIA;

				$subject = "New Regular Practice";

				$this->mailer->send_to = $to;
				$this->mailer->Subject = $subject;
				$this->mailer->Body = stripslashes(nl2br($body));

				$mail_return = $this->mailer->sendMail();

				//audit log
				$audit['tran_id'] 		= $tran->tran_id;
				$audit['audit_to'] 		= $to;
				$audit['audit_type'] 	= 'email';
				$audit['message'] 		= addslashes($body);
				$audit['more_info'] 	= 'New Regular Practice';
				$audit['return_message']= $mail_return;
				$this->Commonmodel->insert_audit_trail($audit);	*/			
			}


			//check if within the period, then send sms
			if( $sched['isPeriod'] ){

				
				$area_row = $this->areas->get_row(array('area_id'=>$area_id));

				//SEND EMAIL
				
						$email_body  =  "<strong>Appointment Details</strong> <br/><br/>";				
						$email_body .= "Area :".$area_row->area_name.'<br/>';
						$email_body .= "Date/Time Booked :".date('d/m/Y H:i:s', strtotime($tran->appt_created_tz)).'<br/><br/>';

						$email_body .=  "<strong>Patient Details</strong> <br/><br/>";				
						$email_body .= "Name : {$patient_set['firstname']}  {$patient_set['lastname']}".'<br/>';				
						$email_body .= "Phone : {$patient_set['phone']}".'<br/>';
						$email_body .= "Mobile No : {$patient_set['mobile_no']}".'<br/>';
						$email_body .= "Address : {$patient_set['street_addr']}".'<br/>';
						$email_body .= "Cross Street : {$patient_set['cross_street']}".'<br/>';
						$email_body .= "Suburb : {$patient_set['suburb']}".'<br/>';
						$email_body .= "Date of Birth : {$patient_set['dob']}".'<br/>';
						$email_body .= "Age : {$post['age']}".'<br/>';
						$email_body .= "Gender : {$patient_set['gender']}".'<br/>';
						$email_body .= "Symptoms : {$patient_set['latest_symptoms']}".'<br/>';
						$email_body .= "Medicare Number : {$patient_set['medicare_number']}".'<br/>';
						$email_body .= "Medicare Ref : {$patient_set['medicare_ref']}".'<br/>';
						$email_body .= "Medicare Exp : {$patient_set['medicare_expiry']}".'<br/>';
						$email_body .= "DVA : ".isset($post['dva_card_gold'])?"Yes":"No".'<br/>';
						$email_body .= "Regular Practice : {$patient_set['regular_practice']}".'<br/>';
						$email_body .= "Regular Doctor : {$patient_set['regular_doctor']}".'<br/>';				
						$email_body .= '<br/>';
						$email_body .= SENT_VIA;


				//SEND SMS to cars			 
				$sms_cars = $this->areas->get_cars_result(array('area_id'=>$area_id)); 					
				$sms_body = 'Booking Made: - '.$patient_set['firstname'].' '.$patient_set['lastname'].' '.stripslashes(@$patient_set['street_addr']).' '.stripslashes(@$patient_set['suburb']);


				$email_subject = 'Booking Made: - '.$patient_set['firstname'].' '.$patient_set['lastname'].' '.stripslashes(@$patient_set['street_addr']).' '.stripslashes(@$patient_set['suburb']);
				
				
				foreach($sms_cars as $row ){ 

					$row->car_mobile = str_replace(' ', '', $row->car_mobile);

					//if( $row->car_mobile != '' ){
					if( $row->car_mobile != ''  && intval($row->send_text)==1){
								
						//$smsglobal = $this->smsglobal->sendSms($row->car_mobile, $sms_body);
						$send_sms = $this->Commonmodel->sendSms($row->car_mobile, $sms_body);

						$audit['tran_id'] 		= $tran->tran_id;
						$audit['audit_to'] 		= $row->car_mobile;
						$audit['audit_type'] 	= 'sms';
						$audit['message'] 		= $sms_body;							
						$audit['return_message']= ($send_sms == 'message_sent')?'SMS - SENT':$send_sms;
						$this->Commonmodel->insert_audit_trail($audit);

					}

					if( $row->car_email != ''  && intval($row->send_email)==1){
								
						$this->mailer->send_to = $row->car_email;
						$this->mailer->Subject = $email_subject;
						$this->mailer->Body = stripslashes(nl2br($email_body));

						$mail_return = $this->mailer->sendMail();

						$audit['tran_id'] 		= $tran->tran_id;
						$audit['audit_to'] 		= $row->car_email;
						$audit['audit_type'] 	= 'email';
						$audit['message'] 		= addslashes($email_body);	
						$audit['more_info'] 	= $email_subject;					
						$audit['return_message']= $mail_return;
						$this->Commonmodel->insert_audit_trail($audit);

					}

					//END SEND EMAIL
				}
				//END SEND SMS

			}

			if( isset($post['add_another_patient']) ){

				$this->session->set_flashdata('fmesg', '<div class="alert alert-success" role="alert">Patient successfully book an appointment, Please do search to book another one.</div>');

				redirect(base_url().'dashboard/patient_search/?appt_id='.$appt_id.'&patient='.$patient_id.'&when='.$post['booking_when'].'&add_another_patient=1');

			}else{

				$this->session->set_flashdata('fmesg', 'Appointment Successfully book');
				redirect(base_url().'dashboard/booking_advicecaller/?appt_id='.$appt_id.'&when='.$post['booking_when'].'&area_id='.$area_id);

			}


		} catch (Exception $e) {
			echo 'Error: '.$e->getMessage();
		}

 	}

 	public function non_booking_submit(){
 		
 		try {
 			
 			$this->load->model('Transactionmodel', 'transaction');
 			$this->load->library('Mailer', 'mailer');

 			if( !$_POST ) throw new Exception("this function accept on post", 1);

 			$post = $this->input->post();

 			/*echo '<pre>';
 			print_r($post);
 			echo '</pre>';*/

 			$col = $post['collection'];

 			if( $post['submit'] == 'suburb-not-serviced' ) {

 				//$set['tran_type'] = 'Suburb not serviced';
 				$set['tran_type'] = 'suburb not on list';
 				$set['tran_desc'] = '';

				$set['caller_phone'] 		= @$col['Phone'];	
				$set['caller_firstname'] 	= @$col['First_Name'];
				$set['caller_lastname'] 	= @$col['Last_Name'];
				$set['caller_suburb'] 		= @$col['Suburb']['suburb'];				

 				/*$details = 'Name : '.@$col['First_Name'].' '.@$col['Last_Name'].PHP_EOL;
 				$details .= 'Phone : '.@$col['Phone'].PHP_EOL;
 				$details .= 'Suburb : '.@$col['Suburb']['suburb'];*/

 				$details = '';
 				$set['tran_details'] = $details;

 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				$tran_id = $this->transaction->add($set);
 				
 				if( $tran_id > 0 ){
							
					$to = $this->Commonmodel->get_admin_email('EMAIL_SUBURB_NOT_ON_LIST');

					$body = "Name: ".$set['caller_firstname'].' '.$set['caller_lastname'].PHP_EOL;
					$body .= "Phone: ".$set['caller_phone'].PHP_EOL;
					$body .= "Suburb: ".$set['caller_suburb'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = "Suburb not on list - ".$set['caller_suburb'];

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);



 					$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
 					redirect(base_url().'dashboard');
 				}
 			}

 			if( $post['submit'] == 'btn-next-day-declined' ) {

 				$set['tran_type'] = 'Booked Out - Declined Next Day Booking';
 				$set['tran_desc'] = '';

				$set['caller_phone'] 		= @$col['Phone'];	
				$set['caller_firstname'] 	= @$col['First_Name'];
				$set['caller_lastname'] 	= @$col['Last_Name'];
				$set['caller_suburb'] 		= @$col['Suburb']['suburb'];	

 				/*$details = 'Name : '.@$col['First_Name'].' '.@$col['Last_Name'].PHP_EOL;
 				$details .= 'Phone : '.@$col['Phone'].PHP_EOL;
 				$details .= 'Suburb : '.@$col['Suburb']['suburb'];
 				$set['tran_details'] = $details;*/

 				$details = '';
 				$set['tran_details'] = $details;

 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				if( $this->transaction->add($set) ){
 					$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
 					redirect(base_url().'dashboard');
 				}

 			}


			if( $post['submit'] == 'midnight-bookedout-no' ) {

 				$set['tran_type'] = 'After midnight - booked out - no later booking required';
 				$set['tran_desc'] = '';

				$set['caller_phone'] 		= @$col['Phone'];	
				$set['caller_firstname'] 	= @$col['First_Name'];
				$set['caller_lastname'] 	= @$col['Last_Name'];
				$set['caller_suburb'] 		= @$col['Suburb']['suburb'];	

 				/*$details = 'Name : '.@$col['First_Name'].' '.@$col['Last_Name'].PHP_EOL;
 				$details .= 'Phone : '.@$col['Phone'].PHP_EOL;
 				$details .= 'Suburb : '.@$col['Suburb']['suburb'];
 				$set['tran_details'] = $details;*/

 				$details = '';
 				$set['tran_details'] = $details;

 				$set['user_id'] = $this->user_id;
 				$set['agent_name'] = $this->agent_name;

 				if( $this->transaction->add($set) ){
 					$this->session->set_flashdata('fmesg', 'Call Successfully submitted');
 					redirect(base_url().'dashboard');
 				}

 			} 			

 		} catch (Exception $e) {
 			echo 'Error: '.$e->getMessage();
 		}
 	}


 	public function booking_advicecaller()
 	{
		
		$this->load->model('Appointmentmodel', 'appointment');
		$this->load->model('Areamodel', 'areas');

		$data = '';

		$get = $this->input->get();
	
		$post = $this->input->post();
		
		$data = $get;

		if( isset($get['appt_id']) ){
			$data['appt'] = $this->appointment->get_row_plain($get['appt_id']);
		}


		if( isset($post['appt_id']) ){
			$data['appt'] = $this->appointment->get_row_plain($post['appt_id']);
		}

		if( $get['area_id'] ) {

			$data['area'] = $this->areas->get_row(array('area_id'=>$get['area_id']));

		}


		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'booking-advicecaller';
		$this->view_data['menu_active'] = 'dasbhoard';
		$this->load->view('index', $this->view_data);
 	}

 	public function outbound_call(){	
 		
 		$this->load->model('Transactionmodel', 'transaction');
 		$this->load->model('Sitesettingsmodel', 'sitesettings');
 		$this->load->library('cma_external');

		$data = '';

		if( $_POST ){
			
			$post = $this->input->post();

			$set_tran['tran_type'] = 'OUTBOUND call';
			$set_tran['tran_desc'] = '';

			$set_tran['caller_phone'] 		= @$post['Phone'];	
			$set_tran['caller_firstname'] 	= @$post['First_Name'];
			$set_tran['caller_lastname'] 	= @$post['Last_Name']; ;

			/*$tran_details = 'Name : '.addslashes($post['outbound_name']).PHP_EOL;
			$tran_details .= 'Phone : '.addslashes($post['outbound_phone']).PHP_EOL;
			$tran_details .= 'Reason : '.addslashes($post['reason_for_call']).PHP_EOL; */				
			

			$set_tran['tran_details'] = 'Reason : '.addslashes($post['reason_for_call']);

			$set_tran['user_id'] = $this->user_id;
			$set_tran['agent_name'] = $this->agent_name;

			if( $this->transaction->add($set_tran) ){
				$this->session->set_flashdata('fmesg', 'Call Successfully submitted');


				$site = $this->sitesettings->get_row(array('set_name'=>'CMA_LIVE'));
				$site_obj = json_decode($site->set_value);
				//autlog to cma calls				

				$cmalog_set['cma_db'] 		= @$site_obj->cma_db;
				$cmalog_set['cust_id'] 		= @$site_obj->cust_id;
				$cmalog_set['contact_id'] 	= @$site_obj->contact_id_issue;
				$cmalog_set['ob1_phone'] 	= @$set_tran['caller_phone'];
				$cmalog_set['ob1_name'] 	= @$set_tran['caller_firstname'].' '.@$set_tran['caller_lastname'];
				$cmalog_set['message'] 		= @$set_tran['tran_details'];
				$cmalog_set['agent_id'] 	= @$this->agent_id;

				$this->cma_external->insert_calls_log($cmalog_set);


				redirect(base_url().'dashboard');
			}			

		}


		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'outbound-call';
		$this->view_data['menu_active'] = 'outbound-call';
		$this->load->view('index', $this->view_data);
 		
 	}

 	public function general_call($call_type=''){	

 		$this->load->model('Transactionmodel', 'transaction');
 		$this->load->library('Mailer', 'mailer');

		$data = '';

		$get = $this->input->get();

		if( $_POST ){
			$post =  $this->input->post();

			if( in_array($call_type, array('call_from_doctor_car', 'general_message_for_admin')) ){

				$set_tran['tran_type'] = $post['tran_type'];
				$set_tran['tran_desc'] = $post['tran_desc'];

				$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
				$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
				$set_tran['caller_phone'] 		= $post['Caller_Phone']; 
				$set_tran['tran_details'] 		= 'Reason: '.addslashes($post['reason_for_call']); 

				$set_tran['user_id'] 			= $this->user_id;
				$set_tran['agent_name'] 		= $this->agent_name;

				$tran_id = $this->transaction->add($set_tran);

				if( $tran_id > 0 ){

					//$this->mailer->send_to = "wayne@housecalldoctor.com.au; gin@housecalldoctor.com.au; gordon@housecalldoctor.com.au; laura@housecalldoctor.com.au; mani@housecalldoctor.com.au";
	 
					if( $call_type == 'general_message_for_admin' ){

						//$to = "wayne@housecalldoctor.com.au; gin@housecalldoctor.com.au; gordon@housecalldoctor.com.au; laura@housecalldoctor.com.au; mani@housecalldoctor.com.au; kellie@welldone.com.au";
							
						$to = $this->Commonmodel->get_admin_email('ADMIN_EMAIL');

						$body .= "Name: ".$post['First_Name'].' '.$post['Last_Name'].PHP_EOL;
						$body .= "Phone: ".$post['Caller_Phone'].PHP_EOL;
						$body .= "Reason: ".$post['reason_for_call'].PHP_EOL;
						$body .= PHP_EOL;
						$body .= SENT_VIA;

						$subject = "General Message for Admin";

						$this->mailer->send_to = $to;
						$this->mailer->Subject = $subject;
						$this->mailer->Body = stripslashes(nl2br($body));

						$mail_return = $this->mailer->sendMail();

						//audit log
						$audit['tran_id'] 		= $tran_id;
						$audit['audit_to'] 		= $to;
						$audit['audit_type'] 	= 'email';
						$audit['message'] 		= addslashes($body);
						$audit['more_info'] 	= $subject;
						$audit['return_message']= $mail_return;
						$this->Commonmodel->insert_audit_trail($audit);

					}

					$this->session->set_flashdata('fmesg', $post['tran_desc'].' Successfully submitted');
					redirect(base_url().'dashboard/call_register');
				}	

			}

			/*if( $call_type == 'no_action_required'  ){
				$set_tran['tran_type'] = $post['tran_type'];
				$set_tran['tran_desc'] = $post['tran_desc'];

				$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
				$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
				$set_tran['caller_phone'] 		= $post['Caller_Phone']; 
				$set_tran['tran_details'] 		= 'Reason: '.addslashes($post['reason_for_call']); 

				$set_tran['user_id'] 			= $this->user_id;
				$set_tran['agent_name'] 		= $this->agent_name;

				$tran_id = $this->transaction->add($set_tran);

				if( $tran_id > 0 ){
					$this->session->set_flashdata('fmesg', $post['tran_desc'].' Successfully submitted');
					redirect(base_url().'dashboard/call_register');
				}
			}
			*/
		
			if( $call_type == 'no_action_required'  ){
				$set_tran['tran_type'] = $post['tran_type'];
				$set_tran['tran_desc'] = $post['tran_desc'];

				$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
				$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
				$set_tran['caller_phone'] 		= $post['Caller_Phone']; 
				$set_tran['tran_details'] 		= 'Reason: '.addslashes($post['reason_for_call']); 

				$set_tran['user_id'] 			= $this->user_id;
				$set_tran['agent_name'] 		= $this->agent_name;

				$tran_id = $this->transaction->add($set_tran);

				if( $tran_id > 0 ){

 					//SEND EMAIL
					$to = 'kellie@welldone.com.au';
					$body = '';				 
					$body .= "Name: ".$set_tran['caller_firstname'].' '.$set_tran['caller_lastname'].PHP_EOL;					
					$body .= "Phone: ".$set_tran['caller_phone'].PHP_EOL;					
					$body .= $set_tran['tran_details'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = 'No Action Required';

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $record->tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);
					//END SEND EMAIL

					$this->session->set_flashdata('fmesg', $post['tran_desc'].' Successfully submitted');
					redirect(base_url().'dashboard/call_register');
				}
			}

			if( $call_type == 'call_from_pharmacy'  ){
				$set_tran['tran_type'] = $post['tran_type'];
				$set_tran['tran_desc'] = $post['tran_desc'];

				$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
				$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
				$set_tran['caller_phone'] 		= $post['Caller_Phone']; 
				$set_tran['tran_details'] 		= 'Reason: '.addslashes($post['reason_for_call']); 

				$set_tran['user_id'] 			= $this->user_id;
				$set_tran['agent_name'] 		= $this->agent_name;

				$tran_id = $this->transaction->add($set_tran);

				if( $tran_id > 0 ){

					$to = $this->Commonmodel->get_admin_email('EMAIL_CALL_PHARMACY');

					$body .= "Name: ".$set_tran['caller_firstname'].' '.$set_tran['caller_lastname'].PHP_EOL;
					$body .= "Phone: ".$post['Caller_Phone'].PHP_EOL;
					$body .= "Reason: ".$post['reason_for_call'].PHP_EOL;
					$body .= PHP_EOL;
					$body .= SENT_VIA;

					$subject = "Call From Pharmacy";

					$this->mailer->send_to = $to;
					$this->mailer->Subject = $subject;
					$this->mailer->Body = stripslashes(nl2br($body));

					$mail_return = $this->mailer->sendMail();

					//audit log
					$audit['tran_id'] 		= $tran_id;
					$audit['audit_to'] 		= $to;
					$audit['audit_type'] 	= 'email';
					$audit['message'] 		= addslashes($body);
					$audit['more_info'] 	= $subject;
					$audit['return_message']= $mail_return;
					$this->Commonmodel->insert_audit_trail($audit);


					$this->session->set_flashdata('fmesg', $post['tran_desc'].' Successfully submitted');
					redirect(base_url().'dashboard/call_register');
				}
			}
		}

		$data['call_type'] = $call_type;
		

		if( $call_type == 'call_from_doctor_car'  ){

			if( isset($get['search2']) ){
				$search2 = explode('|', $get['search2']);

				$data['First_Name'] = $search2[0];
				$data['Last_Name'] = $search2[1];
				$data['Caller_Phone'] = $search2[2];
			}

			$this->view_data['view_file'] = 'general-call-from-hcd-doctor-car';
		}

		if( $call_type == 'general_message_for_admin'  ){

			if( isset($get['search2']) ){
				$search2 = explode('|', $get['search2']);

				$data['First_Name'] = $search2[0];
				$data['Last_Name'] = $search2[1];
				$data['Caller_Phone'] = $search2[2];
			}

			$this->view_data['view_file'] = 'general-call-from-hcd-admin';
		}

		if( $call_type == 'call_from_pharmacy'  ){

			if( isset($get['search2']) ){
				$search2 = explode('|', $get['search2']);

				$data['First_Name'] = $search2[0];
				$data['Last_Name'] = $search2[1];
				$data['Caller_Phone'] = $search2[2];
			}

			$this->view_data['view_file'] = 'general-call-from-pharmacy';
		}

		if( $call_type == 'no_action_required'  ){

			if( isset($get['search2']) ){
				$search2 = explode('|', $get['search2']);

				$data['First_Name'] = $search2[0];
				$data['Last_Name'] = $search2[1];
				$data['Caller_Phone'] = $search2[2];
			}

			$this->view_data['view_file'] = 'general-call-no-action-required';
		}

		
		$this->view_data['data'] = $data;

		$this->view_data['menu_active'] = 'dashboard';
		$this->load->view('index', $this->view_data);
 		
 	}

 	function build_where($params){

		$params['where_str'] = '';
		$search = @$params['search'];
		
		if( isset($params['area']) AND count($params['area'])>0 ){ 
			 
			if( count($params['area']) == 1 ){
				$params['where'] = array('appointment.area_id'=>$params['area'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`area_id` IN ('.implode(',',array_values($params['area'])).')';				
			}
		}


		if( $search != '' ){
			$like = " ( lower(`caller_firstname`) LIKE '%{$search}%' OR ";
			$like .= " lower(`caller_lastname`) LIKE '%{$search}%' OR ";
			$like .= " lower(`caller_phone`) LIKE '%{$search}%' OR ";
			$like .= " lower(`tran_details`) LIKE '%{$search}%' OR ";
			$like .= " lower(concat(`caller_firstname`,' ',`caller_lastname`)) LIKE '%$search%' ) "; 
		 		  
			$query_params['where_str'] = $like;

		}




		return $params;
	}

 	public function call_register(){

		$this->load->model('Areamodel', 'areas');
		$this->load->model('Transactionmodel', 'transaction');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();

		if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}

		$query_params = $this->build_where($params);

		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$params_query = @http_build_query($params);

		$per_page = 20;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;

		$appt_results 				= $this->transaction->get_result_tran_app_pagination($query_params);

		//echo $total_rows;
        $p_config["base_url"] 		= base_url() . "dashboard/call_register/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
 

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'call-register';
		$this->view_data['menu_active'] = 'call-register';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';


		$this->load->view('index', $this->view_data);
 		
 	}

 	public function outbound_register(){

	    $this->load->model('Areamodel', 'areas');
		$this->load->model('Transactionmodel', 'transaction');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$params_query = @http_build_query($params);

		$per_page = 20;


		

	if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}

 		$query_params = $this->build_where($params);



		$query_params['where']['tran_type'] = 'OUTBOUND call';
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;

		$appt_results 				= $this->transaction->get_result_tran_app_pagination($query_params);
		//echo $this->db->last_query();
		//echo $total_rows;
        $p_config["base_url"] 		= base_url() . "dashboard/outbound_register/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
 

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'outbound-register';
		$this->view_data['menu_active'] = 'outbound-register';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';


		$this->load->view('index', $this->view_data);
 		
 	}


 	public function contact( $contact_id = 0)
 	{
 		$data = array();
		$this->load->model('Contactmodel','contact');
 		try {

		 if($contact_id==0)
		 	throw new Exception("Cannot find contact");

				$data['contact'] = $this->contact->get_row(array('id'=>$contact_id));
		   
 		} catch (Exception $e) {
			echo 'Error: '.$e->getMessage(); 			
 		}
		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'contact';
		//$this->view_data['menu_active'] = 'call-register';
		$this->load->view('index', $this->view_data);
	 }

	public function trainingvideos(){

		$this->load->helper('directory');
		$data = '';


		if(isset($_GET['vid']) AND $_GET['vid']!= ''){

			$data['vid_file'] = 'files/training_videos/'.urldecode($_GET['vid']);

		}else{

			$map = directory_map('files/training_videos');

			$map = array_flip($map);

			unset($map['index.html']);

			$data['dir'] = array_flip($map);

			/*echo '<pre>';
			print_r($map);
			echo '<pre>';*/ 
		}

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'traning-videos';
		$this->view_data['menu_active'] = 'trainingvideos';
		$this->load->view('index', $this->view_data);

	}
}
