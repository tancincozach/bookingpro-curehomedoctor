<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	protected  $misc_data = array();

	public function __construct()
	{
		parent::__construct();

		$this->view_data['sidebar'] = array('sidebar_class'=>'');
		$this->misc_data = array('user_id'=>$this->user_id,'agent_name'=>$this->agent_name);
	}

	public function index()
	{
		$this->view_data['view_file'] = 'maintenance/index';
		$this->view_data['sidebar']['sidebar_active'] = '';
		$this->load->view('index', $this->view_data);
	}



    public function paginate($config)
    {
/*		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;*/
		$this->load->library('pagination');
		$this->load->model('Commonmodel', 'common');	    		
		$this->pagination->initialize($this->common->pagination_config($config) ); 
		return $this->pagination->create_links();
    }
 
    public function general_settings(){

    	$this->load->model('Sitesettingsmodel', 'sitesettings');
    	$data = array();

    	if($_POST) {

			$post = $_POST;
			$set  = array();
			$set['updated_by']		= $this->agent_name; 			
			$set['updated_user_id']	= $this->user_id; 			
			$set['set_updated']		= date('Y-m-d H:i:s'); 	

    		switch ($_POST['action']) {
    			case 'greeting':

				

				$html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $post['content_html'] );
				$set['set_value']		= addslashes($html); 			
				

				if($this->sitesettings->update_by_name('GREETING', $set)){
					$this->session->set_flashdata('fmesg', 'Greeting updated successfully');
					redirect(base_url().'maintenance/general_settings');
				}else{
					$this->session->set_flashdata('fmesg', 'Failed to update Greeting');
					redirect(base_url().'maintenance/general_settings');
				}
				break;
    		}
    	}

		$data['greeting'] = $this->sitesettings->get_row(array('set_name'=>'GREETING'));
		$data['max_booking'] = $this->sitesettings->get_row(array('set_name'=>'MAX_BOOKING'));
		$data['override_sched_filter'] = $this->sitesettings->get_row(array('set_name'=>'OVERRIDE_SCHED_FILTER'));

		$email_fields   = array('ADMIN_EMAIL','EMAIL_SUBURB_NOT_ON_LIST','EMAIL_REQUEST_ETA','EMAIL_CANCEL_BOOKING','EMAIL_CALL_PHARMACY','EMAIL_GENERAL','WD_EMAIL');

		$email_result  = array();


		foreach ($email_fields as $field ) {

			if($result = $this->sitesettings->get_row( array('set_name'=>$field),'array')){					

			  $data['email'][]= array('set_name'=>$result['set_name'],'set_value'=>$result['set_value']);
			
			}else{
			   $data['email'][]= array('set_name'=>$field,'set_value'=>'')	;
			}

		}

		 
		$this->view_data['data'] = $data;			
		$this->view_data['view_file'] = 'maintenance/general_settings';
		$this->view_data['sidebar']['sidebar_active'] = 'general';
		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/tinymce/tinymce.min.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/bootstrap-toggle.min.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/generalsettings.js"></script>';		
		$this->view_data['css_file'] = '<link href="assets/css/bootstrap-toggle.min.css" rel="stylesheet">';
		$this->load->view('index', $this->view_data);		
    }




	public function areas()
	{
		$this->load->model('Areamodel', 'areas');


		$parameter = $this->input->get();

		
		

		switch ( @strtolower($parameter['filter']) )
		{				
			   	case 'all':				
					$where  = array();
				break;			
				case 'inactive':				
					$where  = array('areas.area_status'=>0 );
				break;
				
				default:
					$where  = array('areas.area_status'=>1 );
				break;
		}	

	  	
		$pconfig["base_url"] = base_url() . "maintenance/areas";        
		$pconfig["per_page"] = 15;
		$pconfig['query_string_segment'] ='page';    	                
	
		$effective_date = $this->Commonmodel->get_server_converted_date($this->default_timezone); //converted to qld timezone



    	$return = $this->areas->get_result_pagination( array('search'=>isset($parameter['search']) ? $parameter['search']:'','where'=>$where, 'effective_date'=>$effective_date));
    	
    	$data['current_date'] = $effective_date;
    	//echo $this->db->last_query();

		$data['links'] = $this->paginate($pconfig);	

		$data['record'] = $return['result'];		
		$data['misc_data'] = $this->misc_data;
		$data['current_day'] = date('Y-m-d');

		$data['area'] = (isset($parameter['area']) ? $this->areas->get_result(array('area_id'=>$parameter['area'])) :array());		  

		$this->view_data['sidebar']['sidebar_active'] = 'areas';
		$this->view_data['view_file'] = 'maintenance/areas';
		$this->view_data['data'] = $data;
		///$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_areas.js"></script>';
		//$this->view_data['js_file'] .= '</script><script src="assets/plugins/moment/moment.min.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		
		$this->load->view('index', $this->view_data);
	}	

	public function area()
	{

		$this->load->model('Areamodel', 'areas');




		$parameters = $this->input->get();

		if( $_POST ){

			$post = $this->input->post();

			$set = array();

			$area_name = preg_replace('/\s+/', ' ', $post['area_name']);

 			$set['area_name'] = trim(strtoupper($area_name));
 			$set['timezone'] = $post['timezone'];





			if( $post['formtype'] == 'new' )
			{
			
				if($set['area_name']=='' || $set['timezone']=='')
				{
				  $this->session->set_flashdata('error','Error: Empty required field');
				  redirect('maintenance/areas'); 
				}

			
				$return = $this->areas->add(array_merge($set , array('user_id'=>$this->user_id,'agent_name'=>$this->agent_name)));


				if(is_array($return) && array_key_exists('error', $return_array))
				{
  					$this->session->set_flashdata('error',$return['error']);
  					redirect('maintenance/areas'); 				  	
				}
			
	  			$this->session->set_flashdata('fmesg','Successfully added an area.');

			    redirect('maintenance/areas'); 				  	
			
			}

			if( $post['formtype'] == 'update' )
			{	

				if(!isset($post['area']))
				{
				  $this->session->set_flashdata('error','Error: Empty area field');
				  redirect('maintenance/areas'); 
				}
		
				$return = $this->areas->update( $post['area'], array_merge($set,$this->misc_data));

				if(is_array($return) && array_key_exists('error', $return['error']))
				{
  					$this->session->set_flashdata('error',$return['error']);
					redirect('maintenance/areas'); 
				}

  				$this->session->set_flashdata('fmesg','Update successful.');

				redirect('maintenance/areas'); 											
			}	
		}

		if(isset($parameters['activate']) || isset($parameters['disable']))
		{
			if(isset($parameters['activate']))
			{
				$set['area_status']=1;		
				$area_id = 	$parameters['activate'];
			}
			else
			{
				$set['area_status']=0;					            
				$area_id = 	$parameters['disable'];
			}		   

   		   $return = $this->areas->update( $area_id, array_merge($set,$this->misc_data));       

   		    if(isset($parameters['activate']))
			{				
	            $this->session->set_flashdata('fmesg','Area successfully enabled.');
			}
			else
			{			
	            $this->session->set_flashdata('fmesg','Area successfully disabled.');
			}


   		   if(is_array($return) && array_key_exists('error', $return['error']))
		   {
  			 $this->session->set_flashdata('error',$return['error']);

			 redirect('maintenance/areas'); 
		   }



		   redirect('maintenance/areas'); 
		}

	}

	public function area_form_endscript($area_id=''){


		$this->load->model('Areamodel', 'areas');

 
		try {
			
			if( $area_id == '' ) throw new Exception("Error Processing Request", 1);
			
			$data = '';

			if($_POST) {

				$post = $this->input->post();

				$area_id = $post['area_id']; 

				$html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['content_html'] );				
				$set['form_end_script']		= addslashes($html);
				$set['user_id'] = $this->user_id;
				$set['agent_name'] = $this->agent_name;

				if( $this->areas->update( $area_id, $set) > 0 ){
					$this->session->set_flashdata('fmesg','Area ('.$post['area_name'].') End Script successfully updated.');
					redirect('maintenance/areas');
				}else{
					$this->session->set_flashdata('error','Failed to update Area ('.$post['area_name'].') End Script.');
					redirect('maintenance/area_form_endscript/'.$area_id);				
				}

			}

			$data['area'] = $this->areas->get_row(array('area_id'=>$area_id));


			$data['area_id'] = $area_id;

			$this->view_data['sidebar']['sidebar_active'] = 'areas';
			$this->view_data['view_file'] = 'maintenance/areas-form-end-script';
			$this->view_data['data'] = $data; 
			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_areas_endscript.js"></script>'; 
			$this->load->view('index', $this->view_data);


		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


   /*****************  SUBURB *********************/

   public function suburbs()
   {

		$parameter = $this->input->get();

		$this->load->model('Areamodel', 'area');


	    $pconfig = array();
	    $data 	 = array();



	    try{
	    	if (!isset($parameter['area'])  || $parameter['area']==0)
	    	{
	    	  throw new Exception("Error: Empty Area");	    	  		
	    	}



			switch ( @strtolower($parameter['filter']) )
			{				
			   	case 'all':				
					$where  ='area_id = '.$parameter['area'];
				break;			
				case 'inactive':				
					$where  ='area_id = '.$parameter['area'].' AND suburb_status=0';
				break;
				
				default:
					$where  ='area_id = '.$parameter['area'].' AND suburb_status=1';
				break;
			}	




			$pconfig["base_url"] = base_url() . "maintenance/suburbs/?area=".$parameter['area'].(isset($parameter['filter']) && !empty($parameter['filter'])  ? "&filter=".$parameter['filter']:"");        
			$pconfig["per_page"] = 15;
			$pconfig['query_string_segment'] ='page';    	  			


			$return = $this->area->suburb_get_result_pagination( array('search'=>isset($parameter['search']) ? $parameter['search']:'','where'=>$where,'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 'limit'=>$pconfig["per_page"])));


			$pconfig["total_rows"] = $return['total_rows'];	


			$data['links'] = $this->paginate($pconfig);	


			$data['record'] = $return['result'];

			
			$data['areas'] = (isset($parameter['area']) ? $this->area->get_row(array('area_id'=> $parameter['area'])) :array());	

			$data['area_id'] = $parameter['area'];		



			if(isset($parameter['status']) && isset($parameter['suburb_id'])){
				$set = array();
				$set['suburb_status'] = ($parameter['status']=='disable' ? 0:1);

					
				$this->area->update_suburb($parameter['suburb_id'],array_merge($set,$this->misc_data));

			 	 redirect('maintenance/suburbs/?area='. $parameter['area']);
			}
	    }
	    catch(Exception $error)
	    {
	    	  $this->session->set_flashdata('error',$error->getMessage());
	    }       

		$this->view_data['sidebar']['sidebar_active'] = 'suburb';	

		$this->view_data['view_file'] = 'maintenance/suburb_list';		
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		
		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_areas.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';

		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);	
   }


   public function suburb($formtype='new')
	{
			$this->load->model('Areamodel', 'areas');

			$post = $this->input->post();

			$get = $this->input->get();

			$formtype  = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] );	

			$area_id   = (isset($post['area']) ? $post['area']  : $get['area'] ); 	 	

		try{

			switch($formtype){
				case 'new':
			   	case 'update':		

				$suburb_name = preg_replace('/\s+/', ' ', $post['suburb_name']);
				$postcode  = preg_replace('/\s+/', ' ', $post['suburb_postcode']);
			 	$posted_data = array();

				$posted_data['suburb_name'] = trim(strtoupper($suburb_name));
				$posted_data['suburb_postcode'] = trim(strtoupper($postcode));
				$posted_data['area_id'] =  $area_id;				



				 if(count($posted_data)==0)
				 {
				   throw new Exception("Error : Empty input fields. ");
				 }
					 
				if($formtype  == 'new' )
				{

					$return_array = $this->areas->suburb_add(array_merge($posted_data ,$this->misc_data));

			  		if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);			  


			  		if($return_array==0)
			  			 		     throw new Exception('Error: Unable to add suburb');			  

			  		if($return_array > 0)
			  		{
			  		  $this->session->set_flashdata('fmesg', 'Successfully added a suburb');					
					  redirect('maintenance/suburbs/?area='.  $area_id); 	
			  		}
			
				}

				if($formtype  =='update')
				{				
					$posted_data['suburb_updated'] = date('Y-m-d h:i:s');

					$return_array = $this->areas->update_suburb( $post['suburb_id'] , array_merge($posted_data,$this->misc_data));

					if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);		

					 $this->session->set_flashdata('fmesg', 'Successfully updated a suburb');	

					 redirect('maintenance/suburbs/?area='.  $area_id); 	  			    

			    }
		   		break;
			   	case 'delete':
					$posted_data['suburb_updated'] = date('Y-m-d h:i:s');
					$posted_data['suburb_status']  = 0;					

					print_r($posted_data);
					exit();
					$return_array = $this->areas->update_suburb( $get['suburb_id'], array_merge($posted_data,$this->misc_data));



					if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

 					$this->session->set_flashdata('fmesg', 'Successfully deleted a suburb');	

					redirect('maintenance/suburbs/?area='. $area_id);	  			       
			   	break;

			   	default:
			   	 	 redirect('maintenance/suburbs/?area='. $area_id);
			   	break;

			 }	
		}	
		catch( Exception $error)
		{			
		  $this->session->set_flashdata('error',$error->getMessage());

		}
	}

	public  function suburb_form()
	{
		$this->load->model('Areamodel', 'areas');
		$parameter = $this->input->get();

		 try
		 {
		   if(!isset($parameter['area']) || $parameter['area'] ==''  || $parameter['area']==0) throw new Exception("Error: Area cannot be empty");

			$this->view_data['view_file'] = 'maintenance/suburb_form';
			$data['area_id'] = $parameter['area'];
			if(isset($parameter['suburb_id']))
			{
				$data['suburb'] = $this->areas->get_result_suburb(array('suburb_id'=>$parameter['suburb_id']));	  			
			}
			
		 }	
		 catch (Exception $e)
		 {	 	
		 	$this->session->set_flashdata('error',  $e->getMessage());
		 	$this->view_data['view_file'] = 'maintenance/areas';
		 }	 	
			$this->view_data['data'] = $data;
			$this->view_data['sidebar']['sidebar_active'] = 'areas';			
			$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_areas.js"></script>';		
			$this->load->view('index', $this->view_data);
		}


   public function import_suburb()
   {
		$this->load->model('Areamodel', 'areas');		
		$this->load->model('Commonmodel', 'common');	


		try
		{

			$row = 0;
			$result = array();



			$file_path =  $_FILES['suburb']['tmp_name'];     

			$handle = fopen($file_path, "r");

			if(!$handle) throw new Exception('Cannot Open File');
			$delimiter = ",";
			$initial_line = 0;
			$header_exist = array();

			while (($data = fgetcsv( $handle, 0, $delimiter)) !== FALSE) 
			{   
				$insert['suburb_postcode']= isset($data[0]) ?  strtoupper(trim($data[0])):"";		
				$insert['suburb_name']    = isset($data[1]) ?  strtoupper(trim($data[1])) : '';			
				$insert['suburb_area']	  = isset($data[2]) ?  strtoupper(trim($data[2])):"";											
				$insert['user_id']=$this->user_id;
				$insert['agent_name']=$this->agent_name;					
			 	$return_array = $this->areas->suburb_add(array_merge($insert,$this->misc_data));
			 	if(is_array($return_array) &&  array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

			} 

		  $this->session->set_flashdata('fmesg', 'Csv Data Imported Succesfully');
		  
		}
		catch( Exception $error)
		{			
		  $this->session->set_flashdata('error',$error->getMessage());
		  
		}		
		redirect('maintenance/areas');
   } 

	public function import_doctors()
   {


		redirect('maintenance/doctors');
   } 


   public function doctors()
   {
   	   	$data = array();


   	   	$this->load->model('Usermodel', 'user');

   	   	if($_FILES)
   	   	{
	   	   	try
			{

				$row = 0;

				$result = array();

				if(empty($_FILES)) throw new Exception('Please select file to upload.');

				$file_path =  $_FILES['doctors']['tmp_name'];     

				$handle = fopen($file_path, "r");

				if(!$handle) throw new Exception('Cannot Open File');

				$delimiter = ",";

				$insert = array();
				
				$existing = array();

				while (($data = fgetcsv( $handle, 0, $delimiter)) !== FALSE) 
				{   
					$insert['user_fullname']  = $data[1].' '.$data[2];

					$insert['user_name']      = $data[4];

					$insert['user_pass']	  = md5($data[5]);

					$insert['user_level']	  = 5;

					$insert['user_sublevel']  = 2;			

					$insert['prov_num']  =  $data[3];

					$doctor_info = $this->user->get_user_row(" user_name='".trim($data[4])."' OR user_fullname = '".trim($data[1]).' '.trim($data[2])."'",'array');	

					if(count($doctor_info) > 0 )
					{
							$existing[] = $doctor_info;							

							$return_array = $this->user->update($doctor_info['user_id'],array_merge($insert,$this->misc_data));						

					}
					else
					{
						$return_array = $this->user->add(array_merge($insert,$this->misc_data));
						if(is_array($return_array) &&  array_key_exists('error', $return_array)) throw new Exception($return_array['error']);		
					}					
				} 			
			 	

			  $this->session->set_flashdata('fmesg', 'Csv Data Imported Succesfully');
			  
			}
			catch( Exception $error)
			{			
			  $this->session->set_flashdata('error',$error->getMessage());
			  
			}		
	
   	   	}
		$data['existing_doctors'] = @$existing;
		$this->view_data['view_file'] = 'maintenance/doctors';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);
   }

   public function availability_history()
   {

   	$this->load->model('Areamodel', 'areas');	

	$parameter = $this->input->get();

	$data = array();

	$data['record'] = $this->areas->get_area_availabilty_history(
											array(
												'target_table'=>'areas_availability',
												'table_ref_col'=>'area_id',
												'table_ref_id'=>$parameter['area']
												)	
										     );

	
	$this->view_data['view_file'] = 'maintenance/area_availability_history';	
	$this->view_data['data'] = $data;
	$this->view_data['sidebar']['sidebar_active'] = 'areas';			
	$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_areas.js"></script>';		
	$this->load->view('index', $this->view_data);
   }

  /*********************CARS**************************/


  	public function cars()
  	{
		$this->load->model('Areamodel', 'areas');


		$parameter = $this->input->get();

		
		try {



			switch ( @strtolower($parameter['filter']) )
			{				
				   	case 'all':				
						$where  = array();
					break;			
					case 'inactive':				
						$where  = array('car_deleted'=>1 );
					break;
					
					default:
						$where  = array('car_deleted'=>0 );
					break;
			}	

				$pconfig["base_url"] = base_url() . "maintenance/cars";        
				$pconfig["per_page"] = 15;
				$pconfig['query_string_segment'] ='page';    	                
			

		    	$return = $this->areas->get_cars_per_area ( 
		    												array(
		    														'search'=>isset($parameter['search']) ? $parameter['search']:'',
    																'where'=>$where,
    																'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 'limit'=>$pconfig["per_page"]),
		    														'join'=>array(
		    																'table'=>'areas',
		    																'conditional'=>'areas_cars_link.area_id = areas.area_id',
		    																'type'=>'left'
		    																)		    														
		    												  )
		    												);


				$pconfig["total_rows"] = $return['total_rows'];	


		     

				$data['links'] = $this->paginate($pconfig);	


				$data['record'] = $return['result'];		
				$data['misc_data'] = $this->misc_data;
				$data['current_day'] = date('Y-m-d');				

				$data['areas'] = (isset($parameter['area']) ? $this->areas->get_row(array('area_id'=> $parameter['area'],'area_status'=>1)) :array());		  

			

				$this->view_data['sidebar']['sidebar_active'] = 'cars';
				$this->view_data['view_file'] = 'maintenance/car_list';
				$this->view_data['data'] = $data;
				$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

				$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_car.js"></script>';
				//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
				$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
				
				$this->load->view('index', $this->view_data);
					
		} catch (Exception $e){
			$this->session->set_flashdata('error',$e->getMessage());
			redirect('maintenance/areas');
		}
		
  	}


	public  function car_form()
	{
		$this->load->model('Areamodel', 'areas');
		$parameter = $this->input->get();

		$data = array();
		
		 try
		 {		 

			$this->view_data['view_file'] = 'maintenance/car_form';		
			if(isset($parameter['car']))
			{
					$data['car_details'] = (isset($parameter['car']) ? $this->areas->get_row_by_car(array('car_id'=> $parameter['car'])) :array());	

			}		

			
		 }	
		 catch (Exception $e)
		 {	 	
		 	$this->session->set_flashdata('error',  $e->getMessage());
		 	 redirect('maintenance/areas'); 
		 }	
		    $data['areas'] = $this->areas->get_array(array('area_status'=>1));
			$this->view_data['sidebar']['sidebar_active'] = 'cars';
			$this->view_data['data'] = $data;
			$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

			$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_car.js"></script>';
			//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
			$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';

			$this->load->view('index', $this->view_data);
		}

  	 public function car()
	{
			$this->load->model('Areamodel', 'areas');

			$post = $this->input->post();

			$get = $this->input->get();


			$formtype  = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] );	
	


		try{

			switch($formtype){
				case 'new':
			   	case 'update':		

			 	$posted_data = array();
				$posted_data['car_name'] = trim(preg_replace('/\s+/', ' ', $_POST['car_name']));
				$posted_data['area_id'] =  $_POST['area_id'];
				$posted_data['car_plateno'] = trim(preg_replace('/\s+/', ' ', $_POST['car_plateno']));
				$posted_data['car_email'] = trim(preg_replace('/\s+/', ' ', $_POST['car_email']));
				$posted_data['car_mobile'] = trim(preg_replace('/\s+/', ' ', $_POST['car_mobile']));
				$posted_data['dropbox_email'] = trim(preg_replace('/\s+/', ' ', @$_POST['dropbox_email']));			
				$posted_data['dropbox_login'] =  trim(preg_replace('/\s+/', ' ', @$_POST['dropbox_login']));				
				$posted_data['dropbox_secret'] = trim(@$_POST['dropbox_secret']);			
				$posted_data['dropbox_pass'] = trim(@$_POST['dropbox_pass']);			
				$posted_data['dropbox_key'] = trim(@$_POST['dropbox_key']);			
				$posted_data['send_email'] = $_POST['send_email'];			
				$posted_data['send_text']   =  $_POST['send_sms'];			


				 if($posted_data['car_name']=='')
				 {
				   throw new Exception("Error : Empty input fields. ");
				 }
					 
				if($formtype  == 'new' )
				{

					$return_array = $this->areas->car_add(array_merge($posted_data ,$this->misc_data));




			  		if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);			  


			  		if($return_array==0) throw new Exception('Error: Unable to add a car');			  

			  		if($return_array > 0)
			  		{
			  		  $this->session->set_flashdata('fmesg', 'Successfully added a car');		

					  redirect('maintenance/cars/'); 	
			  		}
			
				}

				if($formtype  =='update')
				{				
					$posted_data['car_updated'] = date('Y-m-d h:i:s');



					$return_array = $this->areas->car_update( $post['car'] , array_merge($posted_data,$this->misc_data));

					if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);		

					 $this->session->set_flashdata('fmesg', 'Successfully updated a car');	

					 redirect('maintenance/cars/?car='.$post['car']); 	  			    
 
			    }
		   		break;
			   	case 'delete':
					$posted_data['car_updated'] = date('Y-m-d h:i:s');
					$posted_data['car_deleted']  =1;					

					$return_array = $this->areas->car_update( $get['car'], array_merge($posted_data,$this->misc_data));

					if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

 					$this->session->set_flashdata('fmesg', 'Successfully deleted a car');	

					redirect('maintenance/cars/?car='.$get['car']); 	  			    	       
			   	break;

			   	default:
			   	 	 redirect('maintenance/cars/'); 	  			    
			   	break;

			 }	
		}	
		catch( Exception $error)
		{			
		  $this->session->set_flashdata('error',$error->getMessage());

		}
	}

  /******************END OF CARS********************/

/*****************  PATIENT  *********************/

	public function patients()
	{

		$parameter = $this->input->get();

	
		$this->load->model('PatientModel', 'patient');
		$this->load->model('AreaModel', 'areas');


	    $pconfig = array();
	    $config  = array();

		$pconfig["base_url"] = base_url() . "maintenance/patients";        	

	    if(isset($parameter['search']))
	    {
	    	$pconfig["base_url"] = base_url() . "maintenance/patients?&search=".$parameter['search'];        	
	    }

        
        $pconfig["per_page"] = 15;
		$pconfig['query_string_segment'] ='page';    	                
        


		switch (@$parameter['filter']) {

			case 'show_duplicates':
				
						$return = $this->patient->get_result_pagination(
																		 array(																		 	
																		 	  'search'=>isset($parameter['search']) ? $parameter['search']:'',
																		 	  'where'=> 'patient_status=1',
																			  'limit'=> array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 'limit'=>$pconfig["per_page"]),
																			  'having'=> 'COUNT(*)>1',
																			  'group_by'=> 'firstname,lastname'					
																			)
																		);

						$pconfig["total_rows"] = $return['total_rows'];	
			  

						$data['links'] = $this->paginate($pconfig);				

						$data['record'] = $return['result'];

			break;

			
			default:
						$return = $this->patient->get_result_pagination( 
															 array(
														 	 	  'search'=>isset($parameter['search']) ? $parameter['search']:'',
															 	   'where'=> 'patient_status=1',
															 		'limit'=> array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 'limit'=>$pconfig["per_page"])
															 	  )
													    );

						$pconfig["total_rows"] = $return['total_rows'];	


						$data['links'] = $this->paginate($pconfig);	


						$data['record'] = $return['result'];
				break;
		}

		if(isset($parameter['download'])){

			$this->export_report_patient();

		}


		$this->view_data['areas'] = $this->areas->get_result( array('area_status'=>1));

		$this->view_data['sidebar']['sidebar_active'] = 'patient';	
		$this->view_data['view_file'] = 'maintenance/patient_list';				
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_patient.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';

		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);		
	}


		/**
	 * This is must be update if this is multiple client
	 */
	public function export_report_patient($params=array()) {

		ini_set("memory_limit",-1);

		$this->load->model('PatientModel', 'patient');

		$this->load->library('ExportDataExcel'); 

		$params = $this->input->get();	



		$data = '';


		if(isset($params['dl'])){

			$results	= 		$return = $this->patient->get_result_pagination_export( array( 'where'=> '`patient`.`patient_status`=1 and `areas`.`area_id` ='.$params['dl']));


			

		}else{

			$results	= 		$return = $this->patient->get_result_pagination_export( array('where'=> '`patient`.`patient_status`=1'));

		}

/*		echo '<pre>';
		print_r($results);
		echo '<pre/>';

		exit();
*/

			$header = array(
										'Patient #',
										'Firstname',
										'Lastname',
										'MI',
										'Date of Birth',
										'Gender',
										'Address', 
										'Cross Street', 
										'Suburb',							
										'Area',			
										'Landline',
										'Mobile',
										'Email',
										'Medicare #',
										'Medicare Ref',
										'Medicare Expiry',
										'Health Card Number',
										'Health Card Insurance Provider',
										'Health Card Expiry Date',
										'Latest Symptom',
										'Regular Practice',
										'Regular Doctor',
										'Banned Patient',
										'Banned Reason',
										'Deceased Patient',
										'Notes',
										'Regular Doctor',
										'Emergency Contact Name',
										'Emergency Contact Number'
									);


						foreach( $return['result'] as $row ){
						

								$inRow = array();
								$inRow[] = intval($row->patient_id) + 1000;
								$inRow[] = $row->firstname;
								$inRow[] = $row->lastname; 
								$inRow[] = $row->middleinitial; 
								$inRow[] = date('d/m/Y',strtotime($row->dob)); 
								$inRow[] = $row->gender; 
								$inRow[] = $row->street_addr;
								$inRow[] = $row->cross_street;								
								$inRow[] = $row->suburb;
								$inRow[] = $row->suburb_area;
								$inRow[] = $row->phone;
								$inRow[] = $row->mobile_no; 
								$inRow[] = $row->email;
								$inRow[] = $row->medicare_number;
								$inRow[] = $row->medicare_ref;
								$inRow[] = $row->medicare_expiry;
								$inRow[] = $row->health_fund_number;
								$inRow[] = $row->health_insurance_provider;
								$inRow[] = $row->health_card_expiry_date;
								$inRow[] = $row->latest_symptoms;
								$inRow[] = $row->regular_practice;			
								$inRow[] = $row->regular_doctor;			
								$inRow[] = $row->banned;			
								$inRow[] = $row->banned_reason;			
								$inRow[] = $row->deceased;			
								$inRow[] = $row->patient_notes;			
								$inRow[] = $row->em_contact_name;		
								$inRow[] = $row->em_contact_phone;		
								
								$data[] = $inRow;
				}
		


			$exporter = new ExportDataExcel('browser'); //browser, string, file

			$exporter->filename = 'Patient-'.rand(date('mdYHis'), 5).'.xls';	

			$exporter->initialize();

			$exporter->addRow($header);  
		
			foreach($data as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit();	
	}


	public function patient_form( )
	{
	
	  $parameters = $this->input->get();

	  $data = array();

	  $this->load->model('PatientModel', 'patient');
	  $this->load->model('AreaModel', 'areas');
  	  $this->load->model('Dropdownmodel');
	
		try
		{
		  if(isset($parameters['patient']))
		  {

			  	if(!isset($parameters['patient']))
			  	{
			  		throw new Exception("Error : Empty ID ");			  		
			  	}		   		
			    $data['patient_info'] = $this->patient->get_row(array('patient_id'=>$parameters['patient']));

/*			    echo '<pre>';
			    print_r($data['patient_info']);
			    echo '<pre/>';
			    exit();*/
			   
			    $this->view_data['data'] = $data; 


		  }		  	
		}
		catch( Exception $error)	
		{						
		   $this->session->set_flashdata('error',$error->getMessage());
		}				
			
		$data['suburb'] = $this->areas->get_result_suburb();
		$data['wyhaulist'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'WYHAU')));
		$this->view_data['sidebar']['sidebar_active'] = 'patient';
		$this->view_data['view_file'] = 'maintenance/patient_form';	

		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_patient.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/plugins/combodate.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/jquery.inputmask.bundle.min.js"></script>'.PHP_EOL;
		//$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking_practice_autocomplete.js"></script>'.PHP_EOL;
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);	
	}

	
   public function import_patient()
   {
	 $this->load->model('PatientModel', 'patient');	
		$this->load->model('Commonmodel', 'common');	

		$return_array  =  $this->common->data('patient');

		try
		{
		  $area_id = $this->input->post('area_id');

		  if(array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	
		  		  

		  foreach ($return_array as $row) 
		  {
			    
			     $insert_data[]= array(				    	
				        'firstname'=>$row['firstname'],
				        'lastname'=>$row['lastname'],
				        'street_addr'=>$row['street_addr'],				        
				        'mobile_no'=>$row['mobile'],
				        'phone'=>$row['phone'],
				        'suburb'=>$row['suburb'],
				        'medicare_number'=>$row['medicare_number'],
				        'medicare_ref'=>$row['medicare_ref'],
				        'dva'=>$row['dva']
				    );

			    $this->patient->add($insert_data , array('user_id'=>$this->user_id,'agent_name'=>$this->agent_name ));
		  }			  
		    	$this->session->set_flashdata('fmesg', 'Csv Data Imported Succesfully');
		  
		}
		catch( Exception $error)
		{			
		  $this->session->set_flashdata('error',$error->getMessage());
		  
		}		
		redirect('maintenance/patients');
   } 


	public function patient_merge()
	{	
	    $data 	 = array();	
	  	$parameters = $this->input->get();	   
		$this->load->model('PatientModel', 'patient');

	  	try 
	  	{
	  	  if(!isset($parameters['patient']))
	  	  {
	  	  	throw new Exception("Error : No patient to be compared.");	  	  	
	  	  }

  		$patients = $this->patient->get_duplicates_to_be_compared($parameters['patient']);		

		$data['patient'] = @$patients['base'];
		$data['duplicates'] = @$patients['duplicates'];

	  	} 
	  	catch (Exception $e) 
	  	{
	  	 $this->session->set_flashdata('error',$error->getMessage());
	  	}

		
		$this->view_data['sidebar']['sidebar_active'] = 'patient';	
		$this->view_data['view_file'] = 'maintenance/patient_merge';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_patient.js"></script>"></script>';		

		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);	
	}

	public function compare_patient()
	{
		$parameters = $this->input->get();	   
		$this->load->model('PatientModel', 'patient');
		$this->view_data['sidebar']['sidebar_active'] = 'patient';	
		$this->view_data['view_file'] = 'maintenance/merge';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_patient.js"></script>';		
		$this->load->view('index', $this->view_data);	
	}


	public function patient()
	{
		$this->load->model('PatientModel' , 'patient');

		$filtered_posted_values = array('formtype' , 'patient');
		$post = $this->input->post();
		$get = $this->input->get();
	    $formtype = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] ); 	

		try
		{
		   if($formtype=='')			
		   {
		   	 throw new Exception("Error : No action found. ");
		   }

		   switch ($formtype)
		   {
		   	case 'delete':	

	   	        $patient_id = (isset($post['patient']) ? $post['patient']  : $get['patient'] ); 			   	      	   	       	  
				$posted_data['patient_updated'] = date('Y-m-d h:i:s');
				$posted_data['patient_status']  = 0;			

			      if($formtype=='delete' && $this->patient->update($patient_id , array_merge( $posted_data,$this->misc_data )))
				  {
			       $this->session->set_flashdata('fmesg', 'Successfully deleted a patient');	
			       redirect('maintenance/patients');	  			       
				  }

		   	break;
		   	case 'new':
		   	case 'update':			

		 	$posted_data = array();

	   	 	foreach ($post as $field => $value) 
	     	{
				if($value!='')						
				{
					if(!in_array($field, $filtered_posted_values))
					{

						$posted_data[$field] = $value;						  	
						if($field=='dob')				  
						{
							$f_dob = explode('/', $value);
							//$posted_data[$field] = date('Y-m-d',strtotime($value));						  			
							$posted_data[$field] = $f_dob[2].'-'.$f_dob[1].'-'.$f_dob[0];
						}
						if($field=='dva')				  
						{
							$posted_data[$field] = (isset($value) ? 1 :0);						  						  		
						}				  		
					}						
				}				  
			}

			 if(count($posted_data)==0)
			 {
			   throw new Exception("Error : Empty input fields. ");
			 }
				 
 			if(  $formtype  == 'new' )
			{
  		      if($this->patient->add(array_merge( $posted_data,$this->misc_data )))
			  {			  	
				$this->session->set_flashdata('fmesg', 'Successfully added a patient');					
			    redirect('maintenance/patients');	
			  }	
			}

			if($formtype  =='update')
			{

			  $posted_data['patient_updated'] = date('Y-m-d h:i:s');
		      if( $this->patient->update( $post['patient'], array_merge( $posted_data,$this->misc_data ) ) )
			  {
			    $this->session->set_flashdata('fmesg', 'Successfully updated a patient');				    
			    redirect('maintenance/patient_form?action=edit&patient='.$post['patient']);
			  }
		    }
		   		break;
		   	default:
		   	 redirect('maintenance/patients');
		   	break;
		   }
		} 
		catch (Exception $error)
		{
		   $this->session->set_flashdata('error',$error->getMessage());			

		   if($formtype=='new')
		   {
		   	redirect('maintenance/patient_form');		
		   }
		   if($formtype=='update')
    	   {
    	   	redirect('maintenance/patient_form?action=edit&patient='.$post['patient']);
    	   }
    	   if($formtype=='delete')
    	   {
            redirect('maintenance/patients');	
    	   }
		}
	}


	/*********************USERS*********************/

	

	public function user()
	{


	  	$this->load->model('Usermodel' , 'user');
  	 	$this->load->model('Areamodel' , 'area');

		$filtered_posted_values = array('formtype' , 'user','confirm_pass');
		$post = $this->input->post();
		$get = $this->input->get();
	    $formtype = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] ); 

		try
		{

		   switch ($formtype)
		   {
		   	case 'delete':	

		   	    $user_id = (isset($post['user']) ? $post['user']  : $get['user'] ); 	
		   	       

     	       	$user_info = $this->user->get_user_row(array('user_id'=>$user_id));

   	       	    $posted_data['user_updated'] = date('Y-m-d h:i:s');
				$posted_data['deleted']  = 1;			


				$return  =  $this->user->update(array_merge(array('id'=>$user_id),$this->misc_data),$posted_data);

				if(is_array($return) && array_key_exists('error', $return)) throw new Exception($return['error']);		

			    $this->session->set_flashdata('fmesg', 'Successfully deleted a user');	

			    redirect('maintenance/users');	  			       
				  
		   	break;
		   	case 'new':
		   	case 'update':			

			 	$posted_data = array();
			 	$posted_data_chaperone = array();

			   	 foreach ($post as $field => $value) 
			     {
				  if($value!='')						
				  {
					if(!in_array($field, $filtered_posted_values))
				  	{
				  		if($field=='user_pass')
				  		{

				  		$posted_data[$field] = md5($value);						  	

				  		}
				  		else
				  		{
				  		  if($field=='area_id')
				  		  {
				  		  	$posted_data_chaperone[$field] =  $value;				  		  	
				  		  }
				  		  else{
				  		  	$posted_data[$field] = $value;						  		
				  		  }
						  
				  		}
				    }						
				  }				  
				 }



				 if($post['user_level']!=5)
				 {
				 	$posted_data['user_sublevel'] = 0;
				 	unset($posted_data_chaperone);
				 }


				 $posted_data['user_opts'] = (isset($posted_data['user_opts']) && count($posted_data['user_opts']) > 0 ? json_encode($posted_data['user_opts']):'');			 


				 if(count($posted_data)==0)
				 {
				   throw new Exception("Error : Empty input fields. ");
				 }


					 
	 			if(  $formtype  == 'new' )
				{

				    $return = $this->user->add(array_merge($posted_data , $this->misc_data));

				    if(is_array($return) && array_key_exists('error', $return)) throw new Exception($return['error']);	

				   	$this->area->insert_chaperone_by_area($posted_data_chaperone , $return);

			  	    $this->session->set_flashdata('fmesg', 'Successfully added a user');	
						
				    redirect('maintenance/users');	
		
				}

				if($formtype  =='update')
				{

				 $posted_data['user_updated'] = date('Y-m-d h:i:s');

	  		     $return = $this->user->update($post['user'] , array_merge( $posted_data,$this->misc_data ));
			      
				 if(is_array($return) && array_key_exists('error', $return)) throw new Exception($return['error']);	



				 $this->area->insert_chaperone_by_area($posted_data_chaperone , $return);

				 $this->session->set_flashdata('fmesg', 'Successfully updated a user');				    

				 redirect('maintenance/user_form/?user='.$post['user']);
				  
			    }

				

	   		break;
		   	default:
		   	 redirect('maintenance/users');
		   	break;
		   }
		} 
		catch (Exception $error)
		{
		   $this->session->set_flashdata('error',$error->getMessage());			

		   if($formtype=='new')
		   {
		   	redirect('maintenance/user_form');		
		   }
		   if($formtype=='update')
    	   {
    	   	redirect('maintenance/user_form/?user='.$post['patient']);
    	   }
    	   if($formtype=='delete')
    	   {
            redirect('maintenance/users');	
    	   }
		}
	}

	public function users()
	{
	    $data 	 = array();

	    $pconfig = array();

	    $this->load->model('Usermodel','user');

		$parameter = $this->input->get();		

  		$this->view_data['sidebar']['sidebar_active'] = 'users';

  		$pconfig["base_url"] = base_url() . "maintenance/users";        

		$pconfig["per_page"] = 15;

		$pconfig['query_string_segment'] ='page';    

		$return = $this->user->get_result_pagination( array('search'=>@$parameter['search'],'where'=>array('deleted'=>0 ),'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 'limit'=>$pconfig["per_page"])));	

		$pconfig["total_rows"] = $return['total_rows'];	

		$pconfig["num_links"] = floor($pconfig["total_rows"] / $pconfig["per_page"]);	     

		$data['links'] = $this->paginate($pconfig);	



		$data['record'] = $return['result'];
		

		$this->view_data['view_file'] = 'maintenance/user_list';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_users.js"></script>';		
		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);	
	}



	public function user_form()
	{

        $data 	 = array();		

        $parameter = $this->input->get();

        $this->load->model('Usermodel','user');
        $this->load->model('Areamodel','area');

        try {           

			$this->view_data['view_file'] = 'maintenance/user_form';
			
			if(isset($parameter['user']))
			{
			 $data['user_id'] = $parameter['user'];			
			 $data['user'] = $this->user->get_user_row(array('user_id'=>$parameter['user']));				 
			 $data['chaperone_area_id'] = $this->area->get_area_id_chaperone(array('user_id'=>$parameter['user']));				 
			}						
			$data['area'] = $this->area->get_result(array('area_status'=>1));		


        } catch (Exception $e) {
    	 $this->session->set_flashdata('error',$error->getMessage());			
        }

	 	$this->view_data['sidebar']['sidebar_active'] = 'users';
		$this->view_data['view_file'] = 'maintenance/user_form';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_users.js"></script>';		
		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);	
	}

	/********************END OF USERS*********************/





	/*******************PRACTICES*********************/
	public function practices()
	{


		$this->load->model('Practicesmodel', 'practices');


		$parameter = $this->input->get();		


	  	
		$pconfig["base_url"] = base_url() . "maintenance/practices";        
		$pconfig["per_page"] = 15;
		$pconfig['query_string_segment'] ='page';    	                

		$where= array('practice_status'=>1);

		$return = $this->practices->get_result_pagination( 
															array(
																'search'=>isset($parameter['search']) ? $parameter['search']:'',
																 'where'=>$where,'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 ,
				 												'limit'=>$pconfig["per_page"])
															)	
														);


		$pconfig["total_rows"] = $return['total_rows'];	

	/*	$pconfig["num_links"] = floor($pconfig["total_rows"] / $pconfig["per_page"]);	 */    

		$data['links'] = $this->paginate($pconfig);	


		$data['record'] = $return['result'];						

		$data['practices'] = (isset($parameter['practice']) ? $this->practices->get_result(array('where'=>array('id'=>$parameter['practice']))) :array());

		$this->view_data['sidebar']['sidebar_active'] = 'practices';
		$this->view_data['view_file'] = 'maintenance/practices_list';
		$this->view_data['data'] = $data;
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_practices.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js">';
		$this->view_data['js_file'] .= '</script><script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		
		$this->load->view('index', $this->view_data);
	}	

	public function practices_actions($formtype='new')
	{
			$this->load->model('Practicesmodel', 'practices');

			$post = $this->input->post();

			$get = $this->input->get();

			$posted_data = array();

			$formtype  = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] );
			


		try{


				


			switch($formtype){
				case 'new':
			   	case 'update':		


			    $practice_name = preg_replace('/\s+/', ' ', $post['practice_name']);
				$email  = preg_replace('/\s+/', ' ', $post['email']);	


				$posted_data['practice_name'] = trim(strtoupper($practice_name));
				$posted_data['email'] = trim($email);


				if($posted_data['practice_name']=='' &&  $posted_data['email']=='') 
							   throw new Exception("Error : Empty input fields. ");



				if($formtype  == 'new' )
				{

				 $return_array = $this->practices->add(array_merge($this->misc_data,$posted_data));

				 if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);				  	

				 $this->session->set_flashdata('fmesg', 'Successfully added a Practice');					

				 redirect('maintenance/practices'); 
				 	
				}

				if($formtype  =='update')
				{

				 $practices_id   =  $get['practice']; 	

				 $return_array = $this->practices->update($practices_id,array_merge($this->misc_data,$posted_data));


				 if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);			

				 $this->session->set_flashdata('fmesg', 'Successfully updated a Practice');				    

				 redirect('maintenance/practices'); 
				  
			    }
		   		break;
			   	case 'delete':
					$posted_data['practice_status']=1;

					$practices_id   = $get['practice']; 	


					$return_array = $this->practices->update($practices_id,array_merge($this->misc_data,$posted_data));

 					$this->session->set_flashdata('fmesg', 'Successfully deleted a Practice');					 

 					redirect('maintenance/practices');	


			   	break;
			   	case "search":

			   	default:
			   	 	 redirect('maintenance/practices');	  			       
			   	break;

			 }	
		}	
		catch( Exception $error)
		{			
		  $this->session->set_flashdata('error',$error->getMessage());

		}
	}

	/*******************END OF PRACTICES*********************/


	/*********************************************************/

	public function public_holidays()
	{
		$this->load->model('Holidaymodel' , 'holiday');
		$this->load->model('Areamodel' , 'area');
		$this->load->model('Commonmodel', 'common');	

		$data = array();

		$where = array();

		$parameter = $this->input->get();

		$pconfig["base_url"] = base_url() . "maintenance/public_holidays";        
		$pconfig["per_page"] = 15;
		$pconfig['query_string_segment'] ='page';    	                
	

    	$return = $this->holiday->get_result_pagination( 
												    		array(
												    			  'search'=>isset($parameter['search']) ? $parameter['search']:'',
												    			  'where'=>$where,'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 , 
												    			  'limit'=>$pconfig["per_page"])
												    			 )
    											);


		$pconfig["total_rows"] = $return['total_rows'];	


     

		$data['links'] = $this->paginate($pconfig);	

		$data['holiday'] = (isset($parameter['holiday']) ? $this->holiday->get_row(array('public_holiday.holiday_id'=>$parameter['holiday'])) :array());		

		$data['holiday_nsw'] = $this->common->get_all_holiday_api(array('yr'=>date('Y'),'region'=>'NSW'));
		$data['holiday_nsw'] = @$data['holiday_nsw']->result;

		$data['record'] = $return['result'];		
		$data['area'] = $this->area->get_array(array('area_status'=>1));

		$data['misc_data'] = $this->misc_data;

		$data['holiday'] = (isset($parameter['holiday']) ? $this->holiday->get_row(array('public_holiday.holiday_id'=>$parameter['holiday'])) :array());		

		$this->view_data['sidebar']['sidebar_active'] = 'public_holidays';
		$this->view_data['view_file'] = 'maintenance/public_holiday_list';

		$this->view_data['data'] = $data;
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';
		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_holiday.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/plugins/boostrap-dropdown-with-checkboxes.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>'.PHP_EOL;
		
		$this->load->view('index', $this->view_data);	
	}

public function public_holiday_actions()
{
			$this->load->model('Holidaymodel' , 'holiday');

			$post = $_POST;


			$get = $this->input->get();

			$formtype  = (isset($post['formtype']) ? $post['formtype']  : $get['formtype'] );
			



			try{

				switch($formtype){
					case 'new':
				   	case 'update':										

					if($post['holiday_name']=='' || $post['effective_date']=='')
					{
				    	throw new Exception("Error : Please input required fields. ");										    	
					}
					
						$posted_data['holiday_name'] = trim(strtoupper(preg_replace('/\s+/', ' ', $post['holiday_name'])));

						$ex_date = explode('/', $post['effective_date']);

						//$posted_data['effective_date'] = trim(date('Y-m-d',strtotime($post['effective_date'])));
						$posted_data['effective_date'] = $ex_date[2].'-'.$ex_date[1].'-'.$ex_date[0];

						if(isset($post['area']) &&  count($post['area']) > 0)
						{
							foreach ($post['area'] as $area) {

							$posted_data['area_id'][] = $area;				
							}
						}
						

						if($formtype  == 'new' )
						{
						  $return_value = $this->holiday->add(array_merge($this->misc_data,$posted_data));

						  if(is_array($return_value) && array_key_exists('error', $return_value)) throw new Exception($return_value['error']);	

	 					  $this->session->set_flashdata('fmesg', 'Successfully added a Holiday');		

						}
						
						if($formtype  =='update')
						{
						  $holiday_id   = (isset($post['holiday']) ? $post['holiday']  : $get['holiday'] ); 	

						
					  	  $return_value = $this->holiday->update($holiday_id,array_merge($this->misc_data,$posted_data));

						  if(is_array($return_value) && array_key_exists('error', $return_value)) throw new Exception($return_value['error']);	

						  $this->session->set_flashdata('fmesg', 'Successfully updated a Holiday');					
					
					     
					    }

			   		break;
				   	case 'delete':

			   			  $holiday_id   = (isset($post['holiday']) ? $post['holiday']  : $get['holiday'] ); 

						$return_array = $this->holiday->remove_holiday(array_merge(array('holiday_id'=>$holiday_id),$this->misc_data));

				        if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

 						$this->session->set_flashdata('fmesg', 'Successfully deleted a Holiday');					 

				   	break;
				 }
				redirect('maintenance/public_holidays');		
			}	
			catch( Exception $error)
			{			
			  $this->session->set_flashdata('error',$error->getMessage());
			 redirect('maintenance/public_holidays'); 	

			}
}
	public function holiday_form()
	{
		$data = array();
		$this->view_data['sidebar']['sidebar_active'] = 'public_holiday';
		$this->view_data['view_file'] = 'maintenance/public_holiday_list';
		$this->view_data['data'] = $data;
		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_holiday.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		
		$this->load->view('index', $this->view_data);	
	}

	public function schedules(){

		$this->load->model('Holidaymodel' , 'holiday');

		$data['schedules'] = $this->holiday->get_schedules();
		
		$this->view_data['sidebar']['sidebar_active'] = 'schedules';
		$this->view_data['view_file'] = 'maintenance/schedules';
		$this->view_data['data'] = $data;
		//$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';		

		//$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_holiday.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/moment.min.js"></script>';
		//$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		
		$this->load->view('index', $this->view_data);
	}


	public function cma_setting( $environment = '' )
	{

		$this->load->model('Sitesettingsmodel','settings');
		$data = array();

		if($_POST)
		{
			$post = array();

			foreach ($_POST as $key => $value) {
				$post[strtolower($key)] = $value;
			}	
			$json = json_encode($post);
			
			$this->settings->update_by_name(strtoupper($post['set_name']), array('set_name'=> $post['set_name'],'set_value'=>$json));
			$this->session->set_flashdata('fmesg', 'Successfully Updated the CMA Settings');				    

		   redirect('maintenance/cma_setting'); 
		}
		$row_live = $this->settings->get_row(array("set_name"=>'CMA_LIVE'));
		$row_test = $this->settings->get_row(array("set_name"=>'CMA_TEST'));
		$data['cma_live'] = isset($row_live->set_value) ? json_decode($row_live->set_value) : '';
		$data['cma_test'] =  isset($row_test->set_value) ?   json_decode($row_test->set_value):'';
		$this->view_data['sidebar']['sidebar_active'] = 'cma_setting';
		$this->view_data['view_file'] = 'maintenance/cma_settings.php';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);		
	}

	public function override_current_time(){

		if( in_array($_SERVER['SERVER_NAME'], array('127.0.0.1', 'localhost', 'bookingpro-curehomedoctor-test.welldone.net.au')) ){

			$this->load->model('Sitesettingsmodel','settings');
			$data = array();

			if($_POST)
			{
				$post = $this->input->post(); 
				
				$this->settings->update_by_name('TESTING_OVERRIDE_CURRENT_TIME', array('set_name'=>'TESTING_OVERRIDE_CURRENT_TIME','set_value'=>trim($post['dt_val'])));
				$this->session->set_flashdata('fmesg', 'Successfully Updated Overide Current Time');				    

			   redirect('maintenance/override_current_time'); 
			}
			
			$override_current_time = $this->settings->get_row(array("set_name"=>'TESTING_OVERRIDE_CURRENT_TIME'));  
			
			//print_r($override_current_time);
			//echo $override_current_time->set_value;
			
			$data['oct'] =  @$override_current_time->set_value;

			$this->view_data['sidebar']['sidebar_active'] = 'override_current_time';
			$this->view_data['view_file'] = 'maintenance/override_current_time';
			$this->view_data['data'] = $data;		
			$this->load->view('index', $this->view_data);
		}
	}


	public function contacts()
	{
		$data = array();

		$this->load->model('Contactmodel','contact');


		$parameter = $this->input->get();		


	  	
		$pconfig["base_url"] = base_url() . "maintenance/contact";        
		$pconfig["per_page"] = 15;
		$pconfig['query_string_segment'] ='page';    	                

		$where= array('contact_status'=>1);

		$return = $this->contact->get_result_pagination( 
															array(
																'search'=>isset($parameter['search']) ? $parameter['search']:'',
																 'where'=>$where,'limit'=>array('offset'=> isset($parameter['page']) ? $parameter['page']:0 ,
				 												'limit'=>$pconfig["per_page"])
															)	
														);



		$pconfig["total_rows"] = $return['total_rows'];	

	/*	$pconfig["num_links"] = floor($pconfig["total_rows"] / $pconfig["per_page"]);	 */    

		$data['links'] = $this->paginate($pconfig);	


		$data['record'] = $return['result'];						


		$this->view_data['sidebar']['sidebar_active'] = 'contact';
		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/tinymce/tinymce.min.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_contacts.js"></script>';
		$this->view_data['view_file'] = 'maintenance/contact_list.php';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);		
	}

	public function contact_form()
	{
		$data = array();

		$this->load->model('Contactmodel','contact');

		$post = $this->input->post();
		$get = $this->input->get();

		$formtype  = $this->input->post('formtype');	

		$contact_id   =  $this->input->post('contact');	 	

		if($_POST)
		{
			$set = array();

	

			$set['contact_title'] = $this->input->post('contact_title');
			$set['contact_status'] = $this->input->post('contact_status');
			$set['content_html']  =  addslashes(preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['content_html']));
			//$set['content_html']  =  $this->input->post('content_html');



			switch (  $formtype ) {
				case 'new':

						$return_array = $this->contact->add(array_merge($this->misc_data,$set));

						if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

						$this->session->set_flashdata('fmesg', 'Successfully added a Contact');					

						
					break;

				case 'update':

				 	 	$set['date_updated'] = date('Y-m-d h:i:s');

				       $return_array = $this->contact->update($contact_id,array_merge($this->misc_data,$set));

				      if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

					  $this->session->set_flashdata('fmesg', 'Successfully updated a Holiday');		
							
					break;

				
			}

			 redirect('maintenance/contacts');	
			
		}


		
		$data['contact'] = (isset($get['contact']) ? $this->contact->get_row(array('id'=>$get['contact'])):array());

		$this->view_data['sidebar']['sidebar_active'] = 'contact';
		
		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/tinymce/tinymce.min.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/plugins/tinymce.filemanager/plugin.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_contacts.js"></script>';
		
		$this->view_data['view_file'] = 'maintenance/contacts.php';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);		
	}
	public function notices()
	{
		$data = array();



		$this->load->model('Noticemodel','notice');


		$parameter = $this->input->get();		

		$data['general'] = $this->notice->get_result( array('where'=>array('notice_group'=>1))); 
		$data['roster']  = $this->notice->get_result( array('where'=>array('notice_group'=>2)));


		$this->view_data['sidebar']['sidebar_active'] = 'notice';
		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/tinymce/tinymce.min.js"></script>';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_notices.js"></script>';
		$this->view_data['view_file'] = 'maintenance/notice_list.php';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);
	}
	public function notice_form()
	{
		$data = array();

		  
  		 $this->load->model('NoticeModel','notice');

		$post = $this->input->post();
		$get = $this->input->get();

		$formtype  = $this->input->post('formtype');	

		$notice_id   =  $this->input->post('notice');	 	

		if($_POST)
		{
			$set = array();
	  	 	$set['content_html'] = $this->input->post('content');
	  	 	$set['expire_date'] = $this->input->post('expire');	  	 	
	  	 	$set['notice_status'] = $this->input->post('status');
	  	 	$set['notice_group'] = $this->input->post('group');


			switch (  $formtype ) {
				case 'new':

						$return_array = $this->notice->add(array_merge($this->misc_data,$set));

						if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

						$this->session->set_flashdata('fmesg', 'Successfully added a Notice');					

						
					break;

				case 'update':

						$set['date_updated'] = date('Y-m-d h:i:s');

				       $return_array = $this->notice->update($notice_id,array_merge($this->misc_data,$set));

				      if(is_array($return_array) && array_key_exists('error', $return_array)) throw new Exception($return_array['error']);	

					  $this->session->set_flashdata('fmesg', 'Successfully updated a Notice');		
							
					break;

				
			}

			 redirect('maintenance/notices');	
			
		}


		
		$data['notice'] = (isset($get['notice']) ? $this->notice->get_row(array('id'=>$get['notice'])):array());

		$this->view_data['sidebar']['sidebar_active'] = 'notice';
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_notices.js"></script>';				
		$this->view_data['view_file'] = 'maintenance/notice.php';
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);		
	}


	public function wyhaulist(){

		$this->load->model('Dropdownmodel');

		$data['wyhaulist'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'WYHAU')));

		$parameter = $this->input->get();

		if(isset($parameter['id']))
		{			
			$this->view_data['wyhau'] = $this->Dropdownmodel->get_row(array('dropdown_id'=>$parameter['id']));
		}
		
		$this->view_data['sidebar']['sidebar_active'] = 'wyhau';
		$this->view_data['view_file'] = 'maintenance/wyhau_list';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/wyhau.js"></script>';		
		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);		
	}

	public function wyhau($formtype='new'){

		$this->load->model('Dropdownmodel');

		if($_POST){

			$post = $this->input->post();
			
			$dropdown_value = preg_replace('/\s+/', ' ', $post['dropdown_value']);

			$set['dropdown_group'] = 'WYHAU';
			$set['dropdown_label'] = $dropdown_value;
			$set['dropdown_value'] = $dropdown_value;

			if( $post['formtype'] == 'new' ){

				$dropdown_value = preg_replace('/\s+/', ' ', $post['dropdown_value']);

				$set['dropdown_group'] = 'WYHAU';
				$set['dropdown_label'] = $dropdown_value;
				$set['dropdown_value'] = $dropdown_value;
				
				if( $this->Dropdownmodel->add($set) ){
					$this->session->set_flashdata('fmesg', 'Successfully added 1 item');
					redirect('maintenance/wyhaulist');
				}
			}
			if( $post['formtype'] == 'update' ){

				if( $this->Dropdownmodel->update($post['wyhau'],array_merge($this->misc_data,$set)) ){
					$this->session->set_flashdata('fmesg', 'Successfully updated an item');
					redirect('maintenance/wyhaulist');
				}
			}
			if( $post['formtype'] == 'sort' ){

				$set['dropdown_group'] = 'WYHAU';
				$set['sort_data'] = $post['sort_data'];
				
				if( $this->Dropdownmodel->sort_update($set) ){

				}
			}
		}

		$params = $this->input->get();


		if(isset($params['active']))
		{
		    	if( $this->Dropdownmodel->update($params['id'],array_merge($this->misc_data,array('dropdown_status'=>$params['active'])))){
					$this->session->set_flashdata('fmesg', 'Successfully updated an item');
					
				}

				redirect('maintenance/wyhaulist');
		}


	}

	public function consultnotesdropdown(){
		$this->load->model('Dropdownmodel');

		$data['items']['diagnosis'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'diagnosis', 'dropdown_status'=>'1')));
		$data['items']['diagnosis_reference_code'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'diagnosis_reference_code', 'dropdown_status'=>'1')));
		$data['items']['plan'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'plan', 'dropdown_status'=>'1')));
		$data['items']['cunsult_medication'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'cunsult_medication', 'dropdown_status'=>'1')));
		$data['items']['medication_prescribed'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'medication_prescribed', 'dropdown_status'=>'1')));
		$data['items']['followup_gp'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'followup_gp', 'dropdown_status'=>'1')));
		$data['items']['past_medical_history'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'past_medical_history', 'dropdown_status'=>'1')));
		$data['items']['current_medication'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'current_medication', 'dropdown_status'=>'1')));
		$data['items']['immunisations'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'immunisations', 'dropdown_status'=>'1')));
		$data['items']['allergies'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'allergies', 'dropdown_status'=>'1')));
		$data['items']['social_history'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'social_history', 'dropdown_status'=>'1')));
		$data['items']['life_style_risk_factors'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'life_style_risk_factors', 'dropdown_status'=>'1')));
		$data['items']['observation'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'observation', 'dropdown_status'=>'1')));
	 		
		$this->view_data['sidebar']['sidebar_active'] = 'consult-note-dropdown';
		$this->view_data['view_file'] = 'maintenance/consult_notes_dropdown';		
		$this->view_data['css_file'] = '<link href="assets/css/card-styles.css" rel="stylesheet">';		
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintain_consult_note_dropdown.js"></script>';		
		$this->view_data['data'] = $data;
		$this->load->view('index', $this->view_data);	

	}

	public function smsglobal(){
		//$this->load->library('Smsglobal');
		$data = '';
	
		try{

			if( $_POST ){
 
				if( !isset($_POST['sms-to']) AND @$_POST['sms-to'] == '' ) throw new Exception("Error Processing Request: TO", 1);
				if( !isset($_POST['sms-msg']) AND @$_POST['sms-msg'] == '' ) throw new Exception("Error Processing Request: Message", 1);
				
			   	$to = trim($_POST['sms-to']);
			   	$sms_body = $_POST['sms-msg'];

				$mobile_no = str_replace(' ', '', $to);
				$mobile_no = str_replace(';', ',', $mobile_no);
				$mobile_no = explode(',', $mobile_no);

				$logs = '';
				foreach ($mobile_no as $mobile) {

					//$smsglobal = $this->smsglobal->sendSms($mobile, $sms_body);
					$send_sms = $this->Commonmodel->sendSms($mobile, $sms_body);

					$audit['tran_id'] 		= 0;
					$audit['audit_to'] 		= $mobile;
					$audit['audit_type'] 	= 'sms';
					$audit['message'] 		= $sms_body;	
					$audit['more_info'] 	= 'test via maintenance by '.$this->agent_name;						
					$audit['return_message']= ($send_sms == 'message_sent')?'SMS - SENT':$send_sms;
					$this->Commonmodel->insert_audit_trail($audit);
	 
	 				if( $send_sms != 'message_sent' ){
						$logs .= $mobile.' - Error <br />';
					}else{
						$logs .= $mobile.' - SENT <br />';
					}
					
					$this->session->set_flashdata('fmesg',$logs);

					redirect('maintenance/smsglobal');
				}

			}

		}catch( Exception $error){

		   $this->session->set_flashdata('error',$error->getMessage());
		   redirect('maintenance/smsglobal');
		}				

		$this->view_data['sidebar']['sidebar_active'] = '';
		$this->view_data['view_file'] = 'maintenance/sms_global_form';	

 
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);

	}


	public function consult_notes(){


		$data = '';

		$this->load->model('Usermodel' , 'user');

		$this->load->model('Commonmodel', 'common');


		$post = $this->input->post();


	    $dr_id    = isset($post['doctor']) ? $post['doctor'] : $this->user_id;


		if($_POST){

	
					try
					{

							$row = 0;
							$result = array();


							$file_path =  $_FILES['consult_note']['tmp_name'];     

							$handle = fopen($file_path, "r");

							if(!$handle) throw new Exception('Cannot Open File');



							$delimiter = ",";
							$initial_line = 0;
							$header_exist = array();

							$insert = array();


							$ctr = 0;

	

						    if(isset($dr_id) && !empty($dr_id)){


									$this->db->delete('consult_note_upload', array('doctor_id' => $dr_id));  

									while (($data = fgetcsv( $handle, 0, $delimiter)) !== FALSE) 
									{   

										
										if($row > 0 ){

											$insert['prognosis'] 		  = @$data[0];
											$insert['call_center_triage'] = @$data[1];
											$insert['present_complain']	  = @$data[2];	
											$insert['past_med_history']   = @$data[3];
											$insert['current_medication'] = @$data[4]; 
											$insert['immunisations']	  = @$data[5];
											$insert['allergies']		  = @$data[6];	
											$insert['family_history']     = @$data[7];
											$insert['observations']       = @$data[8];
											$insert['diagnosis']		  = @$data[9];	
											$insert['plan']				  = @$data[10];	
											$insert['medication_administired']= @$data[11];
											$insert['mediated_perscribed']= @$data[12];
											$insert['follow_up_with_gp']  = @$data[13];
											$insert['doctor_id']	  	  = $dr_id;


											$this->db->insert('consult_note_upload',$insert);

										}

									 	$row++;


									} 
						    	
						    }



					  $this->session->set_flashdata('fmesg', 'Csv Data Imported Succesfully');
					  
					}
					catch( Exception $error)
					{			
					  $this->session->set_flashdata('error',$error->getMessage());

					  
					}	



		}

		$this->db->select('*', FALSE);

		$this->db->where('doctor_id',$dr_id);

		$query = $this->db->get('consult_note_upload');

		$result_notes = $query->result();


		$this->view_data['sidebar']['sidebar_active'] = 'consult-notes';
		$this->view_data['view_file'] = 'maintenance/consult_note';	

			
		$data['doctors'] 	  =  $this->user->get_doctors();
		$data['consult_note'] =  $result_notes;


		$this->view_data['data'] = $data;		
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_note.js"></script>';		
		$this->load->view('index', $this->view_data);

	}

	public function training_videos(){

		$data = '';
		$this->load->helper('directory');
		$post = $this->input->post();
		$data = '';

		$map = directory_map('files/training_videos');

		$map = array_flip($map);

		$upload_path = FILES_DIR.'/training_videos/';

		unset($map['index.html']);

		if($_POST){

			ini_set('upload_max_filesize', '300M');
			ini_set('post_max_size', '300M');                               
			ini_set('max_input_time', 4000);                                
			ini_set('max_execution_time', 4000);

				$this->load->helper('url', 'file', 'form');

			
				    	try {



							$config['upload_path'] = $upload_path;					
							$config['allowed_types'] = 'wmv|mp4|avi|mov';
							$config['max_size'] = '300000';					
							$config['max_filename'] = '255';
							$config['encrypt_name'] = FALSE;
							$config['overwrite'] = TRUE;

						
							$video_data = array();
						

				        	if (!$_FILES)  throw new Exception('Select a video file.');
				            	
			                $this->load->library('upload', $config);

			                if (!$this->upload->do_upload('t_video')){	                
			            		throw new Exception($this->upload->display_errors());	                	
			                }


						  	$this->session->set_flashdata('fmesg',"Training video successfully uploaded!.");

						  	redirect('maintenance/training_videos');
					    		
				    	} catch (Exception $e) {

						  $this->session->set_flashdata('error',$e->getMessage());

				    		
				    	}
	
		}

		$get = $this->input->get();

		if(isset($get['vid']) && isset($get['del'])){
			$link_vid = urldecode($get['vid']);
			unlink($upload_path.$link_vid);
		 	redirect('maintenance/training_videos');
		}

		$data['dir'] = array_flip($map);

		$this->view_data['sidebar']['sidebar_active'] = 'training_vid';
		$this->view_data['view_file'] = 'maintenance/training-videos';	
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/page_js/maintain_training_video.js"></script>';		
		$this->view_data['data'] = $data;		
		$this->load->view('index', $this->view_data);

	}

	

}
