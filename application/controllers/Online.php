<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Online extends CI_Controller {

	public $view_data = array('container'=>'container-fluid', 'view_file'=>'default-content', 'js_file'=>'', 'data'=>'');

	public $tel_no = '135566';

	public function __construct()
	{
		parent::__construct();		

		$this->load->model('Transactionmodel', 'transaction');
		$this->load->model('Commonmodel');
 		$this->load->library('Mailer', 'mailer');
	}
	
	public function index(){
		$this->load->view('online/home');
	}

	public function call(){

		$set_tran['tran_type'] = 'Online Form';
		$set_tran['tran_desc'] = 'CALL to HCD';

		$set_tran['caller_firstname'] 	= '';
		$set_tran['caller_lastname'] 	= ''; 
		$set_tran['caller_phone'] 		= ''; 
		$set_tran['tran_details'] 		= ''; 

		$set_tran['user_id'] 			= '0';
		$set_tran['agent_name'] 		= 'Online';

		$tran_id = $this->transaction->add($set_tran);

		/*if( $tran_id > 0 ){
			redirect('tel:'.$this->tel_no);
		}else{
			$this->session->set_flashdata('fmesg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Sorry system encouter problem while processing your request.</div>');
			redirect(base_url().'online');	
		}*/
	}

	public function book(){ 

		$this->view_data['header_active'] = 'book'; 
		$this->view_data['view_file'] = 'online/page-booking';

		$this->load->view('online/page', $this->view_data);

	}
	
	public function eta(){ 

		if($_POST){

			$post = $this->input->post();

			$set_tran['tran_type'] = 'Online Form';
			$set_tran['tran_desc'] = 'ETA Required';

			$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
			$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
			$set_tran['caller_phone'] 		= $post['Phone']; 
			$set_tran['tran_details'] 		= 'DOB: '.addslashes($post['DOB']); 

			$set_tran['user_id'] 			= '0';
			$set_tran['agent_name'] 		= 'Online';

			/*$tran_id = $this->transaction->add($set_tran);

			if( $tran_id > 0 ){

				$to = $this->Commonmodel->get_admin_email('HCD_ADMIN_EMAIL');
				

				$body .= "Name: ".$set_tran['caller_firstname'].' '.$set_tran['caller_lastname'].PHP_EOL;
				$body .= "Phone: ".$set_tran['caller_phone'].PHP_EOL;
				$body .= $set_tran['tran_details'];
				$body .= PHP_EOL;
				$body .= PHP_EOL;
				$body .= PHP_EOL;
				$body .= "sent via housecalldoctor.welldone.net.au/online/eta";

				$subject = "Online Form - ETA Required";


				//add welldone email here
				$wd_email = $this->Commonmodel->get_admin_email('WD_EMAIL');
				
				$this->mailer->send_to = $to.((trim($wd_email) != '')?','.$wd_email:'');
				$this->mailer->Subject = $subject;
				$this->mailer->Body = stripslashes(nl2br($body));

				$mail_return = $this->mailer->sendMail();

				//audit log
				$audit['tran_id'] 		= $tran_id;
				$audit['audit_to'] 		= $to;
				$audit['audit_type'] 	= 'email';
				$audit['message'] 		= addslashes($body);
				$audit['more_info'] 	= $subject;
				$audit['return_message']= $mail_return;
				$this->Commonmodel->insert_audit_trail($audit);


				$this->session->set_flashdata('fmesg', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you, An ETA request will be sent to the doctor</div>');				
				redirect(base_url().'online');			
			}else{
				$this->session->set_flashdata('fmesg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Sorry system encouter problem while processing your request.</div>');
				redirect(base_url().'online/eta');			
			}
*/

				$this->session->set_flashdata('fmesg', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you, An ETA request will be sent to the doctor</div>');				
				redirect(base_url().'online');	

		}


		$this->view_data['header_active'] = 'eta'; 
		$this->view_data['view_file'] = 'online/page-eta'; 

		$this->load->view('online/page', $this->view_data);

	}

	public function enquiry(){ 

		if($_POST){

			$post = $this->input->post();

			$set_tran['tran_type'] = 'Online Form';
			$set_tran['tran_desc'] = 'Enquiry';

			$set_tran['caller_firstname'] 	= addslashes($post['First_Name']);
			$set_tran['caller_lastname'] 	= addslashes($post['Last_Name']); 
			$set_tran['caller_phone'] 		= $post['Phone']; 
			$set_tran['tran_details'] 		= 'DOB: '.addslashes($post['DOB']); 

			$set_tran['user_id'] 			= '0';
			$set_tran['agent_name'] 		= 'Online';

			/*$tran_id = $this->transaction->add($set_tran);

			if( $tran_id > 0 ){ 

				$to = $this->Commonmodel->get_admin_email('HCD_ADMIN_EMAIL');

				$body .= "Name: ".$set_tran['caller_firstname'].' '.$set_tran['caller_lastname'].PHP_EOL;
				$body .= "Phone: ".$set_tran['caller_phone'].PHP_EOL;
				$body .= $set_tran['tran_details'];
				$body .= PHP_EOL;
				$body .= PHP_EOL;
				$body .= PHP_EOL;
				$body .= "sent via housecalldoctor.welldone.net.au/online/enquiry";

				$subject = "Online Form - Required";

				//add welldone email here
				$this->mailer->send_to = $to;
				$this->mailer->Subject = $subject;
				$this->mailer->Body = stripslashes(nl2br($body));

				$mail_return = $this->mailer->sendMail();

				//audit log
				$audit['tran_id'] 		= $tran_id;
				$audit['audit_to'] 		= $to;
				$audit['audit_type'] 	= 'email';
				$audit['message'] 		= addslashes($body);
				$audit['more_info'] 	= $subject;
				$audit['return_message']= $mail_return;
				$this->Commonmodel->insert_audit_trail($audit);

				$this->session->set_flashdata('fmesg', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you, your enquiry was successfully submitted, a staff from housecalldoctor will contact you soon.</div>');
				redirect(base_url().'online');			
			}else{
				$this->session->set_flashdata('fmesg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Sorry, system encouter problem while processing your request.</div>');
				redirect(base_url().'online/enquiry');			
			}*/

			$this->session->set_flashdata('fmesg', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you, your enquiry was successfully submitted, a staff from housecalldoctor will contact you soon.</div>');
			redirect(base_url().'online');

		}



		$this->view_data['header_active'] = 'enquiry'; 
		$this->view_data['view_file'] = 'online/page-enquiry'; 

		$this->load->view('online/page', $this->view_data);

	}

	public function ajax_suburbs(){



	}
}
