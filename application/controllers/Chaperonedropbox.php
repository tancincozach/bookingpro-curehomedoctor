<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chaperonedropbox extends CI_Controller {


	public function __construct()
	{
		parent::__construct();		


		$this->view_data['container'] = 'container-fluid';		
	}



		public function dropbox($stage , $record = array()){

 		try {
 			
 				




				$this->load->helper('file');
				$this->load->library("pdfhelper");		


				$this->load->model('Appointmentmodel', 'appt'); 		
				$this->load->model('Areamodel', 'area'); 		

				$params['key'] = 'rrkx55ui76m0y6h';
				$params['secret'] = '2vp2xz6rg0erbm6';







				if($stage==1)
				{

					$this->session->set_userdata('record', $record);

					$this->load->library('dropbox', $params);
			
					$data = $this->dropbox->get_request_token(site_url("chaperonedropbox/dropbox/2"));
	
					$this->session->set_userdata('token_secret', $data['token_secret']);

					redirect($data['redirect']);
					

				}

				if($stage==2)
				{	
					$this->load->library('dropbox', $params);

					$oauth = $this->dropbox->get_access_token($this->session->userdata('token_secret'));

					$this->session->set_userdata('oauth_token', $oauth['oauth_token']);

					$this->session->set_userdata('oauth_token_secret', $oauth['oauth_token_secret']);

					redirect('chaperonedropbox/dropbox/3');
				}



				if($stage==3)
				{
	 	
					$datef = date('dMy');

					$filename = 'MedicalForm.pdf';

					$html = $this->load->view('chaperone/medicare-pdf-tpl-test', $this->session->userdata('record'), true);

					$pdfoutput = $this->pdfhelper->renderPDF($html, $filename, false);			 

					if ( ! write_file('./files/medicare/'.$filename, $pdfoutput)) {

					       throw new Exception("Error: Unable to write pdf file.", 1);

					}else{

						$params['access'] = array('oauth_token'=>urlencode($this->session->userdata('oauth_token')),
											  'oauth_token_secret'=>urlencode($this->session->userdata('oauth_token_secret')));



					    $this->load->library('dropbox', $params);

						$response = $this->dropbox->add('/DropBoxExternalTest', './files/medicare/'.$filename);

					
						if( isset($response->path) AND $response->path != '' ){
							

							unlink('./files/medicare/'.$filename);


						$this->session->set_flashdata('fmesg', 'Medicare Form successfully UPLOADED to dropbox.');
						redirect(base_url().'chaperonedropbox/medicare_form');


						}else{
							throw new Exception("Unable to save on dropbox", 1);
							
						}
					}
				}
	 
		} catch (Exception $e) {
 			echo $e->getMessage();
 		}
 	}



	public function medicare_form(){


			$this->load->helper('file');
			$this->load->library("pdfhelper");					
			$this->load->model('Commonmodel', 'common');	


			$data = array();
		

				
		    if($_POST)
		    {
		    	$post = $this->input->post();

	 			$array_post['unable_to_sign'] = isset($post['unable_to_sign'])?1:0;

	 			if( isset($post['new_sig']) ){
	 				$array_post['sign_code'] 	= addslashes($_REQUEST['hidden_sig']);
	 			}

	 			if( isset($post['unable_to_sign']) ) {
	 				unset($array_post['sign_code']);
	 			}

	 			if( isset($post['savedropbox']) ){
	 				$array_post['save_dropbox'] 	= 1;
	 			}


	 			$array_post['appt_updated'] 	= date('Y-m-d H:i:s');


	 			//TO-DO check date format later
	 			$dob = explode('/', $post['DOB']); //d/m/Y
	 			$array_post['DOB'] 			= @$dob[2].'-'.@$dob[1].'-'.@$dob[0];
	 			
	 			$array_post['medicare_number'] = $post['Medicare_Number_1'].$post['Medicare_Number_2'].$post['Medicare_Number_3'];
	 			$array_post['medicare_ref'] 	= $post['Medicare_Ref'];
	 			$array_post['medicare_expiry'] = $post['Expiry'];
	 			
	 			$array_post['firstname'] 		= strtoupper($post['First_Name']);
	 			$array_post['middleinitial'] 	= strtoupper($post['Middle_Initial']);
	 			$array_post['lastname'] 		= strtoupper($post['Last_Name']);
	 			$array_post['suburb'] 			= strtoupper(addslashes($post['Suburb']));
	 			$array_post['street_addr'] 	= strtoupper(addslashes($post['Street_Address']));

	 			$med_items = '';
	 			$medicare_items_array = $this->common->medicare_form_item_numbers();

	 			if( isset($post['items']) ){
	 				foreach ($post['items'] as $key=>$val) {

	 					if($key == 'other') {
	 						$med_items['other'] = @$post['items_other'];
	 					}else{
	 						$med_items[$key] = @$medicare_items_array[$key];	 						
	 					}

	 				}
	 				 
	 			}

	 			$array_post['medicare_form_items'] = json_encode($med_items);
	 		



				if( isset($post['savedropbox']) )
			    {

				   $this->dropbox(1,$array_post);
			    }

				
		    }
			

			$data['medicare_items'] = $this->common->medicare_form_item_numbers();
			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/medicare-form-test';
			$this->view_data['menu_active'] = 'chaperone';

			$this->view_data['js_file'] = '<script src="assets/plugins/Touch-enabled-Signature-Plugin-with-jQuery-Canvas/jq-signature.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/medicare_form.js"></script>'.PHP_EOL;
			
			$this->view_data['js_file'] .= '<script src="assets/js/mcautocomplete.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/suburb_dropdown.js"></script>'.PHP_EOL;

			$this->load->view('index2', $this->view_data);
	}
 	


}
