<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('memory_limit', '-1');
class Report extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container-fluid';
		$this->load->model('Areamodel', 'areas');
	}

	public function index(){


	}

	function build_range_date(){
		
		$client_time = $this->Commonmodel->get_server_converted_date('Australia/Queensland');
		$client_time = date('d/m/Y',strtotime($client_time));

		$appt_start = (isset($_GET['appt_start']) AND $_GET['appt_start'] != '')?$_GET['appt_start']:$client_time.' - '.$client_time;

		$format_date = explode(' - ', $appt_start);

		$from = explode('/', $format_date[0]);
		$from = $from[2].'-'.$from[1].'-'.$from[0];
		
		$to = explode('/', $format_date[1]);
		$to = $to[2].'-'.$to[1].'-'.$to[0];
		
		return array('appt_start'=>$appt_start,'from'=>$from, 'to'=>$to);
	}

 	function build_where($params){

		$params['where_str'] = '';
		$search = @$params['search'];
		
		if( isset($params['area']) AND count($params['area'])>0 ){ 
			 
			if( count($params['area']) == 1 ){
				$params['where'] = array('appointment.area_id'=>$params['area'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`area_id` IN ('.implode(',',array_values($params['area'])).')';				
			}
		}


		return $params;
	}


	public function acss(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = ''; 

		$range = $this->build_range_date();

		$data['appt_start'] = $range['appt_start'];
		 
		$record = $this->Reportmodel->appointment($range['from'], $range['to']);

		if( isset($_GET['download']) ){

 
			$fields = array(
		/*		'UR No',
				'Title',
				'Surname',
				'FirstName',
				'Address',
				'Suburb',
				'Postcode',
				'DateofBirth',
				'MC No',
				'MC Ref',
				'Pens No',
				'VA No',
				'PhoneH',
				'PhoneW',
				'PhoneM',
				'Sex',
				'State',
				'Medicare Expiry',
				'',
				'',*/
				'Doctor Name',
				'TypeOfService',
				'ServiceTypeCde',
				/*'Provider #',*/
				'ExtServicingDoctor',
				'ExternalPatientId',
				'ExternalInvoice	',
				'PatientFirstName',
				'PatientFamilyName',
				'PatientSecondInitial',
				'PatientDateOfBirth',
				'PatientGender',
				'PatientAddressLine',
				'PatientAddressLocality',
				'PatientAddressPostCode',
				'AcceptedDisabilityInd',
				'AcceptedDisabilityText',
				'PatientMedicareCardNum',
				'VeteranFileNum',
				'PatientReferenceNum',
				'DateOfService',
				'BClmAmt',
				'ItemNum1',
				'ItemNum2',
				'ItemNum3',
				'ItemNum4',
				'ItemNum5',
				'ChargeAmount1',
				'ChargeAmount2',
				'ChargeAmount2',
				'ChargeAmount3',
				'ChargeAmount4',
				'ChargeAmount5',
				'ServiceText',
				'NoOfPatientsSeen1',
				'NoOfPatientsSeen2',
				'NoOfPatientsSeen3',
				'DistanceKms',
				'TreatmentLocationCde'
			);

	/*			echo "<pre>";
				print_r($record);
				echo "<pre/>";
				exit();*/
			$data_row = array();
			foreach($record as $row){

				$inRow = array();

				/*$inRow[] = '';
				$inRow[] = '';
				$inRow[] = @$row->lastname;
				$inRow[] = $row->firstname;
				$inRow[] = stripslashes(@$row->street_addr);
				$inRow[] = stripslashes(@$row->suburb);
				$inRow[] = $row->postcode;
				$inRow[] = (isset($row->dob) && $row->dob != '0000-00-00')?' '.date('d/m/Y', strtotime($row->dob)):'';
				$inRow[] = @$row->medicare_number;
				$inRow[] = @$row->medicare_ref;
				$inRow[] = '';
				$inRow[] = isset($row->dva) && $row->dva!='' ?'Yes':'No';;
				$inRow[] = @$row->phone;
				$inRow[] = '';
				$inRow[] = @$row->mobile_no;
				$inRow[] = ($row->gender=='M')?'Male':'Female';
				$inRow[] = $this->Commonmodel->getState(@$row->postcode);
				$inRow[] = @$row->medicare_expiry;
				$inRow[] = '';
				$inRow[] = '';*/
				$inRow[] = @$row->doctor_name;
				$inRow[] = '';
				$inRow[] = '';
				$inRow[] = @$row->doctor_prov_num;
				$inRow[] = @$row->patient_ext_id;
				$inRow[] = '';
				$inRow[] = @$row->firstname;
				$inRow[] = @$row->lastname;
				$inRow[] = @$row->middleinitial;
				$inRow[] = (isset($row->dob)  && $row->dob != '0000-00-00')?' '.date('d/m/Y', strtotime($row->dob)):'';
				$inRow[] = @$row->gender;
				$inRow[] = @$row->street_addr;
				$inRow[] = @$row->suburb;
				$inRow[] = @$row->postcode;
				$inRow[] = '';
				$inRow[] = '';
			    $inRow[] = @$row->medicare_number;
			    $inRow[] = '';
			    $inRow[] = @$row->medicare_ref;
			    $inRow[] = isset($row->appt_created_tz) && $row->appt_created_tz!='' ? date('d/m/Y', strtotime($row->appt_created_tz)):'';
		        $inRow[] = '';
			    $inRow[] = $row->item_num;

			    if(isset($row->medicare_form_items) && $row->medicare_form_items!=''){
			    	
					$med_items = json_decode($row->medicare_form_items);

					if(!empty($med_items)){

						foreach($med_items as $num=>$desc){
				    		$inRow[] = $num;
				    	}
					}
			    }

			    
				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'ACSS Input Report -'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/acss_input';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);

	}

	public function appointment(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = '';

		$range = $this->build_range_date(); 


	     $params = array();

		$data['appt_start'] = $range['appt_start'];
		 
		if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}

		 $params_query = $this->build_where($params);

		$record = $this->Reportmodel->appointment($range['from'], $range['to'], $params_query);



		if( isset($_GET['download']) ){
 
			$fields = array(
				'Appt ID',
				'Area Name',
				'Multiple Booking',
				'Date Booking Made',
				'Time Booking Made',
				'Consult Start',
				'Consult End',
				'Time in Queue',
				'Staus',
				'Chaperone',
				'Doctor',
				'Car #',
				'First Name',
				'Last Name',
				'Date of Birth',
				'Age',
				'Sex',
				'Landline Phone',
				'Mobile Phone',
				'Email',
				'Address',
				'Suburb',
				'Postcode',
				'Medicare Num',
				'Medicare Ref',
				'Medicare Exp',
				'DVA Gold Card',
				'Private Health Insurance Provider',
				'Private Health Fund Number',
				'Symptoms',
				'Where Heard',
				'Regular Practice',
				'Practice Email',
				'Regular Doctor',
				'Aboriginal / Torres Strait Island',
				'Cultural & Ethnicity Background',
				'Agent'
			);

			$data_row = array();
			foreach($record as $row){

				$inRow = array();


				$inRow[] = $row->ref_number_formatted;
				$inRow[] = $row->area_name;
				$inRow[] = ($row->related_appt_id > 0)?'Yes':'No';
				$inRow[] = date('Y-m-d', strtotime($row->appt_created_tz));
				$inRow[] = date('H:i', strtotime($row->appt_created_tz));			
				$inRow[] = ' '.$row->consult_start_tz;
				$inRow[] = ' '.$row->consult_end_tz;
				$inRow[] = $row->in_queue;
				$inRow[] = $this->booking_statuses[$row->book_status];
				$inRow[] = $row->chaperone_name;
				$inRow[] = $row->doctor_name;
				$inRow[] = $row->car_name;
				$inRow[] = $row->firstname;
				$inRow[] = $row->lastname;
				$inRow[] = ($row->dob != '0000-00-00')?' '.date('d/m/Y', strtotime($row->dob)):'';
				$inRow[] = $row->patient_age;
				$inRow[] = $row->gender;
				$inRow[] = $row->phone;
				$inRow[] = $row->mobile_no;
				$inRow[] = $row->email;
				$inRow[] = stripslashes($row->street_addr);
				$inRow[] = stripslashes($row->suburb);
				$inRow[] = $row->postcode;
				$inRow[] = $row->medicare_number;
				$inRow[] = $row->medicare_ref;
				$inRow[] = $row->medicare_expiry;
				$inRow[] = $row->dva?'Yes':'No';
				$inRow[] = $row->health_insurance_provider;
				$inRow[] = $row->health_fund_number;
				$inRow[] = stripslashes($row->latest_symptoms);
				$inRow[] = $row->latest_wdyhau;
				$inRow[] = $row->regular_practice;
				$inRow[] = $this->Reportmodel->get_practice_email($row->regular_practice);
				$inRow[] = $row->regular_doctor;
				$inRow[] = ($row->abotsi)?'Yes':'No';
				$inRow[] = stripslashes($row->cultural_ethnic_bg);
				$inRow[] = $row->agent_name;

				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'appointment report-'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/appointment';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);
		
	}

	public function daily(){
		
		$this->load->model('Areamodel', 'areas');
		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = '';

		/*$date = (isset($_GET['appt_start']) AND $_GET['appt_start'] != '' )?$_GET['appt_start']:date('d/m/Y', strtotime($this->Commonmodel->get_server_converted_date($this->default_timezone)));
		$data['formated_date'] 	= $date;
		$format_date 	= explode('/', $date);
		$data['date']  	= $format_date[2].'-'.$format_date[1].'-'.$format_date[0];
		
		$record = $this->Reportmodel->dailyreport($data['date']);*/
		
		$range = $this->build_range_date(); 

		$data['appt_start'] = $range['appt_start'];
		 
		$record = $this->Reportmodel->dailyreport($range['from'], $range['to']);

		if( isset($_GET['download']) ){
 
			$fields = array(
				'Date',
				'Area',
				'Patients Booked',
				'Completed Bookings',
				'Open Bookings',
				'Visit Bookings',
				'Unseen',
				'Cancelled',
				'Actual Bookings'
			);

			$data_row = array();

			$prev_date = '';

			$total = array(
				'all'=>0,
				'complete'=>0,
				'open'=>0,
				'visit'=>0,
				'cancel'=>0,
				'unseen'=>0
			);

			foreach($record as $row){

				$d_date = '';
				if( $prev_date != $row->appt_created_tz ){
					$prev_date = $row->appt_created_tz;
					$d_date = $row->appt_created_tz;
				}

				$total['all'] += $row->all_booking;
				$total['complete'] += $row->book_completed;
				$total['open'] += $row->book_open;
				$total['visit'] += $row->book_visit;
				$total['unseen'] += $row->unseen;
				$total['cancel'] += $row->cancelled_count;

				$inRow = array();

				$inRow[] = $d_date;
				$inRow[] = $row->area_name;
				$inRow[] = $row->all_booking;
				$inRow[] = $row->book_completed;
				$inRow[] = $row->book_open;
				$inRow[] = $row->book_visit;
				$inRow[] = $row->unseen;
				$inRow[] = $row->cancelled_count;
				$inRow[] = ($row->all_booking-$row->unseen-$row->cancelled_count);

				$data_row[] = $inRow;
			}

			$inRow = array();
			$inRow[] = '';
			$inRow[] = '';
			$inRow[] = $total['all'];
			$inRow[] = $total['complete'];
			$inRow[] = $total['open'];
			$inRow[] = $total['visit'];
			$inRow[] = $total['unseen'];
			$inRow[] = $total['cancel'];
			$inRow[] = ($total['all']-$total['unseen']-$total['cancel']);

			$data_row[] = $inRow;

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'Daily Report -'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row);

			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/daily';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);

	}


	public function cancellation(){
		
		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = '';

		$range = $this->build_range_date();

		$data['appt_start'] = $range['appt_start'];
		 
		$record = $this->Reportmodel->cancellation($range['from'], $range['to']);

		if( isset($_GET['download']) ){
 
			$fields = array(
				'Appt ID',
				'Date Booking Made',
				'Time Booking Made',
				'First Name',
				'Last Name',
				'Landline Phone',
				'Mobile Phone',
				'Address',
				'Suburb',
				'Postcode',
				'Cancel Date/Time',
				'Reason Cancelled',
				'Cancelled By'
			);

			$data_row = array();
			foreach($record as $row){

				$inRow = array();
 
				$inRow[] = $row->ref_number_formatted;
				$inRow[] = date('d/m/Y', strtotime($row->appt_created) );
				$inRow[] = date('H:i', strtotime($row->appt_created) );
				$inRow[] = $row->firstname;
				$inRow[] = $row->lastname;
				$inRow[] = $row->phone;
				$inRow[] = $row->mobile_no;
				$inRow[] = $row->street_addr;
				$inRow[] = $row->suburb;
				$inRow[] = $row->postcode;
				$inRow[] = date('d/m/Y H:i', strtotime($row->appt_end_queue) );
				$inRow[] = stripslashes($row->cancelled_reason);
				$inRow[] = $row->cancelled_by;

				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'Cancellation Report -'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/cancellation';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);


	}

	public function wyhau(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$range = $this->build_range_date();

		$data['appt_start'] = $range['appt_start'];

		$record = $this->Reportmodel->wyhau($range['from'], $range['to']);

		if( isset($_GET['download']) ){
  

			 
			$header = array();

			foreach($record as $key=>$row){

				if( $key == '0' ){

					$header = $row;

					$fields = array_values($row);
					
					array_unshift($fields,"");

				}else{

					$inRow = array();
					$inRow[] =  $key;
					foreach($header as $head){						
						$inRow[] = $row[$head];
					}

					$data_row[] = $inRow;

				}
			}


			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'WYHAu Report -'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/wyhau';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);		
	}


	public function call_register(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = '';

		$range = $this->build_range_date();

		$record = $this->Reportmodel->call_register($range['from'], $range['to']);
		//echo $this->db->last_query();
		if( isset($_GET['download']) ){
 
			$fields = array(
				'Date',
				'Time',
				'Type',
				'Caller Name',
				'Phone',
				'Suburb',
				'Patient Name',
				'Details',
				'Agent Name'
			);

			$data_row = array();
			foreach($record as $row){

				$inRow = array();
 				
				$inRow[] = date('Y-m-d', strtotime($row->tran_created_tz)); 
				$inRow[] = date('H:i', strtotime($row->tran_created_tz)); 
				$inRow[] = $row->tran_type.' '.((trim($row->tran_desc)!='')?' - '.$row->tran_desc:''); 
				$inRow[] = stripslashes($row->caller_firstname.' '.$row->caller_lastname); 
				$inRow[] = $row->caller_phone; 
				$inRow[] = stripslashes($row->caller_suburb);
				$inRow[] = (trim($row->firstname.$row->lastname)!='') ? stripslashes(@$row->firstname.' '.@$row->lastname):$row->appt_patient_name; 
				$inRow[] = stripslashes(nl2br($row->tran_details));
				$inRow[] = $row->agent_name; 

				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'Call Register -'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		} 
	}

	public function chaperone(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 


		$data = '';

		$range = $this->build_range_date(); 

		$data['appt_start'] = $range['appt_start'];
		 


		$params = array();

		$data['appt_start'] = $range['appt_start'];
		 
		if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}


		$params_query = $this->build_where($params);

		$record = $this->Reportmodel->chaperone($range['from'], $range['to'], $this->user_id);

		//echo $this->db->last_query();

		if( isset($_GET['download']) ){
 
			$fields = array(
				'Appt ID',
				'Area Name',
				'Date Booking Made',
				'Time Booking Made',
				'Consult Start',
				'Consult End',
				'Time in Queue',
				'Chaperone',
				'Doctor',
				'Car #',
				'First Name',
				'Last Name',
				'Date of Birth',
				'Age',
				'Sex',
				'Landline Phone',
				'Mobile Phone',
				'Email',
				'Address',
				'Suburb',
				'Postcode',
				'Medicare Num',
				'Medicare Ref',
				'Medicare Exp',
				'DVA Gold Card',
				'Private Health Insurance Provider',
				'Private Health Fund Number',
				'Symptoms',
				'Where Heard',
				'Regular Practice',
				'Regular Doctor',
				'Aboriginal / Torres Strait Island',
				'Cultural & Ethnicity Background',
				'Agent'
			);

			$data_row = array();
			foreach($record as $row){

				$inRow = array();

				$inRow[] = $row->ref_number_formatted;
				$inRow[] = $row->area_name;
				$inRow[] = date('Y-m-d', strtotime($row->appt_created_tz) );
				$inRow[] = date('H:i', strtotime($row->appt_created_tz) );			
				$inRow[] = ' '.$row->consult_start_tz;
				$inRow[] = ' '.$row->consult_end_tz;
				$inRow[] = $row->in_queue;
				$inRow[] = $row->chaperone_name;
				$inRow[] = $row->doctor_name;
				$inRow[] = $row->car_name;
				$inRow[] = $row->firstname;
				$inRow[] = $row->lastname;
				$inRow[] = ($row->dob != '0000-00-00')?' '.date('d/m/Y', strtotime($row->dob)):'';
				$inRow[] = $row->patient_age;
				$inRow[] = $row->gender;
				$inRow[] = $row->phone;
				$inRow[] = $row->mobile_no;
				$inRow[] = $row->email;
				$inRow[] = stripslashes($row->street_addr);
				$inRow[] = stripslashes($row->suburb);
				$inRow[] = $row->postcode;
				$inRow[] = $row->medicare_number;
				$inRow[] = $row->medicare_ref;
				$inRow[] = $row->medicare_expiry;
				$inRow[] = $row->dva?'Yes':'No';
				$inRow[] = $row->health_insurance_provider;
				$inRow[] = $row->health_fund_number;
				$inRow[] = stripslashes($row->latest_symptoms);
				$inRow[] = $row->latest_wdyhau;
				$inRow[] = $row->regular_practice;
				$inRow[] = $row->regular_doctor;
				$inRow[] = ($row->abotsi)?'Yes':'No';
				$inRow[] = stripslashes($row->cultural_ethnic_bg);
				$inRow[] = $row->agent_name;

				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'appointment report-'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/chaperone';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);
		
	}

		public function client_admin(){

		$this->load->model('Reportmodel');
		$this->load->library('ExportDataExcel'); 

		$data = '';

		$range = $this->build_range_date(); 

		$data['appt_start'] = $range['appt_start'];



		$params = array();

		$data['appt_start'] = $range['appt_start'];
		 
		if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}

		 $params_query = $this->build_where($params);

		$record = $this->Reportmodel->appointment($range['from'], $range['to'], $params_query);
		

		if( isset($_GET['download']) ){
 
			$fields = array(
				'Appt ID',
				'Area Name',
				'Date Booking Made',
				'Time Booking Made',
				'Consult Start',
				'Consult End',
				'Time in Queue',
				'Chaperone',
				'Doctor',
				'Car #',
				'First Name',
				'Last Name',
				'Date of Birth',
				'Age',
				'Sex',
				'Landline Phone',
				'Mobile Phone',
				'Email',
				'Address',
				'Suburb',
				'Postcode',
				'Medicare Num',
				'Medicare Ref',
				'Medicare Exp',
				'DVA Gold Card',
				'Private Health Insurance Provider',
				'Private Health Fund Number',
				'Symptoms',
				'Where Heard',
				'Regular Practice',
				'Regular Doctor',
				'Aboriginal / Torres Strait Island',
				'Cultural & Ethnicity Background',
				'Agent'
			);

			$data_row = array();
			foreach($record as $row){

				$inRow = array();

				$inRow[] = $row->ref_number_formatted;
				$inRow[] = $row->area_name;
				$inRow[] = date('Y-m-d', strtotime($row->appt_created_tz) );
				$inRow[] = date('H:i', strtotime($row->appt_created_tz) );			
				$inRow[] = ' '.$row->consult_start_tz;
				$inRow[] = ' '.$row->consult_end_tz;
				$inRow[] = $row->in_queue;
				$inRow[] = $row->chaperone_name;
				$inRow[] = $row->doctor_name;
				$inRow[] = $row->car_name;
				$inRow[] = $row->firstname;
				$inRow[] = $row->lastname;
				$inRow[] = ($row->dob != '0000-00-00')?' '.date('d/m/Y', strtotime($row->dob)):'';
				$inRow[] = $row->patient_age;
				$inRow[] = $row->gender;
				$inRow[] = $row->phone;
				$inRow[] = $row->mobile_no;
				$inRow[] = $row->email;
				$inRow[] = stripslashes($row->street_addr);
				$inRow[] = stripslashes($row->suburb);
				$inRow[] = $row->postcode;
				$inRow[] = $row->medicare_number;
				$inRow[] = $row->medicare_ref;
				$inRow[] = $row->medicare_expiry;
				$inRow[] = $row->dva?'Yes':'No';
				$inRow[] = $row->health_insurance_provider;
				$inRow[] = $row->health_fund_number;
				$inRow[] = stripslashes($row->latest_symptoms);
				$inRow[] = $row->latest_wdyhau;
				$inRow[] = $row->regular_practice;
				$inRow[] = $row->regular_doctor;
				$inRow[] = ($row->abotsi)?'Yes':'No';
				$inRow[] = stripslashes($row->cultural_ethnic_bg);
				$inRow[] = $row->agent_name;

				$data_row[] = $inRow;
			}

			$exporter = new ExportDataExcel('browser'); //browser, string, file
			$exporter->filename = 'appointment report-'.rand(date('mdYHis'), 5).'.xls';
			$exporter->initialize();

			$exporter->addRow($fields); 
			foreach($data_row as $row){
				$exporter->addRow($row); 	
			}		
			$exporter->finalize();
			exit;
			
		}

		$data['record'] = $record;

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'report';
		$this->view_data['view_file'] = 'report/chaperone';

		$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/report.js"></script>';

		$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

		$this->load->view('index', $this->view_data);
		
	}

}