<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container-fluid';

		$this->Commonmodel->access('0,1,2,3,52,53');
	}

	function build_where($params){

		$params['where_str'] = '';


	

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = strtolower(trim($params['search']));

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		$like = " ( lower(`ref_number_formatted`) LIKE '%{$search}%' OR ";
				$like .= " lower(concat(`firstname`,' ',`lastname`)) LIKE '%$search%' OR ";
				$like .= " lower(`firstname`) LIKE '%{$search}%' OR ";
				$like .= " lower(`lastname`) LIKE '%{$search}%' OR ";
				$like .= " lower(`phone`) LIKE '%{$search}%' OR ";
				$like .= " lower(`mobile_no`) LIKE '%{$search}%' ) ";
	 		}
	 		$params['where_str'] = $like;	

	 		
		}

		if( isset($params['dt']) AND $params['dt'] != '' ) {

			 
			$dt = explode(' - ',$params['dt']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( $params['where_str'] != '' ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`transaction`.`tran_created`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";
			//$params['where_str'] .= " (DATE_FORMAT(CONVERT_TZ(`tran_created`, '".$this->serv_tz."', '".$this->site_tz."'),'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";


		}

		if(isset($params['appt_start']) AND $params['appt_start'] != '' ){
			
	

			//$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_f}') ";			
			//$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') = '{$dt_f}') ";
			
 			if(!isset($params['search'])){

				$dt_f = $params['appt_start'];			 

				if( $params['where_str'] != '' ) $params['where_str'] .= ' AND ';
				else $params['where_str'] = '';

 				$params['where_str'] .= " (	DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}') ";	

 			}
			


		}


		if( isset($params['area']) AND count($params['area'])>0 ){ 
		 
			if( count($params['area']) == 1 ){
				$params['where'] = array('appointment.area_id'=>$params['area'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `appointment`.`area_id` IN ('.implode(',',array_values($params['area'])).')';				
			}
		}



		unset($params['search']);

		if( isset($params['tran_status']) AND $params['tran_status'] != ''){
			$params['where'] = array('tran_status'=>$params['tran_status']);
		}

		return $params;
	}

	public function index()	 
	{

		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Areamodel', 'areas');
		$this->load->model('Usermodel', 'user');
		$this->load->model('Commonmodel');
		$this->load->library("pagination");

		$params = $this->input->get();
 		$data = array(); 		

 		if($this->user_lvl.$this->user_sub_lvl == 52){
 			$data['area_preset'] = $this->areas->get_areas_by_chaperone($this->user_id, 'array'); 			 
 		}else{
 			$data['area_preset'] = $this->areas->get_array(array('area_status'=>1));
 		}

		if($this->user_lvl.$this->user_sub_lvl == 53){

			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}


 		$timezone = $this->default_timezone;




		//$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:date('Y-m-d');
		$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:$this->Commonmodel->get_server_converted_date($timezone);

		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		//set default date today or via get params
		//$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:date('Y-m-d');
		//
	
		$query_params = $this->build_where($params);
		$params_query = @http_build_query($params);

		//$query_params['where']['book_status'] = BOOK_STATUS_VISIT;		

		if($this->user_lvl.$this->user_sub_lvl == 52){
			$query_params['where']['appointment.doctor_id'] = $this->user_id;
		}




		$per_page = 20;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;		 

		$query_params['sorting'] = "`appointment`.`book_status` DESC, in_queue DESC";
 
		$query_params['consult_notes'] = 1;

	
		$query_params['where_str'] .= ($query_params['where_str'] !=''?'AND':'')." `book_status` IN (".BOOK_STATUS_VISIT.",".BOOK_STATUS_CLOSED.")";


	
		$appt_results 				= $this->appt->get_result_pagination($query_params);


		//echo $this->db->last_query();
		
		//echo $total_rows;
        $p_config["base_url"] 		= base_url() . "doctor/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';

		$data['appt_start'] = $params['appt_start'];

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'doctor/index';
		$this->view_data['menu_active'] = 'doctor';
		$this->view_data['js_file'] = '<script src="assets/js/page_js/doctor.js"></script>';
		//$this->view_data['js_file'] = '<script src="assets/js/mcautocomplete.js"></script>';
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/booking.js"></script>';
		$this->load->view('index', $this->view_data);

 	}

 	public function consult_notes($appt_id){
 		
 		$this->load->model('Appointmentmodel', 'appt');
 		$this->load->model('Dropdownmodel');

 		$data = array();
 		
 		$record = $this->appt->get_row_link_full($appt_id);	 		
 		$consult = $this->appt->get_row_consult_notes($appt_id);	 		
	 	
	 	//print_r($consult);

 		if( $_POST ){

 			$post = $this->input->post();
 			
 			$consult_id = @$post['consult_id'];


 			$set['past_medical_history'] 	= addslashes($post['past_medical_history']);
 			$set['current_medication'] 		= addslashes($post['current_medication']);
 			$set['immunisations'] 			= addslashes($post['immunisations']);
 			$set['allergies'] 				= addslashes($post['allergies']);
 			$set['social_history'] 			= addslashes($post['social_history']);

 			$set['observation'] 			= addslashes($post['observation']);

 			$set['diagnosis'] 				= addslashes($post['diagnosis']);

 			$set['diagnosis_ref_code'] 		= addslashes($post['diagnosis_ref_code']);
			
 			$set['plan'] 					= addslashes($post['plan']);
 			

 			$set['cunsult_medication'] 		= addslashes($post['cunsult_medication']);
 			

 			$set['medication_prescribed'] 	  = addslashes($post['medication_prescribed']);

 			$set['medication_perscribed_sub'] = (is_array($post['medication_perscribed_sub']) && !empty($post['medication_perscribed_sub']) && count($post['medication_perscribed_sub']) > 0 ) ? json_encode($post['medication_perscribed_sub']):'' ;

 			$set['followup_gp'] 			= addslashes($post['followup_gp']);
 			
 			$set['med_cert_given'] 			= addslashes($post['med_cert_given']);
 			$set['ref_form_given'] 			= addslashes($post['ref_form_given']);


 			$set['item_num'] 			    = addslashes($post['item_num']);

 			$set['prognosis_id'] 			= $post['consult_sel'];

 			$set['call_center_triage'] 		= addslashes($post['call_center_triage']);
 			$set['life_style_risk_factors'] = addslashes($post['life_style_risk_factors']);

 
 			if( $post['form_type'] == 'new' ){
 				$set['appt_id']	= $post['appt_id'];
	
 				if( $this->appt->add_consult($set) ){

 					
					$set['symptoms'] = addslashes($post['symptoms']);
					$set_appt['appt_id']  = $post['appt_id'];
					$set_appt['set_appt'] = array('symptoms'=>$set['symptoms']);
					
					$this->appt->update_appointment($set_appt);

 					$this->session->set_flashdata('fmesg','Consult Notes for Booking # '.$record->ref_number_formatted.' is successfully added');
					redirect(base_url().'doctor/consult_notes/'.$appt_id);
				}
 				
 			}

 			if( $post['form_type'] == 'update' ){
 				$set['consult_updated'] 	= date('Y-m-d H:i:s');

				if( $post['symptoms_backup'] != $post['symptoms'] ){
 					$set['symptoms'] = addslashes($post['symptoms']);
				}

 				if( $this->appt->update_consult($consult_id, $set) ){ 					

 					if( isset($set['symptoms']) ){

 						$set_appt['appt_id']  = $post['appt_id'];
 						$set_appt['set_appt'] = array('symptoms'=>$set['symptoms']);
 						$this->appt->update_appointment($set_appt);
 						
 					}

 					$this->session->set_flashdata('fmesg','Consult Notes for Booking # '.$record->ref_number_formatted.' is successfully updated');
					redirect(base_url().'doctor/consult_notes/'.$appt_id);
 				}

 			}

 		}

	 	$data['form_type'] = 'new';
	 	if( count($consult) > 0 ){
	 		$data['form_type'] = 'update';
	 	}
				

	 	$data['consult'] = $consult;
		$data['record'] = $record;
		$data['prev_record'] =  $this->appt->get_cunsult_notes('patient_id='.$record->patient_id);

		$data['dp_diagnosis'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'diagnosis')), 'array');
		$data['dp_plan'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'plan')), 'array');
		$data['dp_cunsult_medication'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'cunsult_medication')), 'array');
		$data['dp_medication_prescribed'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'medication_prescribed')), 'array');
		$data['dp_followup_gp'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'followup_gp')), 'array');
		$data['dp_diagnosis_ref'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'followup_gp')), 'array');
		$data['dp_diagnosis_ref_code'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'diagnosis_reference_code', 'dropdown_status'=>'1')));
/*		echo '<pre>';
		print_r($data['dp_diagnosis_ref_code'] );
		echo '<pre/>';

		exit();*/


		//table_audit_trail
		if( isset($consult->consult_id) ){
			$this->db->where('target_table', 'appointment_consult_notes');
			$this->db->where('table_ref_col', 'consult_id');
			$this->db->where('table_ref_id', $consult->consult_id);
			$this->db->order_by('id', 'desc');
			$table_audit_trail_query = $this->db->get('table_audit_trail');
			$data['table_audit_trail'] = $table_audit_trail_query->result();
		}
		//table_audit_trail
		
/*		echo '<pre>';
		print_r($record);	
		echo '<pre/>';

		exit();	*/

		$this->db->select('id,prognosis', FALSE);

		$this->db->where('doctor_id',$record->dr_id);

		$query = $this->db->get('consult_note_upload');


		$data['def_consult']  = $query->result();



		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'doctor/consult-notes';
		$this->view_data['menu_active'] = 'doctor';

		$this->view_data['js_file'] = '<script src="assets/js/page_js/doctor.js"></script>';
		
		$this->load->view('index', $this->view_data);
 	}

 	public function upload_consult_file(){

 		$this->load->model('Appointmentmodel', 'appt');

      	$post = $this->input->post();

     
        $config['upload_path']          = './files/consult_files/';
        $config['allowed_types']        = '*';//'gif|jpg|png';
        $config['file_name']            = $post['appt_id'].'_'.$post['consult_id'].'_'.$_FILES['consult_file']['name'];
        //$config['max_size']             = 100;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;		 	

        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('consult_file'))
        {
            $data = array('error' => $this->upload->display_errors());

        }
        else
        {
        	$upload_data = $this->upload->data();



        	$set['file_name'] 	= $upload_data['file_name'];
        	$set['file_path'] 	= 'files/consult_files/'.$upload_data['file_name'];
        	$set['appt_id'] 	=  $post['appt_id'];
        	
        	$set['consult_id'] 	=  $post['consult_id'];        	

        	
        		
		   $this->appt->add_consult_upload(array('appt_id'=>$post['appt_id']),$set);	        	
               	

            $data = 'Uploaded file successfully';
                
        }

        header("Location: ".base_url()."doctor/consult_notes/".$post['appt_id']."?fmesg=".$data."#upload_consult_form");

 	}


 	public function consult_notes_export($appt_id)
 	{

		$this->load->helper('file');
		$this->load->library("pdfhelper");	
 		$this->load->model('Appointmentmodel', 'appt');

 		$data = array(); 		

 		$record = $this->appt->get_row_link_full($appt_id);	 		

 		$consult = $this->appt->get_row_consult_notes($appt_id);	


		$data['consult'] = $consult;

		$data['record'] = $record;


		$datef = date('dMy');

		$filename = strtoupper($record->ref_number_formatted.'-'.$datef.$record->firstname.$record->middleinitial.$record->lastname.'_CONSULT');

		$filename = $filename.'.pdf';

		$html = $this->load->view('doctor/consult-notes-export', array('record'=>$record,'consult'=>$consult), true);

		echo $html;


/*	     $pdfoutput = $this->pdfhelper->renderPDF($html, $filename, false);



		if ( ! write_file('./files/consult_notes/'.$filename, $pdfoutput))
		{
		        echo 'Unable to write the file';
		}
		else
		{
		        echo 'File written!';
		}
*/
		//submit to dropbox
		/*
		if( isset($post['savedropbox']) ){
				redirect(base_url().'dropboxupload/dropbox/1/'.$appt_id); 
		}else{
			redirect(base_url().'chaperone'); 
		}*/
		//$this->load->view('index', $this->view_data);
 	}

	public function delete_consult(){

 		$this->load->model('Appointmentmodel', 'appt');

		$file = $this->input->get('file');

		$changes = array();



		if($file){

			$exp_file_name = basename($file);

			$changes['file_path'] = $file;
			$changes['file_name'] = basename($file);
			$changes['consult_file_id'] = $this->input->get('consult_id');


				$insert = array('target_table'=>'appointment_consult_notes',
					'table_ref_col'=>'consult_id',
					'table_action'=>'deleted',
					'table_ref_id'=> $changes['consult_file_id'],
					'user_id'=>$this->user_id,
					'agent_name'=>$this->agent_name,
					'message'=> json_encode($changes));
			
				$this->appt->insert_audit_trail($insert);

				unlink($file);

				  $msg = 'File successfully removed.';

				header("Location: ".base_url()."doctor/consult_notes/".$this->input->get('appt_id')."?fmesg=".$msg);


		}


 	}
}
