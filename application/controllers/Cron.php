<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct(){

		parent::__construct();

	}
	 
	public function cma_users() {

		$this->load->library('Cma_users_cron');

		//echo '<pre>';

		$server_users = $this->cma_users_cron->get();

		if( is_array($server_users) AND !empty($server_users) ){
			
			$now = date('Y-m-d H:i:s');

			foreach( $server_users as $row ){

				$query =  "REPLACE INTO `user_cma` VALUES ({$row->user_id},'{$row->username}', '{$row->password}', {$row->userlevel}, '{$row->timezone}', {$row->countrycode}, {$row->deleted}, '{$now}');";
				$this->db->query($query);
				//echo $query.'<br />';
			}

			echo 'updated';
		}else{
			echo 'ERROR';
		}

		//echo '</pre>';
	}
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */