<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chaperone extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container-fluid';
		$this->Commonmodel->access('0,1,2,3,51,52,53');
	}

	function build_where($params){

		$params['where_str'] = '';

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = strtolower(trim($params['search']));

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		$like = " ( lower(`ref_number_formatted`) LIKE '%{$search}%' OR ";
				$like .= " lower(`firstname`) LIKE '%{$search}%' OR ";
				$like .= " lower(`lastname`) LIKE '%{$search}%' OR ";
				$like .= " lower(`phone`) LIKE '%{$search}%' OR ";
				$like .= " lower(`mobile_no`) LIKE '%{$search}%' ) ";
	 		}
	 		$params['where_str'] = $like;	

	 		unset($params['search']);
		}

		if( isset($params['dt']) AND $params['dt'] != '' ) {

			 
			$dt = explode(' - ',$params['dt']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( $params['where_str'] != '' ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`transaction`.`tran_created`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";
			//$params['where_str'] .= " (DATE_FORMAT(CONVERT_TZ(`tran_created`, '".$this->serv_tz."', '".$this->site_tz."'),'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";


		}

		if( isset($params['appt_start']) AND $params['appt_start'] != '' ){
			
			$dt_f = $params['appt_start'];			 
 			
 			if( $params['where_str'] != '' ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

 			
			//$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_f}') ";			
			
			//$params['where_str'] .= " (DATE_FORMAT(`appointment`.`appt_start`,'%Y-%m-%d') = '{$dt_f}') ";
			
			//$params['where_str'] .= " (	DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$params['timezone']."'), '%Y-%m-%d') = '{$dt_f}') ";
			$params['where_str'] .= " (	DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}') ";
		}

		//filter status
		if( isset($params['chaperone_filter_status']) ){

 			if( count($params['chaperone_filter_status']) == 1 ){
 				
 				$params['where']['book_status'] = $params['chaperone_filter_status'][0];

 			}else{

				if( $params['where_str'] != '' ) $params['where_str'] .= ' AND ';
	 			else $params['where_str'] = '';

 				$params['where_str'] .= " `book_status` IN (".implode(',', $params['chaperone_filter_status']).") ";
 			}
		}
		

		if( isset($params['tran_status']) AND $params['tran_status'] != ''){
			$params['where'] = array('tran_status'=>$params['tran_status']);
		}

		return $params;
	}

	public function index()	 
	{
		$this->load->model('Appointmentmodel', 'appt');
		$this->load->model('Areamodel', 'areas');
		$this->load->model('Usermodel', 'user');
		$this->load->model('Commonmodel');
		$this->load->library("pagination");

 		$data = array();

 		//intialize
 		$data['chaperone_filter_status'] = array();

 		if( $_POST ){
 			
 			$post = $this->input->post();
 			//print_r($post); 			

 			if( $post['submit'] == 'set-area-filter' ) {
 
 				$set['areas'] = (isset($post['area_ids']) AND count($post['area_ids']) > 0)?json_encode($post['area_ids']):NULL;

 				if( $this->user->set_preset($this->user_id, $set) ){

 				}
 			}

 			if( $post['submit'] == 'btn-filter' ) {
	 			if( isset($post['set-filter-status']) ) {
	 				$filter = json_encode(array_values($post['set-filter-status']));
	 				//echo $filter;
	 				$this->session->set_userdata('chaperone_filter_status', $filter);
	 			}else{
	 				$this->session->unset_userdata('chaperone_filter_status');
	 			}		
	 		}

 		}
 		
 		if( $this->session->has_userdata('chaperone_filter_status') ){
 			$data['chaperone_filter_status'] = array_values(json_decode($this->session->userdata('chaperone_filter_status')));
 		}

 		//get user preset
 		//this is used on filter and remembering what the chaperone is set
 		//$user_presets = $this->user->get_preset($this->user_id);
 		///print_r($user_presets->areas);
 		//$data['area_preset'] = (!empty($user_presets->areas))?json_decode($user_presets->areas):array(); 

 		$user_presets_set = array(); 	 

 		if($this->user_lvl.$this->user_sub_lvl == 51){
 			//$chaperone_area_id = $this->
 			$data['area_preset'] = $this->areas->get_areas_by_chaperone($this->user_id, 'array');

	 		$car 	= $this->areas->get_row_by_car(array('car_id'=>$this->user_extra['car']));
	 		$doctor = $this->user->get_user_row(array('user_id'=>$this->user_extra['doctor'])); 			
	 
	 		$user_presets_se = $this->user_extra['areas'];

	 		if( $user_presets_se != '' ){
	 			$xt = explode(',', $user_presets_se);
	 			//print_r($xt);
	 			foreach ($data['area_preset'] as $key => $value) {
	 				if( !in_array($key, $xt) ){
	 					unset($data['area_preset'][$key]);
	 				}
	 			}
	 		}else{
	 			$data['area_preset'] = array();
	 		}
	 		//print_r($data['area_preset'] );
 		}else{
 			//$data['area_preset'] = $this->areas->get_array(array('area_status'=>1));
 			$user_presets = $this->user->get_preset($this->user_id);

			if($this->user_lvl.$this->user_sub_lvl != 53){
	 			$data['area_preset'] = (!empty($user_presets->areas))?json_decode($user_presets->areas):array();
	 		}else{

	 			$area_client_admin = $this->areas->get_areas_by_chaperone($this->user_id, 'array');


	 			if(!empty($user_presets->areas)){

	 					$data['area_preset'] = json_decode($user_presets->areas);
	 			}else{
		 			if(count($area_client_admin) > 0 ){
		 				$data['area_preset'] = array_keys($area_client_admin);
		 			}
	 				
	 			}
	 			

	 		}
 			$user_presets_se = implode(',',(array_values($data['area_preset'])));

 		}
 		//print_r($data['area_preset']);

 		//TODO - allow the user can choose timezone if multiple
 		//$area_keys = array_keys($data['area_preset']);
 		//$timezones = $this->areas->get_area_timezone($area_keys);

 		$timezone = $this->default_timezone;
 

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		//echo date('Y-m-d H:i:s').'<br />';
		//echo $this->Commonmodel->get_server_converted_date($timezone);
		//set default date today or via get params
		
		$timezone_date = $this->Commonmodel->get_server_converted_date($timezone);

		//$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:$this->Commonmodel->get_server_converted_date($timezone);
		$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:$timezone_date;
		
		//echo $params['appt_start'];
		//$params['appt_start'] = (isset($params['appt_start']))?$params['appt_start']:date('Y-m-d');

		$params['timezone'] = $timezone;


 		if(!empty($data['chaperone_filter_status'])){	
			$params['chaperone_filter_status'] = $data['chaperone_filter_status'];
 		} 		

		$query_params = $this->build_where($params);
		$params_query = @http_build_query($params);


		if($this->user_lvl.$this->user_sub_lvl == 51){




			$query_params['where_str'] .= (trim($query_params['where_str'])!=''?' AND ':'').'( `appointment`.`chaperone_id`='.$this->user_id.' OR `appointment`.`chaperone_id` IS NULL) ';

			if( count(explode(',', $user_presets_se)) > 1 ){

				$query_params['where_str'] .= (trim($query_params['where_str'])!=''?' AND ':'').' `appointment`.`area_id` IN ('.$user_presets_se.')';
			
			}elseif( count(explode(',', $user_presets_se)) == 1 ){
				
				$query_params['where']['`appointment`.`area_id` = '] = $user_presets_se;
				
			}else{
				$query_params['where']['`appointment`.`area_id` = '] = '-1';
				$data['error'] = 'No Areas has been set for this User, please contact admin';
			}

		}else{

			if( count($data['area_preset']) > 0 )
				$query_params['where_str'] .= (trim($query_params['where_str'])!=''?' AND ':'').' `appointment`.`area_id` IN ('.$user_presets_se .')';		

		   if($this->user_lvl.$this->user_sub_lvl == 52)	

		   	$query_params['where_str'] .= (trim($query_params['where_str'])!=''?' AND ':'').'( `appointment`.`doctor_id`='.$this->user_id.'   OR `appointment`.`chaperone_id` IS NULL ) ';	
		}

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;
		
		$query_params['sorting'] = "`appointment`.`book_status` DESC, in_queue DESC";
  
		$appt_results 				= $this->appt->get_result_pagination($query_params);
		
				 
        $p_config["base_url"] 		= base_url() . "chaperone/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';

 	 
		$data['appt_start'] = $params['appt_start'];
		$data['timezone_date'] = $timezone_date;
		
		if($this->user_lvl.$this->user_sub_lvl != 53){

			$data['areas'] 	= $this->areas->get_array(array('area_status'=>1));
		}else{

			$data['areas'] 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');
			
		}



		$data['doctor'] = @$doctor;
		$data['car'] 	= @$car;

		$data['area_id'] = implode(',',array_keys($data['area_preset']));

		$data['booking_area'] = $data['area_preset'];

		$temp_array  = array();

		foreach ($data['booking_area'] as $area_id => $area_name) {

			if(isset($area_id) && $area_id!=''){

				$is_open = $this->areas->is_open($area_id);
				$temp_array[] = array('area_id'=>$area_id,'area_name'=>$area_name,'is_open'=>$is_open);			

			}
				
		}	


		$data['booking_area'] = $temp_array;
		unset($temp_array);	

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'chaperone/index';
		$this->view_data['menu_active'] = 'chaperone';

		$this->view_data['js_file'] .= '<script src="assets/plugins/boostrap-dropdown-with-checkboxes.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script src="assets/js/page_js/chaperone.js"></script>'.PHP_EOL;

		$this->load->view('index', $this->view_data);
 	}

 	public function book_appointment($appt_id=0, $book_type=''){

 		try {

 			$this->load->model('Appointmentmodel', 'appt');
 			$this->load->model('Transactionmodel', 'transaction');
 			$this->load->model('Usermodel', 'user');
 			$this->load->model('Areamodel', 'areas');
 			$this->load->model('Sitesettingsmodel', 'sitesettings');

 			if(empty($appt_id)) throw new Exception("Error: Appt ID must not be empty", 1);
 			if(empty($book_type)) throw new Exception("Error: Type must be set", 1);
 		
	 		$data = array();
	 		
	 		$_max_book = @$this->sitesettings->get_row(array('set_name'=>'MAX_BOOKING'))->set_value;
	 		$data['max_book'] = $_max_book;
	 		//echo $max_book;

	 		$record = $this->appt->get_row_link_full($appt_id);

	 		//echo $record->appt_created_tz;
	 		//echo  $this->user_lvl;

	 		$_user_date_total_booking = $this->appt->get_user_count_booking($this->user_id, $record->appt_created_tz);
	 		$data['user_date_total_booking'] = $_user_date_total_booking;


	 		if($this->user_lvl.$this->user_sub_lvl == 51){

	 			$car 	= $this->areas->get_row_by_car(array('car_id'=>$this->user_extra['car']));
	 			$doctor = $this->user->get_user_row(array('user_id'=>$this->user_extra['doctor']));

	 		}else{

	 			$area_id = $record->area_id;
	 			//echo $area_id;
	 			
	 			//get chaperone dropdowns
	 			//get cars dropdown
	 			//get doctors
	 			
	 			if( $record->car_id != '' ){
	 				$car 	= $this->areas->get_row_by_car(array('car_id'=>$record->car_id));
	 			}

	 			$data['doctors_array'] 		= $this->user->get_doctors('', 'dropdown');
	 			$data['chaperone_array'] 	= $this->areas->get_charperones_by_area_id($area_id, 'array');
	 			$data['cars_array'] 		= $this->areas->get_cars('array');
	 			 
	 		}

	 		/*echo '<pre>';
	 		print_r($data['doctors_array']);	
	 		print_r($data['chaperone_array']);
	 		print_r($data['cars_array']);
	 		echo '</pre>';*/
	 		//exit;
	 		//print_r($doctor);

	 		if($_POST){

	 			$post = $this->input->post();

	 			//if( date('Y-m-d') == date('Y-m-d', strtotime($record->appt_start)) ){

 				$date = date('Y-m-d H:i:s');

	 			if( $post['chaperone_book'] == 'book' ){
					
					if( $record->book_status != BOOK_STATUS_OPEN) {
 
						$this->session->set_flashdata('fmesg','Sorry, Booking of Appt # '.$record->ref_number_formatted.' is already been '.$this->booking_statuses[$record->book_status]);
						redirect(base_url().'chaperone/');
			 
					}

					if( $_user_date_total_booking == $_max_book ) {
						$this->session->set_flashdata('fmesg','Sorry, You already reach the '.$_max_book.' maximum booking today. thank you ');
						redirect(base_url().'chaperone/');
					}


					$set['book_status']			= BOOK_STATUS_BOOKED;
					$set['appt_start_queue'] 	= $date;
					$set['chaperone_id']		= $post['chaperone_id'];
					$set['doctor_id']			= $post['doctor_id'];
					$set['car_id']				= $post['car_id'];
					$set['appt_updated']		= $date;

					//$set['flag_eta']			= 0;
				
					//print_r($set);
					if( $this->appt->set_book($appt_id, $set) ){

						$record = $this->appt->get_row_link_full($appt_id);
						$sel_car = $this->areas->get_row_by_car(array('car_id'=>$record->car_id));
						//insert to transaction
						$set_tran['tran_type'] = 'Book';
	 					$set_tran['tran_desc'] = '';
						$set_tran['parent_tran_id'] = $record->tran_id;
						$set_tran['appt_patient_name'] = $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] = 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['tran_details'] .= 'Chaperone: '.$record->chaperone_name.' '.PHP_EOL;
						$set_tran['tran_details'] .= 'Doctor: '.$record->doctor_name.' '.PHP_EOL;
						$set_tran['tran_details'] .= 'CAR: '.$sel_car->car_name.' '.PHP_EOL;
						$set_tran['agent_name'] = $this->agent_name;
						$this->transaction->add($set_tran);
						//echo $this->db->last_query();
						//
						//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-booked';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully '.$this->booking_statuses[$set['book_status']]);
						redirect(base_url().'chaperone/');
					}
	 			}

	 			if( $post['chaperone_book'] == 'unbook' ){

	 				/*if( $record->book_status != BOOK_STATUS_BOOKED) {
 
						$this->session->set_flashdata('fmesg','Sorry, UNBOOK of Appt # '.$record->ref_number_formatted.' is not allowed');
						redirect(base_url().'chaperone/');
			 
					}*/

	 				$set['book_status']			= BOOK_STATUS_OPEN;
					$set['appt_start_queue'] 	= NULL;
					
					$set['chaperone_id']		= NULL;
					$set['doctor_id']			= NULL;
					$set['car_id']				= NULL;

					//$set['car_no']				= NULL;
					$set['appt_updated']		= $date;

					$set['consult_start']		= NULL;

					$set['unable_to_sign']		= NULL;
					$set['sign_code']			= NULL;

	 				$set['flag_eta']			= 0;

	 				if( $this->appt->set_unbook($appt_id, $set) ){

						//insert to transaction
						$set_tran['tran_type'] = 'Unbook';
	 					$set_tran['tran_desc'] = '';
						$set_tran['parent_tran_id'] = $record->tran_id;
						$set_tran['appt_patient_name'] = $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] = 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['agent_name'] = $this->agent_name;
						$this->transaction->add($set_tran);

	 					//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-unbook';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully UNBOOK and set to '.$this->booking_statuses[$set['book_status']]);
						redirect(base_url().'chaperone/');

					}

	 			}


	 			if( $post['chaperone_book'] == 'unseen' ){

	 				/*if( $record->book_status != BOOK_STATUS_BOOKED) {
 
						$this->session->set_flashdata('fmesg','Sorry, UNBOOK of Appt # '.$record->ref_number_formatted.' is not allowed');
						redirect(base_url().'chaperone/');
			 
					}*/

	 				$set['book_status']			= BOOK_STATUS_UNSEEN;
					$set['appt_end_queue'] 		= $date;
					 
					$set['appt_updated']		= $date; 
	 				$set['cancelled_by']		= $this->agent_name;; //unseen by
	 				$set['cancelled_reason']	= addslashes($post['unseen_reason']); //unseen reason

	 				if( $this->appt->set_unseen($appt_id, $set) ){

						//insert to transaction
						$set_tran['tran_type'] 			= 'Unseen';
	 					$set_tran['tran_desc'] 			= '';
						$set_tran['parent_tran_id'] 	= $record->tran_id;
						$set_tran['appt_patient_name'] 	= $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] 		= 'Reason: '.$set['cancelled_reason'].' '.PHP_EOL;
						$set_tran['agent_name'] 		= $this->agent_name;
						$this->transaction->add($set_tran);

	 					//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-unseen';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully set to UNSEEN');
						redirect(base_url().'chaperone/');

					}

	 			}


	 			if( $post['chaperone_book'] == 'visit' ){

	 				if( $record->book_status != BOOK_STATUS_BOOKED) {
	 					$this->session->set_flashdata('fmesg','Sorry, VISIT of Appt # '.$record->ref_number_formatted.' is not allowed');
						redirect(base_url().'chaperone/');
	 				}

	 				$set['book_status']	 		= BOOK_STATUS_VISIT; 
					$set['appt_end_queue'] 		= $date;
					
					$set['consult_start']		= strtotime($date);

					//$set['flag_eta']			= 0;

					if( $this->appt->set_visit($appt_id, $set) ){


						//insert to transaction
						$set_tran['tran_type'] 			= 'Visit';
	 					$set_tran['tran_desc'] 			= '';
						$set_tran['parent_tran_id'] 	= $record->tran_id;
						$set_tran['appt_patient_name'] 	= $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] 		= 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['agent_name'] 		= $this->agent_name;
						$this->transaction->add($set_tran);

						//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-visit';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully '.$this->booking_statuses[$set['book_status']].' Doctor can now set consult notes');
						redirect(base_url().'chaperone/');

					}
	 			}

	 			if( $post['chaperone_book'] == 'complete' ){

	 				if( $record->book_status != BOOK_STATUS_VISIT) {
	 					$this->session->set_flashdata('fmesg','Sorry, Complete of Appt # '.$record->ref_number_formatted.' is not allowed');
						redirect(base_url().'chaperone/');
	 				}

	 				$set['book_status']	 	= BOOK_STATUS_CLOSED;
					
					$set['consult_end']		= strtotime($date);

					//$set['flag_eta']		= 0;

					if( $this->appt->set_visit($appt_id, $set) ){

						//insert to transaction
						$set_tran['tran_type'] 			= 'Complete';
	 					$set_tran['tran_desc'] 			= '';
						$set_tran['parent_tran_id'] 	= $record->tran_id;
						$set_tran['appt_patient_name'] 	= $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] 		= 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['agent_name'] 		= $this->agent_name;
						$this->transaction->add($set_tran);

						//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-complete';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully '.$this->booking_statuses[$set['book_status']]);
						redirect(base_url().'chaperone/');
					}
	 			}


	 			if( $post['chaperone_book'] == 'reinstate-book' ){
					
					if( !in_array($record->book_status, array(BOOK_STATUS_CANCELLED, BOOK_STATUS_UNSEEN) ) ) {
 
						$this->session->set_flashdata('fmesg','Sorry, Booking of Appt # '.$record->ref_number_formatted.' is already been '.$this->booking_statuses[$record->book_status]);
						redirect(base_url().'chaperone/');
			 
					}

					/*$set['book_status']			= BOOK_STATUS_BOOKED;
					$set['appt_start_queue'] 	= $date;
					$set['chaperone_id']		= $this->user_id;
					$set['doctor_id']			= $post['doctor_id'];
					//$set['car_no']				= addslashes($post['car']);
					$set['appt_updated']		= $date;*/
					
	 				$set['book_status']			= BOOK_STATUS_OPEN;
					$set['appt_start_queue'] 	= NULL;
					
					$set['chaperone_id']		= NULL;
					$set['doctor_id']			= NULL;
					$set['car_id']				= NULL;
				 
					$set['appt_updated']		= $date;

					$set['consult_start']		= NULL;

					$set['unable_to_sign']		= NULL;
					$set['sign_code']			= NULL;

					$set['flag_eta']			= 0;

					//print_r($set);
					if( $this->appt->set_book($appt_id, $set) ){

						//insert to transaction
						$set_tran['tran_type'] 			= 'Reinstate';
	 					$set_tran['tran_desc'] 			= '';
						$set_tran['parent_tran_id'] 	= $record->tran_id;
						$set_tran['appt_patient_name'] 	= $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] 		= 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['agent_name'] 		= $this->agent_name;
						$this->transaction->add($set_tran);

						//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-reinstate';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' is successfully '.$this->booking_statuses[$set['book_status']]);
						redirect(base_url().'chaperone/');
					} 

	 			}


				if( $post['chaperone_book'] == 'addnote' ){ 
				 
					$set['appt_notes']	= addslashes($post['appt_notes']); 
				 
					if( $this->appt->set_note($appt_id, $set) ){

						//insert to transaction
						$set_tran['tran_type'] 			= 'Added Note';
	 					$set_tran['tran_desc'] 			= '';
						$set_tran['parent_tran_id'] 	= $record->tran_id;
						$set_tran['appt_patient_name'] 	= $record->firstname.' '.$record->lastname;
						$set_tran['tran_details'] 		= 'Appt # : '.$record->ref_number_formatted.' '.PHP_EOL;
						$set_tran['tran_details'] 		.= 'Note : '.$set['appt_notes'].' '.PHP_EOL;
						$set_tran['agent_name'] 		= $this->agent_name;
						$this->transaction->add($set_tran);

						//audit_trail
						$set_audit['target_table'] 	= 'appointment';
						$set_audit['table_ref_col'] = 'appt_id';
						$set_audit['table_ref_id'] 	= $appt_id;
						$set_audit['table_action'] 	= 'chaperone-addnote';
						$set_audit['message'] 		= json_encode($set);
						$set_audit['user_id'] 		= $this->user_id;
						$set_audit['agent_name'] 	= $this->agent_name;
						$this->db->insert('table_audit_trail', $set_audit);

						$this->session->set_flashdata('fmesg','Appt # '.$record->ref_number_formatted.' NOTE successfully save');
						redirect(base_url().'chaperone/');
					} 

	 			}else{
	 				$this->session->set_flashdata('fmesg','Sorry, Booking of Appt # '.$record->ref_number_formatted.' is scheduled on '.date('Y-m-d', strtotime($record->appt_start)));
					redirect(base_url().'chaperone/');
	 			}


			}

			$data['appt_id'] = $appt_id;
			$data['book_type'] = $book_type;
			$data['record'] = $record;
			$data['car'] 	= @$car;
			$data['doctor'] = @$doctor;

			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/book-appointment';
			$this->view_data['menu_active'] = 'chaperone';
			$this->load->view('index', $this->view_data);

		} catch (Exception $e) {
			echo 'Error: '.$e->getMessage();
		 
		}	
 	}

 	public function register_doctor(){
 		
 		$this->load->model('Usermodel', 'user');

 		$data = '';

 		if($_POST){
 			$post = $this->input->post();
 			$data = $post;

 			$doc_username = trim($post['doc_username']);
 			$where['user_name'] = $doc_username;
 			$user_detail = $this->user->get_user_row($where);
 			
 			if( count($user_detail) > 0 ){
 			
 				$data['user_exist'] = 'Error: Username already exist please choose another one.';
 			
 			}else{

 				$set['user_id'] 	= $this->user_id;
 				$set['agent_name'] 	= $this->agent_name;

 				$set['user_fullname'] 	= $post['doc_fullname'];
 				$set['user_name'] 		= $post['doc_username'];
 				$set['user_pass'] 		= md5(trim($post['doc_pass']));
 				$set['user_level'] 		= 5;
 				$set['user_sublevel'] 	= 2;
 				$set['contact_email'] 	= $post['doc_email'];
 				$set['contact_mobile'] 	= $post['doc_mobile'];


 				if( $this->user->add($set) ){

 					//an email or sms will be sent to doctor login details.
 					//an email will be sent also to HCD Admin for new doctor notification
 					
 					$this->session->set_flashdata('fmesg', 'Doctor successfully registered.');
 					redirect(base_url().'dashboard/chaperone_switch_area_doctor');
 				}
 			}
 			

 		}

		$this->view_data['data'] = $data;
		$this->view_data['view_file'] = 'chaperone/register-doctor';
		$this->view_data['menu_active'] = 'chaperone';
		$this->load->view('index', $this->view_data);

 	}

 	public function medicare_form($appt_id){
		
		//Referrences: http://www.mpdf1.com/mpdf/index.php?page=Examples
		
		$this->load->helper('file');
		$this->load->library("pdfhelper");		

		$this->load->model('Appointmentmodel', 'appt');

		try {
			
	 		$data = '';

	 		if($_POST){

	 			$post = $this->input->post();

	 			//print_r($post);

	 			$appt_id = $post['appt_id'];
	 			$patient_id = $post['patient_id'];

	 			$set_appt['unable_to_sign'] = isset($post['unable_to_sign'])?1:0;

	 			if( isset($post['new_sig']) ){
	 				$set_appt['sign_code'] 	= addslashes($_REQUEST['hidden_sig']);
	 			}

	 			if( isset($post['unable_to_sign']) ) {
	 				unset($set_appt['sign_code']);
	 			}

	 			if( isset($post['savedropbox']) ){
	 				$set_appt['save_dropbox'] 	= 1;
	 			}


	 			$set_appt['appt_updated'] 	= date('Y-m-d H:i:s');


	 			//TO-DO check date format later
	 			$dob = explode('/', $post['DOB']); //d/m/Y
	 			$set_patient['DOB'] 			= @$dob[2].'-'.@$dob[1].'-'.@$dob[0];
	 			
	 			//$set_patient['medicare_number'] = $post['Medicare_Number_1'].$post['Medicare_Number_2'].$post['Medicare_Number_3'];
	 			$set_patient['medicare_number'] = $post['Medicare_Number'];
	 			$set_patient['medicare_ref'] 	= $post['Medicare_Ref'];
	 			$set_patient['medicare_expiry'] = $post['Expiry'];
	 			
	 			$set_patient['firstname'] 		= strtoupper($post['First_Name']);
	 			$set_patient['middleinitial'] 	= strtoupper($post['Middle_Initial']);
	 			$set_patient['lastname'] 		= strtoupper($post['Last_Name']);
	 			$set_patient['suburb'] 			= strtoupper(addslashes($post['Suburb']));
	 			$set_patient['street_addr'] 	= strtoupper(addslashes($post['Street_Address']));

	 			$med_items = '';
	 			$medicare_items_array = $this->Commonmodel->medicare_form_item_numbers();
	 			if( isset($post['items']) ){
	 				foreach ($post['items'] as $key=>$val) {

	 					if($key == 'other') {
	 						$med_items['other'] = @$post['items_other'];
	 					}else{
	 						$med_items[$key] = @$medicare_items_array[$key];	 						
	 					}

	 				}
	 				 
	 			}

	 			$set_appt['medicare_form_items'] = json_encode($med_items);

	 			$params['appt_id'] 		= $appt_id;
	 			$params['set_appt'] 	= $set_appt;

	 			$params['patient_id'] 	= $patient_id;
	 			$params['set_patient'] 	= $set_patient;

	 			//print_r($params);
	 			if( $this->appt->update_medicare($params) ){
	 				$this->session->set_flashdata('fmesg', 'Appt # '.$post['ref_number_formatted'].' Medicare Form successfully updated.');
 					


 					/*
 					//save pdf
 					$record = $this->appt->get_row_link_full($appt_id); 

 					$datef = date('dMy');

 					$filename = strtoupper($record->ref_number_formatted.'-'.$datef.$record->firstname.$record->middleinitial.$record->lastname.'MV');
 					$filename = $filename.'.pdf';
 					$html = $this->load->view('chaperone/medicare-pdf-tpl', $record, true);
		
					$pdfoutput = $this->pdfhelper->renderPDF($html, $filename.".pdf", false);

					//echo $pdfoutput;
					
					$data = 'Some file data';
					if ( ! write_file('./files/medicare/'.$filename, $pdfoutput))
					{
					        echo 'Unable to write the file';
					}
					else
					{
					        echo 'File written!';
					}*/

					//submit to dropbox
					/*
					if( isset($post['savedropbox']) ){
 						redirect(base_url().'dropboxupload/dropbox/1/'.$appt_id); 
					}else{
						redirect(base_url().'chaperone'); 
					}*/
					

	 			}else{
	 				$this->session->set_flashdata('fmesg', 'Appt # '.$post['ref_number_formatted'].' Medicare Form failed to update.');
	 			}

	 			redirect(base_url().'chaperone'); 

	 		}

	 		if( !isset($appt_id) ) throw new Exception("Appt must be empty", 1);

	 		$data['record'] = $this->appt->get_row_link($appt_id);

	 		$data['medicare_items'] = $this->Commonmodel->medicare_form_item_numbers();

			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/medicare-form';
			$this->view_data['menu_active'] = 'chaperone';

			$this->view_data['js_file'] = '<script src="assets/plugins/Touch-enabled-Signature-Plugin-with-jQuery-Canvas/jq-signature.min.js"></script>'.PHP_EOL;
			
			/*$this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/inputmask.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/inputmask.date.extensions.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/min/jquery.inputmask.min.js"></script>'.PHP_EOL;*/
			
			$this->view_data['js_file'] .= '<script src="assets/plugins/combodate.js"></script>'.PHP_EOL;

			$this->view_data['js_file'] .= '<script src="assets/js/mcautocomplete.js"></script>'.PHP_EOL;

			$this->view_data['js_file'] .= '<script src="assets/js/page_js/medicare_form.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/suburb_dropdown.js"></script>'.PHP_EOL;

			$this->load->view('index', $this->view_data);

		} catch (Exception $e) {
			echo $e->getMessage();
		}

 	}


 	public function search_patient(){
 		
 		

 		try {
 			
 			$data = '';

 			$records = '';

 			if($_GET){

 				
 

 			}


 			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/patient-search';
			$this->view_data['menu_active'] = 'chaperone';

			$this->view_data['js_file'] = '<script src="assets/js/page_js/patient_search.js"></script>'.PHP_EOL;

 			$this->load->view('index', $this->view_data);

 		} catch (Exception $e) {
 			echo $e->getMessage();	
 		}

 	}

 	public function verify_suburb(){

 		try {
 			
	 		$data = '';
	 		
	 		if(!$_POST) throw new Exception("Error Processing Request", 1);

	 		//print_r($_POST);

	 		$data['postdata'] = $this->input->post();

			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/patient-suburb';
			$this->view_data['menu_active'] = 'chaperone';

			$this->view_data['js_file'] = '<script src="assets/js/page_js/patient_search.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/suburb_search.js"></script>'.PHP_EOL;

			$this->load->view('index', $this->view_data);

 		} catch (Exception $e) {
 			echo $e->getMessage();
 		}


 	}

 	public function book(){
 		$this->load->model('Patientmodel', 'patient');
 		$this->load->model('Areamodel', 'areas'); 		 
 		$this->load->model('Dropdownmodel');
 		$this->load->helper('text');

 		try {
 			
	 		$data = '';
	 		
	 		if(!$_POST) throw new Exception("Error Processing Request", 1);

	 		//print_r($_POST);

	 		$post = $this->input->post();

	 		unset($post['btn_select']);

	 		$data['wyhaulist'] = $this->Dropdownmodel->get_results(array('where'=>array('dropdown_group'=>'WYHAU')), 'array');


	 		$data['patient_type'] = $post['patient_type'];

	 		$data['patient_id'] = @$post['patient_id'];

	 		$data['rel_appt_id'] = @$post['rel_appt_id'];

	 		$data['area_id'] = $post['area_id'];

	 		if( $post['patient_type'] == 'existing' ){

	 			$patient = $this->patient->get_row(array('patient_id'=>$post['patient_id']));


 				$patient->suburb 	= $post['suburb_name'];
 				$patient->postcode 	= $post['suburb_postcode'];
 				$patient->wdyhau    = 'Existing';

 				$data['patient'] = $patient;

	 			unset($post['First_Name']);
	 			unset($post['Last_Name']);

	 		}else{

				$patient = new stdClass();

 				$patient->firstname = $post['First_Name'];
 				$patient->lastname 	= $post['Last_Name'];
 				$patient->phone 	= '';
 				$patient->suburb 	= $post['suburb_name'];
 				$patient->postcode 	= $post['suburb_postcode'];
 				$patient->street_addr 	= '';
 				$patient->cross_street 	= '';

 				

				/*$patient->medicare_number 	= @$data['collection']['medicare_number'];

				$patient->medicare_ref 		= @$data['collection']['medicare_ref'];
				$patient->medicare_expiry 	= @$data['collection']['medicare_expiry'];
				$patient->dva 				= @$data['collection']['dva'];

				$patient->wdyhau 			= @$data['collection']['wdyhau'];
				$patient->latest_symptoms 	= @$data['collection']['latest_symptoms'];
				$patient->abotsi 			= @$data['collection']['abotsi'];
				$patient->regular_practice 	= @$data['collection']['regular_practice'];
				$patient->regular_doctor 	= @$data['collection']['regular_doctor'];*/

				$data['patient'] = $patient;
	 		}

	 		//$data['postdata'] = $post;


			$this->view_data['data'] = $data;
			$this->view_data['view_file'] = 'chaperone/patient-form';
			$this->view_data['menu_active'] = 'chaperone';

			//$this->view_data['js_file'] .= '<script src="assets/plugins/moment/en-au.js"></script>'.PHP_EOL;		
			$this->view_data['js_file'] .= '<script src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>'.PHP_EOL;						
			
			
			if( in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'localhost')) ){

				$this->view_data['js_file'] .= '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_7sWD-o0bYY8cQviHJFHDTrYxJpI5Nh8&sensor=tru';

			}else{

				$this->view_data['js_file'] .= '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>';
				
			}

			
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking_practice_autocomplete.js"></script>';

			$this->view_data['js_file'] .= '<script src="assets/js/page_js/patient_search.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/suburb_search.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/page_js/booking.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/plugins/inputmask/jquery.inputmask.bundle.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/plugins/combodate.js"></script>'.PHP_EOL;

			$this->view_data['css_file'] = '<link href="assets/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">';

			$this->load->view('index', $this->view_data);

 		} catch (Exception $e) {
 			echo $e->getMessage();
 		}

 	}

 	public function booking_submit(){

		try {

			$this->load->model('Patientmodel', 'patient');
 			$this->load->model('Appointmentmodel', 'appointment');
 			$this->load->model('Areamodel', 'areas');
			$this->load->library('Mailer', 'mailer');
			//$this->load->library('Smsglobal');

			if( !$_POST ) throw new Exception("this function accept on post", 1);
			
			if( !isset($_POST['area_id']) OR trim($_POST['area_id']) == '' ) throw new Exception("Area is missing, something went wrong on the process.", 1);
			
			$post = $this->input->post();


			$patient_id = $post['patient_id'];
			$area_id 	= $post['area_id'];

			$patient = '';

			if( $patient_id > 0 ){

				//$this->patient->get_row(array('patient_id'=>$patient_id));
			}

			//updating or creating a patient
			$patient_set['firstname'] 		= addslashes(strtoupper($post['First_Name']));
			$patient_set['lastname'] 		= addslashes(strtoupper($post['Last_Name']));
			$patient_set['phone'] 			= $post['Landline_Phone'];
			$patient_set['mobile_no'] 		= $post['Mobile_Phone'];

			$patient_set['em_contact_name'] = @$post['Emergency_Contact_Name'];
			$patient_set['em_contact_phone']= @$post['Emergency_Contact_Phone'];

			$patient_set['email'] 			= $post['Email'];
			$patient_set['street_addr'] 	= strtoupper($post['Address']);
			$patient_set['cross_street'] 	= addslashes(strtoupper($post['Cross_Street']));
			$patient_set['suburb'] 			= addslashes(strtoupper($post['Suburb']));
			$patient_set['postcode'] 		= $post['Post_Code'];

			$dob = explode('/', $post['DOB']);
			$patient_set['dob'] 			= (count($dob)==3)?$dob[2].'-'.$dob[1].'-'.$dob['0']:'';
			$patient_set['gender'] 			= $post['Gender'];
			$patient_set['medicare_number'] = $post['Medical_Number'];
			$patient_set['medicare_ref'] 	= $post['Medical_Reference'];
			$patient_set['medicare_expiry'] = $post['Medical_Expirey'];
			$patient_set['dva'] 			= isset($post['dva_card_gold'])?$post['dva_card_gold']:0;
			
			$patient_set['latest_symptoms'] = addslashes($post['Symptoms']);
			$patient_set['abotsi'] 			= $post['abotsi'];
			$patient_set['cultural_ethnic_bg'] = addslashes($post['cultural_ethnic_bg']);
			$patient_set['regular_practice']= strtoupper($post['search_practice']);
			$patient_set['regular_doctor'] 	= strtoupper($post['Regular_Doctor']);
			
			$patient_set['patient_notes'] 	= strtoupper($post['Patient_Notes']);

			$patient_set['health_insurance_provider'] = $post['Health_Insurance_Provider'];
			$patient_set['health_fund_number'] = $post['Health_Fund_Number'];


			//print_r($patient_set);exit;

			//add or update patient
			if( $patient_id > 0 ){				
				$patient_set['patient_updated'] = date('Y-m-d');
				$patient_id = $this->patient->update($patient_id, $patient_set);

				$appt_set['latest_wdyhau'] 	= $post['wdyhau'];

			}else{

				$patient_set['wdyhau'] 		= $post['wdyhau']; //add if first time

				$patient_id = $this->patient->add($patient_set);

				$appt_set['latest_wdyhau'] 	= addslashes($post['wdyhau']); //add to appointment
			}
 
			//setting an appointment
			$appt_set['appt_start'] = date('Y-m-d');

			/*if( $post['booking_when'] == 'nextday' ){

				$appt_set['appt_start'] = date('Y-m-d', strtotime('+1 day'));
			}*/

			$sched = $this->Commonmodel->get_schedules('','',$area_id);

			$appt_set['appt_created'] 		= $sched['appt_created'];
			$appt_set['prev_app_created'] 	= date('Y-m-d H:i:s');			

			$appt_set['area_id'] 		= $area_id;
			$appt_set['patient_id'] 	= $patient_id;
			$appt_set['symptoms'] 		= addslashes($post['Symptoms']);
			
			$appt_set['book_by'] 		= $this->agent_name;
			$appt_set['book_by_user_id'] = $this->user_id;
			$appt_set['booking_when'] 	= 'today'; //unset after inserted to transaction table
			$appt_set['tran_type'] 		= 'Booking Made - Chaperone'; //mostly this coming from midnight button
			
			$appt_set['firstname'] 	= $post['First_Name'];
			$appt_set['lastname'] 	= $post['Last_Name'];
			$appt_set['phone'] 		= $post['Landline_Phone'];
			$appt_set['suburb'] 	= $post['Suburb'];

			
			
			//some filter here
			 
			if( isset($post['rel_appt_id']) AND !in_array(trim($post['rel_appt_id']), array('','0')) ){
				
				$appt_set['related_appt_id'] = $post['rel_appt_id'];

				//$patient_id = $post['rel_appt_id'];

			}



			$appt_id = $this->appointment->add($appt_set);


			//parent
			//set parent thier own appt_id so that we can detect right away rather doing query
			if( isset($post['rel_appt_id']) AND !in_array(trim($post['rel_appt_id']), array('','0')) ){				

				$this->appointment->set_parent_related_appt_id($post['rel_appt_id']);
			}

			$tran = $this->appointment->get_row_link_full($appt_id);



			//email HCD for new practice
			if( trim($post['email_to_hcd']) != '' ){
				//echo HCD HERE;
				 
				/*$to = $this->Commonmodel->get_admin_email();

				$body = "The Regular Practice below does not exist on current list".PHP_EOL.PHP_EOL;
				$body .= strtoupper($post['search_practice']).PHP_EOL;
				$body .= PHP_EOL;
				$body .= "sent via housecalldoctor.welldone.net.au";

				$subject = "New Regular Practice";

				$this->mailer->send_to = $to;
				$this->mailer->Subject = $subject;
				$this->mailer->Body = stripslashes(nl2br($body));

				$mail_return = $this->mailer->sendMail();

				//audit log
				$audit['tran_id'] 		= $tran->tran_id;
				$audit['audit_to'] 		= $to;
				$audit['audit_type'] 	= 'email';
				$audit['message'] 		= addslashes($body);
				$audit['more_info'] 	= 'New Regular Practice';
				$audit['return_message']= $mail_return;
				$this->Commonmodel->insert_audit_trail($audit);	*/			
			}


			//check if within the period, then send sms
			if( $sched['isPeriod'] ){

				$sms_cars = $this->areas->get_cars_result(array('area_id'=>$area_id)); 		


				$area_row = $this->areas->get_row(array('area_id'=>$area_id));

				
				//SEND EMAIL
				
						$email_body  =  "<strong>Appointment Details</strong> <br/><br/>";				
						$email_body .= "Area :".$area_row->area_name.'<br/>';
						$email_body .= "Date/Time Booked :".date('d/m/Y H:i:s', strtotime($tran->appt_created_tz)).'<br/><br/>';

						$email_body .=  "<strong>Patient Details</strong> <br/><br/>";				
						$email_body .= "Name : {$patient_set['firstname']}  {$patient_set['lastname']}".'<br/>';				
						$email_body .= "Phone : {$patient_set['phone']}".'<br/>';
						$email_body .= "Mobile No : {$patient_set['mobile_no']}".'<br/>';
						$email_body .= "Address : {$patient_set['street_addr']}".'<br/>';
						$email_body .= "Cross Street : {$patient_set['cross_street']}".'<br/>';
						$email_body .= "Suburb : {$patient_set['suburb']}".'<br/>';
						$email_body .= "Date of Birth : {$patient_set['dob']}".'<br/>';
						$email_body .= "Age : {$post['age']}".'<br/>';
						$email_body .= "Gender : {$patient_set['gender']}".'<br/>';
						$email_body .= "Symptoms : {$patient_set['latest_symptoms']}".'<br/>';
						$email_body .= "Medicare Number : {$patient_set['medicare_number']}".'<br/>';
						$email_body .= "Medicare Ref : {$patient_set['medicare_ref']}".'<br/>';
						$email_body .= "Medicare Exp : {$patient_set['medicare_expiry']}".'<br/>';
						$email_body .= "DVA : ".isset($post['dva_card_gold'])?"Yes":"No".'<br/>';
						$email_body .= "Regular Practice : {$patient_set['regular_practice']}".'<br/>';
						$email_body .= "Regular Doctor : {$patient_set['regular_doctor']}".'<br/>';
						$email_body .= '<br/>';
						$email_body .= SENT_VIA;
				

				//SEND SMS to cars			 
				$sms_cars = $this->areas->get_cars_result(array('area_id'=>$area_id)); 					
				$sms_body = 'Booking Made: - '.$patient_set['firstname'].' '.$patient_set['lastname'].' '.stripslashes(@$patient_set['street_addr']).' '.stripslashes(@$patient_set['suburb']);


				$email_subject = 'Booking Made: - '.$patient_set['firstname'].' '.$patient_set['lastname'].' '.stripslashes(@$patient_set['street_addr']).' '.stripslashes(@$patient_set['suburb']);



				foreach($sms_cars as $row ){ 

					$row->car_mobile = str_replace(' ', '', $row->car_mobile);

					//if( $row->car_mobile != '' ){
					if( $row->car_mobile != ''  && intval($row->send_text)==1){
								
						//$smsglobal = $this->smsglobal->sendSms($row->car_mobile, $sms_body);
						$send_sms = $this->Commonmodel->sendSms($row->car_mobile, $sms_body);

						$audit['tran_id'] 		= $tran->tran_id;
						$audit['audit_to'] 		= $row->car_mobile;
						$audit['audit_type'] 	= 'sms';
						$audit['message'] 		= $sms_body;							
						$audit['return_message']= ($send_sms == 'message_sent')?'SMS - SENT':$send_sms;
						$this->Commonmodel->insert_audit_trail($audit);

					}
					//END SEND SMS


					if( $row->car_email != ''  && intval($row->send_email)==1){
								
						$this->mailer->send_to = $row->car_email;
						$this->mailer->Subject = $email_subject;
						$this->mailer->Body = stripslashes(nl2br($email_body));

						$mail_return = $this->mailer->sendMail();

						$audit['tran_id'] 		= $tran->tran_id;
						$audit['audit_to'] 		= $row->car_email;
						$audit['audit_type'] 	= 'email';
						$audit['message'] 		= addslashes($email_body);	
						$audit['more_info'] 	= $email_subject;					
						$audit['return_message']= $mail_return;
						$this->Commonmodel->insert_audit_trail($audit);

					}

					//END SEND EMAIL


				}
 			}

 			
			$this->session->set_flashdata('fmesg', 'Appointment Successfully book');
			redirect(base_url().'chaperone/');

		} catch (Exception $e) {
			echo 'Error: '.$e->getMessage();
		}

 	}
}
