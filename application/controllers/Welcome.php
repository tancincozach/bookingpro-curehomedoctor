<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		
		//$this->load->view('index', $this->view_data);
		$get = '';

		if($_GET){
			$get = $this->input->get();
			$get = '?'.http_build_query($get);
		}

		if( $this->session->userdata('logged_in') ){

			if($this->session->userdata('user_lvl') == 5 AND $this->session->userdata('user_sub_lvl') == 1) { //chaperone
				redirect(base_url().'chaperone');
			}elseif($this->session->userdata('user_lvl') == 5 AND $this->session->userdata('user_sub_lvl') == 2) { //doctor
				redirect(base_url().'doctor');
			}else{
				redirect(base_url().'dashboard/'.$get);
			}
		}



		redirect(base_url().'login/'.$get);
	}

	public function login(){

		$data = '';

		if( $_POST ){
			$post = $this->input->post();

			$youruname 	= $this->input->post('youruname', TRUE);
			$yourpass 	= $this->input->post('yourpass', TRUE);
			$login_type 	= $this->input->post('login_type');

			$yourpass = md5($yourpass);

			if( $post['login_type'] == 'client' ){ //LOCAL user login

				$this->db->select('user_id AS id, user_name as username, user_pass as password, user_level AS user_lvl, user_sublevel, user_opts, user_fullname');
				$this->db->where('LOWER(user_name)', strtolower(trim($youruname)));
				$this->db->limit(1);

				$user = $this->db->get('user')->row();

				if( $user->user_lvl != 5 ){
					$user->user_sublevel = '';
				}

			}elseif ( $post['login_type'] == 'callcenter' ) { //CMA login
				
				$this->db->select('user_id AS id, username, password, userlevel AS user_lvl');
				$this->db->where('LOWER(username)', strtolower(trim($youruname)));
				$this->db->limit(1);

				$user = $this->db->get('user_cma')->row();
				$user->user_sublevel = '';
				$user->user_fullname = $user->username;

				$sess['agent_id'] = $user->id; //log agent_id

			}else {
				$this->session->set_flashdata('login_message', $login_type.'Error login attempt!!!'); 
				redirect(base_url().'login');
			}

			if( isset($user->username) AND $user->password == $yourpass ){

				$sess['logged_in'] 		= 1; 
				$sess['user_id'] 		= $user->id;
				$sess['user_lvl'] 		= $user->user_lvl;
				$sess['user_sub_lvl'] 	= @$user->user_sublevel;
				$sess['user_perms'] 	= json_decode($user->user_opts);
				$sess['agent_name'] 	= $user->user_fullname;

				$this->session->set_userdata($sess);

				
				if( $user->user_lvl == 5 AND  $user->user_sublevel == 1) { //chaperone
					redirect(base_url().'chaperone');
				}else if($user->user_lvl == 5 AND  $user->user_sublevel == 2){ //doctor
					redirect(base_url().'doctor');
				}else{
					redirect(base_url().'dashboard');
				}

			}else{

				$this->session->set_flashdata('login_message', $login_type.'|Username or Password is Incorrect');
				redirect(base_url().'login');

			}

		}

		$this->load->view('login/index', $data);
	}

	public function autologin(){

		if(isset($_GET['user']) AND $_GET['user']!=''){

			$get = $this->input->get();

			$user_id = $get['user'];
			$phone = $get['phone'];

			$this->db->select('user_id AS id, username, userlevel AS user_lvl, deleted');
			$this->db->where('user_id', $user_id);
			$this->db->where('deleted', 0);
			$this->db->limit(1);

			$user = $this->db->get('user_cma')->row();

			//print_r($user);
			if( count($user) == 1 ){

				$sess['logged_in'] 		= 1; 
				$sess['user_id'] 		= $user->id;
				$sess['user_lvl'] 		= $user->user_lvl;
				$sess['user_sub_lvl'] 	= '';
				$sess['user_perms'] 	= json_decode(array('edit', 'delete'));
				$sess['agent_name'] 	= $user->username;
				$sess['agent_id'] 		= $user->id; //log agent_id

				$this->session->set_userdata($sess);
				redirect(base_url().'dashboard/?phone='.$phone);

			}else{

				$this->session->set_flashdata('login_message', 'Username or Password is Incorrect');
				redirect(base_url().'login');

			}

		}

	}

	public function reset_password(){
		$data = '';
		$this->load->view('login/reset-password', $data);
	}

	public function logout(){

		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		
		$this->session->set_flashdata('login_message', 'You have succesfully logout'); 

		redirect(base_url().'login');
	}
}
