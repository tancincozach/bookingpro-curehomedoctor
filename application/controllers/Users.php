<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {


	public function __construct()
	{
		parent::__construct();		

		$this->view_data['container'] = 'container';
	}

	public function index()	 
	{
 		$data = array();

 		/*$get = $this->input->get();
 		$data = array_merge($data, $get);*/

		 
		$this->view_data['data']['getvars'] = $data;
		$this->view_data['view_file'] = 'chaperone/index';
		$this->view_data['menu_active'] = 'chaperone';
		//$this->view_data['js_file'] = '<script src="assets/js/mcautocomplete.js"></script>';
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/booking.js"></script>';
		$this->load->view('index', $this->view_data);

 	}
}
