<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropboxupload extends MY_Controller {

	public function __construct()
	{
		parent::__construct();		

	}
 
 	public function dropbox($p=1, $appt_id=''){

 		try {
 			
 		
			$this->load->helper('file');
			$this->load->library("pdfhelper");		

			$this->load->model('Appointmentmodel', 'appt'); 		
			$this->load->model('Areamodel', 'area'); 		

			$record = $this->appt->get_row_link_full($appt_id);
	 
			$car = $this->area->get_row_by_car(array('car_id'=>$record->car_id));

			//block here if car is not set

			if( count($car) == 0 ) throw new Exception("Car is empty", 1);
			

			/*echo '<pre>'; 
			print_r($car);
			echo '</pre>'; */

			//$params['key'] = '1urjc7l5cn672ns';
			//$params['secret'] = 'x5l38brdo067zml';

			// if( $car->dropbox_key == '' ) throw new Exception("Dropbox App Key is not set", 1);
			// if( $car->dropbox_secret == '' ) throw new Exception("Dropbox App Secret is not set", 1);


			/*$params['key'] 		= $car->dropbox_key;
			$params['secret'] 	= $car->dropbox_secret;*/

			/*$params['key'] = '1urjc7l5cn672ns';
			$params['secret'] = 'x5l38brdo067zml';*/
			

			//Ch1.hb@housecalldoctor.com.au
			//Ch1hb14

			$params['key'] = 'rrkx55ui76m0y6h';
			$params['secret'] = '2vp2xz6rg0erbm6';

			/*if( isset($this->session->userdata('oauth_token_secret')) ){
				$p = 3;
			}*/

			if( $p == 1 ){

				$this->load->library('dropbox', $params);

				$data = $this->dropbox->get_request_token(site_url("dropboxupload/dropbox/2/$appt_id/"));
				 
				
				$this->session->set_userdata('token_secret', $data['token_secret']);

				redirect($data['redirect']);
			}
			
			if( $p == 2 ){

				$uri = $this->uri->segment(3);

				$this->load->library('dropbox', $params);

				$oauth = $this->dropbox->get_access_token($this->session->userdata('token_secret'));
	 
	 			$this->session->set_userdata('oauth_token', $oauth['oauth_token']);
				$this->session->set_userdata('oauth_token_secret', $oauth['oauth_token_secret']);

				redirect('dropboxupload/dropbox/3/'.$appt_id);

				 
			}

			if( $p == 3 ){



				$datef = date('dMy');

				/*$filename = strtoupper($record->ref_number_formatted.'-'.$datef.$record->firstname.$record->middleinitial.$record->lastname.'MV');
				$filename = preg_replace("/\\W|_/", '', $filename); //remove non alphanumeric
				$filename = $filename.'.pdf';*/

			
				$fdoc = str_replace(array('DR',' '), '', strtoupper(@$record->doctor_name));
				$fdoc = preg_replace("/\\W|_/", '', $fdoc); //remove non alphanumeric
				
				$fpatient = strtoupper($record->firstname.$record->middleinitial.$record->lastname);
				$fpatient = preg_replace("/\\W|_/", '', $fpatient); //remove non alphanumeric

				$filename = $datef.' '.$fdoc.' - '.$fpatient.'MV';

				$filename = $filename.'.pdf';

				$html = $this->load->view('chaperone/medicare-pdf-tpl', $record, true);
	
				$pdfoutput = $this->pdfhelper->renderPDF($html, $filename, false);			 

				if ( ! write_file('./files/medicare/'.$filename, $pdfoutput)) {

				       throw new Exception("Error: Unable to write pdf file.", 1);

				}else{

					$params['access'] = array('oauth_token'=>urlencode($this->session->userdata('oauth_token')),
										  'oauth_token_secret'=>urlencode($this->session->userdata('oauth_token_secret')));
				
					$this->load->library('dropbox', $params);				 
	 			 
					if( trim($car->car_name) == '' ) $car->car_name = 'others';

					$response = $this->dropbox->add('/'.$car->car_name, 'files/medicare/'.$filename);


					if( isset($response->path) AND $response->path != '' ){
						
						//delete file here;
						unlink('./files/medicare/'.$filename);

						$this->session->set_flashdata('fmesg', 'Appt # '.$record->ref_number_formatted.' Medicare Form successfully UPLOADED to dropbox.');
						redirect(base_url().'chaperone');


					}else{
						throw new Exception("Unable to save on dropbox", 1);
						
					}
				}
 

			}
	 
		} catch (Exception $e) {
 			echo $e->getMessage();
 		}
 	}
 
 	
}
