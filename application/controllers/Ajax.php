<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {
	
	//public $default_timezone = 'Australia/Queensland';
	public $default_timezone = 'Australia/Sydney';
	
	public function __construct(){
		parent::__construct();

		//prevent from direct access
		$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
		strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
		if(!$isAjax) {
		  //$user_error = 'Access denied - not an AJAX request...';
		  //$user_error = 'Not Found';
		  //trigger_error($user_error, E_USER_ERROR);
		  
		    header('HTTP/1.0 404 Not Found');
		    echo "<h1>404 Not Found</h1>";
		    echo "The page that you have requested could not be found.";		  
		  
		  	exit();
		}	
 

	}

	public function build_where($params){

		$params['where_str'] = '';
		$search = @$params['search'];
		
		if( isset($params['area']) AND count($params['area'])>0 ){ 
			 
			if( count($params['area']) == 1 ){
				$params['where'] = array('areas.area_id'=>$params['area'][0]);
			}else{
				if( trim($params['where_str']) != '' ) $params['where_str'] .= ' AND ';
				$params['where_str'] .= ' `areas`.`area_id` IN ('.implode(',',array_values($params['area'])).')';				
			}
		}


		return $params;
	}



	public function suburb_availability(){


		$this->load->model('Areamodel', 'areas');
		$this->load->model('Commonmodel');

		$params= array();

		if($this->user_lvl.$this->user_sub_lvl == 53){


			$areas 	=  $this->areas->get_areas_by_chaperone($this->user_id, 'array');

			$filter_area_client_admin = array();

			foreach ($areas as $i=>$value) {

				$filter_area_client_admin[] = $i;
								
			}

			$params['area'] = $filter_area_client_admin;			

		}

		$params_query = $this->build_where($params);


		$search = $this->input->get('name_startsWith');
		
		$now_date = $this->Commonmodel->get_server_converted_date($this->default_timezone, '%Y-%m-%d %H:%i:%s');

		$result = $this->areas->get_suburb_available($search, $now_date, $params_query);
		
		$json = array();
		$json['total'] = count($result);
		$data = array();
		if(count($result) > 0){
			 
			foreach( $result as $row ){
				$data_row['id'] = $row->suburb_id;
				$data_row['suburb'] = stripslashes($row->suburb_name);
				$data_row['postcode'] = stripslashes($row->suburb_postcode);
				$data_row['area'] = stripslashes($row->area_name);
				$data_row['open_status'] = $row->open_status?'Open':'Closed';
				$data_row['area_id'] = $row->area_id;
				$data_row['area_avail_id'] = $row->area_avail_id;
				$data_row['value'] = $row->suburb_id;
				$data_row['region_type'] = $row->region_type;
				 
				$data[] = $data_row;
			}
		} 
		$json['data'] = $data;
		echo json_encode($data);

	}


	public function check_schedule($region_type){

		$this->load->model('Commonmodel', 'common');		
		$this->load->model('Areamodel', 'areas');

		$get = $this->input->get();
 
		$area = $this->areas->get_row(array('area_id'=>$get['area_id']));
		$custom_sched_closed = in_array(trim(@$area->custom_sched_closed), array('', ' ', '00:00:00'))?'':$area->custom_sched_closed;


		$schedule = $this->common->check_out_of_schedule($region_type, @$custom_sched_closed);

		$uri = http_build_query($schedule);

		$schedule['uri'] = $uri;

		echo json_encode($schedule);

	}


	public function search_patient_match(){

		$this->load->model('Patientmodel', 'patient');

		$search = trim($this->input->get('term'));

		$data = array();

		if( !empty($search) ){

			$search = explode(',', $search);

			$where_str = '';
			if( count($search) > 1 ){
				if( !empty($search[0]) ) $where_str .= " lower(lastname) LIKE '%".strtolower($search[0])."%' ";
				if( !empty($search[1]) ) $where_str .= " AND lower(firstname) LIKE '%".strtolower($search[1])."%' ";
			}else{				
				$where_str .= " lower(lastname) LIKE '%".strtolower($search[0])."%' ";
				$where_str .= " OR lower(firstname) LIKE '%".strtolower($search[0])."%' ";
			}

			$params['where_str'] = $where_str;
			$params['where']['patient_status'] = 1;
			$result = $this->patient->get_result($params);
			//echo $this->db->last_query();
			//print_r($result);

			if( count($result) > 0 ){
				foreach( $result as $row ){
					$data_row['id'] = $row->patient_id;
					/*$data_row['firstname'] 	= stripslashes($row->firstname);
					$data_row['lastname'] 	= stripslashes($row->lastname);
					$data_row['dob'] 		= stripslashes($row->dob);
					$data_row['phone'] 		= stripslashes($row->phone);*/
					$data_row['value'] 		= $row->patient_id;
					$data_row['label'] 		= stripslashes($row->lastname).','.stripslashes($row->firstname);
					$data_row['desc'] 		= '<span class="autocomplete-patient-desc">DOB: '.stripslashes($row->dob).'<br />PH:'.stripslashes($row->phone).'</span>';
					 
					$data[] = $data_row;
				}
				$data_row['id']			= '';
				$data_row['value'] 		= '';
				$data_row['label'] 		= 'Create New';
				$data_row['desc'] 		= 'Press down arrow key and enter or click me to create new record';
				 
				$data[] = $data_row;				
			}else{					
					$data_row['value'] 		= '';
					$data_row['label'] 		= 'No match found.';
					$data_row['desc'] 		= 'Press down arrow key and enter or click me to create new record';
					 
					$data[] = $data_row;				
			}
		}

		//$json['data'] = $data;
		echo json_encode($data);
	}


	public function search_practice_match(){

		$this->load->model('Practicesmodel', 'practice');

		$search = trim($this->input->get('term'));

		$data = array();

		if( !empty($search) ){

			$where_str = '';

			$where_str .= " lower(practice_name) LIKE '%".strtolower($search)."%' "; 

			$params['where_str'] = $where_str;
			$params['where']['practice_status'] = 1;

			$result = $this->practice->get_result($params);
			//echo $this->db->last_query();
			//print_r($result);

			if( count($result) > 0 ){
				foreach( $result as $row ){
					$data_row['id'] 		= $row->id;					 
					$data_row['value'] 		= $row->practice_name;
					$data_row['label'] 		= stripslashes($row->practice_name);
					$data_row['desc'] 		= '';
					 
					$data[] = $data_row;
				}
				 
				$data[] = $data_row;				
			}else{					
					$data_row['value'] 		= '';
					$data_row['label'] 		= 'No match found.';
					$data_row['desc'] 		= 'Click me to copy the Regular Practice to NO MATCH field';
					 
					$data[] = $data_row;				
			}
		}

		//$json['data'] = $data;
		echo json_encode($data);
	}


	/* Asigning an area availability */

	public function set_availability_modal( ){

		$this->load->model('Areamodel', 'areas');

		$get = $this->input->get();

		$area_id = $get['area_id'];
		$agent_name = $get['agent_name'];
		$user_id = $get['user_id'];

		$data['area_id']	= $area_id;
		$data['agent_name']	= $agent_name;
		$data['user_id']	= $user_id;
		$data['record'] = $this->areas->get_row(array('area_id'=>$area_id));
		$data['availability'] = $this->areas->get_areas_availability_row(array('area_id'=>$area_id), 'result', 'area_avail_created DESC');

		//echo $this->db->last_query();
		echo $this->load->view('maintenance/area_availability_modal', $data, true);

	}

	/**
	 * [set_availability2 version 2 for adding availability of the areas with reopen date/time rang
	 */
	public function  set_availability2()
	{
		$this->load->model('Areamodel', 'areas');

		$data = array();

		$post = $this->input->post();

		try {
			 
			$set['area_id'] 	= $post['area_id'];
			$set['user_id'] 	= $post['user_id'];
			$set['agent_name'] 	= $post['agent_name'];
			$set['is_open'] 	= $post['is_open'];

			if( isset($post['more_info']) ){
				$set['more_info'] 	= addslashes($post['more_info']);				
			}
			
			if( $post['is_open'] == '0'){


				if(isset($post['reopen_dt_start'])){

					if( trim($post['reopen_dt_start']) != ''  ){				
						$dt_start_b = explode(' ', $post['reopen_dt_start']);
						$dt_start_d = explode('/', $dt_start_b[0]); 
						$reopen_dt_start = $dt_start_d[2].'-'.$dt_start_d[1].'-'.$dt_start_d[0].' '.$dt_start_b[1].':00';
						$set['reopen_dt_start'] = $reopen_dt_start;


						if( trim($post['reopen_dt_end']) != ''  ){				
							$dt_end_b = explode(' ', $post['reopen_dt_end']);
							$dt_end_d = explode('/', $dt_end_b[0]); 
							$reopen_dt_end = $dt_end_d[2].'-'.$dt_end_d[1].'-'.$dt_end_d[0].' '.$dt_end_b[1].':00';;
							$set['reopen_dt_end'] 	= $reopen_dt_end;			
						}
					}
				}else{

						$now_date = date('Y-m-d');
						$now_time = date('H:i:s');

						$area_row = $this->areas->get_row(array('area_id'=>$set['area_id']));

						$this->load->model('Commonmodel', 'common');

						$now_dt  = $now_date.' '.$now_time;
						$now_day = date('D', strtotime($now_dt));

						$set['reopen_dt_start']  = date('Y-m-d H:i:s');

					if( $area_row->region_type == 'METRO' ){
						
						if( !$this->common->is_holiday_today($now_date) AND in_array( $now_day, array('Sun','Mon','Tue','Wed','Thu'))  ){
						
							if(strtotime($now_dt) < strtotime($now_date.' 16:00:00')){
								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date)).' 16:00:00'; 
							}else{

								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 16:00:00'; 
							}
						
						//If Friday night - RE-OPEN at 10am next morning
						}elseif( !$this->common->is_holiday_today($now_date) AND $now_day == 'Fri'){

							if( (strtotime($now_dt) < strtotime($now_date.' 12:00:00'))){

								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date)).' 12:00:00';

							}else{

								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 12:00:00';
								
							}
							

						//If Saturday night or Public Holiday - RE-OPEN at 6am next morning
						}elseif( ($now_day == 'Sat' OR $this->common->is_holiday_today($now_date) )){

									
							$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 06:00:00'; 


					    }

					}else{



						if( !$this->common->is_holiday_today($now_date) AND in_array( $now_day, array('Sun','Mon','Tue','Wed','Thu'))  ){
					

							if(strtotime($now_dt) < strtotime($now_date.' 18:00:00')){
								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date)).' 18:00:00'; 
							}else{

								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 18:00:00'; 
							}
						
						//If Friday night - RE-OPEN at 10am next morning
						}elseif( !$this->common->is_holiday_today($now_date) AND $now_day == 'Fri'  ){
							
								if(strtotime($now_dt) < strtotime($now_date.' 12:00:00')){
								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date)).' 12:00:00'; 
							}else{

								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 12:00:00'; 
							}

						//If Saturday night or Public Holiday - RE-OPEN at 6am next morning
						}elseif( ($now_day == 'Sat' OR $this->common->is_holiday_today($now_date) ) ){
							
								$set['reopen_dt_end'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 06:00:00'; 

						}


					}
				}

/*							echo '<pre>';
							print_r($set);
							echo '<pre/>';
							exit();	*/
							

			}


			$area_avail_id = $this->areas->check_and_insert_areas_availability2($set);



			$data['status'] = ($area_avail_id>0)?1:0;
			$data['status_msg'] = ($area_avail_id>0)?'Inserted status availability successfully': 'Something went wrong';
			$data['new_sechedule'] = '';

			if( $area_avail_id > 0 ){

				$available = $this->areas->get_areas_availability_row(array('area_avail_id'=>$area_avail_id), 'row', "area_avail_created DESC");

				if( isset($available->area_avail_id) ){					
					$data['new_sechedule'] = '<tr> 
						<td>'.(($available->is_open)?'<span class="text-success">OPEN</span>':'<span class="text-danger">CLOSED</span>').'</td>
						<td>'.(($available->reopen_dt_start != '' AND $available->reopen_dt_start != '0000-00-00 00:00:00')?date('d/m/Y H:i', strtotime($available->reopen_dt_start)):'').'</td>
						<td>'.(($available->reopen_dt_end != '' AND $available->reopen_dt_end != '0000-00-00 00:00:00')?date('d/m/Y H:i', strtotime($available->reopen_dt_end)):'').'</td>
						<td>'.$available->agent_name.'</td>
						<td>'.$available->area_avail_created.'</td>
						<td>'.stripslashes($available->more_info).'</td>
						<td></td>
					</tr>';
				}
			}

			echo json_encode($data);

		} catch (Exception $e) {
			
		}
 		 
 	   
	}

	public function  set_availability()
	{
		$this->load->model('Areamodel', 'areas');

		$data = array();

		$post = $this->input->post();

		if(!empty($post['effective_date']) && !empty($post['area_id']))
		{
			$effective_date = explode('/', $post['effective_date']);
			$effective_date = $effective_date[2].'-'.$effective_date[1].'-'.$effective_date[0];

			/*array('area_id'=>$post['area_id'],
				'additional_info'=>array('user_id'=>@$post['user_id'],
				'agent_name'=>@$post['agent_name']),
				'effective_date'=>date('Y-m-d',strtotime($post['effective_date']))
			)*/
			$params = array(
				'area_id'=>$post['area_id'],
				'additional_info'=>array(
					'user_id'=>@$post['user_id'],
					'agent_name'=>@$post['agent_name']
				),
				'effective_date'=>$effective_date
			);

			if( $return_value = $this->areas->check_and_insert_areas_availability($params) )
			{

				if(is_array($return_value) && array_key_exists('error', $return_value))
				{
					$data['error'] = $return_value['error'];
				}
				else
				{
					$data['msg'] = 'You have successfully set an area area availability';	 	
				}
			}
		}

		echo json_encode($data);	   
	}

	public function reopen_availability()
	{

	 $this->load->model('Areamodel', 'areas');

	 $post = $this->input->post();

	 $data = array();

	 if(!empty($post['avail_id']) && $post['avail_id']!=0)
	 {
	 	$areas_availability_info = $this->areas->get_filtered_areas_availability(array('area_avail_id'=>$post['avail_id']),array('is_open','area_avail_updated'));

	   if($this->areas->update_areas_availability( array('is_open'=>1 ),$post['avail_id'])  == 0)
	   {
	
	   	 $data['error'] = 'Error: Unable to reopen the availability of this area.';
	   }
	   else
		{
		 $areas_availability_info['is_open'] = 1;
	     $this->areas->insert_audit_trail( array(
						'target_table'=>'areas_availability',
						'table_ref_col'=>'area_id',
						'table_ref_id'=>$areas_availability_info['area_id'],
						'table_action'=>'Update',
						'user_id'=>@$post['user_id'],
						'agent_name'=>@$post['agent_name'],
						'message'=> json_encode($areas_availability_info)
						)	
				  );
	   	$data['msg'] = 'You have reopened the availability of this area.';
	   }

	 }
	 echo json_encode($data);
	}

	public function set_area_timezone(){

	   	$this->load->model('Areamodel', 'areas');

		$post = $this->input->post();



		 $data = array();

		 if(!empty($post['area_id']) && $post['area_id']!=0)
		 {
		 	$return_value = $this->areas->update($post['area_id'],array('user_id'=> $post['timezone'],'agent_name'=> $post['agent_name'],'timezone'=> $post['timezone']));
		 	
	 		  if(is_array($return_value) && array_key_exists('error', $return_value))
		   	  {
		   	 	$data['error'] = $return_value['error'];
		   	  }
		   	  else{

		   	  	  	$this->areas->insert_audit_trail( array(
									'target_table'=>'areas_availability',
									'table_ref_col'=>'area_id',
									'table_ref_id'=>$post['area_id'],
									'table_action'=>'Update',
									'user_id'=>@$post['user_id'],
									'agent_name'=>@$post['agent_name'],
									'message'=> json_encode(array('timezone'=> $post['timezone']))
									)	
							  );
		 			  	$data['msg'] = 'You have asigned timezone to this area.';
		   	  }		

	 	}
	 
		echo json_encode($data);

	}

	public function set_region_type(){

	   	$this->load->model('Areamodel', 'areas');

		$post = $this->input->post();
 		
 		$area_id = @$post['id'];
 		$region_type = @$post['region'];
 		$agent_name = @$post['agent_name'];
 		$user_id = @$post['user_id'];

		$data = array('status'=>0);

		if( !empty($area_id) && $area_id !=0 ){

			$set['region_type'] = $region_type;
			$set['agent_name'] = $agent_name;
			$set['user_id'] = $user_id;

		 	$return_value = $this->areas->update($area_id, $set);

	 		if(is_array($return_value) && array_key_exists('error', $return_value)){
		   	 	
		   	 	$data['error'] = $return_value['error'];		   	 	 

		   	}else{

	   	  	  	$this->areas->insert_audit_trail( array(
								'target_table'=>'areas_availability',
								'table_ref_col'=>'area_id',
								'table_ref_id'=>$area_id,
								'table_action'=>'Update',
								'user_id'=>@$user_id,
								'agent_name'=>@$agent_name,
								'message'=> json_encode(array('region_type'=> $region_type))
								)	
						  );
 			  	$data['msg'] = 'Updated to '.$region_type;
 			  	$data['status'] = 1;

		   	}		

	 	}

	 	echo json_encode($data);
	}


	/*public function suburb(){

		$this->load->model('Areamodel', 'suburb' );

		$search = $this->input->get('name_startsWith');
		//$search = $this->input->get('term');
 		 
 		$result = $this->suburb->search($search);

		$json = array();
		$json['total'] = $result['total'];
		$data = array();
		if($result['total'] > 0){
			 
			foreach( $result['data'] as $row ){
				$data_row['id'] = $row->id;
				$data_row['suburb'] = stripslashes($row->suburb);
				$data_row['postcode'] = stripslashes($row->postcode);
				$data_row['council'] = stripslashes($row->council);
				$data_row['value'] = $row->id;
				 
				$data[] = $data_row;
			}
		} 
		$json['data'] = $data;
		echo json_encode($data);
	}*/


	/**
	 * [get_cars_per_area description]
	 * @return options list
	 */
	function get_cars_per_area(){
		
		$this->load->model('Areamodel', 'areas');

		if( $_POST ){
			$post = $this->input->post();

			$cars = $this->areas->get_cars_by_area_id($post['id'], 'array');
			//echo $this->db->last_query();
			$options = '';
			foreach ($cars as $key=>$value) {
				 $options .= '<option value="'.$key.'">'.stripslashes($value).'</option>';
			}

			echo $options;
		}
	}
 

	public function get_chaperone()
	 {
		 $this->load->model('Usermodel', 'users');

		 $area_id = $this->input->post('area_id');

	 	 $return_value = $this->users->get_chaperone_by_area($area_id);
	 	 

	 	 $output = array();

		 $output['total'] = count($return_value);

	 	 if(is_array($return_value) && array_key_exists('error', $return_value))
	   	 {
	   	  $output['error'] = $return_value['error'];
	   	 }
	   	 else
	   	 {
	 	   	foreach($return_value as $row)
	 	  	{
	   		 $output['result'][] = array('user_id'=>$row->user_id,'user_name'=>$row->user_name,'user_fullname'=>$row->user_fullname,'is_selected'=>$row->is_selected);
	 	  	}	 	  
	   	 }		
	 	 echo json_encode($output);
	  }
	  public function assign_chaperone_per_area()
	  {

		$this->load->model('Areamodel', 'area');

	  	$post   = $this->input->post();	 

	  	$output = array(); 	

	    if(isset($post['unselected']) &&  count($post['unselected']) > 0)
	    {
			$this->area->remove_chaperone_by_user( array('area_id'=>$post['area_id'],'user_id'=>$post['unselected']));			
	    }
	    
	    if(isset($post['selected'])  &&  count($post['selected']) > 0)
	    {	    
	      $this->area->insert_chaperone_by_user( array('area_id'=>$post['area_id'],'user_id'=>$post['selected']));			
	    }	

    	 $output['msg'] = 'You have successfully asigned a chaperone.';

    	  echo json_encode($output);
	  }
	  public function get_contact_by_id()
	  {
		$this->load->model('Contactmodel','contact');

		$contact_id = $this->input->post('id');

	  	if($contact_id==0)
	  	{	  		
    		  $output['error'] = 'Please select contact.';
    	       
	  	}
	  	else
	  	{
	  		$contact = (isset($contact_id) ? $this->contact->get_row(array('id'=>$contact_id)):array());	  		
  			$output['msg'] = stripslashes($contact->content_html);		  		
	  	}
	  	echo json_encode($output);

	  }

	  public function get_notification()
	  {
	  	$this->load->model('NoticeModel','notice');

	  	$notice_group  = $this->input->post('group');

	  	try {

	  	  if((int) $notice_group==0) throw new Exception("ERROR: Empty Notice Group");

	  	   $result = $this->notice->get_result( array("where_str"=>"notice_group = {$notice_group}  AND ( expire_date >= '".date('Y-m-d h:i:s')."'  OR expire_date='0000-00-00 00:00:00') ")); 

	
	  	    echo json_encode($result);
	  	   


	  	  
	  	} catch (Exception $e) {

			echo json_encode(array('error'=>$e->getMessage()));	  		
	  	}
	  }

	public function get_notification_by_row()
	 {
  	  	$this->load->model('NoticeModel','notice');
	  	$this->load->model('Commonmodel', 'common');
	  	$id  = $this->input->post('id');
	  	$total = '0';

	  	$error = array('error'=>'', 'total'=>0, 'notice_group'=>'');

	  	try {

  		  	if((int) $id==0) throw new Exception("ERROR: Empty Notice ID");

  	   		$result = $this->notice->get_row( array('id'=>$id) , 'array'); 

  	   		$total = $this->common->count_notice_open($result['notice_group']);
  	   		$result['total'] 	= $total;

  	   		$error['total']			= $total;
  	   		$error['notice_group']	= $result['notice_group'];


  	   		if(  ($result['expire_date'] < date('Y-m-d h:i:s') && $result['expire_date'] != '0000-00-00 00:00:00' ) ){
	  	   			throw new Exception("The newly added notice was expired.")	;
  	   		}  	   		

	  	   	echo json_encode($result);
	  	 

	  	  
	  	} catch (Exception $e) {
	  		$error['error']	= $e->getMessage();
			echo json_encode($error);	  		
	  	}
  	}

	  public function update_notice()
	  {
  		 $this->load->model('NoticeModel','notice');

	  	 $id = $this->input->get('id');



	  	 if($_POST)
	  	 {
	  	 	$set = array();

	  	 	$set['content_html'] = $this->input->post('content');
	  	 	$set['expire_date'] = $this->input->post('expire');
	  	 	$set['notice_group'] = $this->input->post('group');
	  	 	$set['user_id'] = $this->input->post('user_id');
	  	 	$set['agent_name'] = $this->input->post('agent_name');
	  	 	$set['date_updated'] = date('Y-m-d h:i:s');

	  	 	if(count($set) > 0)
	  	 	{	

 			  $return_value = $this->notice->update($id,$set);

	  	 	 if(is_array($return_value) && array_key_exists('error', $return_value))
		   	  {
		   	 	$data['error'] = $return_value['error'];
		   	  }
		   	  else{

		 		$data['message'] = 'You have successfully updated a notice.';
		   	  }	
	  	 		echo json_encode($data);
	  	 	}
	  	 	
	  	 }
	  }
	   public function add_notice()
	  {
  		 $this->load->model('NoticeModel','notice');
	  	 

	  	 if($_POST)
	  	 {
	  	 	$insert = array();
	  	 	$insert['content_html'] = $this->input->post('content');
	  	 	$insert['expire_date'] = $this->input->post('expire');
	  	 	$insert['notice_group'] = $this->input->post('group');
	  	 	$insert['user_id'] = $this->input->post('user_id');
	  	 	$insert['agent_name'] = $this->input->post('agent_name');

	  		$return_value = $this->notice->add($insert);

	  	 	if(count($insert) > 0)
	  	 	{	
 			 if(is_array($return_value) && array_key_exists('error', $return_value))
		   	  {
		   	 	$data['error'] = $return_value['error'];
		   	  }
		   	  else{
				$data['last_id'] = $return_value;
		 		$data['message'] = 'You have successfully added a notice.';
		   	  }	
	  	 		echo json_encode($data);
	  	 	}
	  	  }
	  	 	
	  }

	 
   	public function delete_notice()
	{
  		$this->load->model('NoticeModel','notice');
  		$this->load->model('Commonmodel', 'common');
	  	 
	  	 if($_POST)
	  	 {
	  	 	$id 			= $this->input->post('id');
	  	 	$agent_name 	= $this->input->post('agent_name');
	  	 	$notice_group 	= $this->input->post('notice_group');
	  	 	$data = array();

	  		try {

			  	if((int) $id==0) throw new Exception("ERROR: Empty Notice ID");
	  			
	  			$this->notice->delete(array('id'=>$id,'agent_name'=>$agent_name));

	  			$total = $this->common->count_notice_open($notice_group);

	  			$data['message'] = 'You have successfully deleted` a notice.';

	  			$data['notice_group'] = $notice_group;
	  			$data['total'] = $total;

	  			echo json_encode($data);
	  			
	  		} catch (Exception $e) {
	  			echo json_encode(array('error'=>$e->getMessage()));	
	  		}	  			  			  	 
	  	  }	  	 	
	  }	

	public function patient_search(){

		$this->load->model('Patientmodel', 'patient');
		$this->load->helper('text');

		$data = '';

		try {
			
			if(!$_POST) throw new Exception("Error Processing Request", 1);
			
			$post = $this->input->post();

			if( isset($post['First_Name']) AND trim($post['First_Name']) != '' ){ 					
				$where['firstname'] = trim($post['First_Name']); 
			}

			if( isset($post['Last_Name']) AND trim($post['Last_Name']) != '' ){
				$where['lastname'] = trim($post['Last_Name']); 
			}

			if( !isset($where) ) throw new Exception("First Name or Last Name is required to search", 1);
			
			$rel_appt_id = (isset($post['rel_appt_id']) AND $post['rel_appt_id'] != '')?$post['rel_appt_id']:0;

			$patients = $this->patient->get_result(array('where'=>$where));

			//echo count($patients);
			$data['total'] = count($patients);
			
			$table = '<table class="table">';

			foreach($patients as $row){

				$name = highlight_phrase(strtoupper(trim($row->firstname)), strtoupper(trim($post['First_Name'])), '<span class="highlight_search_match">', '</span>');
	 			$name .= ' ';
	 			$name .= highlight_phrase(strtoupper(trim($row->lastname)), strtoupper(trim($post['Last_Name'])), '<span class="highlight_search_match">', '</span>');

				$table .= '<tr>
					<td>
		 				<p>Name: '.$name.'</p>
		 				<p>DOB: '.date('d/m/Y', strtotime($row->dob)).'</p>
						<p>Phone: '.$row->phone.'</p>
		 				<p>Address: '.stripslashes($row->street_addr).' '.$row->suburb.'</p>
	 				</td>
		 			<td>
		 				'.form_open('chaperone/verify_suburb', 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post" onsubmit="" ').'
		 				<input type="hidden" name="patient_id" value="'.$row->patient_id.'" />
		 				<input type="hidden" name="rel_appt_id" value="'.$rel_appt_id.'" />
		 				<button class="btn btn-sm btn-primary" type="submit" name="patient_type" value="existing" onclick=""><strong>Book Existing Patient</strong></button> 
		 				</form>
		 			</td>
 				</tr>';
			}

			$table .= '<tr>
					<td>
						<p class="bold">Click "Book New Patient" if patient is not on the list</p>
					</td>
		 			<td>
		 				'.form_open('chaperone/verify_suburb', 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post" onsubmit="" ').'
		 				<input type="hidden" name="First_Name" value="'.@$post['First_Name'].'" />
		 				<input type="hidden" name="Last_Name" value="'.$post['Last_Name'].'" />
		 				<input type="hidden" name="patient_id" value="" />
		 				<input type="hidden" name="rel_appt_id" value="'.$rel_appt_id.'" />
		 				<button class="btn btn-sm btn-success" type="submit" name="patient_type" value="new" ><strong>Book New Patient</strong></button> 
		 				</form>
		 			</td>
 				</tr>';


			$table .= '</table>';

			$data['records'] = $table;

			echo json_encode($data);exit;

		} catch (Exception $e) {
			$data['error'] = $e->getMessage();

			echo json_encode($data);
		}
		
	}

	public function suburb_search(){
		
		$this->load->model('Areamodel', 'areas');

		try {

			if(!$_POST) throw new Exception("Error Processing Request", 1);
			
			$post = $this->input->post();

			if( trim($post['suburb_text']) == '' ) throw new Exception("Subrub search must not be empty", 1);

			$where = "area_status = 1 AND lower(suburb_name) like '%".strtolower(trim($post['suburb_text']))."%'";

			$suburbs = $this->areas->get_suburb_area($where);

			$data['total'] = count($suburbs);
			
			unset($post['t_hcd_09']);

			$hidden = '';
			foreach ($post as $field => $value) {
				 $hidden .= '<input type="hidden" name="'.$field.'" value="'.$value.'" />';
			}

			$table = '<table class="table">';

			foreach ($suburbs as $row) {
				$table .= '
					<tr>
						<td>'.$row->suburb_name.'</td>
						<td>'.$row->suburb_postcode.'</td>
						<td>'.$row->area_name.'</td>
						<td>
							'.form_open('chaperone/book', 'class="form-inline" id="suburb_select_form" name="suburb_select_form" method="post" onsubmit="" ').'
							'.$hidden.'
			 				<input type="hidden" name="area_id" value="'.$row->area_id.'" />
			 				<input type="hidden" name="suburb_id" value="'.$row->suburb_id.'" />
			 				<input type="hidden" name="suburb_postcode" value="'.$row->suburb_postcode.'" />
			 				<input type="hidden" name="area_name" value="'.$row->area_name.'" />
			 				<input type="hidden" name="suburb_name" value="'.$row->suburb_name.'" />
			 				<button class="btn btn-sm btn-primary" type="submit" name="btn_select" value="btn_select" onclick=""><strong>Select</strong></button> 
			 				</form>
						</td>
					</tr>
				';
			}

			$table .= '</table>';

			$data['records'] = $table;

			echo json_encode($data);exit;

		} catch (Exception $e) {
			$data['error'] = $e->getMessage();
			echo json_encode($data);exit;
		}

	}

	   public function  get_email_sms_configuration()
	  {

			$this->load->model('Sitesettingsmodel', 'sitesettings');

			$array['email_sms_config'] = $this->sitesettings->get_results_where_string('set_name="SEND_EMAIL" OR set_name="SEND_SMS"','array');

			if(count($array) > 0 )
			{
			echo json_encode($array);
			}
			else{

			return false;
			}
			
	  }	

	public function  post_sms_email_configuration(){

		$this->load->model('Sitesettingsmodel', 'sitesettings');

		if($_POST){
			$set['set_value']		= $_POST['set_value']; 				

			if($_POST['option']=='send_email'){
				$this->sitesettings->update_by_name('SEND_EMAIL', $set);
			}else{
				$this->sitesettings->update_by_name('SEND_SMS', $set);
			}
		}
	}

	public function  post_override_filter_schedule(){

		$this->load->model('Sitesettingsmodel', 'sitesettings');

		if($_POST){
			$set['set_value'] = $_POST['set_value']; 				

			 
			$this->sitesettings->update_by_name('OVERRIDE_SCHED_FILTER', $set);
		 
		}
	}


	  public function post_email_settings()	
	  {
	  	$this->load->model('Sitesettingsmodel', 'sitesettings');

	  	 if($_POST)
	  	 {
	  	 	$set = array();

	  	 	$set['set_value']    = $this->input->post('set_value');
	  	 	$set['set_updated']  = date('Y-m-d h:i:s');
	  	 	$set['updated_user_id'] = $this->input->post('user_id');
	  	 	$set['updated_by']      = $this->input->post('agent_name');	  	 		  	 	

	  	 	if(count($set) > 0)
	  	 	{	
 			  

	  	 	 if($this->sitesettings->update_by_name($this->input->post('set_name'), $set))
		   	  {
		   	  	$data['message'] = 'Update Successful.';
		   	 	
		   	  }
		   	  else{

				$data['error'] = 'ERROR : Failure to update.';
		 		
		   	  }	
	  	 		echo json_encode($data);
	  	 	}
	  	 	
	  	 }
	  }


	public function ajax_chap_max_booking(){

		$this->load->model('Sitesettingsmodel', 'sitesettings');

		if($_POST) {
			
			$set = array();
			
			$post = $this->input->post();

			$set['set_value']    = $post['maxno'];
			$set['set_updated']  = date('Y-m-d h:i:s');
			$set['updated_user_id'] = $post['user_id'];
			$set['updated_by']      = $post['agent_name'];

			if(count($set) > 0){	 
				if($this->sitesettings->update_by_name('MAX_BOOKING', $set)) {
					$data['message'] = 'Update Successful.';

				}else{

					$data['error'] = 'ERROR : Failure to update.';

				}	
				echo json_encode($data);
			}

		}
	}	  

	public function get_completed_batch()
	{
		set_time_limit (0);
		ini_set('memory_limit', '-1');


		$this->load->model('Appointmentmodel', 'appt');

		$posted_data = $this->input->post();

		$params = array();

		
		try {

			if(!isset($posted_data)) throw new Exception("Error: Date is Empty", 1);

			$dt = explode(' - ',$posted_data['download_date']); 

			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));		

			$where = array();

			if(isset($posted_data['area']) && $posted_data['area']!='')
				array_push($where,"`appointment`.`area_id`={$posted_data['area']}");

			if(isset($posted_data['download_date']))
				array_push($where," (DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}')");

			$params['where_str'] = (count($where) > 0 ) ? implode(' AND ',$where) : implode('',$where);


			$result = $this->appt->get_result_pagination_ajax_without_limit($params,'count');
			echo json_encode($result);
			
		} catch (Exception $e) {
			$data['error'] = $e->getMessage();

			echo json_encode($data);
		}

	}

		public function get_completed_batch_notes()
	{
		set_time_limit (0);
		ini_set('memory_limit', '-1');


		$this->load->model('Appointmentmodel', 'appt');

		$posted_data = $this->input->post();

		$params = array();

		
		try {

			if(!isset($posted_data)) throw new Exception("Error: Date is Empty", 1);

			$dt = explode(' - ',$posted_data['download_date']); 

			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));		

			$where = array();

			if(isset($posted_data['area']) && $posted_data['area']!='')
				array_push($where,"`appointment`.`area_id`={$posted_data['area']}");

			if(isset($posted_data['download_date']))
				array_push($where," (DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}')");

			$params['where_str'] = (count($where) > 0 ) ? implode(' AND ',$where) : implode('',$where);


			$result = $this->appt->get_result_pagination_ajax_notes_without_limit($params,'count');
			echo json_encode($result);
			
		} catch (Exception $e) {
			$data['error'] = $e->getMessage();

			echo json_encode($data);
		}

	}

	public function  recursive_rmdir($src) {

    $dir = opendir($src);

    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            $full = $src . '/' . $file;
            if ( is_dir($full) ) {
                rrmdir($full);
            }
            else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);

	}

	public function generate_medical_form(){
			set_time_limit (0);
			ini_set('memory_limit', '-1');
						
			$this->load->helper('file');
			$this->load->library(array("pdfhelper","zip"));	
			$this->load->model('Appointmentmodel', 'appt');			
			$this->load->model('Commonmodel','common');
	
			$posted_data = $this->input->post();

			try {

				if(!isset($posted_data)) throw new Exception("Error: Date is Empty", 1);


				$dt = explode(' - ',$posted_data['download_date']); 

				$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));			


				$where = array();

				if(isset($posted_data['area']) && $posted_data['area']!='')
					array_push($where,"`appointment`.`area_id`={$posted_data['area']}");
				
				if(isset($posted_data['download_date']))
					array_push($where," (DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}')");

				$params['where_str'] = (count($where) > 0 ) ? implode(' AND ',$where) : implode('',$where);


				$result = $this->appt->get_result_pagination_ajax_without_limit($params,'result');


				$datef = date('d.m.Y',strtotime($dt_f));

				$name  = $datef.'-MV';

				$root_dir  =  "files/medicare/batch/";
				$unix_time = strtotime('now');
				$unix_dir  = $root_dir. $unix_time.'/';
				$name_dir  = $unix_dir.$name.'/';
				$zip_file  = $unix_dir.$name.".zip";


				if(!is_dir($unix_dir))
				{

					if(!mkdir($unix_dir,0777,TRUE))
					{
							throw new Exception("Error: Unable to create UNIX Directory");
					}	
					    chmod($unix_dir, 0777);

				}

				if(!is_dir($name_dir))
				{

					if(!mkdir($name_dir,0777,TRUE))
					{
							throw new Exception("Error: Unable to create ".strtotime('now')."");
					}	
					    chmod($name_dir, 0777);

				}


				$data = array();


			    foreach ($result['results'] as $row) {				

					$fdoc = str_replace(array('DR',' '), '', strtoupper(@$row->doctor_name));

					$fdoc = preg_replace("/\\W|_/", '', $fdoc); //remove non alphanumeric
					
					$fpatient = strtoupper($row->firstname.$row->middleinitial.$row->lastname);
					$fpatient = preg_replace("/\\W|_/", '', $fpatient); //remove non alphanumeric

					$filename = $datef.' '.$fdoc.' - '.$fpatient.'MV';

					$filename = $filename.'.pdf';

					$html = $this->load->view('chaperone/medicare-pdf-tpl', $row, true);

					$file = $this->pdfhelper->renderPDF($html, $filename,false);

					$file_name = $this->file_newname($name_dir,$filename);

					write_file($name_dir.$file_name, $file);
				 }
					

				$return = $this->zip_folder(array('file_name'=>$name,'dir'=>$name_dir,'zip_file'=>$zip_file));
				 
				if(is_array($return)) throw new Exception($return['error']);
			

				delete_files($name_dir, true); 

				rmdir($name_dir);				


				$data['message'] = 'Archive Successful.';

				$data['result']   = array('url'=>$zip_file,'name'=>$name.'.zip','unix'=>$unix_time);

				echo json_encode($data);

		} catch (Exception $e) {

			$data['error'] = $e->getMessage();

			echo json_encode($data);
		}
		

	}


	function zip_folder($parameter =array()){

		error_reporting(E_ALL);
		ini_set('display_errors', true);

		$this->load->helper('file');

		try {

			if(empty($parameter) || count($parameter) <=0) 
				throw new Exception("Error: Empty parameters for zip folder method");

			if(!is_readable($parameter['dir'])) 
				throw new Exception("Error: Unable to read directory to be archived");			
			
			$zip = new ZipArchive();

			if($zip->open($parameter['zip_file'], ZipArchive::CREATE)!==TRUE) 
			    throw new Exception("Error: Cannot create or open file ".$parameter['file_name'].'.zip');
			

			$files = get_filenames($parameter['dir']);


			if(count($files)<=0) 
				throw new Exception("Error: Empty files to be archived");
			
			foreach ($files as $name => $file)
			{
				$file = $parameter['dir'].$file;

			   if(file_exists($file) && is_readable($file)){
			   	
				$split_values = explode("/",$file);				

				$new_filename = end($split_values);			   

				$zip->addFile(realpath($file),ltrim(pathinfo($file, PATHINFO_BASENAME), '/')) or die('Cannot add '.$file);

			   }			   			     			   
			}

			 $zip->close();
			 
			 return true;
			
		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
				
		}
		 chmod($parameter['zip_file'], 0777);	

	}

	public function clear_signature()
	{
			$this->load->model('Appointmentmodel', 'appt');			

			$appt_id = $this->input->post('appt_id');		

			$set = array('appt_id'=>$appt_id,'set_appt'=>array('sign_code'=>''));

			try {

				if($appt_id == '')
				{
					throw new Exception("Apt ID is Empty");										
				}

				$this->appt->update_appointment($set);
			    $data['message'] = 'Update Successful.';
				echo json_encode($data);

				
			} catch (Exception $e) {

				$data['error'] = $e->getMessage();

				echo json_encode($data);
				
			}
		}

		function file_newname($dir, $filename){

			    if ($pos = strrpos($filename, '.')) {
			           $name = substr($filename, 0, $pos);
			           $ext = substr($filename, $pos);
			    } else {
			           $name = $filename;
			    }


			    $newpath = $dir.$filename;

			    $newname = $filename;

			    $counter = 1;


			   if(file_exists($newpath))
			   {

		   		   while (file_exists($newpath)) {
			           $newname = $name .'_'. $counter . $ext;
			           $newpath = $dir.'/'.$newname;
			           $counter++;
		     		}
			   }
			   return  $newname;			   
		}

		public function generate_notes(){

			set_time_limit (0);
			ini_set('memory_limit', '-1');
						
			$this->load->helper('file');
			$this->load->library(array("pdfhelper","zip"));	
			$this->load->model('Appointmentmodel', 'appt');			
			$this->load->model('Commonmodel','common');
	
			$posted_data = $this->input->post();

			try {

				if(!isset($posted_data)) throw new Exception("Error: Date is Empty", 1);


				$dt = explode(' - ',$posted_data['download_date']); 

				$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));			


				$where = array();

				if(isset($posted_data['area']) && $posted_data['area']!='')
					array_push($where,"`appointment`.`area_id`={$posted_data['area']}");
				
				if(isset($posted_data['download_date']))
					array_push($where," (DATE_FORMAT(CONVERT_TZ(`appointment`.`appt_created`, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '{$dt_f}')");

				$params['where_str'] = (count($where) > 0 ) ? implode(' AND ',$where) : implode('',$where);


				$result = $this->appt->get_result_pagination_ajax_notes_without_limit($params,'result');


				$datef = date('d.m.Y',strtotime($dt_f));

				$name  = $datef.'-MV';

				$root_dir  =  "files/consult_notes/batch/";
				$unix_time = strtotime('now');
				$unix_dir  = $root_dir. $unix_time.'/';
				$name_dir  = $unix_dir.$name.'_CONSULT/';
				$zip_file  = $unix_dir.$name.'_CONSULT.zip';  


				if(!is_dir($unix_dir))
				{

					if(!mkdir($unix_dir,0777,TRUE))
					{
							throw new Exception("Error: Unable to create UNIX Directory");
					}	
					    chmod($unix_dir, 0777);

				}

				if(!is_dir($name_dir))
				{

					if(!mkdir($name_dir,0777,TRUE))
					{
							throw new Exception("Error: Unable to create ".strtotime('now')."");
					}	
					    chmod($name_dir, 0777);

				}


				$data = array();

		

			    foreach ($result['results'] as $row) {				


					$fdoc = str_replace(array('DR',' '), '', strtoupper(@$row->doctor_name));

					$fdoc = preg_replace("/\\W|_/", '', $fdoc); //remove non alphanumeric\

					$fpatient = strtoupper($row->firstname.$row->middleinitial.$row->lastname);
					

					$fpatient = preg_replace("/\\W|_/", '', $fpatient); //remove non alphanumeric

					$filename = $datef.' '.$fdoc.'-'.$fpatient.'MV-'.(empty($row->consult_id) || $row->consult_id=='' ? ' N ':' Y ');

					$filename = $filename.'.pdf';

					$html = $this->load->view('doctor/consult-notes-export', array('record'=>$row) , true);

					$file = $this->pdfhelper->renderPDF($html, $filename,false);

					$file_name = $this->file_newname($name_dir,$filename);

					write_file($name_dir.$file_name, $file);		
							
					  	
				 }
				

				$return = $this->zip_folder(array('file_name'=>$name,'dir'=>$name_dir,'zip_file'=>$zip_file));
				

				delete_files($name_dir, true); 

				rmdir($name_dir);				


				$data['message'] = 'Archive Successful.';

				$data['result']   = array('url'=>$zip_file,'name'=>$name.'_CONSULT.zip','unix'=>$unix_time);

				echo json_encode($data);

		} catch (Exception $e) {

			$data['error'] = $e->getMessage();

			echo json_encode($data);
		}
		

	}

	public function dropdown_item_action(){

		try{

			if(!$_POST) throw new Exception("Error request method", 1);

			if(!isset($_POST['type']) ) throw new Exception("Error request type", 1);
			

			
			if( $_POST['type'] == 'add'){

				$json['msg'] = 'Successfully added';

				$set['dropdown_value'] = addslashes($_POST['text']);
				$set['dropdown_label'] = addslashes($_POST['text']);
				$set['dropdown_group'] = $_POST['group'];

				if( !$this->db->insert('dropdown', $set) ){
					throw new Exception("Failed to Add dropdown", 1);
				}

				$json['id'] = $this->db->insert_id();

                $json['item'] = '<li class="list-group-item clearfix" data-id="'.$this->db->insert_id().'">
                            <div class="item" >
                                <span class="text pull-left">'.$set['dropdown_value'].'</span>
                                <span class="tools pull-right">
                                    <a href="#" onclick="consult.edit_item(this, event)" class="text-primary edit-item"><i class="glyphicon glyphicon-edit "></i></a>
                                    <a href="#" onclick="consult.delete_item(this, event)" class="text-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                </span>
                            </div>
                            <div class="edit" style="display: none">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" onclick="consult.save_item(this, event)" class="btn btn-success btn-flat" title="Save"><i class="glyphicon glyphicon-save "></i></button>
                                        <button type="button" onclick="consult.cancel_item(this, event)" class="btn btn-danger btn-flat" title="Cancel"><i class="glyphicon glyphicon-triangle-right "></i></button>
                                    </span>
                              </div>                      
                            </div>
                        </li>';

			}elseif ($_POST['type'] == 'update') {
				
				if(!isset($_POST['id']) ) throw new Exception("Error request item id", 1); 

				$json['msg'] = 'Successfully added dropdown';

				$set['dropdown_value'] 		= addslashes($_POST['text']);
				$set['dropdown_label'] 		= addslashes($_POST['text']);
				$set['dropdown_updated'] 	= date('Y-m-d H:i:s');

				$this->db->where('dropdown_id', $_POST['id']);
				if( !$this->db->update('dropdown', $set) ){
					throw new Exception("Failed to Update dropdown", 1);
				}

			}elseif ($_POST['type'] == 'delete') {
				
				if(!isset($_POST['id']) ) throw new Exception("Error request item id", 1); 

				$json['msg'] = 'Successfully deleted dropdown: '.$_POST['text'];

				$set['dropdown_status'] 	= '0';
				$set['dropdown_updated'] 	= date('Y-m-d H:i:s');
				
				$this->db->where('dropdown_id', $_POST['id']);
				if( !$this->db->update('dropdown', $set) ){
					throw new Exception("Failed to Update dropdown", 1);
				}
			}else{
				throw new Exception("Error request type", 1);
			}

			
			echo json_encode($json);exit;
		}catch(Exception $e){

			$json['error'] = 1;
			$json['error_msg'] = $e->getMessage();
			echo json_encode($json); exit;
		}

	}


	public function get_consult_dropdown(){

		try {
			
			if( !isset($_GET) )  throw new Exception("Error request method", 1);
			//$_GET['group'] = 'medication_prescribed';
			if(!isset($_GET['group']) OR trim($_GET['group']) == '' ) throw new Exception("Error request group", 1);

			$this->db->where('dropdown_group', trim($_GET['group']));
			$this->db->where('dropdown_status', 1);
			$this->db->select('dropdown_value');
			$query = $this->db->get('dropdown');
			$result = $query->result();

			$json = array();
			foreach ($result as $row) {
				$json[] = $row->dropdown_value;
			}

			echo json_encode($json); exit;
		} catch (Exception $e) {
			$json[] = $e->getMessage();
			echo json_encode($json); exit;
		} 
	}

	public function booked_out_area(){

		$this->load->model('Areamodel', 'areas');
		$this->load->model('Commonmodel', 'common');

		try {

			if( !isset($_POST) )  throw new Exception("Error request method", 1);

			if( !isset($_POST['agent_id'])  && !isset($_POST['agent_name']))  throw new Exception("Error request method", 1);

			$set = array();

			$post = $this->input->post();
			$set['area_id'] = $post['area_id'];
			$set['user_id'] = $post['agent_id'];
			$set['agent_name'] = $post['agent_name'];
 
 			/*$date_now_arr = explode('-',date('Y-m-d'));

 			$now = date('Y-m-d H:i:s',mktime(8,0,0,$date_now_arr[1],$date_now_arr[2],$date_now_arr[0]));
 			$tom = date('Y-m-d H:i:s',strtotime($now .' +1 day'));


			if( strtotime(date('Y-m-d H:i:s')) > strtotime(date('Y-m-d').'08:00;00')){
				$set['reopen_dt_start'] = $tom;
			}else{
				$set['reopen_dt_start'] = $now;
			}*/

			//$now_day = date('D');
			$now_date = date('Y-m-d');
			$now_time = date('H:i:s');

	        //debugging overide
	        //$now_day = 'Wed';
	        //$now_date = '2018-03-01';
	        //$now_time  = '18:05:00';

	        if( $this->common->test_override_current_time() != '' ){
	            $debug_dt = explode(' ', $this->common->test_override_current_time());
	            $now_date = $debug_dt[0];
	            $now_time = $debug_dt[1];
	        }


	        $now_dt  = $now_date.' '.$now_time;
	        $now_day = date('D', strtotime($now_dt));



	        //echo $now_dt.'<br />';
	        //echo $now_day.'<br />';

	        //echo 'is_holiday_today:'.$this->common->is_holiday_today($now_date);

			//If Sunday, Monday, Tuesday, Wednesday, Thursday night - RE-OPEN at 4pm next day
			/*if( !$this->common->is_holiday_today($now_date) AND in_array( $now_day, array('Sun','Mon','Tue','Wed','Thu')) AND $this->common->check_date_is_within_range($now_date.' 16:00:00', $now_date.' 23:59:59', $now_dt) ){
			
				$set['reopen_dt_start'] = date('Y-m-d',strtotime('now +1 day')).' 16:00:00';
			
			//If Friday night - RE-OPEN at 10am next morning
			}elseif( !$this->common->is_holiday_today($now_date) AND $now_day == 'Fri' AND $this->common->check_date_is_within_range($now_date.'16:00:00', $now_date.' 23:59:59', $now_dt) ){
				
				$set['reopen_dt_start'] = date('Y-m-d',strtotime('now +1 day')).' 10:00:00';

			//If Saturday night or Public Holiday - RE-OPEN at 6am next morning
			}elseif( ($now_day == 'Sat' OR $this->common->is_holiday_today($now_date) ) AND $this->common->check_date_is_within_range($now_date.'16:00:00', $now_date.' 23:59:59', $now_dt) ){
				
				$set['reopen_dt_start'] = date('Y-m-d',strtotime('now +1 day')).' 06:00:00';


			}else{
				$set['reopen_dt_start'] = date('Y-m-d H:i:s');
			}*/

			
			$area_row = $this->areas->get_row(array('area_id'=>$post['area_id']));
 

			//UPDATED: March 1 2018
			//If Sunday, Monday, Tuesday, Wednesday, Thursday night - RE-OPEN at 4pm next day
			

			if( $area_row->region_type == 'METRO' ){
				
				if( !$this->common->is_holiday_today($now_date) AND in_array( $now_day, array('Sun','Mon','Tue','Wed','Thu')) AND $this->common->check_date_is_within_range($now_date.' 16:00:00', $now_date.' 23:59:59', $now_dt) ){
				
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 16:00:00'; 
				
				//If Friday night - RE-OPEN at 10am next morning
				}elseif( !$this->common->is_holiday_today($now_date) AND $now_day == 'Fri' AND $this->common->check_date_is_within_range($now_date.'18:00:00', $now_date.' 23:59:59', $now_dt) ){
					
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 12:00:00';

				//If Saturday night or Public Holiday - RE-OPEN at 6am next morning
				}elseif( ($now_day == 'Sat' OR $this->common->is_holiday_today($now_date) ) AND $this->common->check_date_is_within_range($now_date.'18:00:00', $now_date.' 23:59:59', $now_dt) ){
					
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 06:00:00';

				}else{
					//$set['reopen_dt_start'] = date('Y-m-d H:i:s');
					$set['reopen_dt_start'] = $now_dt;
				}

			}else{
				if( !$this->common->is_holiday_today($now_date) AND in_array( $now_day, array('Sun','Mon','Tue','Wed','Thu')) AND $this->common->check_date_is_within_range($now_date.' 18:00:00', $now_date.' 23:59:59', $now_dt) ){
				
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 18:00:00'; 
				
				//If Friday night - RE-OPEN at 10am next morning
				}elseif( !$this->common->is_holiday_today($now_date) AND $now_day == 'Fri' AND $this->common->check_date_is_within_range($now_date.'18:00:00', $now_date.' 23:59:59', $now_dt) ){
					
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 12:00:00';

				//If Saturday night or Public Holiday - RE-OPEN at 6am next morning
				}elseif( ($now_day == 'Sat' OR $this->common->is_holiday_today($now_date) ) AND $this->common->check_date_is_within_range($now_date.'18:00:00', $now_date.' 23:59:59', $now_dt) ){
					
					$set['reopen_dt_start'] = date('Y-m-d',strtotime($now_date.' +1 day')).' 06:00:00';

				}else{
					//$set['reopen_dt_start'] = date('Y-m-d H:i:s');
					$set['reopen_dt_start'] = $now_dt;
				}

			}
 

			$set['is_open'] = 0;
			$set['is_book_out'] = 1;

			$json = array();
  

			if(!$this->areas->check_and_insert_areas_availability2($set)) 
			 	throw new Exception("Error request method", 1);

			$json['message'] = 'Area Successfully booked out.';

			echo json_encode($json); exit;
			
		} catch (Exception $e) {
			$json[] = $e->getMessage();
			echo json_encode($json); exit;
		}




	}

	public function booked_reopen_area(){
	$this->load->model('Areamodel', 'areas');
		try {

			if( !isset($_POST) )  throw new Exception("Error request method", 1);

			if( !isset($_POST['agent_id'])  && !isset($_POST['agent_name']))  throw new Exception("Error request method", 1);

			$set = array();

			$post = $this->input->post();
			$set['area_id'] = $post['area_id'];
			$set['user_id'] = $post['agent_id'];
			$set['agent_name'] = $post['agent_name'];
			
			$set['is_open'] = 1;
			$set['is_book_out'] = 1;

			$json = array();

			if(!$this->areas->check_and_insert_areas_availability2($set)) 
			 throw new Exception("Error request method", 1);

			$json['message'] = 'Area Successfully Re-opened.';

			echo json_encode($json); exit;
			
		} catch (Exception $e) {
			$json[] = $e->getMessage();
			echo json_encode($json); exit;
		}
	}


	public function get_consult_notes(){


		$post = $this->input->post();


		$json = array();

		try {

				if (!isset($post['doctor_id']) && empty($post['doctor_id'])) 

					throw new Exception("Error: Empty Doctor ID");
				
					$this->db->select('*', FALSE);

					$this->db->where('doctor_id',$post['doctor_id']);

					$query = $this->db->get('consult_note_upload');




					$json = $query->result();



					echo json_encode($json); exit;

			
		} catch (Exception $e) {

					$json[] = $e->getMessage();
					echo json_encode($json); exit;
			
		}


		echo $json;


	}


	public function get_consult_info(){


			$id = $this->input->get('cid');

			$json = array();

			try {

					if (!isset($id) && empty($id)) 

						throw new Exception("Error: Empty Doctor ID");
					
						$this->db->select('*', FALSE);

						$this->db->where('id',$id);

						$query = $this->db->get('consult_note_upload');


						$json = $query->result();



						echo json_encode($json); exit;

				
			} catch (Exception $e) {

						$json[] = $e->getMessage();
						echo json_encode($json); exit;
				
			}

	}



public function test(){
	
}


}
