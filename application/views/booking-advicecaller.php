<?php echo form_open('', 'class="form-horizontal" id="booking_patient_form" name="booking_patient_form" method="post"'); ?>
<input type="hidden" name="appt_id" value="<?php echo @$appt_id?>"/>
<div class="jumbotron clearfix text-center" >

    <div class="row" style="border-bottom:2px solid gray;">
        <h4>Booking successful - Booking Appt # is <?php echo @$appt->ref_number_formatted;  ?></h4>
    </div>

    <div class="col-lg-8 col-md-8 center-block float-none">
 
        <br />
    	<!--<h4>Thank you, I confirm you are now booked in and our doctor will be there to see you <?php echo $when=='today'?'shortly':'tomorrow'; ?> </h4>-->


        <?php if( isset($area->area_id) AND trim(@$area->form_end_script) != '' ): ?>
            <?php echo stripslashes(@$area->form_end_script); ?>
        <?php else: ?>
            <h4>I can now confirm that you have been booked in and the doctor will see
                you as soon as possible.
                <br /><br />
                The doctor begins his consultations at, <font class="text-danger" style="font-weight:bold">XXXXX (XXX = time appts start for the day) </font>,<br/><br/>
                You will receive a text message with an estimated time of arrival within
                the next few hours.
                <br /><br />
                If you have any pets in the house could you please contain them.
                If the symptoms get worse we advise you to either call an ambulance or
                go to the hospital.
                <br/><br/>
                Can you please leave the front light on for the doctor.    
            </h4>
            <br/>
            <h4 class="text-primary">
                Lastly, do you know about our online booking?
            </h4>
            <br/>
            <h4 class="text-primary" style="font-weight:bold">
                    IF YES
           </h4>
           <h4 class="text-primary">

                Good, this is very easy to use and will save your time waiting in the queue.  We will give you a call <br/> after you make a booking to confirm
           </h4>

            <br/><br/> 

            <h4 class="text-primary" style="font-weight:bold">
                    IF NO
           </h4>

           <h4 class="text-primary">

               You can also make a booking online or phone app.  It is easy and quick and will save your time 
               <br/>waiting in a queue.  We will then call you to confirm booking
                <br/><br/>
                Do you have any questions ?
           </h4>
           <br/>
           <h4 style="font-weight:bold">
               END WITH
           </h4>


            <h4>Is there anything else I can help you with ?</h4> 
            <br/>
        <?php endif; ?>
    
    </div>

    <div class="row" style="border-top:2px solid gray;">

        <div class="col-sm-12 col-md-12 col-lg-12" style="margin-top:50px">

            <h4 class="text-primary" style="font-weight:bold">ONLINE BOOKING INFORMATION</h4> 
            <br/>

            <h4><strong>To do an online booking</strong> - GO TO <a href="http://www.13cure.com.au/" target="_blank"/>http://www.13cure.com.au/</a>  &amp;  click on <strong>Book Online</strong> </h4>    
            <h4><strong>Phone App</strong> can be downloaded from the App Store</h4><br/><br/>
            <a href="dashboard" class="btn btn-primary">Dashboard</a>
        </div>
    </div>
 
</div>
<?echo form_close();?>