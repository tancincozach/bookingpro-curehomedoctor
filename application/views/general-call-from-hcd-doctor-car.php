 
<h4 class="page-header">Call From Doctor / Car</h4>

<div class="col-sm-8">
	<?php echo form_open('', 'class="form-horizontal" id="call_form" name="call_form" method="post" onsubmit="hcd.common.confirm()"'); ?>

		<input type="hidden" name="tran_type" value="Call From Doctor / Car">
		<input type="hidden" name="tran_desc" value="">
		<input type="hidden" name="call_type" value="<?php echo $call_type; ?>">

		<div class="form-group">
		    <label for="First_Name" class="col-sm-4 control-label">Caller Name</label>
		    <div class="col-sm-6">
		    	<div class="row">
		    		<div class="col-sm-6">
		    			<input type="text" class="form-control" name="First_Name" value="<?php echo @$First_Name; ?>" placeholder="First Name">
	    			</div>
		    		<div class="col-sm-6">
		    			<input type="text" class="form-control" name="Last_Name" value="<?php echo @$Last_Name; ?>" placeholder="Last Name">
		    		</div>

		    	</div>
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="Caller_Phone" class="col-sm-4 control-label">Phone</label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" name="Caller_Phone" value="<?php echo @$Caller_Phone; ?>">
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="reason_for_call" class="col-sm-4 control-label">Reason for Call</label>
		    <div class="col-sm-6">
		    	<textarea class="form-control" name="reason_for_call" rows="3"></textarea>
		    </div>
		</div>
		   
		<div class="form-group">
		    <div class="col-sm-offset-4 col-sm-6">
		      <button type="submit" class="btn btn-primary">Send</button>
		    </div>
		</div>

	</form>

</div>