<h4 class="page-header">Cancel Booking</h4>


<div class="row">

	<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	
		<h4>Patient Details</h4>

		<!-- <p>
			<label>Name : </label> <?php echo stripslashes($record->firstname).' '.stripslashes($record->lastname); ?><br />
			<label>Age : </label> <?php echo $record->patient_age; ?><br /> 
			<label>Phone : </label> <?php echo $record->phone; ?><br /> 
			<label>Mobile No : </label> <?php echo $record->mobile_no; ?><br /> 
			<label>Email : </label> <?php echo stripslashes($record->email); ?><br /> 
			<label>Address : </label> <?php echo stripslashes($record->street_addr).' '.stripslashes($record->suburb).' '.$record->postcode; ?><br /> 
			<label>Symptom(s) : </label> <?php echo stripslashes($record->symptoms); ?><br /> 
		</p>
 -->

    <?php

    echo '<table class="table table-bordered">
                <tr>
                    <td class="col-lg-4 bold" style="">Name</td>
                    <td style="">'.stripslashes($record->firstname).' '.stripslashes($record->lastname).'</td>
                </tr>
                <tr>
                    <td class="bold">Phone</td>
                    <td>'.$record->phone.'</td>
                </tr>
                <tr>
                    <td class="bold">Mobile No</td>
                    <td>'.$record->mobile_no.'</td>
                </tr>
                <tr>
                    <td class="bold">Address</td>
                    <td>'.stripslashes($record->street_addr).'</td>
                </tr>
                <tr>
                    <td class="bold">Cross Street</td>
                    <td>'.stripslashes($record->cross_street).'</td>
                </tr>                
                <tr>
                    <td class="bold">Suburb</td>
                    <td>'.stripslashes($record->suburb).'</td>
                </tr>
                <tr>
                    <td class="bold">Symptom(s)</td>
                    <td>'.stripslashes($record->symptoms).'</td>
                </tr>
                <tr>
                    <td class="bold">Date of Birth</td>
                    <td>'.date('d/m/Y',strtotime($record->dob)).'</td>
                </tr>
                <tr>
                    <td class="bold">Age</td>
                    <td>'.$record->patient_age.'</td>
                </tr>
                <tr>
                    <td class="bold">Gender (sex)</td>
                    <td>'.$record->gender.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Number</td>
                    <td>'.$record->medicare_number.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Ref</td>
                    <td>'.$record->medicare_ref.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Exp</td>
                    <td>'.$record->medicare_expiry.'</td>
                </tr>
                <tr>
                    <td class="bold">DVA</td>
                    <td>'.($record->dva?'Yes':'No').'</td>
                </tr>
                <tr>
                    <td class="bold">Aboriginal or Torres Straight Islander</td>
                    <td>'.($record->abotsi?'Yes':'No').'</td>
                </tr>
                <tr>
                    <td class="bold">Cultural &#38; Ethnicity Background</td>
                    <td>'.stripslashes($record->cultural_ethnic_bg).'</td>
                </tr>
                <tr>
                    <td class="bold">Where did you hear about us</td>
                    <td>'.stripslashes($record->latest_wdyhau).'</td>
                </tr>                
                <tr>
                    <td class="bold">Regular Practice</td>
                    <td>'.$record->regular_practice.'</td>
                </tr>
                <tr>
                    <td class="bold">Regular Doctor</td>
                    <td>'.$record->regular_doctor.'</td>
                </tr>
            </table>';

    ?>

	</div>
       
    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 ">
        <h4>Cancel Booking</h4>
       
        <?php echo form_open('', 'class="form-horizontal" id="booking_cancel_form" name="booking_cancel_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm CANCELLATION of this appointment!\')"'); ?>
            
            <input type="hidden" name="form_type" value="cancel_booking" />
            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
            <input type="hidden" name="book_status" value="<?php echo $record->book_status ?>" />

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Appt #</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Date/Time Booked</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Chaperone</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Car #</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                    <p class="form-control-static">
                    <?php
                            if( isset($car->car_name) )
                                echo $car->car_name.' ('.$car->car_plateno.')'; 
                        ?>
                    </p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Doctor</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                </div>
            </div>

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label" style="text-align: left">Cancel Reason</label>
                <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
                    <textarea class="form-control" rows="4" name="cancel_reason" required></textarea>
                </div>
            </div>

            <br />

            <div class="form-group">
                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-3 control-label">&nbsp;</label>
                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                    <input class="btn btn-primary" type="submit" value="Cancel Booking">
                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                </div>
            </div>

        </form>

    </div> <!-- endcol-lg-8 col-md-8 col-sm-8 -->

</div> <!-- end .row -->