<h4 class="page-header">Appointment Info</h4>


<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6">
	
		<h4>Patient Details</h4>

		<!-- <p>
			<label>Name : </label> <?php echo stripslashes($record->firstname).' '.stripslashes($record->lastname); ?><br />
			<label>Age : </label> <?php echo $record->patient_age; ?><br /> 
			<label>Phone : </label> <?php echo $record->phone; ?><br /> 
			<label>Mobile No : </label> <?php echo $record->mobile_no; ?><br /> 
			<label>Email : </label> <?php echo stripslashes($record->email); ?><br /> 
			<label>Address : </label> <?php echo stripslashes($record->street_addr).' '.stripslashes($record->suburb).' '.$record->postcode; ?><br /> 
			<label>Symptom(s) : </label> <?php echo stripslashes($record->symptoms); ?><br /> 
		</p> -->

		<?php


			echo '<table class="table table-bordered">
	                <tr>
	                    <td class="col-lg-4 bold" style="">Name</td>
	                    <td style="">'.stripslashes($record->firstname).' '.stripslashes($record->lastname).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Phone</td>
	                    <td>'.$record->phone.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Mobile No</td>
	                    <td>'.$record->mobile_no.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Emergency Contact Name</td>
	                    <td>'.$record->em_contact_name.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Emergency Contact PHone</td>
	                    <td>'.$record->em_contact_phone.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Address</td>
	                    <td>'.stripslashes($record->street_addr).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Cross Street</td>
	                    <td>'.stripslashes($record->cross_street).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Suburb</td>
	                    <td>'.stripslashes($record->suburb).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Symptom(s)</td>
	                    <td>'.stripslashes($record->symptoms).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Date of Birth</td>
	                    <td>'.date('d/m/Y',strtotime($record->dob)).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Age</td>
	                    <td>'.$record->patient_age.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Gender (sex)</td>
	                    <td>'.$record->gender.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Medicare Number</td>
	                    <td>'.$record->medicare_number.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Medicare Ref</td>
	                    <td>'.$record->medicare_ref.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Medicare Exp</td>
	                    <td>'.$record->medicare_expiry.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">DVA</td>
	                    <td>'.($record->dva?'Yes':'No').'</td>
	                </tr>
                 	<tr>
	                    <td class="bold">Private Health Insurance Provider</td>
	                    <td>'.@$record->health_insurance_provider.'</td>
	                </tr>
	                 <tr>
	                    <td class="bold">Private Health Fund Number</td>
                         <td>'.@$record->health_fund_number.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Does patient have a Pension, Concession or Healthcare Card</td>
                         <td>'.@$record->patient_has_health_card.'</td>
	                </tr>

	                 <tr>
	                    <td class="bold">Pension, Concession or Healthcare Card Expiry Date</td>
                         <td>'.(isset($record->health_card_expiry_date) ? $record->health_card_expiry_date:'0000-00-00').'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Aboriginal or Torres Straight Islander</td>
	                    <td>'.($record->abotsi?'Yes':'No').'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Cultural &#38; Ethnicity Background</td>
	                    <td>'.stripslashes($record->cultural_ethnic_bg).'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Where did you hear about us</td>
	                    <td>'.stripslashes($record->latest_wdyhau).'</td>
	                </tr>	                
	                <tr>
	                    <td class="bold">Regular Practice</td>
	                    <td>'.$record->regular_practice.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Regular Doctor</td>
	                    <td>'.$record->regular_doctor.'</td>
	                </tr>
	            </table>';

	    ?>

	</div>
       <!--   <tr>
	                    <td class="bold">Does patient have a Pension, Concession or Healthcare Card</td>
                         <td>'.(isset($record->expiry_date) && $record->expiry_date!='')  ? 'Yes':'No'.'</td>
	                </tr>
	                <tr>
	                    <td class="bold">Pension , Concession , Healthcare Card Expiry Date</td>
                         <td>'.(isset($record->expiry_date) && $record->expiry_date!='')  ? @$record->expiry_date:'N/A'.'</td>
	                </tr> -->
	<div class="col-lg-6 col-md-6 col-sm-6">
		
		<h4>Booking Details</h4>

		<!-- <p>
			<label>Appt # : </label> <?php echo $record->ref_number_formatted ?> <br />
			<label>Booking Status : </label> <?php echo ($record->book_status==4)?$this->booking_statuses[0]:$this->booking_statuses[$record->book_status]; ?> <br />
			<label>Date/Time Booked : </label> <?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?> <br />
			<label>Time in queue : </label> <?php echo $record->in_queue ?> <br />
			<label>Consult Start : </label> <?php echo ($record->consult_start != '')?date('d/m/Y H:i', $record->consult_start):''; ?> <br />
			<label>Consult End : </label> <?php echo ($record->consult_end != '')?date('d/m/Y H:i', $record->consult_end):''; ?> <br />			
			<label>Chaperone : </label> <?php echo $record->chaperone_name ?> <br />
			<label>Doctor : </label> <?php echo $record->doctor_name ?> <br />
			<label>Car # : </label> 
				<?php
					 if( isset($car->car_name) )
						echo @$car->car_name.' ('.@$car->car_plateno.')'; 
				?> <br />
		</p> -->

		<table class="table table-bordered">
			<tr>
				<td class="bold">Appt # </td>
				<td><?php echo $record->ref_number_formatted ?></td>
			</tr>
			<tr>
				<td class="bold">Booking Status</td>
				<td><?php 

					//echo ($record->book_status==4)?$this->booking_statuses[0]:$this->booking_statuses[$record->book_status]; 

					if( $record->book_status == 1 ){
						echo $this->booking_statuses[$record->book_status]. ' | Cancel Reason: '.$record->cancelled_reason;
					}else{
						echo $this->booking_statuses[$record->book_status];
					}

				?>
				</td>
			</tr>
			<tr>
				<td class="bold">Date/Time Booked</td>
				<td><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></td>
			</tr>
			<tr>
				<td class="bold">Time in queue</td>
				<td><?php echo $record->in_queue ?></td>
			</tr>
			<tr>
				<td class="bold">Consult Start</td>
				<td><?php echo ($record->consult_start != '')?date('d/m/Y H:i', $record->consult_start):''; ?></td>
			</tr>
			<tr>
				<td class="bold">Consult End</td>
				<td><?php echo ($record->consult_end != '')?date('d/m/Y H:i', $record->consult_end):''; ?></td>
			</tr>
			<tr>
				<td class="bold">Chaperone</td>
				<td><?php echo $record->chaperone_name; ?></td>
			</tr>
			<tr>
				<td class="bold">Doctor</td>
				<td><?php echo $record->doctor_name ?></td>
			</tr>
			<tr>
				<td class="bold">Car #</td>
				<td>
				<?php
					 if( isset($car->car_name) )
						echo @$car->car_name.' ('.@$car->car_plateno.')'; 
				?>
				</td>
			</tr>
			<tr>
				<td class="bold">Item #</td>
				<td>
				<?php
					 if( isset($consult->item_num) )
						echo @$consult->item_num; 
				?>
				</td>
			</tr>

			<tr>
				<td class="bold">Agent Name</td>
				<td><?php echo $record->agent_name ?></td>
			</tr>

		</table>
<label for="patient_note">Note <br/>                        
                              <textarea name="patient_notes"  rows="10" cols="53"  id="patient_notes" style="background:#F2DEDE"   class="form-control" readonly><?php echo @$record->patient_notes;?> </textarea> 
                            </label>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">		
		<a class="btn btn-warning" href="javascript:history.go(-1)" role="button">Back</a>
	</div>
</div>