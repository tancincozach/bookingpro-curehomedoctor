<h4 class="page-header">Booking Register</h4>

<?php if( SHOW_MILESTONE ): ?>
<div class="alert alert-warning alert-dismissible milestone-box">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="milestone-body">
        <dl>
            <dt>
                Description:
            </dt>
            <dd>
                Allow Chaperone to book an appointment
            </dd>
            <dt>
                Features/Statuses:
            </dt>
            <dd>                    
                <ul> 
                     <li>
                        If Chaperone login: only shows available and already booked appointments
                     </li>
                     <li>
                        Action buttons
                        <ul>
                            <li>Book - able to book an appointment, can set car and doctor</li>
                            <li>Unbook - able to unbook the appointment and change status to open so that other chaperone can book it</li>
                            <li>Visit - After the chaperone w/ doctor visit the patient chaperone will change the status 
                            to VISIT so that doctor can set consult notes</li>
                        </ul>
                     </li>
                     <li>Available - All available appointments waiting for Chaperone to book</li>
                     <li>Booked Appointment(s) - All appointments that the Chaperone </li>
                </ul>
            </dd>
            <dt>
                Need some clarification here
            </dt>
            <dd>
                Do we have to display all the available appointments on all Chaperone or display only specific to areas?
            </dd>
            
        </dl>        
    </div> 
</div>
<?php endif; ?>

<?php 
    $search_filter = ''; 
    $s_area = $s_bst = $s_chap = $s_doc = $s_car = array();
?>
<div class="alert alert-info">
  <strong>SEARCH TIP:</strong> You can search (1) patient name or (2) patient medicare #  ---  in order to do so, clear the date input field, enter the patient name or medicare # into the search box, then click SEARCH
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
            
            <div class="form-group">
                <label for="appt_start" class="">Date: </label>                            
                <input type="text" class="form-control input-sm"   id="filter_appt_start" name="appt_start" value="<?php echo $search_daterange; ?>">     
                <button class="btn btn-warning  btn-sm clear-date">Clear date</button>                   
            </div>

            <div class="form-group">
                <label for="search" class="">Search</label>
                <input type="text" class="form-control input-sm"  name="search" placeholder="Search appt #, firstname, lastname" value="<?php echo @$_GET['search']; ?>" title="Search: appt #, firstname, lastname, phone, mobile , medicare">
            </div>
            
            <div class="btn-group btn-sm areas">
                <button type="button" class="btn btn-default btn-sm">Areas</button>
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                <?php

                    $area = (isset($_GET['area']) AND COUNT($_GET['area'])>0 )?$_GET['area']:array();
                    foreach($areas as $key=>$val):
                        $is_check = '';
                        if( in_array($key, $area) ) {
                            $is_check = "checked";
                            $s_area[] = $val;
                        }
                ?>
                    <li>
                        <a href="return false;"  alt ="<?php echo $val; ?>" class="small noclose" data-value="<?php echo $key; ?>" tabIndex="-1">
                            <label for="area[]"><input type="checkbox" name="area[]" value="<?php echo $key; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $val; ?></label>
                        </a>
                    </li>
                <?php endforeach; ?> 
                </ul>
            </div>            

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm">Booking Status</button>
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                <?php
                    $_booking_statuses = array(
                        BOOK_STATUS_BOOKED=>'BOOKED', 
                        BOOK_STATUS_CLOSED=>'COMPLETE', 
                        BOOK_STATUS_CANCELLED=>'CANCELLED', 
                        BOOK_STATUS_VISIT=>'VISIT', 
                        BOOK_STATUS_OPEN=>'OPEN'
                    );

                    $book_status = $_booking_statuses;
                    $showing_status = '';
                    $bst = (isset($_GET['bst']) AND COUNT($_GET['bst'])>0 )?$_GET['bst']:array();
                    foreach($book_status as $key=>$val):
                        $is_check = ''; 
                        if( in_array($key, $bst) ){ 
                            $is_check = "checked";
                            $s_bst[] = $val;
                        }
                ?>
                    <li>
                        <a href="return false;" class="small noclose" data-value="<?php echo $key; ?>" tabIndex="-1">
                            <label for="bst[]"><input type="checkbox" name="bst[]" value="<?php echo $key; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $val; ?></label>
                        </a>
                    </li>
                <?php endforeach; ?> 
                </ul>
            </div>
            
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm">Chaperone's</button>
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                <?php
                    $chap = (isset($_GET['chap']) AND COUNT($_GET['chap'])>0 )?$_GET['chap']:array();
                    foreach($user_chaperone as $row):
                        $is_check = '';
                    if( in_array($row->user_id, $chap) ) {
                        $is_check = "checked";
                        $s_chap[] = $row->user_fullname;
                    }
                ?>
                    <li>
                        <a href="return false;" class="small noclose"   tabIndex="-1">
                            <label for="chap[]"><input type="checkbox" name="chap[]" value="<?php echo $row->user_id; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $row->user_fullname; ?></label>
                        </a>
                    </li>
                <?php endforeach; ?> 
                </ul>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm">Doctor's</button>
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                <?php
                    $doc = (isset($_GET['doc']) AND COUNT($_GET['doc'])>0 )?$_GET['doc']:array();
                    foreach($user_doctor as $row):
                        $is_check = '';
                        if( in_array($row->user_id, $doc) ) {
                            $is_check = "checked";
                            $s_doc[] = $row->user_fullname;
                        }
                ?>
                    <li>
                        <a href="return false;" class="small noclose"  tabIndex="-1">
                            <label for="doc[]"><input type="checkbox" name="doc[]" value="<?php echo $row->user_id; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $row->user_fullname; ?></label>
                        </a>
                    </li>
                <?php endforeach; ?> 
                </ul>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm">Car's</button>
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                <?php
                    $car = (isset($_GET['car']) AND COUNT($_GET['car'])>0 )?$_GET['car']:array();
                    foreach($cars as $row):
                        $is_check = '';
                    if( in_array($row->car_id, $car) ) {
                        $is_check = "checked";
                        $s_car[] = $row->car_name;
                    }
                ?>
                    <li>
                        <a href="return false;" class="small noclose" tabIndex="-1">
                            <label for="car[]"><input type="checkbox" name="car[]" value="<?php echo $row->car_id; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $row->car_name; ?></label>
                        </a>
                    </li>
                <?php endforeach; ?> 
                </ul>
            </div>            


            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm">Search</button>
                <a href="callcenter" type="button" class="btn btn-warning btn-sm">Reset</a>
                
            </div>
             <div class="form-group pull-right">
             <button  class="btn btn-success btn-sm download-medicard "><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download Medicare</button>
             <button  class="btn btn-danger btn-sm download-consult-notes "><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download Consult Notes</button>
                 
             </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 bg-info">
        <strong>DROPDOWN FILTER'S:</strong>
        <?php echo '<strong>AREAS</strong>: <span class="text-primary">'.((count($s_area)>0)?implode(',', $s_area):'None').'</span>'; ?> |
        <?php echo '<strong>BOOKING STATUS</strong>: <span class="text-primary">'.((count($s_bst)>0)?implode(',', $s_bst):'None').'</span>'; ?> |
        <?php echo '<strong>CHAPERONE\'S</strong>: <span class="text-primary">'.((count($s_chap)>0)?implode(',', $s_chap):'None').'</span>'; ?> |
        <?php echo '<strong>DOCTOR\'S</strong>: <span class="text-primary">'.((count($s_doc)>0)?implode(',', $s_doc):'None').'</span>'; ?> |
        <?php echo '<strong>CAR\'S</strong>: <span class="text-primary">'.((count($s_car)>0)?implode(',', $s_car):'None').'</span>'; ?>
    </div>
</div>

<div class="clearfix"><br /></div>
 
<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">

        <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
        
        
        
        <table class="table table-bordered table-condensed table-appointment">
            <thead>
                <tr>
                    <th style="width: 105px">Appt #</th>                     
                    <th>Area</th>
                    <th>Chaperone Notes</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Suburb</th>
                    <th>Postcode</th>
                    <th>Phone</th>
                    <th>Age</th>
                    <th>Symptoms</th>
                    <th>Status</th>
                    <th>Date Booked</th>
                    <th>Time Booked</th>
                    <th>Time in Queue (h:m:s)</th>
                    <th>Booked By</th>
                    <th>Start Appt</th>
                    <th>End Appt</th>
                    <th>Regular Practice</th>
                    <th width="125px">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 

                if( count($results) > 0 ):
                    foreach($results as $row): 

                        $tr_bg_cls = '';

                        $q_ex = explode(':',$row->in_queue);
                        $hr = intval($q_ex[0]);                    

                        if( $row->book_status == BOOK_STATUS_OPEN ){

                            if( $hr > 3  ) $tr_bg_cls = 'OPEN_QUEUE_MORE_3HR';
                            if( $hr >= 2 AND $hr <= 3  ) $tr_bg_cls = 'OPEN_QUEUE_BET_2_3HR';
                            if( $hr < 2  ) $tr_bg_cls = 'OPEN_QUEUE_LESS_2HR';

                        }elseif ($row->book_status == BOOK_STATUS_BOOKED ) {
                            $tr_bg_cls = 'BOOKED';
                        }elseif ($row->book_status == BOOK_STATUS_CANCELLED ) {
                            $tr_bg_cls = 'CANCELLED';
                        }elseif ($row->book_status == BOOK_STATUS_UNSEEN ) {
                            $tr_bg_cls = 'UNSEEN'; 
                        }else{
                            $tr_bg_cls = 'VISIT_CLOSED';
                        }                    
                ?>
                <tr class="<?php echo $tr_bg_cls; ?>">
                    <td><?php echo $row->ref_number_formatted; ?></td>
                    <td><?php 
                        echo @$row->area_name; 
                        if( $row->flag_eta ) echo ' <span style="font-weight: bold; background-color: yellow; color: red">ETA</span>';
                    ?></td>
                    <td><?php echo stripslashes($row->appt_notes); ?></td>
                    <td><?php echo stripslashes($row->firstname.' '.$row->lastname); ?></td>
                    <td><?php echo stripslashes($row->street_addr); ?></td>
                    <td><?php echo $row->suburb; ?></td>
                    <td><?php echo $row->postcode; ?></td>
                    <td><?php echo $row->phone; ?></td>
                    <td><?php echo $row->patient_age; ?></td>
                    <td><?php echo stripslashes($row->symptoms); ?></td>
                    <!--<td><strong><?php echo ($row->book_status==4)?$this->booking_statuses[0]:$this->booking_statuses[$row->book_status]; ?></strong></td>-->
                    <td><strong><?php echo $this->booking_statuses[$row->book_status]; ?></strong></td>
                    <td><?php echo date('d/m/Y', strtotime($row->appt_created_tz))?></td>
                    <td><?php echo date('H:i', strtotime($row->appt_created_tz))?></td>
                    <td><?php echo $row->in_queue; ?></td>
                    <td><?php 
                        if($row->book_status == BOOK_STATUS_CANCELLED)
                            echo 'Reason <br />'.stripslashes($row->cancelled_reason);
                        else
                            echo @$row->chaperone_name.'/'.@$row->doctor_name.'/'.@$row->car_name; 
                    ?></td>
                    <td><?php echo ($row->appt_start_queue!='')?date('H:i', $row->consult_start):''; ?></td>
                    <td><?php echo ($row->appt_end_queue!='')?date('H:i', $row->consult_end):''; ?></td>
                    <td><?php echo $row->regular_practice;?></td>
                    <td>

                        <?php  if( isset($_GET['stype']) AND $_GET['stype'] == 'cancel'): ?>
                            <a class="btn btn-danger btn-xs" href="callcenter/cancel_booking/<?php echo $row->appt_id; ?>">Cancel Booking</a>
                        <?php  elseif( isset($_GET['stype']) AND $_GET['stype'] == 'eta'): ?>
                            <a class="btn btn-info btn-xs" href="callcenter/eta_required/<?php echo $row->appt_id; ?>">ETA Required</a>
                        <?php  else: ?>
                        <div class="btn-group dropup pull-right">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="callcenter/view/<?php echo $row->appt_id; ?>">View</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="callcenter/cancel_booking/<?php echo $row->appt_id; ?>">Cancel Booking</a></li>
                                <li><a href="callcenter/eta_required/<?php echo $row->appt_id; ?>">ETA Required</a></li>
                                <li><a href="callcenter/send_dr_info/<?php echo $row->appt_id; ?>">Send Dr Info</a></li>
                                <?php  if(isset($row->consult_id)): ?>
                                 <li><a href="callcenter/download_consult_note/<?php echo $row->appt_id; ?>">Download Consult Note</a></li>
                                <?php endif; ?>     
                                <?php  if($row->book_status == BOOK_STATUS_CLOSED): ?>
                                <li><a href="callcenter/download_medicare_form/<?php echo $row->appt_id; ?>">Download Medicare Form</a></li>
                            <?php endif; ?>
                            </ul>
                        </div>
                        <?php  endif; ?>
                    </td>
                </tr>
                <?php endforeach; 
                    else:
                ?>
                <tr>
                    <td colspan="19">No Records Found</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>

        
        <div class="clearfix">
            <div class="pull-left text-left">
                 <?php echo $showing; ?>
            </div>
            <div class="pull-right text-right">
                <ul class="pagination pagination-sm">
                    <?php echo $links; ?>
                </ul>
            </div> 
        </div>

        <div class="clearfix">
        </div>
        <input type="hidden" id="download_medicard_default_date" value="<?php echo @$download_medicard_default_date?>"/>
    </div>


</div>