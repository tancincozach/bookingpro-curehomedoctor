 
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2 class="page-header">Make an Enquiry</h2>

			<?php echo form_open('', 'class="form-horizontal online-form" id="online-form" name="online_form" method="post" onsubmit="return hcd.common.confirm(\'You are about to submit, please confirm!\')"'); ?>
				<div class="form-group">
					<label for="First_Name" class="col-sm-3 col-md-3 control-label">First Name</label>
					<div class="col-sm-4 col-md-4">
						<input type="text" class="form-control" name="First_Name" required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="Last_Name" class="col-sm-3 col-md-3 control-label">Last Name</label>
					<div class="col-sm-4 col-md-4">
						<input type="text" class="form-control" name="Last_Name" required>
					</div>
				</div>

				<div class="form-group">
					<label for="Phone" class="col-sm-3 col-md-3 control-label">Phone</label>
					<div class="col-sm-4 col-md-4">
						<input type="text" class="form-control" name="Phone" required>
					</div>
				</div>

				<div class="form-group">
					<label for="DOB" class="col-sm-3 col-md-3 control-label">Date of Birth</label>
					<div class="col-sm-4 col-md-4">
						<input type="text" class="form-control dob" name="DOB" required data-format="DD/MM/YYYY" data-template="D MMM YYYY" >
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-3 col-md-4">
					<button type="submit" class="btn btn-default">Submit</button>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div> 
