<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>House Call Doctor Online Booking</title>

	<base href="<?php echo base_url(); ?>" />

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet">

    <link href="assets/css/styles-online.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">

     


    </style>

</head>
<body> 
   <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">House Call Doctor</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
 
          <ul class="nav navbar-nav navbar-right">
				<li class="active">
					<a href="online"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
				</li>
				<li><a href="#">New Booking</a></li>
				<li><a href="#">Call HCD</a></li>
				<li><a href="#">Make an Enquiry</a></li>
				<li><a href="#">Request ETA</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

 	

   	<div class="boxed">

   		<div class="container">
   			<div class="row">
   				<div class="col-md-12">

   					<h2 class="page-header">Request ETA</h2>

					<form class="form-horizontal">
						<div class="form-group">
							<label for="First_Name" class="col-sm-3 col-md-3 control-label">First Name</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="First_Name" required>
							</div>
						</div>
						
						<div class="form-group">
							<label for="Last_Name" class="col-sm-3 col-md-3 control-label">Last Name</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="Last_Name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="Phone" class="col-sm-3 col-md-3 control-label">Phone</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="Phone" required>
							</div>
						</div>

						<div class="form-group">
							<label for="DOB" class="col-sm-3 col-md-3 control-label">Date of Birth</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control dob" name="DOB" required data-format="DD/MM/YYYY" data-template="D MMM YYYY" >
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-3 col-md-4">
							<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</div>
					</form>
   				</div>
   			</div>
   		</div>

   	</div>


   	<div class="boxed">

   		<div class="container">
   			<div class="row">
   				<div class="col-md-12">

   					<h2 class="page-header">Make an Enquiry</h2>

					<form class="form-horizontal">
						<div class="form-group">
							<label for="First_Name" class="col-sm-3 col-md-3 control-label">First Name</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="First_Name" required>
							</div>
						</div>
						
						<div class="form-group">
							<label for="Last_Name" class="col-sm-3 col-md-3 control-label">Last Name</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="Last_Name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="Phone" class="col-sm-3 col-md-3 control-label">Phone</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="Phone" required>
							</div>
						</div>

						<div class="form-group">
							<label for="DOB" class="col-sm-3 col-md-3 control-label">Date of Birth</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control dob" name="DOB" required data-format="DD/MM/YYYY" data-template="D MMM YYYY" >
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-3 col-md-4">
							<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</div>
					</form>
   				</div>
   			</div>
   		</div>

   	</div>


	<div class="boxed">

   		<div class="container">
   			<div class="row">
   				<div class="col-md-12">

   					<h2 class="page-header">New Booking</h2>

					<form class="form-horizontal">
						<div class="form-group">
							<label for="suburb" class="col-sm-3 col-md-3 control-label">Suburb</label>
							<div class="col-sm-4 col-md-4">
								<input type="text" class="form-control" name="suburb" required>
							</div>
						</div>
						
						
					</form>
   				</div>
   			</div>
   		</div>

   	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/plugins/moment/moment.min.js"></script>
	<script src="assets/plugins/moment/en-au.js"></script>
	<script src="assets/plugins/moment/moment-timezone-with-data-2010-2020.js"></script>
	<script src="assets/plugins/moment/moment-timezone.min.js"></script>

	<script src="assets/plugins/combodate.js"></script>
	<script src="assets/plugins/select2/js/select2.min.js"></script>
	 
	<script src="assets/js/apps.js"></script>
	<script src="assets/js/page_js/online.js"></script>

	<script type="text/javascript">
		


	</script>
</body>
</html>