<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>House Call Doctor Online Booking</title>

	<base href="<?php echo base_url(); ?>" />

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet">

    <link href="assets/css/styles-online.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
	    
	    var hcd_globals = {        

	        cookie_name:"<?php echo $this->config->item('sess_cookie_name'); ?>",
	        n:'<?php echo $this->security->get_csrf_token_name(); ?>',
	        h:'<?php echo $this->security->get_csrf_hash(); ?>',
	        base_url: '<?php echo base_url(); ?>'

	    }

	</script>

</head>
<body>


   <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand img-logo" href="online" alt="House Call Doctor"><img class="" src="assets/images/logo.png" /></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">

			</div><!--/.nav-collapse -->
		</div>

    </nav>

	
	<div class="boxed" id="features"> 
		<div class="container">
			<div class="row">
				<div class="text-center">
					<h2 class="page-header page-header-custom">Online Booking</h2>				
				</div>

				<?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg">'.$this->session->flashdata('fmesg').'</div>':''; ?>

				<div class="box-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<a class="" href="online/book">
						<i class="fa fa-book fa-4x"></i>
						<h3>New Booking</h3>
						 
					</a>
				</div>

				<div class="box-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<a class="" href="javascript:void(0)" onclick="online.call('<?php echo $this->tel_no; ?>')"> 
						<i class="fa fa-phone fa-4x"></i>
						<h3>Call House Call Doctor</h3>
						 
					</a>
				</div>
				
				<div class="clearfix visible-lg-block"></div>

				<div class="box-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<a class="" href="online/enquiry">
						<i class="fa fa-inbox fa-4x"></i>
						<h3>Make an Enquiry</h3>
						 
					</a>
				</div>

				<div class="box-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<a class="" href="online/eta">
						<i class="fa fa-info-circle fa-4x"></i>
						<h3>Request ETA from doctor</h3>
						 
					</a>
				</div>

				<div class="clearfix visible-sm-block"></div> 
	 
	    	</div>
	   </div>
   	</div>
 	
    <div id="footer">
        
        <div class="container">
          <div class="row">
              <br />
              <div class="col-lg-12 text-center">
                  <p><?php echo SITE_FOOTER; ?></p>
              </div>
          </div> 
        </div>

    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/plugins/moment/moment.min.js"></script>
	<script src="assets/plugins/moment/en-au.js"></script>
	<script src="assets/plugins/moment/moment-timezone-with-data-2010-2020.js"></script>
	<script src="assets/plugins/moment/moment-timezone.min.js"></script>

	<script src="assets/plugins/combodate.js"></script>
	<script src="assets/plugins/select2/js/select2.min.js"></script>
	 
	<script src="assets/js/apps.js"></script>
	<script src="assets/js/page_js/online.js"></script>

	<script type="text/javascript">
		


	</script>
</body>
</html>