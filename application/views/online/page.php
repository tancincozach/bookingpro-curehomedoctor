<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>House Call Doctor Online Booking</title>

	<base href="<?php echo base_url(); ?>" />

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet">

    <link href="assets/css/styles-online.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        
        var hcd_globals = {        

            cookie_name:"<?php echo $this->config->item('sess_cookie_name'); ?>",
            n:'<?php echo $this->security->get_csrf_token_name(); ?>',
            h:'<?php echo $this->security->get_csrf_hash(); ?>',
            base_url: '<?php echo base_url(); ?>'

        }

    </script>

</head>
<body> 
   <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container page">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand img-logo" href="#" alt="House Call Doctor">
          	<img class="" src="assets/images/logo.png" />
          </a>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
 
          <ul class="nav navbar-nav navbar-right online-navbar">
				<li class="<?php echo($header_active =='home')?'active':'';  ?>" >
					<a href="online">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 
                        <span class="nav-item-title">Home</span>
                    </a>
				</li>
				<li class="<?php echo($header_active =='book')?'active':'';  ?>" >
					<a href="online/book" title="New Booking">
                        <span class="glyphicon glyphicon-book" aria-hidden="true"></span> 
                        <span class="nav-item-title">New Booking</span>
                    </a>
				</li>
				<li class="<?php echo($header_active =='call')?'active':'';  ?>" >
					<a href="javascript:void(0)" onclick="online.call('<?php echo $this->tel_no; ?>')" title="Call House Call Doctor">
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                        <span class="nav-item-title">Call House Call Doctor</span>
                    </a>
				</li>
				<li class="<?php echo($header_active =='eta')?'active':'';  ?>" >
					<a href="online/enquiry" title="Make an Enquiry">
                        <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span>
                        <span class="nav-item-title">Make an Enquiry</span>
                    </a>
				</li>
				<li class="<?php echo($header_active =='enquiry')?'active':'';  ?>" >
					<a href="online/eta" title="Request ETA from doctor">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        <span class="nav-item-title">Request ETA from doctor</span> 
                    </a>
				</li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

 	

   	<div class="boxed">

   		<?php $this->load->view($view_file, @$data); ?>

   	</div> 

    <div id="footer">
        
        <div class="container">
          <div class="row">
              <br />
              <div class="col-lg-12 text-center">
                  <p><?php echo SITE_FOOTER; ?></p>
              </div>
          </div> 
        </div>

    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/plugins/moment/moment.min.js"></script>
	<script src="assets/plugins/moment/en-au.js"></script>
	<script src="assets/plugins/moment/moment-timezone-with-data-2010-2020.js"></script>
	<script src="assets/plugins/moment/moment-timezone.min.js"></script>

	<script src="assets/plugins/combodate.js"></script>
	<script src="assets/plugins/select2/js/select2.min.js"></script>
	 
	<script src="assets/js/apps.js"></script>
	<script src="assets/js/page_js/online.js"></script>

	<script type="text/javascript">
		


	</script>
</body>
</html>