<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="shortcut icon" href="assets/ico/favicon.png">-->
<base href="<?php echo base_url(); ?>" />

<title><?php echo BRAND_NAME; ?> - Request Reset Password </title>

<!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet">
 
<script src="assets/js/jquery-1.11.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script>
     

   
</script>
  
<style>

    .center-block {
        float: none;
    }

    .logo_div {
        padding: 50px;
    }

    .login_div {
       /* padding-top: 10px;
        margin-top: 10px;*/
        /*border-top: 1px solid #ccc;*/
    }

    .tabs-left > .nav-tabs {
      border-bottom: 0;
    }

    .tabs-left > .nav-tabs > li {
      float: none;
    }

    .tabs-left > .nav-tabs > li > a {
      min-width: 74px;
      margin-right: 0;
      margin-bottom: 3px;
    }

    .tabs-left > .nav-tabs {
      float: left;
      margin-right: 19px;
      border-right: 1px solid #ddd;
    }

    .tabs-left > .nav-tabs > li > a {
      margin-right: -1px;
      -webkit-border-radius: 4px 0 0 4px;
         -moz-border-radius: 4px 0 0 4px;
              border-radius: 4px 0 0 4px;
    }

    .tabs-left > .nav-tabs > li > a:hover,
    .tabs-left > .nav-tabs > li > a:focus {
      border-color: #eeeeee #dddddd #eeeeee #eeeeee;
    }

    .tabs-left > .nav-tabs .active > a,
    .tabs-left > .nav-tabs .active > a:hover,
    .tabs-left > .nav-tabs .active > a:focus {
      border-color: #ddd transparent #ddd #ddd;
      *border-right-color: #ffffff;
    }

    img.logo {
        display: block; 
        margin: 0 auto;
    }

    @media (max-width: 768px) {
        img.logo {
            width: 95%;
        }

        .logo_div {
            padding: 10px 0px;
        }
        
    }

</style>

</head>

<body>
<div class="container"> 
    <div class="row logo_div">
        <div class="col-xs-12 col-sm-10 center-block">
            <img class="logo" alt="<?php echo BRAND_NAME; ?>" src="<?php echo BRAND_LOGO_PATH; ?>" />
        </div>
    </div>
    
  
    <div class="row">
 
        <div class="col-sm-8 center-block clearfix" style="border: 1px solid #ccc; padding: 20px">
            
            <?php
                $log_msg = @trim($this->session->flashdata('login_message'));
                
                $log_msg = explode('|', $log_msg);

                $login_type = isset($log_msg[0])?$log_msg[0]:'';
                $login_message = isset($log_msg[1])?$log_msg[1]:'';
               
                if($login_message != ''){
                    echo '<p class="text-danger">'.$login_message.'</p>';
                }
            ?>

            <div class="clearfix">
 

           
                <div class="col-sm-10 clearfix">
                    <h4>Request reset password</h4>
                    
                    <p class="note">
                        Password will be sent to your email or mobile phone.

                    </p>

                    <?php echo form_open('', 'class="form-horizontal" role="form" method="post"'); ?>
                        
                        <input type="hidden" name="login_type" value="callcenter" />

                        <!--<div class="form-group">
                            <label for="username" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="youruname" value="" required >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="username" class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="youruname" value="" required >
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label for="username" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="youruname" value="" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="yourpass" class="col-sm-3 control-label">Set Password</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="yourpass" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-7">
                                <button type="submit" class="btn btn-primary">Submit</button>                                        
                            </div>
                        </div>
                    </form>
                </div>
                     
 
                 

                <br />
                <br />
            </div>

            <a href="login" class="">go back to Login page</a>
 
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-lg-12 text-center">
            <p><?php echo SITE_FOOTER; ?></p>
        </div>
    </div>

</div>
</body>
</html>