<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="shortcut icon" href="assets/ico/favicon.png">-->
<base href="<?php echo base_url(); ?>" />

<title><?php echo BRAND_NAME; ?> - Sign In </title>

<!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
 
<script src="assets/js/jquery-1.11.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){
      $("#yourname").focus(); 
    });

    var Login = {
      onSubmit: function(form){
        //window.location = "dashboard";
        var go = true;
        $(form).find('input').each(function(){
            var fv = jQuery.trim($(this).val());

            /*if(fv == '' && $(this).hasAttr('required') ){
            go = false;
            }*/
            //console.log($(this).val());
        });

        if(go){
            return true;
        }

        $('#login_message').html('<p>All fields are required</p>');
            return false;
      },

      get_chaperone_details: function(){
        
      }

       
    }
</script>
  
<style>

    .center-block {
        float: none;
    }

    .logo_div {
        padding: 50px;
    }

    .login_div {
       /* padding-top: 10px;
        margin-top: 10px;*/
        /*border-top: 1px solid #ccc;*/
    }

    .tabs-left > .nav-tabs {
      border-bottom: 0;
    }

    .tabs-left > .nav-tabs > li {
      float: none;
    }

    .tabs-left > .nav-tabs > li > a {
      min-width: 74px;
      margin-right: 0;
      margin-bottom: 3px;
    }

    .tabs-left > .nav-tabs {
      float: left;
      margin-right: 19px;
      border-right: 1px solid #ddd;
    }

    .tabs-left > .nav-tabs > li > a {
      margin-right: -1px;
      -webkit-border-radius: 4px 0 0 4px;
         -moz-border-radius: 4px 0 0 4px;
              border-radius: 4px 0 0 4px;
    }

    .tabs-left > .nav-tabs > li > a:hover,
    .tabs-left > .nav-tabs > li > a:focus {
      border-color: #eeeeee #dddddd #eeeeee #eeeeee;
    }

    .tabs-left > .nav-tabs .active > a,
    .tabs-left > .nav-tabs .active > a:hover,
    .tabs-left > .nav-tabs .active > a:focus {
      border-color: #ddd transparent #ddd #ddd;
      *border-right-color: #ffffff;
    }

    img.logo {
        display: block; 
        margin: 0 auto;
    }

    @media (max-width: 768px) {
        img.logo {
            width: 95%;
        }

        .logo_div {
            padding: 10px 0px;
        }

    }


</style>

</head>

<body>
<div class="container"> 
    <div class="row logo_div">
        <div class="col-xs-12 col-sm-6 center-block text-center ">
            <img class="logo" alt="<?php echo BRAND_NAME; ?>" src="<?php echo BRAND_CLIENT_LOGO_PATH; ?>" style="height: 120px" />
            <br />
            <img class="logo" alt="<?php echo BRAND_NAME; ?>" src="<?php echo BRAND_LOGO_PATH; ?>" />
        </div>
    </div>

    <div class="">
 
        <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6 center-block clearfix" style="border: 1px solid #ccc; padding: 20px">
            
            <?php
                $log_msg = @trim($this->session->flashdata('login_message'));
                
                $log_msg = explode('|', $log_msg);

                $login_type = isset($log_msg[0])?$log_msg[0]:'';
                $login_message = isset($log_msg[1])?$log_msg[1]:'';
               
                if($login_message != ''){
                    echo '<p class="text-danger">'.$login_message.'</p>';
                }
            ?>

            <div class="clearfix">

                <ul class="nav nav-tabs ">
                    <li class="<?php echo ($login_type == 'client' OR $login_type == '')?'active':''; ?>"><a data-toggle="tab" href="#menu1">Client</a></li> 
                    <li class="<?php echo ($login_type == 'callcenter' )?'active':''; ?>"><a data-toggle="tab" href="#home">CMA</a></li>
                </ul>

                <div class="tab-content clearfix">
                    

                    <div id="menu1" class="tab-pane fade <?php echo ($login_type == 'client' OR $login_type == '' )?'in active':''; ?>">
                        <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12 clearfix">
                            <h3>Login as : Client</h3>
                        
                            <?php echo form_open('', 'class="form-horizontal" role="form" method="post" onSubmit="return Login.onSubmit(this);"'); ?>
                                
                                <input type="hidden" name="login_type" value="client" />

                                <div class="form-group">
                                    <label for="username" class="col-sx-4 col-sm-2 col-md-3 col-lg-3 control-label">Username</label>
                                    <div class="col-sx-4 col-sm-6 col-md-6 col-lg-6">
                                        <input type="text" class="form-control" name="youruname" value="" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="yourpass" class="col-sx-4 col-sm-2 col-md-3 col-lg-3 control-label">Password</label>
                                    <div class="col-sx-4 col-sm-6 col-md-6 col-lg-6">
                                        <input type="password" class="form-control" name="yourpass" value="" required>                                         
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-offset-2 col-sm-6 col-md-offset-4 col-md-6 col-lg-offset-3 col-lg-6">
                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div id="home" class="tab-pane fade <?php echo ($login_type == 'callcenter' )?'in active':''; ?>">
                        <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12 clearfix">
                            <h3>Login as : CMA</h3>
                        
                            <?php echo form_open('', 'class="form-horizontal" role="form" method="post" onSubmit="return Login.onSubmit(this);"'); ?>
                                
                                <input type="hidden" name="login_type" value="callcenter" />

                                <div class="form-group">
                                    <label for="username" class="col-sx-4 col-sm-2 col-md-3 col-lg-3 control-label">Username</label>
                                    <div class="col-sx-4 col-sm-6 col-md-6 col-lg-6">
                                        <input type="text" class="form-control" name="youruname" value="" required >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="yourpass" class="col-sx-4 col-sm-2 col-md-3 col-lg-3 control-label">Password</label>
                                    <div class="col-sx-4 col-sm-6 col-md-6 col-lg-6">
                                        <input type="password" class="form-control" name="yourpass" value="" required>                                        
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-offset-2 col-sm-6 col-md-offset-4 col-md-6 col-lg-offset-3 col-lg-6">
                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                 
                </div>
            </div>
            
            <div class="clearfix text-center">
                <br />
                <a href="welcome/reset_password">Request reset password</a> 
            </div>
            
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-lg-12 text-center">
            <p><?php echo SITE_FOOTER; ?></p>
        </div>
    </div> 

</div>
</body>
</html>