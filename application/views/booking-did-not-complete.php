 
<h4 class="page-header">Booking did not complete</h4>


    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Booking an appointment
                </dd>
                <dt>
                    Features/Statuses:
                </dt>
                <dd>                    
                    <ul> 
                        <li>Booking: today</li> 
                        <li>Booking: next day</li> 
                        <li>Booking: holiday
                        	<ul>
                        		<li>Plan: if booking next day and its holiday the system should advice operator that the booking will be happen on the date</li>
                        	</ul>
                        </li> 
                        <li>Booking: Duplicate patient today</li> 
                        <li>Age: the system should automatically calculate date base on date of birth </li> 
                    </ul>
                </dd>
                  
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>
 

 
 	<div class="col-lg-8 col-md-8 col-sm-8 clearfix">

		<?php echo form_open('', 'class="form-horizontal" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm appt declined!\')"'); ?>

            <?php 

                foreach($collection as $field=>$value):
                    echo  '<input type="hidden" name="'.$field.'" value="'.$value.'" />';
                endforeach; 
            ?>
            <div class="form-group">
                <label for="doctor" class="col-sm-3 control-label" style="text-align: left">Name</label>
                <div class="col-lg-4">
                    <p class="form-control-static"><?php echo @$collection['First_Name'].' '.@$collection['Last_Name']; ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-sm-3 control-label" style="text-align: left">Phone</label>
                <div class="col-lg-4">
                    <p class="form-control-static"><?php echo @$collection['Phone']; ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-sm-3 control-label" style="text-align: left">Suburb</label>
                <div class="col-lg-4">
                    <p class="form-control-static"><?php echo @$collection['Suburb']; ?></p>
                </div> 
            </div>

            <div class="form-group">
                <label for="doctor" class="col-sm-3 control-label" style="text-align: left">Cancel Reason</label>
                <div class="col-lg-5">
                    <textarea class="form-control" rows="4" name="reason" required></textarea>
                </div>
            </div>
           
            <br />
            <br />
            <button  class="btn btn-primary" type="submit" name="btn-submit" value="1">Submit Appts Available, declined Appt</button>

		</form>
 
 
	</div>
 