 
<h4 class="page-header">Call Register</h4>

<!-- search -->
<div>
    <?php echo form_open('report/call_register', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
        <div class="form-group">
            <label for="appt_start">Date</label>
            <input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>" required>
        </div>
        <button type="submit" name="download" class="btn btn-warning">Download Data</button> 
    </form>
</div>
<br />

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

        <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
        
        <table class="table table-bordered table-condensed table-appointment">
            <thead>
                <tr>
                    <!-- <th style="width: 105px">Appt #</th> -->
                    <th>Date/Time</th>
                    <th>Type</th>
                    <th>Caller Name</th>
                    <th>Phone</th>
                    <th>Suburb</th>
                    <th>Patient Name</th>
                    <th>Details</th>
                    <th>Agent Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($results as $row): ?>
                <tr>
                    <!-- <td><?php echo $row->ref_number_formatted; ?></td> -->
                    <td><?php echo $row->tran_created_tz; ?></td>
                    <td><?php echo $row->tran_type.' '.((trim($row->tran_desc)!='')?' - '.$row->tran_desc:''); ?></td>
                    <td><?php echo stripslashes($row->caller_firstname.' '.$row->caller_lastname); ?></td>
                    <td><?php echo $row->caller_phone; ?></td>
                    <td><?php echo stripslashes($row->caller_suburb); ?></td>
                    <td><?php echo (trim($row->firstname.$row->lastname)!='') ? stripslashes(@$row->firstname.' '.@$row->lastname):$row->appt_patient_name; ?></td>
                    <td><?php echo stripslashes(nl2br($row->tran_details)); ?></td>
                    <td><?php echo $row->agent_name; ?></td>
                    <td></td>
                </tr>
                <?php endforeach; ?> 
            </tbody>
        </table>

        <div class="clearfix">
            <div class="pull-left text-left">
                 <?php echo $showing; ?>
            </div>
            <div class="pull-right text-right">
                <ul class="pagination pagination-sm">
                    <?php echo $links; ?>
                </ul>
            </div> 
        </div>

    </div>
</div>
