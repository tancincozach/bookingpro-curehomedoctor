<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>House Call Doctor</title>

<base href="<?php echo base_url(); ?>" />

<!-- Bootstrap -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <img alt="House Call Doctor Log" src="assets/images/logo.png" />
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 text-right schedule-label">
                        <strong>Schedule:</strong>
                    </div>
                    <div class="col-md-8" style="font-size: 13px; ">
                        Mon-Fri - From 6pm <br/>
                        Saturday - From 12 Midday <br/>
                        Sunday & Pub Holiday - All Day
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default navbar-static-top" role="navigation" id="custom-bootstrap-menu">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span><span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">House Call Doctor</a>
                </div>
                
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>                        
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Booking<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">New</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Cancel</a></li>                                 
                            </ul>
                        </li>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Call<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Register</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Outbound</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="#">Chaperone</a></li>
                        <li><a href="#">Doctor</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                     
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maintenance<strong class="caret"></strong></a>
                            <div class="dropdown-menu">
                                <div class="row" style="width: 300px;">
                                    <div class="col-md-6">
                                        <ul>
                                            <li><a href="#">test 1</a></li>
                                            <li><a href="#">test 1</a></li>
                                            <li><a href="#">test 1</a></li>
                                            <li><a href="#">test 1</a></li>
                                            <li><a href="#">test 1</a></li>
                                            <li><a href="#">test 1</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">test 1</div>
                                </div>
                           </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setting<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Logout</a>
                                </li>                                
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </nav>
            <div class="jumbotron">
                <h2>
                    Hello, world!
                </h2>
                <p>
                    This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.
                </p>
                <p>
                    <a class="btn btn-primary btn-large" href="#">Learn more</a>
                </p>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    Copyright © 2015 House Call Doctor Application | Welldone International
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery-1.11.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>