 
<?php $patient = @$getvars['patient']; ?>

<?php 
	echo strpos('61244223878@efaxsend.com', '@efaxsend.com');
?>

<h4 class="page-header">Booking Form - Search Patient</h4>


    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Booking an appointment
                </dd>
                <dt>
                    Features/Statuses:
                </dt>
                <dd>                    
                    <ul> 
                        <li>Booking: today</li> 
                        <li>Booking: next day</li> 
                        <li>Booking: holiday
                        	<ul>
                        		<li>Plan: if booking next day and its holiday the system should advice operator that the booking will be happen on the date</li>
                        	</ul>
                        </li> 
                        <li>Booking: Duplicate patient today</li> 
                        <li>Age: the system should automatically calculate date base on date of birth </li> 
                    </ul>
                </dd>
                  
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>

<div class="row text-center" style="font-weight: 700">
	<?php 
		if( $when == 'today' ) {
			
			echo '<strong style="color: red">TODAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y');

		}elseif($when == 'nextday'){
			echo '<strong style="color: red">NEXT DAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y', strtotime('+1 day'));
		}



	 ?>
	 
</div>

<div class="row">
 
	<?php if( $getvars['patient_type'] == '' ): ?>
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="row">

                <?php 
                	$hidden = '';
                    foreach($getvars['collection'] as $field=>$value): 

                        if( $field == 'Suburb' ){
                            foreach( $value as $suburb_field=>$suburb_val ){
                                $hidden .= '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                            }
                        }else
                            $hidden .= '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                
                    endforeach; 
                ?>   
			 	 
			 	Search matches:
			 	<table class="table">
			 		<tr>
			 			<th>Search</th>
			 			<th>Matches</th>
			 			<th></th>
			 		</tr>
			 		<?php  
			 		 
			 			foreach($getvars['record'] as $row): ?>
			 		<tr>
			 			<td>			 				
					 		<p>Name: <strong><?php echo $getvars['collection']['First_Name'].' '.$getvars['collection']['Last_Name']; ?></strong></p>
						 	<p>&nbsp;</p>
						 	<p>Phone: <strong><?php echo $getvars['collection']['Phone']; ?></strong> </p>						 	
						 	<p>Suburb: <strong><?php echo $getvars['collection']['Suburb']['suburb']; ?></strong></p>
			 			</td>
			 			<td>
			 				<p>Name: 
			 				<?php
			 					
			 					echo highlight_phrase(strtoupper(trim($row->firstname)), strtoupper(trim($getvars['collection']['First_Name'])), '<span class="highlight_search_match">', '</span>');
			 					echo ' ';
			 					echo highlight_phrase(strtoupper(trim($row->lastname)), strtoupper(trim($getvars['collection']['Last_Name'])), '<span class="highlight_search_match">', '</span>');
			 				?>
			 				</p>

			 				<p>DOB: <?php echo date('d/m/Y', strtotime($row->dob)); ?></p>
							<p>Phone: <?php echo highlight_phrase(strtoupper(trim($row->phone)), strtoupper(trim($getvars['collection']['Phone'])), '<span class="highlight_search_match">', '</span>'); ?></p>
			 				<p>Address: <?php echo stripslashes($row->street_addr).' '.highlight_phrase(strtoupper(trim($row->suburb)), strtoupper(trim($getvars['collection']['Suburb']['suburb'])), '<span class="highlight_search_match">', '</span>'); ?></p>
			 			</td>
			 			<td>
			 				<?php echo form_open('', 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post"'); ?>
			 					<input type="hidden" name="collection[patient_id]" value="<?php echo $row->patient_id ?>">
			 					<?php echo $hidden; ?>
			 					<button class="btn btn-sm btn-primary" type="submit" name="patient_type" value="existing"><strong>Book Existing Patient</strong></button>

			 				</form>
			 			</td>
			 		</tr>
			 		<?php endforeach; 
			 			 
			 		?>
			 		<tr class="bg-warning">
			 			<td>
					 		<p>Name: <strong><?php echo $getvars['collection']['First_Name'].' '.$getvars['collection']['Last_Name']; ?></strong></p>						 	 
						 	<p>Phone: <strong><?php echo $getvars['collection']['Phone']; ?></strong> </p>						 	
						 	<p>Suburb: <strong><?php echo $getvars['collection']['Suburb']['suburb']; ?></strong></p>
			 			</td>
			 			<td style="vertical-align: middle; font-weight: bold"> No match found? Please click the the Booking New Patient</td>
			 			<td>
			 				<?php echo form_open('', 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post"'); ?>
			 				<input type="hidden" name="collection[patient_id]" value="">
							<?php echo $hidden; ?>
							<button class="btn btn-sm btn-success" type="submit" name="patient_type" value="new"><strong>Book New Patient</strong></button>
							</form>
			 			</td>
			 		</tr>
			 	</table>

                <!--
				<button style="display:none" id="btn-patient_type-1" class="btn btn-primary" type="submit" name="patient_type" value="existing"><strong>Book</strong> - existing patient</button>
				<button style="display:none" id="btn-patient_type-2" class="btn btn-primary" type="submit" name="patient_type" value="new"><strong>Book</strong> - new patient</button>
				-->
			</form>
		</div>
	</div>
	<?php endif; ?>
</div>