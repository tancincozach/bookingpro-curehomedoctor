<h4 class="page-header">Set Area/Doctor</h4>


 
<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 center-block" style="float: none">

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
     
    <?php if( isset($error) ): ?>
        <div class="box_border">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <h5>Please set: </h5>
    <?php echo form_open('', 'class="form-horizontal" role="form" method="post" onSubmit="return hcd.common.confirm();"'); ?>

        <div class="form-group">
            <label for="area_id" class="col-xs-3 col-sm-2 control-label">Area</label>
            <div class="col-xs-9 col-sm-10">
                <?php foreach($areas as $area_id=>$area_name): ?>
                <div class="checkbox" >
                    <label>
                        <input type="checkbox" name="areas[]" value="<?php echo $area_id; ?>"> <?php echo $area_name; ?>
                    </label>
                </div>
                <?php endforeach; ?>               
            </div>
        </div>

        <div class="form-group">
            <label for="car_id" class="col-xs-3 col-sm-2 control-label">Car</label>
            <div class="col-xs-9 col-sm-10">
                 
                    <select class="form-control" name="pcar" required>
                        <?php foreach($cars as $id=>$car_name): ?>
                            <option value="<?php echo $id; ?>"><?php echo $car_name; ?></option>
                        <?php endforeach; ?>
                    </select>
            </div>
        </div>

        <div class="form-group">
            <label for="doctor_id" class="col-xs-3 col-sm-2 control-label">Doctor</label>
            <div class="col-xs-9 col-sm-10">
            
                <select class="form-control" name="pdoctor" required>
                    <?php foreach($doctors as $id=>$doctor_name): ?>
                        <option value="<?php echo $id; ?>"><?php echo $doctor_name; ?></option>
                    <?php endforeach; ?>
                </select>
               
            </div>
        </div>

        <div class="form-group">
            <label for="doctor_id" class="col-xs-3 col-sm-2 control-label">&nbsp;</label>
            <div class="col-xs-9 col-sm-10"> 
                <a href="chaperone/register_doctor" class="btn btn-sm btn-info">Register New Doctor</a>
            </div>
        </div>

        <br />
        <div class="form-group">
            <div class="col-xs-12 col-sm-offset-2 col-sm-6">
                <button type="submit" class="btn btn-primary btn-block">Submit and Proceed booking</button>
            </div>
        </div>

    </form>

</div>

 