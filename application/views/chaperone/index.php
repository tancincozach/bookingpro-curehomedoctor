<!--<h4 class="page-header">Bookings </h4>-->

<?php if( SHOW_MILESTONE ): ?>
<div class="alert alert-warning alert-dismissible milestone-box">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="milestone-body">
        <dl>
            <dt>
                Description:
            </dt>
            <dd>
                Allow Chaperone to book an appointment
            </dd>
            <dt>
                Features/Statuses:
            </dt>
            <dd>
                <ul> 
                     <li>
                        If Chaperone login: only shows available and already booked appointments
                     </li>
                     <li>
                        Action buttons
                        <ul>
                            <li>Book - able to book an appointment, can set car and doctor</li>
                            <li>Unbook - able to unbook the appointment and change status to open so that other chaperone can book it</li>
                            <li>Visit - After the chaperone w/ doctor visit the patient chaperone will change the status 
                            to VISIT so that doctor can set consult notes</li>
                        </ul>
                     </li>
                     <li>Available - All available appointments waiting for Chaperone to book</li>
                     <li>Booked Appointment(s) - All appointments that the Chaperone </li>
                </ul>
            </dd>
            <dt>
                Need some clarification here
            </dt>
            <dd>
                Do we have to display all the available appointments on all Chaperone or display only specific to areas?
            </dd>
            
        </dl>        
    </div> 
</div>
<?php endif; ?>

<!--
<div class="box_border">
    
    <?php echo form_open('', 'class="form-inline" id="booking_preset_form" name="booking_preset_form" method="post"'); ?>
        <div class="form-group">
            <label for="area_ids">FILTER AREAS:</label> Please select the areas to be shown only <br />
            <?php                 
                foreach($areas as $area_id=>$area_val): 
            ?>
            <div class="checkbox" style="padding-right: 6px;">
                <label> <input type="checkbox" class="user-area-preset-checkbox" name="area_ids[]" <?php echo (in_array($area_id, $area_preset))?'checked':''; ?> value="<?php echo $area_id ?>"> <?php echo $area_val; ?></label>
            </div>  
            <?php endforeach; ?>
            &nbsp;&nbsp;
            <button class="btn btn-info btn-xs" type="submit" name="submit" value="set-area-filter">Set Filter</button>
        </div>
    </form>
    
    <br />

</div>
-->
<?php if( isset($error) ): ?>
    <div class="box_border">
        <?php echo $error; ?>
    </div>
<?php endif; ?>
<div class="chaperone-view">

<div class="box_border chaperone-set-info">
    <?php if($this->user_lvl.$this->user_sub_lvl == 51):  ?>
        <!-- <div class="text-right">
            <?php
           // if(isset($booking_area_out) && (int)$booking_area_out==0):
             ?>
                <button class="btn btn-warning btn-sm reopen_booking" alt="reopen_booking" data-area="<?php //echo implode(',', array_keys($area_preset));?>" style="font-weight:bold">Area <br/> RE-OPENED</button> 
            <?php// else:?>            
                <button class="btn btn-danger btn-sm book_out" alt="booked_out" data-area="<?php //echo implode(',', array_keys($area_preset));?>" style="font-weight:bold">Area <br/> BOOKED OUT</button> 
          <?php //endif;?>              
        </div> -->
        <p style="padding:20px;font-weight: bold"> Note: To book out an area, please click the area dropdown and select "Book out this area ", If you want to reopen the area, just click it again and select "Re-open this area".
Areas that has been booked out is in RED and GREEN means it's open or available.


        </p>         
        <h4><label class="col-xs-2 col-lg-1">Area</label>:

       

        <?php foreach($booking_area as $key=>$area):?>

            <div class="btn-group">
              <button type="button" class="btn btn-sm  <?php echo ((int)$area['is_open']==1 ? 'btn-success':'btn-danger');?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?php echo $area['area_name'];?> <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">

                <li class="bg-danger" <?php echo ((int)$area['is_open']==1) ?'style="display:block;"':'style="display:none;"'?> ><a href="#" class="book_out" alt="<?php echo $area['area_id'];?>"><strong>Book out this area.</strong></a></li>
                <li class="bg-success" <?php echo ((int)$area['is_open']==0) ?'style="display:block;"':'style="display:none;"'?>><a href="#" class="reopen_booking" alt="<?php echo $area['area_id'];?>"><strong>Re-open this area.</strong></a></li>
                <li role="separator" class="divider"></li>
                


              </ul>
            </div>
  
        <?php endforeach?>
         </h4>
        <h4><label class="col-xs-2 col-lg-1">Car</label>: <?php echo @$car->car_name.' ('.@$car->car_plateno.')'; ?></h4>
        <h4><label class="col-xs-2 col-lg-1">Doctor</label>: <?php echo $doctor->user_fullname; ?></h4>

        <div class="text-right">
            <a class="btn btn-success btn-sm" href="chaperone/search_patient/" role="button">New Booking</a>
        </div>
    <?php else: ?> 
        <?php echo form_open('', 'class="form-inline" id="booking_preset_form" name="booking_preset_form" method="post"'); ?>
            <div class="form-group">
                <label for="area_ids">FILTER AREAS:</label> Please select the areas to be shown only, if leave all uncheck it will display all. <br />
                <?php                 
                    foreach($areas as $area_id=>$area_val): 
                ?>
                <div class="checkbox" style="padding-right: 6px;">
                    <label> <input type="checkbox" class="user-area-preset-checkbox" name="area_ids[]" <?php echo (in_array($area_id, $area_preset))?'checked':''; ?> value="<?php echo $area_id ?>"> <?php echo $area_val; ?></label>
                </div>  
                <?php endforeach; ?>
                &nbsp;&nbsp;
                <button class="btn btn-info btn-xs" type="submit" name="submit" value="set-area-filter">Set Filter</button>
            </div>
        </form>
    
    <br />

  

    <?php endif; ?>
</div>

<div class="row text-center">
    <h4> Bookings of <?php echo ($appt_start==$timezone_date)?'Today':date('d/m/Y',strtotime($appt_start)); ?></h4>
</div> 

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left"><a class="btn btn-info btn-sm" href="chaperone/?appt_start=<?php echo date('Y-m-d', strtotime($appt_start.' -1 day')); ?>" role="button"> Prev Day</a></div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
            <a class="btn btn-primary btn-sm" href="chaperone" role="button"> Today </a>        
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right"><a class="btn btn-info btn-sm" href="chaperone/?appt_start=<?php echo date('Y-m-d', strtotime($appt_start.' +1 day')); ?>" role="button"> Next Day</a></div>
    </div>
    <br class="clearfix" />
    <br />
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <?php echo form_open('', 'class="" id="" name="booking_preset_status" method="post"'); ?>
            <div class="button-group">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-cog"></span> 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php


                        $_booking_statuses = array(
                            BOOK_STATUS_BOOKED=>'BOOKED', 
                            BOOK_STATUS_CLOSED=>'COMPLETE', 
                            BOOK_STATUS_CANCELLED=>'CANCELLED', 
                            BOOK_STATUS_UNSEEN=>'UNSEEN', 
                            BOOK_STATUS_VISIT=>'VISIT', 
                            BOOK_STATUS_OPEN=>'OPEN'
                        );

                        $book_status = $_booking_statuses;
                        $showing_status = '';
                        foreach($book_status as $key=>$val):
                            $is_check = '';
                            if( (in_array($key, @$chaperone_filter_status)) ){
                                $is_check = 'checked';
                                $showing_status[] = $this->booking_statuses[$key];
                            }

                    ?>
                        <li>
                            <a href="#" class="small noclose" data-value="<?php echo $key; ?>" tabIndex="-1">
                                <label for="set-filter-status[]"><input type="checkbox" name="set-filter-status[]" value="<?php echo $key; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $val; ?></label>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <li>
                        <br />
                        <button style="margin: 3px 20px;" class="btn btn-primary btn-xs small" type="submit" name="submit" value="btn-filter">Apply</button>
                    </li>
                </ul>
                Showing <?php echo (!empty($showing_status))?'<strong style="color: #003F79">'.implode(', ', $showing_status).'</strong>':'ALL Booking(s)'; ?>
            </div>
        </form>

    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

        <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
        
        <div class="table-responsive">
            <br />
            <table class="table table-bordered table-condensed table-appointment">
                <thead>
                    <tr>
                        <th></th>
                        <th>Appt #</th>                     
                        <th>Area</th>
                        <th>Action</th>
                        <th>Notes</th>
                        <th>Multiple <br/>Booking</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Suburb</th>
                        <th>Postcode</th>
                        <th>Phone</th>
                        <th>Mobile</th>
                        <th>DOB</th>
                        <th>Age</th>
                        <th>Symptoms</th>
                        <th>Status</th>
                        <!-- <th>Server Date/Time</th> -->
                        <th>Date/Time</th>
                        <!-- <th>Date Booked</th>
                        <th>Time Booked</th> -->
                        <th>Time in Queue <br />(h:m:s)</th>
                        <th>Booked By</th>
                        <th>AorTSI</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($results as $row): 

                        $tr_bg_cls = '';

                        $q_ex = explode(':',$row->in_queue);
                        $hr = intval($q_ex[0]);                    

                        if( $row->book_status == BOOK_STATUS_OPEN ){

                            if( $hr > 3  ) $tr_bg_cls = 'OPEN_QUEUE_MORE_3HR';
                            if( $hr >= 2 AND $hr <= 3  ) $tr_bg_cls = 'OPEN_QUEUE_BET_2_3HR';
                            if( $hr < 2  ) $tr_bg_cls = 'OPEN_QUEUE_LESS_2HR';

                        }elseif ($row->book_status == BOOK_STATUS_BOOKED ) {
                            $tr_bg_cls = 'BOOKED';
                        }elseif ($row->book_status == BOOK_STATUS_CANCELLED ) {
                            $tr_bg_cls = 'CANCELLED';
                        }elseif ($row->book_status == BOOK_STATUS_UNSEEN ) {
                            $tr_bg_cls = 'UNSEEN'; 
                        }elseif ($row->book_status == BOOK_STATUS_CLOSED ) {
                            $tr_bg_cls = 'COMPLETE';
                        }else{
                            $tr_bg_cls = 'VISIT_CLOSED';
                        }
                    
                        $limit_ref = '...'.substr($row->ref_number_formatted, 7, strlen($row->ref_number_formatted)-1);

                    ?>
                    <tr class="<?php echo $tr_bg_cls; ?>" >

                        <td> 
                            <div class="btn-group dropup pull-left">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>&nbsp;</li>
                                    <li><a href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/addnote" role="button">Add Note</a></li>
                                    <li><a href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/view" role="button">View</a></li>
                                </ul>
                            </div>
                        </td>

                        <td title="<?php echo $row->tran_type; ?>">
                            <label style="cursor: pointer" onclick="hcd.common.ref_num_toggle(this, '<?php echo $limit_ref; ?>', '<?php echo $row->ref_number_formatted; ?>')" title="Click to show <?php echo $row->ref_number_formatted; ?>" ><?php echo $limit_ref; ?></label>
                        </td>
                        <td><?php 
                            echo @$row->area_name;
                            if( $row->flag_eta ) echo ' <span style="font-weight: bold; background-color: yellow; color: red">ETA</span>';
                        ?></td>

                        <td>
                            <?php     
                                if( $row->book_status == BOOK_STATUS_CANCELLED OR $row->book_status == BOOK_STATUS_UNSEEN): 
                            ?>
                                <a class="btn btn-warning btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/reinstate" role="button">Reinstate</a>
                            <?php elseif( $row->book_status == BOOK_STATUS_VISIT): ?>
                                <a class="btn btn-info btn-xs" href="chaperone/medicare_form/<?php echo $row->appt_id; ?>" role="button">Medicare Form <?php echo ($row->sign_code!='')?'<span style="color: #449D44; background-color: #fff; border: 1px solid #000; font-weight: bold" class="glyphicon glyphicon-ok" aria-hidden="true"></span>':'' ?> </a>
                                <a class="btn btn-primary btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/unbook">Unbook</a>
                                <a class="btn btn-success btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/complete">Complete</a>
                                <!-- <a class="btn btn-danger btn-xs" href="chaperone/add_patient/<?php echo $row->appt_id; ?>">Add Patient</a> -->
                            <?php else: ?>    
                               
                                <?php if( $row->book_status == BOOK_STATUS_OPEN ): ?>
                                <a class="btn btn-primary btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/book">Book</a>
                                <?php endif; ?>

                                <?php if( $row->book_status == BOOK_STATUS_BOOKED ): //  AND $row->chaperone_id == $this->user_id ?>
                                <a class="btn btn-primary btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/unbook">Unbook</a>
                                <?php endif; ?>

                                <?php if( $row->book_status == BOOK_STATUS_BOOKED  ): //AND $row->chaperone_id == $this->user_id ?>
                                <a class="btn btn-primary btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/visit">Visit</a>
                                <?php endif; ?>

                                <?php if( $row->book_status == BOOK_STATUS_CLOSED  ): //AND $row->chaperone_id == $this->user_id ?>
                                <a class="btn btn-info btn-xs" href="chaperone/medicare_form/<?php echo $row->appt_id; ?>" role="button">Medicare Form <?php echo ($row->sign_code!='')?'<span style="color: #449D44; background-color: #fff; border: 1px solid #000; font-weight: bold" class="glyphicon glyphicon-ok" aria-hidden="true"></span>':'' ?></a>
                                <?php endif; ?>

                                <?php if( $row->book_status != BOOK_STATUS_UNSEEN  ): //AND $row->chaperone_id == $this->user_id ?>
                                <a class="btn btn-danger btn-xs" href="chaperone/book_appointment/<?php echo $row->appt_id; ?>/unseen">Unseen</a>
                                <?php endif; ?>

                                <a class="btn btn-primary btn-xs" href="chaperone/search_patient/?rel_appt_id=<?php echo ($row->related_appt_id > 0)?$row->related_appt_id:$row->appt_id; ?>&rel_patient_id=<?php echo $row->patient_id; ?>" role="button">Add Another Patient</a>
                            <?php 
                                endif;
                            ?>
                        </td>
                        <td><?php echo stripslashes($row->appt_notes); ?></td>
                        <td><?php echo ($row->related_appt_id)?'Yes':'No'; ?></td>
                        <td><?php echo $row->firstname.' '.$row->lastname; ?></td>
                        <td><?php echo stripslashes($row->street_addr); ?></td>
                        <td><?php echo stripslashes($row->suburb); ?></td>
                        <td><?php echo $row->postcode; ?></td>
                        <td><?php echo $row->phone; ?></td>
                        <td><?php echo $row->mobile_no; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($row->dob)); ?></td>
                        <td><?php echo $row->patient_age; ?></td>
                        <td><?php echo stripslashes($row->symptoms); ?></td>
                        <td><strong><?php echo $this->booking_statuses[$row->book_status]; ?></strong></td>
                     
                        <td><?php 
                            echo date('d/m/Y H:i', strtotime($row->appt_created_tz));                             
                        ?></td>
                        <td><?php echo $row->in_queue; ?></td>
                        <td>
                            <?php 
                                if($row->book_status == BOOK_STATUS_CANCELLED)
                                    echo '<strong>Cancel Reason</strong>: <br />'.stripslashes($row->cancelled_reason);
                                elseif($row->book_status == BOOK_STATUS_UNSEEN)
                                    echo '<strong>Unseen Reason</strong>: <br />'.stripslashes($row->cancelled_reason);
                                else
                                    echo @$row->chaperone_name.'/'.@$row->doctor_name.'/'.@$row->car_name; 
                            ?>
                            <?php  //echo @$row->chaperone_name.'/'.@$row->doctor_name; ?>
                        </td>
                        <td><?php echo ($row->abotsi)?'Yes':'No'; ?></td>                        

                    </tr>
                    <?php endforeach; ?> 
                </tbody>
            </table>
        </div>

        <div class="clearfix">
            <div class="pull-left text-left">
                 <?php echo $showing; ?>
            </div>
            <div class="pull-right text-right">
                <ul class="pagination pagination-sm">
                    <?php echo $links; ?>
                </ul>
            </div> 
        </div>

    </div>

</div>

</div>