<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Medicare Form</title>
  <meta name="description" content="Medicare Form">
  <meta name="author" content="Welldone International">
 
</head>

<body>

<div align="right" style="">
  <img alt="Cure Logo" style="width:50%" src="<?php echo BRAND_CLIENT_LOGO_PATH; ?>" />
</div>
<div align="center">
<h4>Medicare Form</h4>
</div>
<table>
	<tr>
		<td width="200px">Date of Service</td>
		<td><?php echo @date('d/m/Y', strtotime($appt_created_tz)); ?></td>
	</tr>
	<tr>
		<td width="200px">First Name</td>
		<td><?php echo $firstname; ?></td>
	</tr>
	<tr>
		<td>Initial</td>
		<td><?php echo $middleinitial; ?></td>
	</tr>
	<tr>
		<td>Surname Name</td>
		<td><?php echo $lastname; ?></td>
	</tr>
	<tr>
		<td>Residential Address</td>
		<td><?php echo $street_addr.' '.$suburb; ?></td>
	</tr>
	<tr>
		<td>Date of Birth</td>
		<td><?php echo date('d/m/Y',strtotime($dob)); ?></td>
	</tr>
	<tr>
		<td>Medicare Numer</td>
		<td>
		<?php 
			/*$mdn1 = substr($medicare_number, 0, 4);
			$mdn2 = substr($medicare_number, 4, 5);
			$mdn3 = substr($medicare_number, 9, 1);	*/

			//echo @$mdn1.' '.@$mdn2.' '.@$mdn3; 
			echo $medicare_number;
		?>
		</td>
	</tr>
	<tr>
		<td>Medicare Reference</td>
		<td><?php echo $medicare_ref; ?></td>
	</tr>
	<tr>
		<td>Expiry (yy/mm)</td>
		<td><?php echo $medicare_expiry; ?></td>
	</tr>
	<tr>
		<td>Patient unable to sign</td>
		<td><?php echo ($unable_to_sign)?'Yes':'No'; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			Signature <br />

			<?php 
				if( $sign_code != '' ){
					echo '<img src="'.$sign_code.'">';
				}
			?>
		</td>
	</tr>
	<tr>
		<td>Doctor</td>
		<td><?php echo @$doctor_name; ?></td>
	</tr>
	<tr>
		<td>Provider Number</td>
		<td><?php echo @$prov_num; ?></td>
	</tr>
</table>

<?php 
	$med_items = json_decode($medicare_form_items);
?>
<table>
     
    <tr>                    
        <th>Item Number</th>
        <th align="left">Description</th>
    </tr>
 
    <?php 
    	//print_r($med_items);
   	 	if( !empty($med_items) ): ?>
		<?php foreach($med_items as $num=>$desc): ?>
		<tr>            		
			<td align="center"><?php echo $num; ?></td>
			<td><?php echo $desc; ?></td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan="2">No Items</td>
		</tr>
	<?php endif; ?>

	 <tr>                    
        <th>Item Number</th>
        <th align="left">Description - Consult Note</th>
    </tr>
    <?php if(isset($item_num) && @$item_num!=''):?>
    <tr>
    	<td><?php echo @$item_num; ?></td>
    	<td></td>
    </tr>
   <?php else:?>
   	<tr>
		<td colspan="2">No Item Number.</td>
	</tr>
	<?php endif;?>
    <tr>
		<th colspan="2"> Pension, Concession or Healthcare Card</th>
	</tr>
	<?php 
	if(isset($patient_has_health_card) && @$patient_has_health_card=='Yes'):
		$expiry_date_arr = explode('-',$health_card_expiry_date);
	?>
     <tr>
    	<td><?php echo @$patient_has_health_card;?></td>
    	<td>EXP : <?php echo @$expiry_date_arr[2].'/'.@$expiry_date_arr[0].'/'.@$expiry_date_arr[1];?></td>
    </tr>
   <?php else:?>
   	<tr>
		<td colspan="2">No Pension, Concession or Healthcare Card.</td>
	</tr>
   	<?php endif;?>
</table>

</body>
</html>