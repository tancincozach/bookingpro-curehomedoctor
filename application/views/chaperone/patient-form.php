<h4 class="page-header">Booking Form</h4>

<div class="row">

	 
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<br />

		<div class="box_border">

			<?php echo form_open('chaperone/booking_submit', 'id="booking_patient_form" name="booking_patient_form" class="bg-grey" method="post" onsubmit="return hcd.common.confirm(\'Confirm submit booking!\')"'); ?>
				
				<input type="hidden" name="patient_type" value="<?php echo $patient_type; ?>">
				<input type="hidden" name="patient_id" value="<?php echo $patient_id; ?>">
				<input type="hidden" name="rel_appt_id" value="<?php echo $rel_appt_id; ?>">

				<input type="hidden" name="area_id" value="<?php echo $area_id; ?>">


				<!-- START GOOGLE GEOCODING STORAGE FIELDS // -->
				<div style="display: none;">
					<input type="text" name="sm[streetPrefix]" id="sm_streetPrefix" /><!-- Street Number : "116A" // -->
					<input type="text" name="sm[street]" id="sm_street" /><!-- Street : "Hill Street" // -->
					<input type="text" name="sm[area]" id="sm_area" /><!-- Town : "Newton" // -->
					<input type="text" name="sm[state]" id="sm_state" /><!-- State : "Queensland" // -->
					<input type="text" name="sm[text]" id="sm_text" /><!-- Full Address : "116A Hill Street, Newton QLD 4350, Australia" // -->
					<input type="text" name="sm[latlong]" id="sm_latlong" /><!-- Latitude : -27.560453 , Longitude : 151.934665 // -->
				</div>
				<!-- END GOOGLE GEOCODING STORAGE FIELDS // -->

				<div class="col-lg-6 col-md-6 col-sm-6 bg-padding " style="border-right: 1px solid #ccc;">
					<div class="col-lg-8 col-md-8 col-sm-8">

						<div class="row">

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="First_Name">First Name</label>
									<input type="text" class="form-control" name="First_Name" value="<?php echo stripslashes(@$patient->firstname); ?>" required>
								</div>	
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Last_Name">Last Name</label>
									<input type="text" class="form-control" name="Last_Name" value="<?php echo stripslashes(@$patient->lastname); ?>" required>
								</div>	
							</div>
					 
							<div class="clearfix"></div>

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Landline_Phone">Landline Phone</label>
									<input type="text" class="form-control" name="Landline_Phone" value="<?php echo stripslashes(@$patient->phone); ?>" required>
								</div>	
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Mobile_Phone">Mobile Phone</label>
									<input type="text" class="form-control" name="Mobile_Phone" value="<?php echo stripslashes(@$patient->mobile_no); ?>" required>
								</div>	
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						 
						<div class="form-group">
							<label for="Last_Name">Notes</label>							
							<textarea class="form-control" name="Patient_Notes" style="background-color: #f2dede" rows="3"><?php echo stripslashes(@$patient->patient_notes); ?></textarea>
						</div>	
						 
					</div>

					<div class="clearfix"></div>

					<!--
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Emergency_Contact_Name">Emergency Contact Name</label>
							<input type="text" class="form-control" name="Emergency_Contact_Name" id="Emergency_Contact_Name" value="<?php echo stripslashes(@$patient->em_contact_name); ?>" required>
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Emergency_Contact_Phone">Emergency Contact PHone</label>
							<input type="text" class="form-control" name="Emergency_Contact_Phone" id="Emergency_Contact_Phone" value="<?php echo stripslashes(@$patient->em_contact_phone); ?>" required>
						</div>	
					</div>

					<div class="clearfix"></div>
					-->

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Email">Email</label>
							<input type="text" class="form-control" name="Email" value="<?php echo stripslashes(@$patient->email); ?>">
						</div>	
					</div>			

					<div class="clearfix"></div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Address">Address</label>
							<input type="text" class="form-control" name="Address" id="Address" value="<?php echo stripslashes(@$patient->street_addr); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Suburb">Subub</label>
							<input type="text" class="form-control" name="Suburb" id="Suburb" value="<?php echo stripslashes(@$patient->suburb); ?>" readonly required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Post_Code">Postcode</label>
							<input type="text" class="form-control" name="Post_Code" id="Post_Code" value="<?php echo stripslashes(@$patient->postcode); ?>" readonly required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Cross_Street">Cross Street</label>
							<input type="text" class="form-control" name="Cross_Street" id="Cross_Street" value="<?php echo stripslashes(@$patient->cross_street); ?>" >
						</div>	
					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12 clearfix">
					
					Google maps here

						<div id="addr_checkbox" style="display: none; padding: 10px; " class="bg-danger">
							<strong>Please check with the caller that the address displayed below is correct. 
							<!--A correct address is required for the Fleet management software to work correctly.--></strong><br />
							<strong>Is the address text and location displayed on the map below correct?</strong> 
							<select name="addr_correct" id="addr_correct" class="" required>
								<option value="0" selected="selected">No</option>
								<option value="1">Yes</option><br />
							</select> 
						</div>					
						<div id="returned_addr" style="display: none; padding-left: 10px;" class="bg-danger"></div>
						<div id="map-canvas" style="height: 300px; width: 100%"></div>

					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Symptoms">Symptoms</label>							
							<textarea class="form-control" name="Symptoms" rows="5" required></textarea>
						</div>	
					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6  bg-padding" >
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="DOB">Date of Birth (dd/mm/yyyy)</label>
							<input type="text" class="form-control datepicker" id="dob" name="DOB" 
							data-format="DD/MM/YYYY"
							data-template="DD/MM/YYYY"
							 value="<?php echo (isset($patient->dob))?date('d/m/Y', strtotime(@$patient->dob)):''; ?>" 
							required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="age">Age</label>
							<input type="text" class="form-control" name="age" id="patient_age" value="<?php echo @$patient->age; ?>" readonly>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Gender">Gender</label>							
							<select class="form-control" name="Gender" required>
								<option value=""> --- Select Gender --- </option>
								<option value="M" <?php echo (@$patient->gender =='M')?'selected':''; ?> >Male</option>
								<option value="F" <?php echo (@$patient->gender =='F')?'selected':''; ?> >Female</option>
							</select>
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Number">Medicare Number</label>
							<input type="text" class="form-control"  name="Medical_Number" maxlength="10" value="<?php echo stripslashes(@$patient->medicare_number); ?>" required>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Reference">Medicare Reference</label>
							<input type="text" class="form-control" name="Medical_Reference" value="<?php echo stripslashes(@$patient->medicare_ref); ?>" required>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Expirey">Medicare Expiry <small>(mm/yy)</small></label>
							<input type="text" class="form-control" name="Medical_Expirey" value="<?php echo stripslashes(@$patient->medicare_expiry); ?>" required>
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="dva_card_gold">TICK if "Gold DVA" card</label>
							<input type="checkbox" class="form-control" name="dva_card_gold" <?php echo (@$patient->dva_card_gold)?'checked':''; ?>>
						</div>	
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Medical_Expirey">Where did you hear about us?</label>							 
							<?php echo form_dropdown('wdyhau', $wyhaulist, @$patient->wdyhau, 'class="form-control"'); ?>
						</div>	
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="Health_Insurance_Provider">Private Health Insurance Provider</label>
							<input type="text" class="form-control"  name="Health_Insurance_Provider"  value="<?php echo @$patient->health_insurance_provider; ?>">
						</div>	
					</div>
				
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Health_Fund_Number">Private Health Fund Number</label>
							<input type="text" class="form-control" name="Health_Fund_Number" value="<?php echo @$patient->health_fund_number; ?>" >
						</div>	
					</div>
				
			

					<div class="clearfix"></div>

					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="Medical_Expirey">Aboriginal OR Torres Straight Islander</label>
							<select class="form-control" name="abotsi" required>								
								<option value=""> --- Select --- </option>
								<option value="0" <?php echo (@$patient->abotsi =='0')?'selected':''; ?> >No</option>								
								<option value="1" <?php echo (@$patient->abotsi =='1')?'selected':''; ?> >Yes</option>
							</select>							
						</div>	
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Medical_Expirey">Cultural &#38; Ethnicity Background</label>
							<textarea class="form-control" name="cultural_ethnic_bg"><?php echo @$patient->cultural_ethnic_bg; ?></textarea>						
						</div>	
					</div>

					<div class="clearfix"></div>
					
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Medical_Expirey">Search Regular Practice</label>
							<div class="row">
								<div class="col-lg-11 col-md-11 col-sm-11">
									<input type="text" class="form-control" name="search_practice" id="practice_autocomplete" value="<?php echo @$patient->regular_practice; ?>" placeholder="Search by name" required >
								</div>
								<!--<div class="col-lg-6 col-md-6 col-sm-6">
									<input type="text" class="form-control" name="search_practice_postcode" value="" placeholder="Search by postcode" >
								</div>-->
							</div>
						</div>
						 
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="form-group">
							<label for="Regular_Doctor">IF NO MATCH - enter practice here</label>
							<input type="text" class="form-control" name="email_to_hcd" id="no_match_practice" value="">
						</div>	
					</div> 

					<div class="clearfix"></div>

					<div class="col-lg-6 col-md-6 col-sm-6" >
						<div class="form-group">
							<label for="Regular_Doctor">Regular Doctor</label>
							<input type="text" class="form-control" name="Regular_Doctor" value="<?php echo @$patient->regular_doctor; ?>">
						</div>	
					</div>
					
				</div>	

				<div class="clearfix"></div>

				 
				<br />

				<div class="col-lg-12 col-md-12 col-sm-12 text-center">

					<div style="padding-bottom: 30px">
						<button class="btn btn-primary" type="submit" value="submit">Submit Booking</button>
					</div>
				 
				</div>

				<div class="clearfix"></div>

				<br />
			</form>

			<div class="clearfix"></div>

		</div>
	</div>

</div>