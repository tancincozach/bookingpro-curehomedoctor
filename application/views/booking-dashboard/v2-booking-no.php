    

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard', 'class="" name="dashboard_form" method="get"'); ?>
                    
                    <input type="hidden" name="step" value="3">

                    <?php foreach($getvars['collection'] as $field=>$value): ?>
                        <input type="hidden" name="collection[<?php echo $field; ?>]" value="<?php echo $value; ?>" />   
                    <?php endforeach; ?>

                    <h4>How can I help you?</h4>
                       
                    <div class="row boxes-center center-block"> 
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="callcenter/?stype=cancel&search2=<?php echo @$getvars['search_more']; ?>">Cancel Booking</a></div>
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="callcenter/?stype=eta&search2=<?php echo @$getvars['search_more']; ?>">Wanting ETA</a></div>
                    </div>

                    <div class="row boxes-center center-block">  
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/call_from_doctor_car/?search2=<?php echo @$getvars['search_more']; ?>">Call from Doctor / Car</a></div>
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/call_from_pharmacy/?search2=<?php echo @$getvars['search_more']; ?>">Call from Pharmacy</a></div>
                        
                    </div>

                     <div class="row boxes-center center-block">
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/general_message_for_admin/?search2=<?php echo @$getvars['search_more']; ?>">General Message for Admin</a></div>                
                     </div>

                     <div class="row boxes-center center-block">
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/no_action_required/?search2=<?php echo @$getvars['search_more']; ?>">No Action Required</a></div>                
                     </div>

                </form> 

            </div>
        </div> 