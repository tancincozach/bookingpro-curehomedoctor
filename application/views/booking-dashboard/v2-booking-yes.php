
<!--

    Step: 2
    bookingtype: yes
 
-->
 
    <div class="jumbotron clearfix text-center" >
        <div class="col-lg-12 col-md-12">

            <?php echo form_open('dashboard', 'class="" id="dashboard_form" name="dashboard_form" method="get"'); ?>

                <input type="hidden" name="step" value="3">

                <?php foreach($getvars['collection'] as $field=>$value): ?>
                    <input type="hidden" name="collection[<?php echo $field; ?>]" value="<?php echo $value; ?>" />   
                <?php endforeach; ?>
                
                <input type="hidden" name="collection[Suburb][id]" value="" />   
                <input type="hidden" name="collection[Suburb][area]" value="" />   
                <input type="hidden" name="collection[Suburb][postcode]" value="" /> 
                <input type="hidden" name="collection[Suburb][area_id]" value="" /> 

                <input type="hidden" name="collection[area_open_status]" value="" />   
                <input type="hidden" name="collection[area_avail_id]" value="" />   

                <div class="row">
                    
                    <div class="col-sm-2 text-left">
                        
                        <h3>Booking Times</h3>

                        <p style="margin-bottom: 1px;"><strong>METRO</strong></p>
                        <p style="font-style: italic;">Mon - Frid after 4pm </p>

                        <br />
                        <p style="margin-bottom: 1px"><strong>REGIONAL</strong></p>
                        <p style="font-style: italic;">Mon - Frid after 6pm </p>

                        <br />
                        <p>Saturday after 12pm</p>
                        <p>Sunday & Pub Hols - after 6am </p>


                    </div>
                    
                    <div class="col-sm-7">

                        <h4>Can I please have the suburb you are calling from?</h4>
                           
                        <div class="col-sm-5 center-block" style="float:none">  
                            <span class="text-primary bg-primary" style="padding: 2px; margin-bottom: 2px">Search by SUBURB or POSTCODE</span>
                            <input type="text" class="suburb form-control" id="collection_suburb_suburb"  name="collection[Suburb][suburb]" value="" onblur="this.value=(this.value).trim()" placeholder="Search by SUBURB or POSTCODE" autofocus  /> 
                        </div> 

                        <br />
                        <br />

                        <div style="display:none" id="btn-service-1" class="btn-service-cls">
                            <button class="btn btn-primary" type="submit" name="btn-service" value="1">Serviced YES - Bookings Available</button>
                        </div>

                        <div style="display:none"  id="btn-service-2" class="btn-service-cls">
                            <button  class="btn btn-info" type="submit" name="btn-service" value="2">Serviced YES - No Booking</button>
                        </div>

                        <div style="display:none" id="btn-service-3" class="btn-service-cls">
                            <p>ENSURE the FULL suburb name is entered as this willl log to "suburb not on list", THEN click <strong>"Service NO"</strong> BUTTON to complete call</strong></p>
                            <button class="btn btn-warning" type="submit" name="btn-service" value="3">Serviced NO</button>
                        </div>

                        <div id="suburb-not-display-note">
                            <br />
                            <p><strong>If SUBURB does not display</strong> "DOUBLE CHECK" by starting with 2 characters</p>
                            <p>if still no match</p>
                            <p>1. CLICK on <stong style="color: blue">"no match found-press table or hit enter".</stong></p>
                        </div>


                    </div>

                    <div class="col-sm-3">
                        
                        <div class="overnight-staff-options" style="display:none">

                            <h4>Overnight Staff Options  (After Midnight)</h4>

                            <button class="btn btn-danger" type="submit" name="btn-service" value="Mon - Frid after 6pm">Mon - Frid after 6pm</button>
                            <br />
                            <br />
                            <button class="btn btn-danger" type="submit" name="btn-service" value="Saturday after 12pm">Saturday after 12pm</button>
                            <br />
                            <br />
                            <button class="btn btn-danger" type="submit" name="btn-service" value="Sunday & public holiday - anytime">Sunday & public holiday - anytime</button>

                        </div>


                    </div>




                </div>

                <div class="clearfix">
                    <hr style="border: 1px solid red" />
                    <a class="btn btn-danger" href="dashboard/did_not_complete/?<?php echo $getvars['uri_not_complete']; ?>" >DID NOT COMPLETE</a>
                </div>

            </form> 

        </div>
    </div>
     
