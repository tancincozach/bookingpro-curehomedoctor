 
<h4 class="page-header">Outbound Call</h4>

<div class="col-sm-8">
	<?php echo form_open('', 'class="form-horizontal" id="outbound_call_form" name="outbound_call_form" method="post" onsubmit="hcd.common.confirm()"'); ?>

		<div class="form-group">
		    <label for="outbound_name" class="col-sm-4 control-label">Outbound Name</label>
		    <div class="col-sm-6">
		    	<div class="row">
			    	<div class="col-sm-6">
			      		<input type="text" class="form-control" name="First_Name" value="" placeholder="First Name">
			      	</div>
			    	<div class="col-sm-6">
			      		<input type="text" class="form-control" name="Last_Name" value="" placeholder="Last Name">
			      	</div>
		      	</div>
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="outbound_phone" class="col-sm-4 control-label">Outbound Phone</label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" name="Phone" value="">
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="reason_for_call" class="col-sm-4 control-label">Reason for Call</label>
		    <div class="col-sm-6">
		    	<textarea class="form-control" name="reason_for_call" rows="3"></textarea>
		    </div>
		</div>
		   
		<div class="form-group">
		    <div class="col-sm-offset-4 col-sm-6">
		      <button type="submit" class="btn btn-primary">Send</button>
		    </div>
		</div>

	</form>

</div>