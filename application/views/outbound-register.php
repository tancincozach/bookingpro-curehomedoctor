 
<h4 class="page-header">Call Register</h4>

<!-- search -->
<div>
    <?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
        <div class="form-group">
            <label for="search" class="">Search</label>
            <input type="text" class="form-control input-sm"  name="search" placeholder="Search Name, Phone, Reason for Call" value="<?php echo @$_GET['search']; ?>" title="Search: appt #, firstname, lastname, phone, mobile , medicare">
        </div>
        <button type="submit" name="" class="btn btn-primary">Search</button> 
        <a href="dashboard/outbound_register" class="btn btn-warning">Reset</a>
    </form>
</div>
<br />

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

        <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
        
        <table class="table table-bordered table-condensed table-appointment">
            <thead>
                <tr>
                    <!-- <th style="width: 105px">Appt #</th> -->
                    <th>Date/Time</th>
                    <th>Type</th>
                    <th>Outbound Name</th>
                    <th>Outbound Phone</th>
                    <th>Reason for Call</th>
                    <th>Agent Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($results as $row): ?>
                <tr>
                    <!-- <td><?php echo $row->ref_number_formatted; ?></td> -->
                    <td><?php echo $row->tran_created_tz; ?></td>
                    <td><?php echo $row->tran_type.' '.((trim($row->tran_desc)!='')?' - '.$row->tran_desc:''); ?></td>
                    <td><?php echo stripslashes($row->caller_firstname.' '.$row->caller_lastname); ?></td>
                    <td><?php echo $row->caller_phone; ?></td>                    
                    <td><?php echo stripslashes(nl2br($row->tran_details)); ?></td>
                    <td><?php echo $row->agent_name; ?></td>
                    <td></td>
                </tr>
                <?php endforeach; ?> 
            </tbody>
        </table>

        <div class="clearfix">
            <div class="pull-left text-left">
                 <?php echo $showing; ?>
            </div>
            <div class="pull-right text-right">
                <ul class="pagination pagination-sm">
                    <?php echo $links; ?>
                </ul>
            </div> 
        </div>

    </div>
</div>
