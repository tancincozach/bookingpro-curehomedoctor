 
<?php $patient = @$getvars['patient']; ?>

<h4 class="page-header">Booking Form</h4>


    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Booking an appointment
                </dd>
                <dt>
                    Features/Statuses:
                </dt>
                <dd>                    
                    <ul> 
                        <li>Booking: today</li> 
                        <li>Booking: next day</li> 
                        <li>Booking: holiday
                        	<ul>
                        		<li>Plan: if booking next day and its holiday the system should advice operator that the booking will be happen on the date</li>
                        	</ul>
                        </li> 
                        <li>Booking: Duplicate patient today</li> 
                        <li>Age: the system should automatically calculate date base on date of birth </li> 
                    </ul>
                </dd>
                  
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>

<div class="row text-center" style="font-weight: 700">
	<?php 
		if( $when == 'today' ) {
			
			echo '<strong style="color: red">TODAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y');

		}elseif($when == 'nextday'){
			echo '<strong style="color: red">NEXT DAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y', strtotime('+1 day'));
		}



	 ?>
	 
</div>

<div class="row">
 
	<?php if( $getvars['patient_type'] == '' ): ?>
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="row col-lg-6 col-md-6 col-sm-6">
			<?php echo form_open('', 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="get"'); ?>
				
				<input type="hidden" name="collection[patient_id]" value="">

                <?php 
                    foreach($getvars['collection'] as $field=>$value): 

                        if( $field == 'Suburb' ){
                            foreach( $value as $suburb_field=>$suburb_val ){
                                echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                            }
                        }else
                            echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                
                    endforeach; 
                ?>   
				<div class="form-group">
					<input type="text" class="suburb form-control" id="search_patient_match" name="Name" value="<?php echo @$getvars['collection']['Last_Name'].','.$getvars['collection']['First_Name']; ?>"   placeholder=" "/> 
				</div>

				<button style="display:none" id="btn-patient_type-1" class="btn btn-primary" type="submit" name="patient_type" value="existing"><strong>Book</strong> - existing patient</button>
				<button style="display:none" id="btn-patient_type-2" class="btn btn-primary" type="submit" name="patient_type" value="new"><strong>Book</strong> - new patient</button>

			</form>
		</div>
	</div>
	<?php endif; ?>

	<?php if( !empty($getvars['patient_type']) ): ?>
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<br />

		<div class="box_border">

			<?php echo form_open('dashboard/booking_submit', 'class="" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm submit booking!\')"'); ?>

				<input type="hidden" name="booking_when" value="<?php echo $when; ?>">
				<input type="hidden" name="patient_type" value="<?php echo $getvars['patient_type']; ?>">
				<input type="hidden" name="patient_id" value="<?php echo @$patient->patient_id; ?>">

                <?php 
                    foreach($getvars['collection'] as $field=>$value): 

                        if( $field == 'Suburb' ){
                            foreach( $value as $suburb_field=>$suburb_val ){
                                echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                            }
                        }else
                            echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                
                    endforeach; 
                ?> 


				<!-- START GOOGLE GEOCODING STORAGE FIELDS // -->
				<div style="display: none;">
					<input type="text" name="sm[streetPrefix]" id="sm_streetPrefix" /><!-- Street Number : "116A" // -->
					<input type="text" name="sm[street]" id="sm_street" /><!-- Street : "Hill Street" // -->
					<input type="text" name="sm[area]" id="sm_area" /><!-- Town : "Newton" // -->
					<input type="text" name="sm[state]" id="sm_state" /><!-- State : "Queensland" // -->
					<input type="text" name="sm[text]" id="sm_text" /><!-- Full Address : "116A Hill Street, Newton QLD 4350, Australia" // -->
					<input type="text" name="sm[latlong]" id="sm_latlong" /><!-- Latitude : -27.560453 , Longitude : 151.934665 // -->
				</div>
				<!-- END GOOGLE GEOCODING STORAGE FIELDS // -->

				<div class="col-lg-6 col-md-6 col-sm-6" style="border-right: 1px solid #ccc">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="First_Name">First Name</label>
							<input type="text" class="form-control" name="First_Name" value="<?php echo stripslashes(@$patient->firstname); ?>" required>
						</div>	
					</div>
			
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Last_Name">Last Name</label>
							<input type="text" class="form-control" name="Last_Name" value="<?php echo stripslashes(@$patient->lastname); ?>" required>
						</div>	
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Landline_Phone">Landline Phone</label>
							<input type="text" class="form-control" name="Landline_Phone" value="<?php echo stripslashes(@$patient->phone); ?>">
						</div>	
					</div>
			
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Mobile_Phone">Mobile Phone</label>
							<input type="text" class="form-control" name="Mobile_Phone" value="<?php echo stripslashes(@$patient->mobile_no); ?>">
						</div>	
					</div>
					
					<div class="clearfix"></div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Email">Email</label>
							<input type="text" class="form-control" name="Email" value="<?php echo stripslashes(@$patient->email); ?>">
						</div>	
					</div>			

					<div class="clearfix"></div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Address">Address</label>
							<input type="text" class="form-control" name="Address" id="Address" value="<?php echo stripslashes(@$patient->street_addr); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Suburb">Subub</label>
							<input type="text" class="form-control" name="Suburb" id="Suburb" value="<?php echo stripslashes(@$patient->suburb); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Post_Code">Postcode</label>
							<input type="text" class="form-control" name="Post_Code" id="Post_Code" value="<?php echo stripslashes(@$patient->postcode); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>
					
					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12 clearfix">
					
					Google maps here

						<div id="addr_checkbox" style="display: none; padding: 10px; " class="bg-danger">
							<!--<strong>Please check with the caller that the address displayed below is correct. A correct address is required for the Fleet management software to work correctly.</strong><br />
							<strong>Is the address text and location displayed on the map below correct?</strong> 
							<select name="addr_correct" id="addr_correct" class="form_element">
								<option value="0" selected="selected">No</option>
								<option value="1">Yes</option><br />
							</select>--> 
						</div>					
						<div id="returned_addr" style="display: none; " class="bg-danger"></div>
						<div id="map-canvas" style="height: 300px; width: 100%"></div>

					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Symptoms">Symptoms</label>							
							<textarea class="form-control" name="Symptoms" rows="5"></textarea>
						</div>	
					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6">
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="DOB">Date of Birth</label>
							<input type="text" class="form-control datepicker" id="dob" name="DOB" value="<?php echo (isset($patient->dob))?date('d/m/Y', strtotime(@$patient->dob)):''; ?>" required>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="age">Age</label>
							<input type="text" class="form-control" name="age" id="patient_age" value="<?php echo @$patient->age; ?>" readonly>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Gender">Gender</label>							
							<select class="form-control" name="Gender" required>
								<option value=""> --- Select Gender --- </option>
								<option value="M" <?php echo (@$patient->gender =='M')?'selected':''; ?> >Male</option>
								<option value="F" <?php echo (@$patient->gender =='F')?'selected':''; ?> >Female</option>
							</select>
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Number">Medical Number</label>
							<input type="text" class="form-control" name="Medical_Number" value="<?php echo stripslashes(@$patient->medicare_number); ?>" >
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Reference">Medical Reference</label>
							<input type="text" class="form-control" name="Medical_Reference" value="<?php echo stripslashes(@$patient->medicare_ref); ?>">
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Expirey">Medical Expirey <small>(mm/yy)</small></label>
							<input type="text" class="form-control" name="Medical_Expirey" value="<?php echo stripslashes(@$patient->medicare_expiry); ?>">
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="dva_card_gold">TICK if "Gold DVA" card</label>
							<input type="checkbox" class="form-control" name="dva_card_gold" <?php echo (@$patient->dva_card_gold)?'checked':''; ?>>
						</div>	
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Medical_Expirey">Where did you hear about us?</label>							 
							<?php echo form_dropdown('wdyhau', $getvars['wyhaulist'], @$patient->wdyhau, 'class="form-control"'); ?>
						</div>	
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-7 col-md-7 col-sm-7">
						<div class="form-group">
							<label for="Medical_Expirey">Aboriginal OR Torres Straight Islander</label>
							<select class="form-control" name="abotsi" required>								
								<option value=""> --- Select --- </option>
								<option value="0" <?php echo (@$patient->abotsi =='0')?'selected':''; ?> >No</option>								
								<option value="1" <?php echo (@$patient->abotsi =='1')?'selected':''; ?> >Yes</option>
							</select>							
						</div>	
					</div>

					<div class="clearfix"></div>
					
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Medical_Expirey">Search Regular Practice</label>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
									<input type="text" class="form-control" name="search_practice" value="<?php echo @$patient->regular_practice; ?>" placeholder="Search by name" >
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<input type="text" class="form-control" name="search_practice_postcode" value="" placeholder="Search by postcode" >
								</div>
							</div>
						</div>
						 
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="form-group">
							<label for="Regular_Doctor">IF NO MATCH - enter practice here</label>
							<input type="text" class="form-control" name="email_to_hcd" value="">
						</div>	
					</div> 

					<div class="clearfix"></div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Regular_Doctor">Regular Doctor</label>
							<input type="text" class="form-control" name="Regular_Doctor" value="<?php echo @$patient->regular_doctor; ?>">
						</div>	
					</div>
					
				</div>	

				<div class="clearfix"></div>

				<div class="col-lg-12 col-md-12 col-sm-12 bg-info text-center">
					<p>NOTE:</p>
					<p>if call disconnects, call back if you know the number Use PREFIX "31"</p>
				</div>
				
				<div class="clearfix"></div>
				<br />

				<div class="col-lg-12 col-md-12 col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Click to Complete Booking</button>
					<button class="btn btn-warning" type="button">DID NOT COMPLETE</button>

				</div>

				<div class="clearfix"></div>

				<br />
			</form>

			<div class="clearfix"></div>

		</div>
	</div>
 	<?php endif; ?> <!-- end patient_type -->
</div>