 
<h4 class="page-header">Training Videos</h4>

<?php 
	if(isset($dir)): 
		if( count($dir) ):
?>
	<ul>
	<?php foreach($dir as $key=>$file):?>
		<li>
			<a href="dashboard/trainingvideos/?vid=<?php echo urlencode($file); ?>"><?php echo $file; ?></a>
		</li>
	<?php endforeach;  ?>
	</ul>
<?php 
		else:
			echo '<h5>No traning videos Available</h5>';
		endif;	
	endif; 
?>


<?php if( isset($vid_file) ): ?>

<div>
	<video width="100%" height="auto" autoplay>
		<source src="<?php echo $vid_file; ?>" type="video/mp4">
		Your browser does not support the video tag.
	</video>
</div>

<div>
<a href="dashboard/trainingvideos" class="btn btn-info btn-xs">Back</a>
</div>

<?php endif; ?>
