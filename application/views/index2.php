<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo BRAND_NAME; ?></title>

<base href="<?php echo base_url(); ?>" />

<!-- jQuery UI -->
<link href="assets/css/redmond/jquery-ui-1.10.4.custom.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->

<?php echo @$css_file; ?>

<script type="text/javascript">
    
    var hcd_globals = {        
        agent_name:"<?php echo @$this->agent_name;?>",
        user_id:"<?php echo @$this->user_id;?>",
        cookie_name:"<?php echo $this->config->item('sess_cookie_name'); ?>",
        n:'<?php echo $this->security->get_csrf_token_name(); ?>',
        h:'<?php echo $this->security->get_csrf_hash(); ?>',
        base_url: '<?php echo base_url(); ?>'
    }

</script>

</head>
<body>

<div class="<?php echo @$container; ?>">
    
    <div class="row">        
        <div class="col-lg-12 col-md-12 main-content">                    
            
            <?php $this->load->view($view_file, @$data); ?>
        
        </div>
    </div>
    <div class="row">
        <br />
        <div class="col-lg-12 text-center">
            <p><?php echo SITE_FOOTER; ?></p>
        </div>
    </div>    
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery-1.11.3.min.js"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/moment/moment.min.js"></script>
<script src="assets/plugins/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script src="assets/js/apps.js"></script>
<?php echo @$js_file; ?>

</body>
</html>