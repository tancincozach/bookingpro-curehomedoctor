
<div id="dynamic_content">
	<h4 class="page-header">Search Patient</h4>

	<form id="patient_search_form" class="form-inline" accept-charset="utf-8" method="post" name="patient_search_form" action="">

		<input type="hidden" name="rel_appt_id" value="<?php echo @$_GET['rel_appt_id'] ?>" />
		<input type="hidden" name="rel_patient_id" value="<?php echo @$_GET['rel_patient_id'] ?>" />

		<div class="form-group">		 
			<input type="text" class="form-control" name="First_Name" value="<?php echo @$First_Name; ?>" placeholder="First Name">
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="Last_Name" value="<?php echo @$Last_Name; ?>" placeholder="Last Name">
		</div>

		<button type="button" class="btn btn-default" onclick="Patient.search(this)">Search</button>

	</form>

	<div id="search_results">

	</div>
</div>