<h4 class="page-header">Medicare Form</h4>

  <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    

<?php echo form_open('', 'class="form-horizontal appointment-form" id="medicare_form" name="medicare_form" method="post" onsubmit="return Medicare.confirm();"'); ?>


    
    <div class="form-group">
	    <label for="First_Name" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">First Name</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
	       <input type="text" class="form-control" name="First_Name" value="<?php echo @$record->firstname; ?>" >
	    </div> 
	</div>
	
    <div class="form-group">
	    <label  for="Middle_Initial" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Initial</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<input type="text" class="form-control" name="Middle_Initial" value="<?php echo @$record->middleinitial; ?>" >
	    </div> 
	</div>	

    <div class="form-group">
	    <label for="Last_Name" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Surname</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<input type="text" class="form-control" name="Last_Name" value="<?php echo @$record->lastname; ?>" >
	    </div> 
	</div>

    <div class="form-group clearfix">
	    <label  for="Street_Address" class="col-sm-4 col-md-4 col-lg-4 control-label">Residential Address</label>
	    <div class="col-sm-5 col-md-5 col-lg-5">
	    	<div class="row">
	    		<div class="col-sm-6 col-md-6 col-lg-6">
	    			<input type="text" class="form-control" name="Street_Address" value="<?php echo @$record->street_addr; ?>" >
	    		</div>
	    		<div class="col-sm-6 col-md-6 col-lg-6">
	    			<input type="text" class="form-control suburb" name="Suburb" value="<?php echo @$record->suburb; ?>" >
	    		</div>

	    	</div>			 
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="Street_Address" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Date of Birth</label>
	    <div class="col-xs-5 col-sm-5 col-md-2 col-lg-2">
			<input type="text" class="form-control datepicker" name="DOB" value="<?php echo date('d/m/Y',strtotime(@$record->dob)); ?>" >
	    </div> 
	</div>

	<?php
		//@$record->medicare_number = '1234 56789 0';
		
		@$mdn1 = substr(@$record->medicare_number, 0, 4);
		@$mdn2 = substr(@$record->medicare_number, 4, 5);
		@$mdn3 = substr(@$record->medicare_number, 9, 1);
	?>

    <div class="form-group">
	    <label  for="Medicare_Number_1" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Medicare Numer</label>
	    <div class="col-xs-8 col-sm-5 col-md-5 col-lg-5">
	    	<div class="row">
		    	<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
		       		<input type="text" class="form-control" name="Medicare_Number_1" maxlength="4" value="<?php echo @$mdn1; ?>" >
		       	</div>
		    	<div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" style="padding-left: 0px">
		       		<input type="text" class="form-control" name="Medicare_Number_2" maxlength="5" value="<?php echo @$mdn2; ?>" >
		       	</div>
		    	<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="padding-left: 0px">
		       		<input type="text" class="form-control" name="Medicare_Number_3" maxlength="1" value="<?php echo @$mdn3; ?>" >
		       	</div>
	       	</div>
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="Medicare_Ref" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Medicare Reference</label>
	    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
			<input type="text" class="form-control" name="Medicare_Ref" maxlength="50" value="<?php echo @$record->medicare_ref; ?>" >
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="Expiry" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Expiry (yy/mm)</label>
	    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
			<input type="text" class="form-control" name="Expiry" maxlength="5" value="<?php echo @$record->medicare_expiry; ?>" >
	    </div> 
	</div>

	<div class="form-group">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Patient unable to sign</label>
	    <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
			<input type="checkbox" class="form-control" name="unable_to_sign" <?php echo (@$record->unable_to_sign)?'checked':''; ?> onchange="Medicare.sig_(this)" >
	    </div> 
	</div>

	<div class="form-group cls-sign " style="<?php echo (@$record->unable_to_sign)?'display:none':''; ?>">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Sign</label>
	    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	    	<?php if( @$record->sign_code == '' ): ?>
				<div class=" js-signature" style="<?php echo (@$record->unable_to_sign)?'display:none':''; ?>" data-width="600" data-height="100" data-border="1px solid #ccc" data-background="#fff" data-line-color="#000" data-auto-fit="true"></div>
			<?php else: ?>
				<img src="<?php echo stripslashes(@$record->sign_code); ?>">
			<?php endif; ?>
	    </div> 
	</div>
	
	<input type="hidden" id="hidden_sig" name="hidden_sig" value="<?php echo stripslashes(@$record->sign_code); ?>"  />
	
	<?php if( @$record->sign_code == '' ): ?>
	<input type="hidden" name="new_sig" value="1"  />
	<?php endif; ?>


 	<div class="form-group">
 		<label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Items</label>
 		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
 			<?php //echo @$record->medicare_form_items; 
 				@$med_items = json_decode(@$record->medicare_form_items);
 			?>
	        <table class="table table-bordered">
	            <thead>
	                <tr>
	                    <th>Tick if Applicable</th>
	                    <th>Item Number</th>
	                    <th>Description</th>
	                </tr>
	            </thead>
	            <tbody>
	            	<?php foreach($medicare_items as $num=>$desc): ?>
	            	<tr>
	            		<td align="center"> <input type="checkbox" name="items[<?php echo @$num; ?>]"  /> </td>
	            		<td><?php echo @$num; ?></td>
	            		<td><?php echo @$desc; ?></td>
	            	</tr>
	            	<?php endforeach; ?> 
	            	<tr>
	            		<td align="center"> <input type="checkbox" name="items[other]"  /> </td>
	            		<td>Other</td>
	            		<td>
	            			Please used the box below if item(s) is not on the list. Enter Item number and Description
	            			<textarea name="items_other" rows="5" style="width: 100%"></textarea>
	            		</td>
	            	</tr>
	            </tbody>

	        </table>
        </div>
    </div>

	<div class="form-group">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<button class="btn btn-info" type="submit" name="save">Save</button>
			<button class="btn btn-primary" type="submit" name="savedropbox">Submit & Save to dropbox </button>
			<input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
	    </div> 
	</div>

</form>