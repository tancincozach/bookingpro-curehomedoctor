

<?php
    //echo $user_date_total_booking;
    $booking_max_msg = '<h3 class="bg-warning text-danger">You already reach the maximum of '.$max_book.' bookings today. Thank you</h3>';

    $patient_details = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4>Patient Details</h4> 
        <div class="patient_details">
            <table class="table table-bordered">
                <tr>
                    <td class="col-lg-4 bold" style="">Name</td>
                    <td style="">'.stripslashes($record->firstname).' '.stripslashes($record->lastname).'</td>
                </tr>
                <tr>
                    <td class="bold">Phone</td>
                    <td>'.$record->phone.'</td>
                </tr>
                <tr>
                    <td class="bold">Mobile No</td>
                    <td>'.$record->mobile_no.'</td>
                </tr>
                <tr>
                    <td class="bold">Emergency Contact Name</td>
                    <td>'.$record->em_contact_name.'</td>
                </tr>
                <tr>
                    <td class="bold">Emergency Contact PHone</td>
                    <td>'.$record->em_contact_phone.'</td>
                </tr>                
                <tr>
                    <td class="bold">Address</td>
                    <td>'.stripslashes($record->street_addr).'</td>
                </tr>
                <tr>
                    <td class="bold">Cross Street</td>
                    <td>'.stripslashes($record->cross_street).'</td>
                </tr>
                <tr>
                    <td class="bold">Suburb</td>
                    <td>'.stripslashes($record->suburb).'</td>
                </tr>
                <tr>
                    <td class="bold">Symptom(s)</td>
                    <td>'.stripslashes($record->symptoms).'</td>
                </tr>
                <tr>
                    <td class="bold">Date of Birth</td>
                    <td>'.date('d/m/Y', strtotime($record->dob)).'</td>
                </tr>
                <tr>
                    <td class="bold">Age</td>
                    <td>'.$record->patient_age.'</td>
                </tr>
                <tr>
                    <td class="bold">Gender (sex)</td>
                    <td>'.$record->gender.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Number</td>
                    <td>'.$record->medicare_number.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Ref</td>
                    <td>'.$record->medicare_ref.'</td>
                </tr>
                <tr>
                    <td class="bold">Medicare Exp</td>
                    <td>'.$record->medicare_expiry.'</td>
                </tr>
                <tr>
                    <td class="bold">DVA</td>
                    <td>'.($record->dva?'Yes':'No').'</td>
                </tr>
                 <tr>
                    <td class="bold">Private Health Insurance Provider</td>
                    <td>'.@$record->health_insurance_provider.'</td>
                </tr>
                 <tr>
                    <td class="bold">Private Health Fund Number</td>
                      <td>'.@$record->health_fund_number.'</td>
                </tr>
                  <tr>
                        <td class="bold">Does patient have a Pension, Concession or Healthcare Card</td>
                         <td>'.@$record->patient_has_health_card.'</td>
                    </tr>

                     <tr>
                        <td class="bold">Pension, Concession or Healthcare Card Expiry Date</td>
                         <td>'.(isset($record->health_card_expiry_date) ? $record->health_card_expiry_date:'0000-00-00').'</td>
                    </tr>
                <tr>
                    <td class="bold">Aboriginal or Torres Straight Islander</td>
                    <td>'.($record->abotsi?'Yes':'No').'</td>
                </tr>
                <tr>
                    <td class="bold">Cultural &#38; Ethnicity Background</td>
                    <td>'.stripslashes($record->cultural_ethnic_bg).'</td>
                </tr>
                <tr>
                    <td class="bold">Where did you hear about us</td>
                    <td>'.stripslashes($record->latest_wdyhau).'</td>
                </tr>
                <tr>
                    <td class="bold">Regular Practice</td>
                    <td>'.@$record->regular_practice.'</td>
                </tr>
                <tr>
                    <td class="bold">Regular Doctor</td>
                    <td>'.@$record->regular_doctor.'</td>
                </tr>
            </table>
        </div>

         
       
    </div>';

?>

<?php if($book_type == 'book'): ?>
    
    <h4 class="page-header">Book Appointment</h4>

    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    

        


        <div class="clearfix">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Set Booking</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li> 
                
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <h4>Set Booking</h4>
                   
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm BOOKING this appointment!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="book" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="3" />

                            <?php if( $this->user_lvl.$this->user_sub_lvl == 51 ): ?>
                            <input type="hidden" name="car_id" value="<?php echo $car->car_id; ?>" />
                            <input type="hidden" name="doctor_id" value="<?php echo $doctor->user_id ?>" />
                            <input type="hidden" name="chaperone_id" value="<?php echo $this->user_id ?>" />
                            <?php endif; ?>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-5 col-lg-4">
                                    <?php if( $this->user_lvl.$this->user_sub_lvl == 51 ): ?>
                                        <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                    <?php else: 
                                            echo form_dropdown('chaperone_id', $chaperone_array, '', 'class="form-control" required ');
                                        endif;
                                    ?>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-5 col-lg-4">
                                    <?php if( $this->user_lvl.$this->user_sub_lvl == 51 ): ?>                     
                                        <p class="form-control-static"><?php echo $car->car_name.' ('.$car->car_plateno.')'; ?></p>
                                    <?php else: 
                                            echo form_dropdown('car_id', $cars_array, '', 'class="form-control" required ');
                                        endif;
                                    ?>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-5 col-lg-4">
                                    <?php if( $this->user_lvl.$this->user_sub_lvl == 51 ): ?>    
                                        <p class="form-control-static"><?php echo $doctor->user_fullname; ?></p>
                                    <?php else: 
                                            echo form_dropdown('doctor_id', $doctors_array, '', 'class="form-control" required ');
                                        endif;
                                    ?>
                                </div>
                            </div>

                            <br />

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-3 col-lg-3 control-label">&nbsp;</label>
                                <div class="col-xs-8 col-sm-8 col-md-5 col-lg-4">
                                    <?php if( $user_date_total_booking < $max_book ): ?>
                                    <input class="btn btn-primary" type="submit" value="Book">
                                    <?php endif; ?>
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                            <?php 
                                if( $user_date_total_booking == $max_book ){
                                    echo $booking_max_msg;
                                }
                            ?>
                        </form>
                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->

        </div>
 

    </div>

<?php endif; ?>


<?php if($book_type == 'unbook'): ?>

    <h4 class="page-header">Unbook Appointment</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    



        <div class="clearfix">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Set Unbook</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li> 
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <h4>Unbook Reason</h4>
                       
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm UNBOOKING this appointment!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="unbook" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="2" />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php
                                        if( isset($car->car_name) )
                                            echo $car->car_name.' ('.$car->car_plateno.')'; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Reason</label>
                                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-4">
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>
                            </div>-->

                            <br />

                            <div class="form-group">
                                <label class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Unbook">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->

        </div>

    </div>
<?php endif; ?>

<?php if($book_type == 'visit'): ?>
    <h4 class="page-header">Visit Appointment</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    



        <div class="clearfix">

            <ul class="nav nav-tabs "> 
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Set Unbook</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li>
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h4>Set Visit</h4>
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm VISIT this appointment!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="visit" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="2" />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php
                                        if( isset($car->car_name) )
                                            echo $car->car_name.' ('.$car->car_plateno.')'; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                                </div>
                            </div>

                            <br />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Set Visit">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->
        </div>

    </div>
<?php endif; ?>


<?php if($book_type == 'reinstate'): ?>
    <h4 class="page-header">Reinstate Appointment</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    


       <div class="clearfix">

            <ul class="nav nav-tabs ">

                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Booking Details</a>
                </li>            
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li> 
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h4>Booking Details</h4>

                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm BOOKING this appointment!\')"'); ?>
                
                            <input type="hidden" name="chaperone_book" value="reinstate-book" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="3" />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                             

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php
                                        if( isset($car->car_name) )
                                            echo $car->car_name.' ('.$car->car_plateno.')'; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Status</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $this->booking_statuses[$record->book_status]; ?></p>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Reason</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo stripslashes($record->cancelled_reason); ?></p>
                                </div>
                            </div>                            

                            <br />

                            <p>
                                Clicking "Submit to Reinstate Appt" will make the appointment back to pool with OPEN status
                            </p>

                            <div class="form-group">
                                <label for="doctor" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Submit to Reinstate Appt">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>
            </div> <!-- end tab-content -->

        </div>

    </div>
<?php endif; ?>


<?php if($book_type == 'complete'): ?>
    <h4 class="page-header">Complete Appointment</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    



        <div class="clearfix">

            <ul class="nav nav-tabs "> 
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Set Complete</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li>
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h4>Set Complete</h4>
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm COMPLETE this appointment!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="complete" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="2" />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php
                                        if( isset($car->car_name) )
                                            echo $car->car_name.' ('.$car->car_plateno.')'; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: left">Consult Start</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', $record->consult_start); ?></p>
                                </div> 
                            </div>
 

                            <br />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Set Complete">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->
        </div>

    </div>
<?php endif; ?>

<?php if($book_type == 'addnote'): ?>
    <h4 class="page-header">Add Note</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    



        <div class="clearfix">

            <ul class="nav nav-tabs "> 
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Set Note</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li>
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm save NOTE!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="addnote" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" /> 

                            <br />

                            <div class="form-group">
                                <label class="col-md-4 col-lg-4" style="text-align: left">Appointment Note</label>
                                <div class="col-md-7 col-lg-8">
                                    <textarea class="form-control" name="appt_notes" rows="4"><?php echo $record->appt_notes; ?></textarea>
                                </div> 
                            </div>
 

                            

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Save Note">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->
        </div>

    </div>
<?php endif; ?>


<?php if($book_type == 'view'): ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4>Appointment Details</h4>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="col-lg-4 bold">Appt #</td>
                    <td><?php echo $record->ref_number_formatted ?></td>
                </tr> 
                <tr>
                    <td class="col-lg-4 bold">Status</td>
                    <td>
                        <strong><?php echo $this->booking_statuses[$record->book_status]; ?></strong>

                        <?php
                            if($record->book_status == BOOK_STATUS_CANCELLED)
                                echo 'Reason: <br />'.stripslashes($record->cancelled_reason);
                        ?>
                    </td>
                </tr> 
                <tr>
                    <td class="col-lg-4 bold">Area</td>
                    <td><?php echo $record->area_name ?></td>
                </tr> 
                <tr>
                    <td class="col-lg-4 bold">Date Booked</td>
                    <td><?php echo date('d/m/Y H:i', strtotime($record->appt_created_tz)); ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Symptom(s)</td>
                    <td><?php echo stripslashes($record->symptoms); ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Time in Queue</td>
                    <td><?php echo $record->in_queue; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Chaperone</td>
                    <td><?php echo $record->chaperone_name; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Car</td>
                    <td>
                        <?php 
                            if( isset($car->car_name) )
                                echo $car->car_name.' ('.$car->car_plateno.')';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Item #</td>
                    <td>
                        <?php 
                            if( isset($consult->item_num) )
                                echo @$consult->item_num;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Doctor</td>
                    <td><?php echo $record->doctor_name ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4 bold">Note(s)</td>
                    <td><?php echo $record->appt_notes ?></td>
                </tr>
            </tbody>

        </table>        
    </div>
    <?php echo  $patient_details; ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <input class="btn btn-warning" type="button" value="Back" onclick="history.go(-1)">
    </div>
</div>
<?php endif; ?>



<?php if($book_type == 'unseen'): ?>

    <h4 class="page-header">Unseen Appointment</h4>
    <div class="row">
        
        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Booking Chaperone an appointment and assign to a doctor
                    </dd>
                    <dt>
                        Features/Statuses:
                    </dt>
                    <dd>                    
                         
                    </dd>
                      
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>    



        <div class="clearfix">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a data-toggle="tab" href="#appt_form">Booking Details</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#appt_details">Patient Details</a>
                </li> 
            </ul>

            <div class="tab-content clearfix">

                <div id="appt_form" class="tab-pane fade in active">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <h4>Unseen Reason</h4>
                       
                        <?php echo form_open('', 'class="form-horizontal appointment-form" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm UNSEEN this appointment!\')"'); ?>
                            
                            <input type="hidden" name="chaperone_book" value="unseen" />
                            <input type="hidden" name="appt_id" value="<?php echo $record->appt_id ?>" />
                            <input type="hidden" name="book_status" value="<?php echo BOOK_STATUS_UNSEEN; ?>" />

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Appt #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->ref_number_formatted ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Date/Time Booked</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo date('d/m/Y H:i', strtotime($record->appt_created)); ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Chaperone</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                   <p class="form-control-static"><?php echo $record->chaperone_name ?></p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Car #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php
                                        if( isset($car->car_name) )
                                            echo $car->car_name.' ('.$car->car_plateno.')'; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                             <div class="form-group">
                                <label for="" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Item #</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static">
                                    <?php                                        
                                            echo @$consult->item_num; 
                                    ?>
                                    </p>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Doctor</label>
                                <div class="col-xs-8 col-sm-8 col-md-7 col-lg-8">
                                    <p class="form-control-static"><?php echo $record->doctor_name ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label" style="text-align: left">Reason</label>
                                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-4">
                                    <textarea class="form-control" name="unseen_reason" rows="4" required></textarea>
                                </div>
                            </div>

                            <br />

                            <div class="form-group">
                                <label class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <input class="btn btn-primary" type="submit" value="Submit Unseen">
                                    <input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div id="appt_details" class="tab-pane fade ">
                    <?php echo  $patient_details; ?>
                </div>

            </div> <!-- end tab-content -->

        </div>

    </div>
<?php endif; ?>