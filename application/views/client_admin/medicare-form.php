<h4 class="page-header">Medicare Form</h4>

<?php //echo strtotime('now'); ?>

<?php echo form_open('', 'class="form-horizontal appointment-form" id="medicare_form" name="medicare_form" method="post" onsubmit="return Medicare.confirm();"'); ?>

    <input type="hidden" name="appt_id" value="<?php echo @$record->appt_id ?>" />
    <input type="hidden" name="patient_id" value="<?php echo @$record->patient_id ?>" />
    <input type="hidden" name="ref_number_formatted" value="<?php echo @$record->ref_number_formatted ?>" />
    
    <div class="form-group">
	    <label for="First_Name" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">First Name</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
	       <input type="text" class="form-control" name="First_Name" value="<?php echo $record->firstname; ?>" required>
	    </div> 
	</div>
	
    <div class="form-group">
	    <label  for="Middle_Initial" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Initial</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<input type="text" class="form-control" name="Middle_Initial" value="<?php echo $record->middleinitial; ?>" >
	    </div> 
	</div>	

    <div class="form-group">
	    <label for="Last_Name" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Surname</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<input type="text" class="form-control" name="Last_Name" value="<?php echo $record->lastname; ?>" required>
	    </div> 
	</div>

    <div class="form-group clearfix">
	    <label  for="Street_Address" class="col-sm-4 col-md-4 col-lg-4 control-label">Residential Address</label>
	    <div class="col-sm-5 col-md-5 col-lg-5">
	    	<div class="row">
	    		<div class="col-sm-6 col-md-6 col-lg-6">
	    			<input type="text" class="form-control" name="Street_Address" value="<?php echo $record->street_addr; ?>" required>
	    		</div>
	    		<div class="col-sm-6 col-md-6 col-lg-6">
	    			<input type="text" class="form-control suburb" name="Suburb" value="<?php echo $record->suburb; ?>" required>
	    		</div>

	    	</div>			 
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="DOB" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Date of Birth (dd/mm/YY)</label>
	    <div class="col-xs-6 col-sm-5 col-md-2 col-lg-2">
			<input type="text" class="form-control" id="dob" name="DOB" value="<?php echo date('d/m/Y',strtotime($record->dob)); ?>" data-format="DD/MM/YYYY" data-template="D MMM YYYY">
	    </div> 
	</div>

	<?php
		//$record->medicare_number = '1234 56789 0';
		
		$mdn1 = substr($record->medicare_number, 0, 4);
		$mdn2 = substr($record->medicare_number, 4, 5);
		$mdn3 = substr($record->medicare_number, 9, 1);
	?>

    <div class="form-group">
	    <label  for="Medicare_Number_1" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Medicare Numer</label>
	    <div class="col-xs-8 col-sm-5 col-md-3 col-lg-3">
			<input type="text" class="form-control" name="Medicare_Number" maxlength="50" value="<?php echo $record->medicare_number; ?>" required>
	    	<!--<div class="row">
		    	<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
		       		<input type="text" class="form-control" name="Medicare_Number_1" maxlength="4" value="<?php echo $mdn1; ?>" required>
		       	</div>
		    	<div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" style="padding-left: 0px">
		       		<input type="text" class="form-control" name="Medicare_Number_2" maxlength="5" value="<?php echo $mdn2; ?>" required>
		       	</div>
		    	<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="padding-left: 0px">
		       		<input type="text" class="form-control" name="Medicare_Number_3" maxlength="1" value="<?php echo $mdn3; ?>" required>
		       	</div>
	       	</div>-->
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="Medicare_Ref" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Medicare Reference</label>
	    <div class="col-xs-8 col-sm-5 col-md-3 col-lg-3">
			<input type="text" class="form-control" name="Medicare_Ref" maxlength="50" value="<?php echo $record->medicare_ref; ?>" required>
	    </div> 
	</div>

    <div class="form-group">
	    <label  for="Expiry" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Expiry (yy/mm)</label>
	    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
			<input type="text" class="form-control" name="Expiry" maxlength="5" value="<?php echo $record->medicare_expiry; ?>" required>
	    </div> 
	</div>

	<div class="form-group">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Patient unable to sign</label>
	    <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
			<input type="checkbox" class="form-control" name="unable_to_sign" <?php echo ($record->unable_to_sign)?'checked':''; ?> onchange="Medicare.sig_required(this)" >
	    </div> 
	</div>

	<div class="form-group cls-sign " style="<?php echo ($record->unable_to_sign)?'display:none':''; ?>">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Sign</label>
	    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		<div class="js-signature"  ontouchmove="event.preventDefault();" style="<?php echo ($record->unable_to_sign)?'display:none':''; ?>" data-width="100%" data-height="100" data-border="1px solid #ccc" data-background="#fff" data-line-color="#000" data-auto-fit="true"></div>
				
			<?php if( $record->sign_code != '' ): ?>
				<img  class="signature-image" src="<?php echo stripslashes($record->sign_code); ?>">
			<?php endif; ?>


			<a class="btn btn-primary clear_sign"  alt="<?php echo $record->appt_id;?>"> Clear Signature</a>			
	    </div> 
	</div>
	
	<input type="hidden" id="hidden_sig" name="hidden_sig" value="<?php echo stripslashes($record->sign_code); ?>"  required/>
	
	<?php if( $record->sign_code == '' ): ?>
	<input type="hidden" name="new_sig" value="1"  />
	<?php endif; ?>


 	<div class="form-group">
 		<label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Items</label>
 		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
 			<?php //echo $record->medicare_form_items; 
 				$med_items = json_decode($record->medicare_form_items);
 			?>
	        <table class="table table-bordered">
	            <thead>
	                <tr>
	                    <th>Tick if Applicable</th>
	                    <th>Item Number</th>
	                    <th>Description</th>
	                </tr>
	            </thead>
	            <tbody>
	            	<?php foreach($medicare_items as $num=>$desc): ?>
	            	<tr>
	            		<td align="center"> <input type="checkbox" name="items[<?php echo $num; ?>]" <?php echo isset($med_items->$num)?'checked':''; ?> /> </td>
	            		<td><?php echo $num; ?></td>
	            		<td><?php echo $desc; ?></td>
	            	</tr>
	            	<?php endforeach; ?> 
	            	<tr>
	            		<td align="center"> <input type="checkbox" name="items[other]" <?php echo isset($med_items->other)?'checked':''; ?> /> </td>
	            		<td>Other</td>
	            		<td>
	            			Please used the box below if item(s) is not on the list. Enter Item number and Description
	            			<textarea name="items_other" rows="5" style="width: 100%"><?php echo isset($med_items->other)?$med_items->other:''; ?></textarea>
	            		</td>
	            	</tr>
	            </tbody>

	        </table>
        </div>
    </div>

	<div class="form-group">
	    <label  for="unable_to_sign" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
	    <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
			<button class="btn btn-info" type="submit" name="save">Save</button>
			<button class="btn btn-primary" type="submit" name="savedropbox">Submit & Save to dropbox </button>
			<input class="btn btn-warning" type="button" value="Cancel" onclick="history.go(-1)">
	    </div> 
	</div>

</form>