<h4 class="page-header">Chaperone: Register New Doctor</h4>

<p class="bg-info text-info" style="padding: 5px;">
	 
	Please used this page to register new doctor if doesn't exist on the dropdown
	 
</p>

<div class="col-md-6">

<?php 
	
	if( isset($user_exist) AND @$user_exist != '' ){
		echo '<p class="bg-danger text-danger">'.$user_exist.'</p>';
	}
	
?>
	<?php echo form_open('', 'class="form-horizontal" role="form" method="post" onSubmit="return hcd.common.confirm();"'); ?>

		<div class="form-group">
	        <label for="doc_fullname" class="col-sm-4 col-md-4 col-lg-4 control-label">Doctor Name</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	            <input type="text" class="form-control" name="doc_fullname" value="<?php echo @$doc_fullname; ?>" required>
	        </div>
	    </div>

		<div class="form-group">
	        <label for="doc_username" class="col-sm-4 col-md-4 col-lg-4 control-label">Username</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	            <input type="text" class="form-control" name="doc_username" class="form-control" value="<?php echo @$doc_username; ?>" required>
	        </div>
	    </div>

		<div class="form-group">
	        <label for="doc_pass" class="col-sm-4 col-md-4 col-lg-4 control-label">Password</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	            <input type="text" class="form-control" name="doc_pass" value="<?php echo @$doc_pass; ?>" required>
	        </div>
	    </div>

		<div class="form-group">
	        <label for="doc_email" class="col-sm-4 col-md-4 col-lg-4 control-label">Email</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	            <input type="text" class="form-control" name="doc_email" value="<?php echo @$doc_email; ?>" >
	        </div>
	    </div>

		<div class="form-group">
	        <label for="doc_mobile" class="col-sm-4 col-md-4 col-lg-4 control-label">Mobile</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	            <input type="text" class="form-control" name="doc_mobile" value="<?php echo @$doc_mobile; ?>" required>
	        </div>
	    </div>

		<div class="form-group">
	        <label for="" class="col-sm-4 col-md-4 col-lg-4 control-label">&nbsp;</label>
	        <div class="col-sm-6 col-md-6 col-lg-6">
	           	<button type="submit" class="btn btn-primary">Register</button>
	        </div>
	    </div>

	</form>

</div>