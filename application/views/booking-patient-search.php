 
<h4 class="page-header">Booking Form - Search Patient</h4>

<div class="col-sm-6 col-md-6 col-lg-6 box_border">
	<div class="row">
		<h4>Search Patient</h4>
		 
		<div style="padding: 10px;" class="text-center">

			<h4>Ask Caller</h4>
			<h4>Is the booking for yourself or another person?</strong></h4>

			<p style="color: red; font-style: italic">If booking for another person, change name below... THEN "click" on search to check if already in system</p>
		</div>
		
		<?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg">'.$this->session->flashdata('fmesg').'</div>':''; ?>  

		<?php echo form_open('dashboard/patient_search', 'class="form-inline" id="patient_search_form" name="patient_search_form" method="post"'); ?>

            <?php 
            	$hidden = '';
	            
	            $hidden .= '<input type="hidden" name="First_Name" value="'.@$First_Name.'" />';
	            $hidden .= '<input type="hidden" name="Last_Name" value="'.@$Last_Name.'" />';
	            $hidden .= '<input type="hidden" name="when" value="'.$when.'" />';

            	if( isset($collection) ){

	                foreach($collection as $field=>$value): 

	                    if( $field == 'Suburb' ){
	                        foreach( $value as $suburb_field=>$suburb_val ){
	                            $hidden .= '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
	                        }
	                    }else
	                        $hidden .= '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
	            
	                endforeach; 
            	}

                echo $hidden;
            ?>  

			<div class="form-group">
				<label class="" for="First_Name">First Name</label>
				<input type="text" class="form-control" name="First_Name" value="<?php echo @$First_Name; ?>" required>
			</div>
			<div class="form-group">
				<label class="" for="First_Name">Last Name</label>
				<input type="text" class="form-control" name="Last_Name" value="<?php echo @$Last_Name; ?>">
			</div>

			<button type="submit" class="btn btn-default">Search</button>
		</form>
	</div>

	<div class="row">
		<table class="table">
		<?php 
			if( isset($patients) ):

			foreach($patients as $row):?>
			<tr>
				<td>
	 				<p>Name: 
	 				<?php
	 					
	 					echo highlight_phrase(strtoupper(trim($row->firstname)), $First_Name, '<span class="highlight_search_match">', '</span>');
	 					echo ' ';
	 					echo highlight_phrase(strtoupper(trim($row->lastname)), $Last_Name, '<span class="highlight_search_match">', '</span>');
	 				?>
	 				</p>

	 				<p>DOB: <?php echo date('d/m/Y', strtotime($row->dob)); ?></p>
					<p>Phone: <?php echo $row->phone; ?></p>
	 				<p>Address: <?php echo stripslashes($row->street_addr).' '.$row->suburb; ?></p>

	 				<?php if(trim($row->patient_notes) != ''):  ?>
	 				<p class="bg-danger">Notes: <?php echo stripslashes($row->patient_notes).' '.$row->suburb; ?></p>
	 				<?php endif; ?>
	 			</td>
	 			<td>
	 				<?php echo form_open('dashboard/booking/'.$when, 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post"'); ?>
	 					
	 					<input type="hidden" name="collection[patient_id]" value="<?php echo $row->patient_id ?>">

	 					<?php echo $hidden; ?>

	 					<button class="btn btn-sm btn-primary" type="submit" name="patient_type" value="existing"><strong>Book Existing Patient</strong></button>

	 				</form>
	 			</td>
 			<tr>
		<?php endforeach; ?>
			<tr>
				<td></td>
				<td>
	 				<?php echo form_open('dashboard/booking/'.$when, 'class="form-inline" id="booking_patient_search_form" name="booking_patient_search_form" method="post"'); ?>

	 					<input type="hidden" name="collection[patient_id]" value="">

	 					<?php echo $hidden; ?>

	 					<button class="btn btn-sm btn-success" type="submit" name="patient_type" value="new"><strong>Book New Patient</strong></button>

	 				</form>
	 			</td>
			</tr>
		<?php endif;?>
		</table>
	</div>
</div>

<div class="col-sm-6 col-md-6 col-lg-6 box_border">
	<div class="row">
		 
	</div>
</div>

<br />
<br />
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <hr style="border: 1px solid red" />
    <a class="btn btn-danger" href="dashboard/did_not_complete/?<?php echo @$uri_not_complete; ?>" >DID NOT COMPLETE</a>
</div>