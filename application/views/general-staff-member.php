 
<h4 class="page-header">Message or HCD Admin Followup</h4>

<div class="col-sm-8">
	<?php echo form_open('', 'class="form-horizontal" id="call_form" name="call_form" method="post" onsubmit="hcd.common.confirm()"'); ?>

		<input type="hidden" name="tran_type" value="General Message">
		<input type="hidden" name="tran_desc" value="Message or HCD Admin Followup">

		<div class="form-group">
		    <label for="outbound_name" class="col-sm-4 control-label">Outbound Name</label>
		    <div class="col-sm-6">
				<select class="form-control" name="staff">
					<option value="">Select staff member</option>
				</select>
		    </div>
		</div>

		<div class="form-group">
		    <label for="outbound_name" class="col-sm-4 control-label">Outbound Name</label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" name="outbound_name" value="">
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="outbound_phone" class="col-sm-4 control-label">Outbound Phone</label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" name="outbound_phone" value="">
		    </div>
		</div>
		  
		<div class="form-group">
		    <label for="reason_for_call" class="col-sm-4 control-label">Reason for Call</label>
		    <div class="col-sm-6">
		    	<textarea class="form-control" name="reason_for_call" rows="3"></textarea>
		    </div>
		</div>
		   
		<div class="form-group">
		    <div class="col-sm-offset-4 col-sm-6">
		      <button type="submit" class="btn btn-primary">Send</button>
		    </div>
		</div>

	</form>

</div>