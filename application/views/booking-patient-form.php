 
<?php $patient = @$getvars['patient']; ?>

<h4 class="page-header">Booking Form</h4>


    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Booking an appointment
                </dd>
                <dt>
                    Features/Statuses:
                </dt>
                <dd>                    
                    <ul> 
                        <li>Booking: today</li> 
                        <li>Booking: next day</li> 
                        <li>Booking: holiday
                        	<ul>
                        		<li>Plan: if booking next day and its holiday the system should advice operator that the booking will be happen on the date</li>
                        	</ul>
                        </li> 
                        <li>Booking: Duplicate patient today</li> 
                        <li>Age: the system should automatically calculate date base on date of birth </li> 
                    </ul>
                </dd>
                  
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>

<div class="row text-center" style="font-weight: 700">
	<?php 
		if( $when == 'today' ) {
			
			echo '<strong style="color: red">TODAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y');

		}elseif($when == 'nextday'){
			echo '<strong style="color: red">NEXT DAY [ '.$getvars['collection']['Suburb']['area'].' ]</strong> <br />';
			echo date('l d F Y', strtotime('+1 day'));
		}



	 ?>
	 
</div>

<div class="row">

	 
	<div class="col-lg-12 col-md-12 col-sm-12">
		
		<br />

		<div class="box_border">

			<?php echo form_open('dashboard/booking_submit', 'class="bg-grey" id="booking_patient_form" name="booking_patient_form" method="post" onsubmit="return hcd.common.confirm(\'Confirm submit booking!\')"'); ?>

				<input type="hidden" name="booking_when" value="<?php echo $when; ?>">
				<input type="hidden" name="patient_type" value="<?php echo $getvars['patient_type']; ?>">
				<input type="hidden" name="patient_id" value="<?php echo @$patient->patient_id; ?>">

                <?php 
                    foreach($getvars['collection'] as $field=>$value): 

                        if( $field == 'Suburb' ){
                            foreach( $value as $suburb_field=>$suburb_val ){
                                echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                            }
                        }else
                            echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                
                    endforeach; 
                ?> 


				<!-- START GOOGLE GEOCODING STORAGE FIELDS // -->
				<div style="display: none;">
					<input type="text" name="sm[streetPrefix]" id="sm_streetPrefix" /><!-- Street Number : "116A" // -->
					<input type="text" name="sm[street]" id="sm_street" /><!-- Street : "Hill Street" // -->
					<input type="text" name="sm[area]" id="sm_area" /><!-- Town : "Newton" // -->
					<input type="text" name="sm[state]" id="sm_state" /><!-- State : "Queensland" // -->
					<input type="text" name="sm[text]" id="sm_text" /><!-- Full Address : "116A Hill Street, Newton QLD 4350, Australia" // -->
					<input type="text" name="sm[latlong]" id="sm_latlong" /><!-- Latitude : -27.560453 , Longitude : 151.934665 // -->
				</div>
				<!-- END GOOGLE GEOCODING STORAGE FIELDS // -->

				<div class="col-lg-6 col-md-6 col-sm-6" style="border-right: 1px solid #ccc;padding-top:10px">

					<div class="col-lg-8 col-md-8 col-sm-8">

						<div class="row">

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="First_Name">First Name</label>
									<input type="text" class="form-control" name="First_Name" value="<?php echo stripslashes(@$patient->firstname); ?>" required>
								</div>	
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Last_Name">Last Name</label>
									<input type="text" class="form-control" name="Last_Name" value="<?php echo stripslashes(@$patient->lastname); ?>" required>
								</div>	
							</div>
					 
							<div class="clearfix"></div>

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Landline_Phone">Landline Phone</label>
									<input type="text" class="form-control" name="Landline_Phone" value="<?php echo stripslashes(@$patient->phone); ?>" required>
								</div>	
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label for="Mobile_Phone">Mobile Phone</label>
									<input type="text" class="form-control" name="Mobile_Phone" value="<?php echo stripslashes(@$patient->mobile_no); ?>" required>
								</div>	
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						 
						<div class="form-group">
							<label for="Last_Name">Notes</label>							
							<textarea class="form-control" name="Patient_Notes" rows="3" style="background-color: #f2dede"><?php echo stripslashes(@$patient->patient_notes); ?></textarea>
						</div>	
						 
					</div>

					<div class="clearfix"></div>

					<!--
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Emergency_Contact_Name">Emergency Contact Name</label>
							<input type="text" class="form-control" name="Emergency_Contact_Name" id="Emergency_Contact_Name" value="<?php echo stripslashes(@$patient->em_contact_name); ?>" required>
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Emergency_Contact_Phone">Emergency Contact PHone</label>
							<input type="text" class="form-control" name="Emergency_Contact_Phone" id="Emergency_Contact_Phone" value="<?php echo stripslashes(@$patient->em_contact_phone); ?>" required>
						</div>	
					</div>

					<div class="clearfix"></div>
					-->

					<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="row">

								<div class="col-lg-4 col-md-4 col-sm-4">
						             <div class="form-group">                            
		                                <label for="em_contact_name pull-left">Emergency Contact Name </label>                     
		                              	<input name="Emergency_Contact_Name"  rows="10" cols="53"  id="Emergency_Contact_Name" style="background:#FFC000" value="<?php echo @$patient_info->em_contact_name;?> " class="form-control" required>
		                            </div>
									
								</div>

								<div class="col-lg-4 col-md-4 col-sm-4">
		                            <div class="form-group">
		                            	
		                            	<label for="em_contact_phone pull-left"> Emergency Contact Phone </label> 
		                              	<input name="Emergency_Contact_Phone"  rows="10" cols="53"  id="Emergency_Contact_Phone"  style="background:#FFC000"  class="form-control" value="<?php echo @$patient_info->em_contact_phone;?> " required>
		                            </div>
									
								</div>

								<div class="col-lg-4 col-md-4 col-sm-4">
			                            <div class="form-group">
			                            <label for="em_contact_phone pull-left">Relationship</label>
			                             <input name="Emergency_Relationship" type="text" class="form-control"  id="Emergency_Relationship" style="background:#FFC000" value="<?php echo @$patient_info->em_relationship; ?>" placeholder="" required >
			                            	
			                            </div>
									
								</div>

								
							</div>
							<div class="row">

								<div class="col-lg-4 col-md-4 col-sm-4">
						             <div class="form-group">                            
		                                <label for="Next_of_Kin pull-left">Next of Kin </label>                     
		                              	<input name="Next_of_Kin"  rows="10" cols="53"  id="next_kin" style="background:#FFC000" value="<?php echo @$patient_info->next_kin;?> " class="form-control" required>
		                            </div>
									
								</div>

								<div class="col-lg-4 col-md-4 col-sm-4">
		                            <div class="form-group">
		                            	
		                            	<label for="Next_Of_Kin_Contact_Phone pull-left"> Next of Kin Contact Phone </label> 
		                              	<input name="Next_Of_Kin_Contact_Phone"  rows="10" cols="53"  id="Next_Of_Kin_Contact_Phone"  style="background:#FFC000"  class="form-control" value="<?php echo @$patient_info->next_kin_contact_phone;?> " required>
		                            </div>
									
								</div>
								
							</div>
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-8">
									<div class="form-group">
										<label for="Email">Email</label>
										<input type="text" class="form-control" name="Email" value="<?php echo stripslashes(@$patient->email); ?>">
									</div>	
								</div>			
							</div>
                                                    
                    </div>

					<div class="clearfix"></div>


					<div class="clearfix"></div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Address">Address</label>
							<input type="text" class="form-control" name="Address" id="Address" value="<?php echo stripslashes(@$patient->street_addr); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Suburb">Suburb</label>
							<input type="text" class="form-control" name="Suburb" id="Suburb" value="<?php echo stripslashes(@$patient->suburb); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Post_Code">Postcode</label>
							<input type="text" class="form-control" name="Post_Code" id="Post_Code" value="<?php echo stripslashes(@$patient->postcode); ?>" required onblur="Booking.google_map_codeAddress()">
						</div>	
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Cross_Street">Cross Street</label>
							<input type="text" class="form-control" name="Cross_Street" id="Cross_Street" value="<?php echo stripslashes(@$patient->cross_street); ?>" >
						</div>	
					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12 clearfix">
					
					Google maps here

						<div id="addr_checkbox" style="display: none; padding: 10px; " class="bg-danger">
							<strong>Please check with the caller that the address displayed below is correct. 
							<!--A correct address is required for the Fleet management software to work correctly.--></strong><br />
							<strong>Is the address text and location displayed on the map below correct?</strong> 
							<select name="addr_correct" id="addr_correct" class="" required>
								<option value="" selected="selected">No</option>
								<option value="1">Yes</option><br />
							</select> 
						</div>					
						<div id="returned_addr" style="display: none; padding-left: 10px;" class="bg-danger"></div>
						<div id="map-canvas" style="height: 300px; width: 100%"></div>

					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Symptoms">Symptoms</label>							
							<textarea class="form-control" name="Symptoms" rows="5" required><?php echo (@$getvars['collection']['latest_symptoms']==''?'':$getvars['collection']['latest_symptoms']); ?></textarea>
						</div>	
					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6" style="padding-top:10px">

					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="pension">Does patient have a Pension, Concession or Healthcare Card</label>														
							<select name="health_care_card_question"  required>		
									<option value="">**Select**</option>							
									<option value="Yes" <?php echo (isset($patient->patient_has_health_card) && $patient->patient_has_health_card=='Yes' ? 'SELECTED':'');?>>Yes</option>
									<option value="No" <?php echo (isset($patient->patient_has_health_card) && $patient->patient_has_health_card=='No' ? 'SELECTED':'');?>>No</option>
							</select>
						</div>	
					</div>
			<!-- 		<div class="col-lg-5 col-md-5 col-sm-5 expiry_date_container"  style="<?php// echo (isset($patient->patient_has_health_card) && $patient->patient_has_health_card=='Yes' ? '':'display:none');?>">
					<label for="pension">EXPIRY DATE of Pension, Concession or Health Card</label>			
					<div class="form-group">							
							 <input type="text" name="expiry_date"  value="<?php //echo isset($patient->health_card_expiry_date) ? date('d/m/Y', strtotime(@$patient->health_card_expiry_date)):''; ?>" class="form-control"  /> 

						</div>	
					</div> -->

					<div class="clearfix">&nbsp;</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="DOB">Date of Birth</label>
							<br />
							<!--<input type="text" class="form-control datepicker" id="dob" name="DOB" value="<?php echo (isset($patient->dob))?date('d/m/Y', strtotime(@$patient->dob)):''; ?>" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">-->
							<input type="text" class="form-control datepicker" id="dob" name="DOB" value="<?php echo (isset($patient->dob))?date('d/m/Y', strtotime(@$patient->dob)):''; ?>"  data-format="DD/MM/YYYY" data-template="D MMM YYYY">
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="age">Age</label>
							<input type="text" class="form-control" name="age" id="patient_age" value="<?php echo @$patient->age; ?>" readonly>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Gender">Gender</label>							
							<select class="form-control" name="Gender" required>
								<option value=""> --- Select Gender --- </option>
								<option value="M" <?php echo (@$patient->gender =='M')?'selected':''; ?> >Male</option>
								<option value="F" <?php echo (@$patient->gender =='F')?'selected':''; ?> >Female</option>
							</select>
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">

						<p class="text-center bg-info small">
							<strong>Operator Instruction</strong><br />
							Medicare Card MUST be collected.  If caller does not have this # they need to have the Medicare Card OR Other Identification available for when the doctor & chaperone arrive, If they do not, they may be billed.
						</p>
						<div class="form-group">
							<label for="Medical_Number">Medicare Number</label>							
							<input type="text" class="form-control" name="Medical_Number" maxlength="10" value="<?php echo stripslashes(@$patient->medicare_number); ?>" required>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Reference">Medicare Reference</label>
							<input type="text" class="form-control" name="Medical_Reference" value="<?php echo stripslashes(@$patient->medicare_ref); ?>" required>
						</div>	
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="Medical_Expirey">Medicare Expiry <small>(mm/yy)</small></label>
							<input type="text" class="form-control" name="Medical_Expirey" value="<?php echo stripslashes(@$patient->medicare_expiry); ?>" required>
						</div>	
					</div>

					<div class="clearfix"></div>
				
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="form-group">
							<label for="dva_card_gold">TICK if "Gold DVA" card</label>
							<input type="checkbox"  name="dva_card_gold" <?php echo (@$patient->dva_card_gold)?'checked':''; ?> style="margin-left:10px;">
						</div>	
					</div>
					
					<div class="clearfix"></div>

					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="Health_Insurance_Provider">Private Health Insurance Provider</label>
							<input type="text" class="form-control"  name="Health_Insurance_Provider"  value="<?php echo @$patient->health_insurance_provider; ?>">
						</div>	
					</div>
				
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Health_Fund_Number">Private Health Fund Number</label>
							<input type="text" class="form-control" name="Health_Fund_Number" value="<?php echo @$patient->health_fund_number; ?>" >
						</div>	
					</div>
				
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Medical_Expirey">Where did you hear about us?</label>							 
							<?php echo form_dropdown('wdyhau', $getvars['wyhaulist'], @$patient->wdyhau, 'class="form-control"'); ?>
						</div>	
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="form-group">
							<label for="Medical_Expirey">Aboriginal OR Torres Straight Islander</label>
							<select class="form-control" name="abotsi" required>								
								<option value=""> --- Select --- </option>
								<option value="0" <?php echo (@$patient->abotsi =='0')?'selected':''; ?> >No</option>								
								<option value="1" <?php echo (@$patient->abotsi =='1')?'selected':''; ?> >Yes</option>
							</select>							
						</div>	
					</div> 
				 

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Medical_Expirey">Cultural &#38; Ethnicity Background</label>
							<textarea class="form-control" name="cultural_ethnic_bg"><?php echo @$patient->cultural_ethnic_bg; ?></textarea>						
						</div>	
					</div>

					<div class="clearfix"></div>
					
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="form-group">
							<label for="Medical_Expirey">Search Regular Practice</label>
							<div class="row">
								<div class="col-lg-11 col-md-11 col-sm-11">
									<input type="text" class="form-control" name="search_practice" id="practice_autocomplete" value="<?php echo @$patient->regular_practice; ?>" placeholder="Search by name" required="required" >
								</div>
								<!--<div class="col-lg-6 col-md-6 col-sm-6">
									<input type="text" class="form-control" name="search_practice_postcode" value="" placeholder="Search by postcode" >
								</div>-->
							</div>
						</div>
						 
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="form-group">
							<label for="Regular_Doctor">IF NO MATCH - enter practice here</label>
							<input type="text" class="form-control" name="email_to_hcd" id="no_match_practice" value="">
						</div>	
					</div> 

					<div class="clearfix"></div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group">
							<label for="Regular_Doctor">Regular Doctor</label>
							<input type="text" class="form-control" name="Regular_Doctor" value="<?php echo @$patient->regular_doctor; ?>">
						</div>	
					</div>
					
				</div>	

				<div class="clearfix"></div>

				<div class="col-lg-12 col-md-12 col-sm-12 bg-info text-center">
					<p>NOTE:</p>
					<p>if call disconnects, call back if you know the number Use PREFIX "2"</p>
				</div>
				
				<div class="clearfix"></div>
				<br />

				<div class="col-lg-12 col-md-12 col-sm-12 text-center">

					<div style="padding-bottom: 30px">
						<button class="btn btn-primary" type="submit" value="submit">Click to Complete Booking</button>
					</div>
					<button class="btn btn-success" type="submit" name="add_another_patient" value="more">Submit and ADD another Patient</button>
					&nbsp;&nbsp;
					
					&nbsp;&nbsp;
					<a class="btn btn-warning" href="dashboard/did_not_complete/?<?php echo $getvars['uri_not_complete']; ?>" >DID NOT COMPLETE</a>

				</div>

				<div class="clearfix"></div>

				<br />
			</form>

			<div class="clearfix"></div>

		</div>
	</div>

</div>