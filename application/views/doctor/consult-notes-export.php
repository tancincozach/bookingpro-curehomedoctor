<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Doctor  Consult Note</title>
  <meta name="description" content="Medicare Form">
  <meta name="author" content="Welldone International">
 
</head>
<body>
<div class="clearfix">

	  <!-- Tab panes -->
	  <img alt="Cure Logo" style="width:50%" src="<?php echo BRAND_CLIENT_LOGO_PATH; ?>" />

		<div class="clearfix">			
			<hr/>
		<table  style="margin:0 auto;width: 100%;" cellspacing="0" cellpadding="0">
						<tr>
							<td width="50%">
				<h4 style="text-transform: uppercase">Patient Details</h4>
									<table  style="margin:0 auto;width: 100%;"  cellspacing="0" cellpadding="0">
										<tr>
											<td style=" font-size: 12px; width: 30%; "><strong>Booking #  </strong></td>							  
											<td style=" font-size:12px;"><?php echo $record->ref_number_formatted; ?></td>
										</tr>	
										<tr>
											<td style=" font-size: 12px;"><strong>Date of Service  </strong></td>							  
											<td style=" font-size:12px;"><?php echo date('d/m/Y',strtotime($record->appt_created)); ?></td>
										</tr>
										<tr>
											<td style="font-size: 12px;"><strong>First Name  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo $record->firstname; ?></td>
										</tr>	
										<tr>
											<td style=" font-size: 12px;"><strong>Last Name  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo $record->lastname; ?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>DOB  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo  date('d/m/Y', strtotime($record->dob));?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>Age  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo  $record->patient_age;?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>Gender  </strong></td>
											  
											<td style=" font-size:12px;"> <?php  if( $record->gender == 'M' ) echo 'Male'; if( $record->gender == 'F' ) echo 'Female';?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>Address  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo stripslashes($record->street_addr).' '.($record->suburb).' '.$record->postcode; ?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>Medicare Number  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo $record->medicare_number; ?></td>
										</tr>
										<tr>
											<td style=" font-size: 12px; "><strong>Medicare Expiry  </strong></td>
											  
											<td style=" font-size:12px;"><?php echo $record->medicare_expiry; ?></td>
										</tr>
				<!-- 						<tr>
											<td style="  font-size: 12px;" colspan="2"><strong>Aboriginal or Torres Straight Islander  </strong><?php echo ($record->abotsi)?'Yes':'No'; ?></td>											 
										</tr>
										<tr>
											<td style="  font-size: 12px;" colspan="2"><strong>Cultural &#38; Ethnicity Background  </strong><?php echo stripslashes($record->cultural_ethnic_bg); ?></td>											 
										</tr>
										<tr>
											<td style="  font-size: 12px;" colspan="2"><strong>Regular Practice  </strong><?php echo $record->regular_practice; ?></td>											 
										</tr>
										<tr>
											<td style="  font-size: 12px;" colspan="2"><strong>Regular Doctor  </strong><?php echo $record->regular_doctor; ?></td>											 
										</tr> -->


								</table>
							</td>
							<td width="50%" valign="top">
												<h4 style="text-transform: uppercase">Contact Details</h4>
													 <table  style="margin:0 auto;width: 100%;" cellspacing="0" cellpadding="1">
															<tr>
																<td style=" font-size: 12px; width:30%  "><strong>Home Phone </strong></td>
																  
																<td style=" font-size:12px;width:70%"><?php echo $record->phone; ?></td>
															</tr>	
															<tr>
																<td style=" font-size: 12px;  "><strong>Mobile Phone  </strong></td>
																  
																<td style=" font-size:12px;"><?php echo $record->mobile_no; ?></td>
															</tr>	
															<tr>
																<td style=" font-size: 12px;  "><strong>Email  </strong></td>
																  
																<td style=" font-size:12px;"><?php echo $record->email; ?></td>
															</tr>															<tr>
																<td style=" font-size: 12px;  "><strong>Aboriginal or Torres Straight Islander  </strong></td>
																  
																<td style=" font-size:12px;"><?php echo ($record->abotsi)?'Yes':'No'; ?></td>
															</tr>															<tr>
																<td style=" font-size: 12px;  "><strong>Cultural &#38; Ethnicity Background   </strong></td>
																  
																<td style=" font-size:12px;"><?php echo stripslashes($record->cultural_ethnic_bg); ?></td>
															</tr>
															<tr>
																<td style=" font-size: 12px;  "><strong>Regular Practice  </strong></td>
																  
																<td style=" font-size:12px;"><?php echo $record->regular_practice; ?></td>
															</tr>
															<tr>
																<td style=" font-size: 12px;  "><strong>Regular Doctor  </strong></td>
																  
																<td style=" font-size:12px;"><?php echo $record->regular_doctor; ?></td>
															</tr>
														

													</table>
							</td>
						</tr>
						<tr>
							<td width="50%" valign="top">
								<h4 style="text-transform: uppercase">Appointment Details</h4>
								 <table  style="margin:0 auto;width: 100%;" cellspacing="0" cellpadding="0">
															<tr>
																<td style=" font-size: 12px; width:30%  "><strong>Appointment START time </strong></td>
																  
																<td style=" font-size:12px;width:70%"><?php echo $record->phone; ?></td>
															</tr>	
															<tr>
																<td style=" font-size: 12px;  "><strong>Appointment FINISH time </strong></td>
																  
																<td style=" font-size:12px;"><?php echo $record->mobile_no; ?></td>
															</tr>	
														<!-- 	<tr>
																<td style=" font-size: 12px;  "><strong>Attending Doctor </strong></td>
																  
																<td style=" font-size:12px;"><?php //echo stripslashes($record->doctor_name); ?></td>
															</tr>															<tr>
																<td style=" font-size: 12px;  "><strong>Provider # </strong></td>
																  
																<td style=" font-size:12px;"><?php //echo stripslashes($record->prov_num); ?></td>
															</tr> -->
														

													</table>
							</td>
							<td>
												 <table  style="margin:0 auto;width: 100%;margin-top: 30px;" cellspacing="0" cellpadding="0">
															
															<tr>
																<td style=" font-size: 12px; width: 30%"><strong>Attending Doctor </strong></td>
																  
																<td style=" font-size:12px;"><?php echo stripslashes($record->doctor_name); ?></td>
															</tr>															<tr>
																<td style=" font-size: 12px;  "><strong>Provider # </strong></td>
																  
																<td style=" font-size:12px;"><?php echo stripslashes($record->prov_num); ?></td>
															</tr>
														

													</table>
							</td>
						</tr>
				</table>
				<br/>

				 <table  style="margin:0 auto;width: 100%;border:1px solid" cellspacing="0" cellpadding="0">
					 <tr>
					 	<td valign="top" style="padding:5px;font-weight:bold;font-size:12px;width:40%;border:1px solid">Presenting Complaint</td>	
					 	<td style=" font-size:12px;padding-left:5px;padding-bottom:10px;border:1px solid"><?php echo stripslashes(@$record->symptoms); ?></td>
					 </tr>					 
					<tr>
						<td valign="top" style="padding:5px;font-size:12px;border:1px solid"><strong>Past Medical History</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->past_medical_history); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding:5px; font-size:12px;"><strong>Current Medications</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->current_medication); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding:5px;font-size:12px;border:1px solid"> <strong>Immunisations</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->immunisations); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding:5px;font-size:12px;border:1px solid"> <strong>Allergies</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->allergies); ?></td>
					</tr>
					<tr>
					<td valign="top" style="padding:5px;font-size:12px;border:1px solid"><strong>Social / Family History</strong></td>
						  
						<td style=" font-size:12px;border:1px solid"><?php echo stripslashes(@$record->social_history); ?></td>
					</tr>
					<tr>
					<td valign="top" style="padding:5px;font-size:12px;border:1px solid"><strong>Life Style Risk Factors</strong></td>
						  
						<td style=" font-size:12px;border:1px solid"><?php echo stripslashes(@$record->life_style_risk_factors); ?></td>
					</tr>
					<tr>
					<td valign="top" style="padding:5px;font-size:12px;border:1px solid"><strong>Observations</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->observation); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding-left:5px; font-size:12px;border:1px solid"> <strong>Diagnosis</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->diagnosis); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding-left:5px; font-size:12px;border:1px solid"> <strong>Diagnosis Reference Code</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->diagnosis_ref_code); ?></td>
					</tr>
					<tr>
					<td valign="top" style="padding:5px;font-size:12px;border:1px solid"> <strong>Plan</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->plan); ?></td>
					</tr>
					<tr>
						<td valign="top" style=" padding-left:5px;font-size:12px;border:1px solid"><strong>Medication administered during record</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->cunsult_medication); ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding-left:5px; font-size:12px;border:1px solid"> <strong>Medication Prescribed</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php 


						   					if(isset($record->medication_perscribed_sub) && $record->medication_perscribed_sub!=''):

						   						$med_perscribed = json_decode($record->medication_perscribed_sub);

						   						if(!empty($med_perscribed) && count($med_perscribed)):
						   							
						   							if(count($med_perscribed) > 1):
						   								
							   							foreach ($med_perscribed as $row) :

							   								foreach ($row as $key => $row_val) :
							   									
							   									echo "<strong>".ucfirst(@$key)." :</strong>&nbsp;".@$row->{$key}."<br/>";
							   								endforeach;

							   								echo "<br/>";
							   							endforeach;
							   						else:
							   							foreach ($med_perscribed as $key=>$row):
							   									echo "<strong>".ucfirst(@$key)." :</strong>&nbsp;".@$row->{$key}."<br/>";
							   							endforeach;
							   								echo "<br/>";
						   							endif;
						   						endif;
						   					endif;

							//echo stripslashes(@$record->medication_prescribed); 

						?></td>
					</tr>
					<tr>
						<td valign="top" style=" padding-left:5px;font-size:12px;border:1px solid"><strong>Followup with GP</strong></td>
						  
						<td style="padding:5px;font-size:12px;border:1px solid"><?php echo stripslashes(@$record->followup_gp); ?></td>
					</tr>
				</table>
			

		   
		</div>
	</div>

</body>
</html>

