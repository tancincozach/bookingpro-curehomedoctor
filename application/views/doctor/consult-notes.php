<h4 class="page-header">Doctor: Consult Notes</h4>

<?php echo form_open('', 'class="form-horizontal" role="form" method="post"'); ?>
	
	<input type="hidden" name="form_type" value="<?php echo $form_type; ?>">
	<input type="hidden" name="appt_id" value="<?php echo @$record->appt_id; ?>">
	<input type="hidden" name="consult_id" value="<?php echo @$consult->consult_id; ?>">	

	<div class="clearfix">
 
		    <div  class="row" >

		    	<div class="col-sm-4  col-md-4 col-lg-4">
		    		<h4>Patient Details</h4>

					<p><strong>Booking # : </strong><?php echo $record->ref_number_formatted; ?></p>
					<p><strong>First Name : </strong><?php echo $record->firstname; ?></p>
					<p><strong>Last Name : </strong><?php echo $record->lastname; ?></p>
					<p><strong>DOB : </strong><?php echo date('d/m/Y', strtotime($record->dob)); ?></p>
					<p><strong>Age : </strong><?php echo $record->patient_age; ?></p>
					<p><strong>Address : </strong><?php echo stripslashes($record->street_addr).' '.($record->suburb).' '.$record->postcode; ?></p>
					<p><strong>Aboriginal or Torres Straight Islander : </strong><?php echo ($record->abotsi)?'Yes':'No'; ?></p>					
					<p><strong>Cultural &#38; Ethnicity Background : </strong><?php echo stripslashes($record->cultural_ethnic_bg); ?></p>
					<!-- <p><strong>Present Complain : </strong><?php echo stripslashes($record->symptoms); ?></p> -->					
					<p><strong>Regular Practice : </strong><?php echo stripslashes($record->regular_practice); ?></p>
					<p><strong>Regular Doctor : </strong><?php echo stripslashes($record->regular_doctor); ?></p>
					<p><strong>Use Default Consult Note : </strong>
						<select class="form-control" name="consult_sel">
									<option>SELECT</option>
						<?php 
							if(isset($def_consult) && count($def_consult) > 0):
						
								foreach($def_consult as $row):?>
									<option value="<?php echo $row->id;?>" <?php echo isset($consult->prognosis_id) && (int)$consult->prognosis_id==(int)$row->id ? 'SELECTED':''?>><?php  echo @$row->prognosis;?></option>
						<?php
								endforeach;
							endif;

						?>
						</select>
					</p>	
				 
		    	</div>
		    	
		    	<div class="col-sm-4 col-md-4 col-lg-4">
		    		<h4>Contact Details</h4>
		    		<p><strong>Home Phone : </strong><?php echo $record->phone; ?></p>
		    		<p><strong>Mobile Phone : </strong><?php echo $record->mobile_no; ?></p>
		    		<p><strong>Email : </strong><?php echo $record->email; ?></p>

		    	</div>

		    	<div class="col-sm-4 col-md-4 col-lg-4">
		    		<h4>Attending Doctor : <?php echo $record->doctor_name; ?></h4>
		    	</div>		 		    	
		    	<div class="row"  style="clear:both;padding:0 14px 0 14px;">
		    		<div class="col-sm-4 col-md-4 col-lg-4"><p ><strong>Present Complain : </strong>
						<textarea class="form-control" name="symptoms_backup" style="display: none"><?php echo stripslashes(@$record->symptoms); ?></textarea>
						<textarea class="form-control" name="symptoms"><?php echo stripslashes(@$record->symptoms); ?></textarea>					
					</div>

					<div class="col-sm-4 col-md-4 col-lg-4">
					&nbsp;
					</div>
		    		<div class="col-sm-4 col-md-4 col-lg-4">
		    			<p>
		    				<strong>Item #: </strong>
							<input type="text" class="form-control" name="item_num"  value="<?php echo  stripslashes(@$consult->item_num); ?>">
						</p>
					</div>		    		
		    	</div>
		    	<div class="row" style="margin-left:1px">

					<div class="col-sm-4 col-md-4 col-lg-4"><p ><strong>CallCentre/ Triage Notes : </strong>						
						<textarea class="form-control" name="call_center_triage"><?php echo stripslashes(@$consult->call_center_triage); ?></textarea>					
					</div>
		    	</div>
			
		    	<div class="col-sm-12 col-md-12 col-lg-12 text-right" style="padding-bottom: 5px;">
		    		<div class="clearfix">

		    			<p class="pull-left text-info">
		    				* Multiple autocomplete are now enabled on the text field below, Just keyin any key a menu will show. 
		    				You can still type even the word is not available just separate with comma (,) so that you can select from the menu
		    			</p>

			    		<a class="btn btn-xs btn-warning pull-right" data-toggle="collapse" href="#collapseHistConsults" aria-expanded="false" aria-controls="collapseHistConsults" >
			    			<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> 
			    			Show Previous Consult Notes
		    			</a>	
		    		</div>

		    		<div class="collapse" id="collapseHistConsults">
		    			<br />
					  	<div style="">
						   	<table class="table table-bordered table-condensed table-striped" style="font-size: 12px;">
						   		<thead>
							   		<tr>
							   			<th class="text-center">Booking# Date</th>
							   			<th class="text-center">Doctor</th>
							   			<th class="text-center">Present Complain</th>
							   			<th class="text-center">Past Medical History</th>
							   			<th class="text-center">Current Medications</th>
							   			<th class="text-center">Immunisations</th>
							   			<th class="text-center">Allergies</th>
							   			<th class="text-center">Social / Family History / Smoker / Alcohol</th>
							   			<th class="text-center">Observations</th>
							   			<th class="text-center">Diagnosis</th>
							   			<th class="text-center">Plan</th>
							   			<th class="text-center">Medication administered during consult</th>
							   			<th class="text-center">Medication Prescribed</th>
							   			<th class="text-center">Follow up with GP</th>
							   			<th class="text-center"> </th>
							   		</tr>
						   		</thead>
						   		<tbody>
						   			<?php foreach($prev_record as $prec): 
						   				$tr_bg = '';
						   				if($record->appt_id == $prec->appt_id) $tr_bg = 'background-color: #FFFF99';
						   			?>
						   			<tr class="text-left" style="<?php echo $tr_bg; ?>">
						   				<td>
						   					<?php echo 'B#:'.$prec->ref_number_formatted; ?><br />
						   					<?php echo date('d/m/Y H:i:s', strtotime($prec->appt_start_queue)); ?>
						   					
						   				</td>
						   				 
						   				<td><?php echo $prec->doctor_name; ?></td>
						   				<td><?php echo stripslashes(@$prec->symptoms); ?></td>
						   				<td><?php echo stripslashes(@$prec->past_medical_history); ?></td>
						   				<td><?php echo stripslashes(@$prec->current_medication); ?></td>
						   				<td><?php echo stripslashes(@$prec->immunisations); ?></td>
						   				<td><?php echo stripslashes(@$prec->allergies); ?></td>
						   				<td><?php echo stripslashes(@$prec->social_history); ?></td>					   				
						   				<td><?php echo stripslashes(@$prec->observation); ?></td>
						   				<td><?php echo stripslashes(@$prec->diagnosis); ?></td>
						   				<td><?php echo stripslashes(@$prec->plan); ?></td>
						   				<td><?php echo stripslashes(@$prec->cunsult_medication); ?></td>
						   				<td><?php echo stripslashes(@$prec->medication_prescribed); ?></td>
						   				<td><?php echo stripslashes(@$prec->followup_gp); ?></td>
						   				<td>
						   					<?php if($record->appt_id != $prec->appt_id): ?>
						   					<a class="btn btn-xs btn-primary"  href="javascript:void(0)" onclick="Doctor.copy_consult_note(this)"  ><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Copy </a>
						   					<?php endif; ?>
						   				</td>
						   			</tr>
						   			<?php endforeach; ?>
						   		</tbody>
						   	</table>
					  	</div>
					</div>
		    	</div>

		    </div>
		    <div class="row clearfix" >
				
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<label for="past_medical_history" class="col-sm-3 col-md-3 col-lg-2 control-label">Past Medical History</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="past_medical_history" id="past_medical_history" required><?php echo stripslashes(@$consult->past_medical_history); ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="current_medication" class="col-sm-3 col-md-3 col-lg-2 control-label">Current Medications</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="current_medication"><?php echo stripslashes(@$consult->current_medication); ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="immunisations" class="col-sm-3 col-md-3 col-lg-2 control-label">Immunisations</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="immunisations"><?php echo stripslashes(@$consult->immunisations); ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="allergies" class="col-sm-3 col-md-3 col-lg-2 control-label">Allergies</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="allergies"><?php echo stripslashes(@$consult->allergies); ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="social_history" class="col-sm-3 col-md-3 col-lg-2 control-label">Social / Family History</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="social_history"><?php echo  stripslashes(@$consult->social_history); ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="social_history" class="col-sm-3 col-md-3 col-lg-2 control-label">Life Style Risk Factors</label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<textarea class="form-control consult_dropdowns" name="life_style_risk_factors"><?php echo  stripslashes(@$consult->life_style_risk_factors); ?></textarea>
						</div>
					</div>
					 <div class="form-group">
						<label for="observation" class="col-sm-5 col-md-4 col-lg-2 control-label">Observations</label>
						<div class="col-sm-7 col-md-8 col-lg-10">							 
							<textarea class="form-control consult_dropdowns" name="observation"><?php echo  stripslashes(@$consult->observation); ?></textarea>
						</div>
					</div>

				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
				
					<div class="form-group">
						<label for="diagnosis" class="col-sm-5 col-md-4 col-lg-3 control-label">Diagnosis</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
							<textarea class="form-control consult_dropdowns" name="diagnosis"><?php echo stripslashes(@$consult->diagnosis); ?></textarea>							 
						</div>
					</div>
				
					<div class="form-group">
						<label for="diagnosis_ref_code" class="col-sm-5 col-md-4 col-lg-3 control-label">Diagnosis Reference Code</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
						<select class="form-control" name="diagnosis_ref_code">
											<option>SELECT</option>
								<?php 
									if(isset($dp_diagnosis_ref_code) && count($dp_diagnosis_ref_code) > 0):
								
										foreach($dp_diagnosis_ref_code as $row):?>
											<option value="<?php echo $row->dropdown_value;?>" <?php echo isset($consult->diagnosis_ref_code) && $row->dropdown_value==$consult->diagnosis_ref_code ? 'SELECTED':''?>><?php  echo @$row->dropdown_value;?></option>
								<?php
										endforeach;
									endif;

								?>
						</select>
						</div>
					</div>

					<div class="form-group">
						<label for="plan" class="col-sm-5 col-md-4 col-lg-3 control-label">Plan</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
							<textarea class="form-control consult_dropdowns" name="plan"><?php echo stripslashes(@$consult->plan); ?></textarea>	
						</div>
					</div>

					<div class="form-group">
						<label for="cunsult_medication" class="col-sm-5 col-md-4 col-lg-3 control-label">Medication administered</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
							<textarea class="form-control consult_dropdowns" name="cunsult_medication" required="required"><?php echo stripslashes(@$consult->cunsult_medication); ?></textarea>								
						</div>
					</div>

					<div class="form-group" style="display:none">
						<label for="medication_prescribed" class="col-sm-5 col-md-4 col-lg-3 control-label">Medication Prescribed</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
							<textarea class="form-control consult_dropdowns" name="medication_prescribed"><?php echo stripslashes(@$consult->medication_prescribed); ?></textarea>	
						</div>
					</div>

					<div class="form-group">
						<label for="medication_prescribed" class="col-sm-5 col-md-4 col-lg-3 control-label">Medication Prescribed</label>
						<div class="col-sm-7 col-md-8 col-lg-9 ">
								<?php 
									 $medication_perscribed = json_decode(@$consult->medication_perscribed_sub);
								 ?>

								 <a class="btn btn-success add-med" style="margin-bottom:10px;" title="Add" href="#"><span class="glyphicon glyphicon-plus"></span></a>
								 <div id="med-perc-container">
								 <?php
								 	if(isset($medication_perscribed) && !empty($medication_perscribed) && is_array($medication_perscribed)):
								 	 	foreach( $medication_perscribed as $key=> $row):?>
										<table class="table table-condensed med-prec-frm" style="width:100%">
											<tr>
												<td><label>Name</label></td>
												<td><input type="text" name="medication_perscribed_sub[<?php echo $key?>][name]" class="form-control input-sm" value="<?php echo @$row->name;?>" ></td>
												<td><label>Strength</label></td>
												<td><input type="text" name="medication_perscribed_sub[<?php echo $key?>][strength]" class="form-control input-sm" value="<?php echo @$row->strength;?>" ></td>
											</tr>
											<tr>
												<td><label>Frequency</label></td>
												<td><input type="text" name="medication_perscribed_sub[<?php echo $key?>][frequency]" class="form-control input-sm" value="<?php echo @$row->frequency;?>" ></td>
												<td><label>Quantity</label></td>
												<td><input type="text" name="medication_perscribed_sub[<?php echo $key?>][quantity]" class="form-control input-sm" value="<?php echo @$row->quantity;?>"></td>
											</tr>			
											<tr>
												<td><label>Repeats</label></td>
												<td><input type="text" name="medication_perscribed_sub[<?php echo $key?>][repeats]" class="form-control input-sm" value="<?php echo @$row->repeats;?>"></td>
												<td><a class="btn btn-danger remove-med" style="margin-bottom:10px;"  href="#" title="remove"><span class="glyphicon glyphicon-remove "></span></a></td>
											</tr>
										</table>
 
								 <?php 
								 		endforeach;
							 		else:?>
								 		<table class="table table-condensed med-prec-frm" style="width:100%">
											<tr>
												<td><label>Name</label></td>
												<td><input type="text" name="medication_perscribed_sub[0][name]" class="form-control input-sm" value="" ></td>
												<td><label>Strength</label></td>
												<td><input type="text" name="medication_perscribed_sub[0][strength]" class="form-control input-sm" value=""></td>
											</tr>
											<tr>
												<td><label>Frequency</label></td>
												<td><input type="text" name="medication_perscribed_sub[0][frequency]" class="form-control input-sm" value=""></td>
												<td><label>Quantity</label></td>
												<td><input type="text" name="medication_perscribed_sub[0][quantity]" class="form-control input-sm" value="" ></td>
											</tr>			
											<tr>
												<td><label>Repeats</label></td>
												<td><input type="text" name="medication_perscribed_sub[0][repeats]" class="form-control input-sm" value="" ></td>
											</tr>
										</table>
								 		<?php

								 	endif;
								 		?>
								 	
								 </div>

						</div>
					</div>

					<div class="form-group">
						<label for="followup_gp" class="col-sm-5 col-md-4 col-lg-3 control-label">Followup with GP</label>
						<div class="col-sm-7 col-md-8 col-lg-9">
							<textarea class="form-control consult_dropdowns" name="followup_gp"><?php echo stripslashes(@$consult->followup_gp); ?></textarea>	
						</div>
					</div>
				</div>
				
				<div class="clearfix"><br /></div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="med_cert_given" class="col-sm-4 control-label">Medical Certificate Given</label>
						<div class="col-sm-5">
							<select name="med_cert_given" required="">
								<option value="Yes" <?php echo (@$consult->med_cert_given=='Yes')?'selected="selected"':''; ?>>Yes</option>
								<option value="No" <?php echo (@$consult->med_cert_given=='No' ||  @$consult->med_cert_given=='')?'selected="selected"':''; ?>>No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="ref_form_given" class="col-sm-4 control-label">Referral Form Given</label>
						<div class="col-sm-5">
							<select name="ref_form_given" required="">
								<option value="">Select</option>
								<option value="Yes" <?php echo (@$consult->ref_form_given=='Yes')?'selected="selected"':''; ?>>Yes</option>
								<option value="No" <?php echo (@$consult->ref_form_given=='No' || @$consult->ref_form_given=='')?'selected="selected"':''; ?>>No</option>
							</select>
						</div>
					</div>


				</div>
		    </div>

		    <div class="row  clearfix">
				<div class="clearfix text-center" style="padding-top: 20px; ">
					<div class="col-sm-6 col-md-6 col-lg-16 text-right">
						<button class="btn btn-primary" type="submit">Submit and Save Consult Notes</button>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-16 text-right">
						 
					</div>
				</div>
		    </div>
	</div>
</form>

<div class="row  clearfix">
	<div class="clearfix text-center" >
		<div class="col-sm-8 col-md-8 col-lg-8 text-right">
			<p class="text-success"><strong><?php echo @$_GET['fmesg']; ?></strong></p>
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4 text-right">
			<?php echo form_open_multipart('doctor/upload_consult_file', 'class="form-horizontal" id="upload_consult_form" role="form" method="post"'); ?>
				
				<input type="hidden" name="appt_id" value="<?php echo @$record->appt_id; ?>">
				<input type="hidden" name="consult_id" value="<?php echo @$consult->consult_id; ?>">	

				<div class="form-group pull-right">						    
				    <input type="file" name="consult_file" id="consult_file" class="btn btn-success btn-xs">
				    <p class="help-block">Upload files here</p>
			  	</div>
		  	</form>
		</div>
	</div>
</div>

		    <hr />

		    <div  class="row clearfix">
		    	<div class="col-sm-12 col-md-12 col-lg-12 text-right" style="padding-bottom: 5px;">
		    		<a class="btn btn-xs btn-info" data-toggle="collapse" href="#collapseChanges" aria-expanded="true" aria-controls="collapseChanges" >
		    			<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> 
		    			Show/Hide Changes Trail
		    		</a>
		    		<div id="collapseChanges">
		    			<br />
					  	<div style="">
					  		<?php if( isset($consult->consult_id)): ?>
					  		<p>
					  			Here will be shown what are the changes has been made, if some fields are empty it means that fields are not updated.
					  		</p>
						   	<table class="table table-bordered table-condensed table-striped" style="font-size: 12px;">
						   		<thead>
							   		<tr>
							   			<th class="text-center">Date/Time</th>							   			
							   			<th class="text-center">Present Complain</th>
							   			<th class="text-center">Past Medical History</th>
							   			<th class="text-center">Current Medications</th>
							   			<th class="text-center">Immunisations</th>
							   			<th class="text-center">Allergies</th>
							   			<th class="text-center">Social / Family History / Smoker / Alcohol</th>
							   			<th class="text-center">Observations</th>
							   			<th class="text-center">Diagnosis</th>
							   			<th class="text-center">Diagnosis <br/> Reference <br/> Code</th>
							   			<th class="text-center">Plan</th>
							   			<th class="text-center">Medication administered during consult</th>
							   			<th class="text-center">Medication Prescribed</th>
							   			<th class="text-center">Follow up with GP</th>
							   			<th class="text-center">File</th>
							   			<th class="text-center"></th>
							   			<th class="text-center"></th>
							   		</tr>
						   		</thead>
						   		<tbody>
						   			<?php foreach($table_audit_trail as $trail): 
						   				$prec = json_decode($trail->message);
						   			?>
						   			<tr class="text-left">
						   				<td> <?php echo date('d/m/Y H:i:s', strtotime($trail->created)); ?> </td>
						   				<td><?php echo stripslashes(@$prec->symptoms); ?></td>
						   				<td><?php echo stripslashes(@$prec->past_medical_history); ?></td>
						   				<td><?php echo stripslashes(@$prec->current_medication); ?></td>
						   				<td><?php echo stripslashes(@$prec->immunisations); ?></td>
						   				<td><?php echo stripslashes(@$prec->allergies); ?></td>
						   				<td><?php echo stripslashes(@$prec->social_history); ?></td>					   				
						   				<td><?php echo stripslashes(@$prec->observation); ?></td>
						   				<td><?php echo stripslashes(@$prec->diagnosis); ?></td>
						   				<td><?php echo stripslashes(@$prec->diagnosis_ref_code); ?></td>
						   				<td><?php echo stripslashes(@$prec->plan); ?></td>
						   				<td><?php echo stripslashes(@$prec->cunsult_medication); ?></td>
						   				<td><?php 
						   					if(isset($prec->medication_perscribed_sub) && $prec->medication_perscribed_sub!=''):

						   						$med_perscribed = json_decode($prec->medication_perscribed_sub);
						   						
						   				

							   						if(!empty($med_perscribed) && count($med_perscribed)):

							   								if(is_array($med_perscribed)):
										   							foreach ($med_perscribed as $row) :

									   									$table='<table class="table table-condensed" style="width:100%">';

										   								foreach ($row as $key => $row_val) :
										   									$table.='<tr><td><label>'.ucfirst(@$key).'</label></td><td>'.@$row->{$key}.'</td></tr>';											   																   									
										   								endforeach;
									   									$table.='</table>';	

										   							 	echo $table;
										   							endforeach;

									   						endif;
									   				endif;
						   					
						   					endif;

						   				 ?></td>
						   				<td><?php echo stripslashes(@$prec->followup_gp); ?></td>
						   				<td><?php 
						   					//echo @$prec->file_path; 
						   				 	if( isset($prec->file_path) && file_exists($prec->file_path) ){

						   				 		?>
						   				 		<a target="_blank" href="<?php echo $prec->file_path?>">
						   				 		<?php echo  (isset($prec->file_name)?$prec->file_name:$prec->file_path);?></a>&nbsp;&nbsp;
						   				 		<a  onclick = "if (! confirm('Are you sure you want to remove this note ?')) { return false; }"  href="doctor/delete_consult/?appt_id=<?php echo $record->appt_id?>&consult_id=<?php echo $consult->consult_id;?>&file=<?php echo $prec->file_path;?>"/>remove</a>
						   				 		<?php 
						   				 		echo '';
						   				 	}
						   				 	else{


						   				 		 		echo '<span style="text-decoration: Line-Through;color:red">'.(isset($prec->file_name)?@$prec->file_name:@$prec->file_path).'</span>';
						   				 	}
						   				?></td>
						   				<td><?php echo $trail->agent_name; ?></td>
						   				<td><?php echo $trail->table_action; ?></td>
						   			</tr>
						   			<?php endforeach; ?>
						   		</tbody>
						   	</table>
						   <?php endif; ?>
					  	</div>
					</div>
		    	</div>
		    </div>


	





