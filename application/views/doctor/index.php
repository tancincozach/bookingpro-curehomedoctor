
<?php

    $date = ($appt_start==date('Y-m-d'))? date('d/m/Y'):date('d/m/Y',strtotime($appt_start));
?>
<h4 class="page-header">Doctor</h4>

<div class="row text-center">

    <h4>
        <?php if(!isset($_GET['search'])):?>
        Bookings of <?php echo ($appt_start==date('Y-m-d'))?'Today ('.date('d/m/Y').')':date('d/m/Y',strtotime($appt_start)); ?>
    <?php else:?>
        Booking Search Result
    <?php endif;?>
         
     </h4>

</div> 
<div class="clearfix">&nbsp;</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_doctore_search_form" method="get"'); ?>
            <div class="form-group">
                <label for="search" class="">Search</label>
                <input type="text" class="form-control input-sm"  name="search" placeholder="Search appt #, firstname, lastname" value="<?php echo @$_GET['search']; ?>" title="Search: appt #, firstname, lastname, phone, mobile">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm">Search</button>
                <a href="doctor" type="button" class="btn btn-warning btn-sm">Reset</a>
                
            </div>
        <?php echo form_close();?>
    </div>
</div>

<Di
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left"><a class="btn btn-info btn-sm" href="doctor/?appt_start=<?php echo date('Y-m-d', strtotime($appt_start.' -1 day')); ?>" role="button"> Prev Day</a></div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
            <a class="btn btn-primary btn-sm" href="doctor" role="button"> Today </a>        
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right"><a class="btn btn-info btn-sm" href="doctor/?appt_start=<?php echo date('Y-m-d', strtotime($appt_start.' +1 day')); ?>" role="button"> Next Day</a>
      
        </div>

    </div>  
    <br class="clearfix" />
    <br />

</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
        <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
        
        <div class="table-responsive">
        <table class="table table-bordered table-condensed table-appointment">
            <thead>
                <tr>
                    <th style="width: 105px">Appt #</th>                     
                    <th>Name/Address</th>
                    <th>Phone</th>
                    <th>Age</th>
                    <th>Symptoms</th>
                    <th>Status</th>
                    <th>Date Booked</th>
                    <th>Time Booked</th>                    
                    <th>Start Appt</th>
                    <th>End Appt</th>
                    <th>Booked By</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($results as $row): 
 
                    $tr_bg_cls = 'VISIT_CLOSED';

                ?>
                <tr class="<?php echo $tr_bg_cls; ?>" >
                    <td><?php echo $row->ref_number_formatted; ?></td>
                    <td><?php echo $row->firstname.' '.$row->lastname.'<br />'.$row->street_addr.' '.$row->suburb.' '.@$row->area_name;  ?></td>
                    <td><?php echo $row->phone; ?></td>
                    <td><?php echo $row->patient_age; ?></td>
                    <td><?php echo $row->symptoms; ?></td>
                    <td><strong><?php echo $this->booking_statuses[$row->book_status]; ?></strong></td>
                    <td><?php echo date('d/m/Y', strtotime($row->appt_created))?></td>
                    <td><?php echo date('H:i', strtotime($row->appt_created))?></td>
                    <td><?php echo ($row->appt_start_queue!='')?date('H:i', strtotime($row->appt_start_queue)):''; ?></td>
                    <td><?php echo ($row->appt_end_queue!='')?date('H:i', strtotime($row->appt_end_queue)):''; ?></td>
                    <td><?php echo @$row->chaperone_name.'/'.@$row->doctor_name.'/'.@$row->car_name; ?></td>
                    <td>
                    <?php                         
                        if( $row->consult_id == '' ): ?>
                            <a class="btn btn-info btn-xs" href="doctor/consult_notes/<?php echo $row->appt_id; ?>" role="button">Create consult notes</a>
                    <?php else: ?>
                            <a class="btn btn-info btn-xs" href="doctor/consult_notes/<?php echo $row->appt_id; ?>" role="button">Edit Consult Notes</a>
                    <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?> 
            </tbody>
        </table>
        </div>
        
        <div class="clearfix">
            <div class="pull-left text-left">
                 <?php echo $showing; ?>
            </div>
            <div class="pull-right text-right">
                <ul class="pagination pagination-sm">
                    <?php echo $links; ?>
                </ul>
            </div> 
        </div>
	</div>
</div>