<style type="text/css">
	.reporttable.table{
		width: auto;
	}
</style>

<h4 class="page-header">Where Did You Hear About Us Report</h4>

<!-- search -->
<div>
	<?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
		<div class="form-group">
			<label for="appt_start">Date</label>
			<input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>">
		</div>
		<button type="submit" name="search" class="btn btn-primary">Filter</button>
		<button type="submit" name="download" class="btn btn-warning">Download</button> 
	</form>
</div>
<br />

<!-- list --> 

<p>DATE: <?php echo $appt_start; ?></p>
<table class="reporttable table table-bordered">
	<tbody>	
	<?php 
		
		$header = array();

		foreach($record as $key=>$row){

			if( $key == '0' ){

				$header = $row;

				$tr = '<tr>';
				$tr .= '<td>&nbsp;</td>';
				foreach($header as $head){
					$tr .= '<td><strong>'.$head.'</strong></td>';
				}
 				$tr .= '</tr>';
 				echo $tr;

			}else{

				$tr = '<tr>';
				$tr .= '<td><strong>'.$key.'</strong></td>';
				foreach($header as $head){
					$tr .= '<td align="center">'.$row[$head].'</td>';
				}
 				$tr .= '</tr>';
 				echo $tr;

			}
		}
 
 	?>
 	</tbody>
</table>
 