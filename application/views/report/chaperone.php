<h4 class="page-header">Appointment Report</h4>

<!-- search -->
<div>
	<?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
		<div class="form-group">
			<label for="appt_start">Date</label>
			<input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>">
		</div>
		<button type="submit" name="search" class="btn btn-primary">Filter</button>
		<button type="submit" name="download" class="btn btn-warning">Download</button> 
	</form>
</div>
<br />

<!-- list --> 
<p>DATE: <?php echo $appt_start; ?></p>
<div class="table-responsive">
<table class="table table-bordered">
	<thead>
		<tr>				
			<th>Appt ID</th>
			<th>Area Name</th>
			<th>Date Booking Made</th>
			<th>Time Booking Made</th>
			<th>Consult Start</th>
			<th>Consult End</th>
			<th>Time in Queue</th>
			<th>Chaperone</th>
			<th>Doctor</th>
			<th>Car #</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Date of Birth</th>
			<th>Age</th>
			<th>Sex</th>
			<th>Landline Phone</th>
			<th>Mobile Phone</th>
			<th>Email</th>
			<th>Address</th>
			<th>Suburb</th>
			<th>Postcode</th>
			<th>Medicare Num</th>
			<th>Medicare Ref</th>
			<th>Medicare Exp</th>
			<th>DVA Gold Card</th>
			<th>Private Health Insurance Provider</th>
			<th>Private Health Fund Number</th>
			<th>Symptoms</th>
			<th>Where Heard</th>
			<th>Regular Practice</th>
			<th>Regular Doctor</th>
			<th>Aboriginal / Torres Strait Island</th>
			<th>Cultural &#38; Ethnicity Background</th>
			<th>Agent</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($record as $row): ?>
		<tr>
			<td><?php echo $row->ref_number_formatted; ?></td>
			<td><?php echo $row->area_name; ?></td>
			<td><?php echo date('d/m/Y', strtotime($row->appt_created_tz) ); ?></td>
			<td><?php echo date('H:i', strtotime($row->appt_created_tz) ); ?></td>			
			<td><?php echo ($row->consult_start_tz != '')?date('d/m/Y H:i', strtotime($row->consult_start_tz)):''; ?></td>
			<td><?php echo ($row->consult_end_tz != '')?date('d/m/Y H:i', strtotime($row->consult_end_tz)):''; ?></td>
			<td><?php echo $row->in_queue; ?></td>
			<td><?php echo $row->chaperone_name; ?></td>
			<td><?php echo $row->doctor_name; ?></td>
			<td><?php echo $row->car_name; ?></td>
			<td><?php echo $row->firstname; ?></td>
			<td><?php echo $row->lastname; ?></td>
			<td><?php echo ($row->dob != '0000-00-00')?date('d/m/Y', strtotime($row->dob)):''; ?></td>
			<td><?php echo $row->patient_age; ?></td>
			<td><?php echo $row->gender; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td><?php echo $row->mobile_no; ?></td>
			<td><?php echo $row->email; ?></td>
			<td><?php echo stripslashes($row->street_addr); ?></td>
			<td><?php echo stripslashes($row->suburb); ?></td>
			<td><?php echo $row->postcode; ?></td>
			<td><?php echo $row->medicare_number; ?></td>
			<td><?php echo $row->medicare_ref; ?></td>
			<td><?php echo $row->medicare_expiry; ?></td>
			<td><?php echo $row->dva?'Yes':'No'; ?></td>
			<td><?php echo $row->health_insurance_provider; ?></td>
			<td><?php echo $row->health_fund_number; ?></td>
			<td><?php echo stripslashes($row->latest_symptoms); ?></td>
			<td><?php echo $row->latest_wdyhau; ?></td>
			<td><?php echo $row->regular_practice; ?></td>
			<td><?php echo $row->regular_doctor; ?></td>
			<td><?php echo ($row->abotsi)?'Yes':'No'; ?></td>
			<td><?php echo stripslashes($row->cultural_ethnic_bg); ?></td>
			<td><?php echo $row->agent_name; ?></td>
			 
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>