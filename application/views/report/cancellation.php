<h4 class="page-header">Cancellation Report</h4>

<!-- search -->
<div>
	<?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
		<div class="form-group">
			<label for="appt_start">Date</label>
			<input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>">
		</div>
		<button type="submit" name="search" class="btn btn-primary">Filter</button>
		<button type="submit" name="download" class="btn btn-warning">Download</button> 
	</form>
</div>
<br />

<!-- list --> 
<p>DATE: <?php echo $appt_start; ?></p>
<table class="table table-bordered">
	<thead>
		<tr>				
			<th>Appt ID</th>
			<th>Date Booking Made</th>
			<th>Time Booking Made</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Landline Phone</th>
			<th>Mobile Phone</th>
			<th>Address</th>
			<th>Suburb</th>
			<th>Postcode</th> 
			<th>Cancel Date/Time</th>
			<th>Reason Cancelled</th>
			<th>Cancelled By</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($record as $row): ?>
		<tr>
			<td><?php echo $row->ref_number_formatted; ?></td>
			<td><?php echo date('d/m/Y', strtotime($row->appt_created) ); ?></td>
			<td><?php echo date('H:i', strtotime($row->appt_created) ); ?></td>
			<td><?php echo $row->firstname; ?></td>
			<td><?php echo $row->lastname; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td><?php echo $row->mobile_no; ?></td>
			<td><?php echo $row->street_addr; ?></td>
			<td><?php echo $row->suburb; ?></td>
			<td><?php echo $row->postcode; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($row->appt_end_queue) ); ?></td>
			<td><?php echo stripslashes($row->cancelled_reason); ?></td>
			<td><?php echo $row->cancelled_by; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
 