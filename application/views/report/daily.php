<h4 class="page-header">Daily Report</h4>

<!-- search -->
<div>
	<?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
		<div class="form-group">
			<label for="appt_start">Date</label>
			<input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>">
		</div>
		<button type="submit" name="search" class="btn btn-primary">Filter</button>
		<button type="submit" name="download" class="btn btn-warning">Download</button> 
	</form>
</div>
<br />

<!-- list -->

<table class="table table-bordered">
	<thead>
		<tr>				
			<th>Date</th>
			<th>Area</th>
			<th>Patients Booked</th>

			<th>Completed</th>
			<th>Open</th>
			<th>Visit</th>

			<th>Cancelled</th>
			<th>Unseen</th>
			<th>Actual Bookings</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$prev_date = '';

			$total = array(
				'all'=>0,
				'complete'=>0,
				'open'=>0,
				'visit'=>0,
				'unseen'=>0,
				'cancel'=>0
			);

			foreach($record as $row):
				$d_date = '';
				if( $prev_date != $row->appt_created_tz ){
					$prev_date = $row->appt_created_tz;
					$d_date = $row->appt_created_tz;
				}

				//$all_booking += $row->all_booking;
				//$cancelled_count += $row->cancelled_count;

				$total['all'] += $row->all_booking; 
				$total['complete'] += $row->book_completed;
				$total['open'] += $row->book_open;
				$total['visit'] += $row->book_visit;
				$total['unseen'] += $row->unseen;
				$total['cancel'] += $row->cancelled_count;

		?>	
		<tr>
			<td><?php echo $d_date; ?></td>
			<td><?php echo $row->area_name; ?></td>
			<td align="center"><?php echo $row->all_booking; ?></td>

			<td align="center"><?php echo $row->book_completed; ?></td>
			<td align="center"><?php echo $row->book_open; ?></td>
			<td align="center"><?php echo $row->book_visit; ?></td>

			<td align="center"><?php echo $row->cancelled_count; ?></td>
			<td align="center"><?php echo $row->unseen; ?></td>
			<td align="center"><?php echo ($row->all_booking-$row->unseen-$row->cancelled_count); ?></td>
		</tr>
		<?php endforeach; ?>

		<tr >
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['all']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['complete']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['open']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['visit']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['cancel']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['unseen']; ?></td>
			<td style="font-weight: bold; color: red" align="center"><?php echo $total['all']-$total['unseen']-$total['cancel']; ?></td>
		</tr>
	</tbody>
</table>
 