<h4 class="page-header">ACSS Input</h4>

<!-- search -->
<div>
	<?php echo form_open('', 'class="form-inline" id="booking_search_form" name="booking_search_form" method="get"'); ?>
		<div class="form-group">
			<label for="appt_start">Date</label>
			<input type="text" class="form-control rangeDatePicker" style="height: 27px;" name="appt_start" value="<?php echo @$appt_start; ?>">
		</div>
		<button type="submit" name="search" class="btn btn-primary">Filter</button> 
		<button type="submit" name="download" class="btn btn-warning">Download</button> 
	</form>
</div>
<br />

<!-- list --> 
<p>DATE: <?php echo $appt_start; ?></p>
<div class="table-responsive">
<table class="table table-bordered">
	<thead>
		<tr>
			<th>UR No</th>
			<th>Title</th>
			<th>Surname</th>
			<th>FirstName</th>
			<th>Address</th>
			<th>Suburb</th>
			<th>Postcode</th>
			<th>DateofBirth</th>
			<th>MC No</th>
			<th>MC Ref</th>
			<th>Pens No</th>
			<th>VA No</th>
			<th>PhoneH</th>
			<th>PhoneW</th>
			<th>PhoneM</th>
			<th>Sex</th>
			<th>State</th>
			<th>Medicare Expiry</th>
			<!--<th></th>
			<th>Pat Type</th>
			<th>Pat Family</th>
			<th></th>
			<th>Site ID</th>
			<th>Req Title</th>
			<th>Req Name</th>
			<th>Req Surname</th>
			<th>Req City</th>
			<th>Req Provider</th>
			<th>Req Date</th>
			<th>Req Period</th>
			<th>HF Number</th>
			<th>HF Name</th>
			<th></th>
			<th>Email</th>
			<th>Notes</th>
			<th>Mobile</th>
			<th>NOK LastN</th>
			<th>NOK FirstN</th>
			<th>NOKRelat</th>
			<th>NOKPhone</th>
			<th>MI</th>
			<th>VAType</th>
			<th>Indigenous</th>
			<th>Ref Pract</th>
			<th>Ref Add</th>
			<th>Ref Add1</th>
			<th>Decrease</th>
			<th>HFAFirstN</th>
			<th>HFALastN</th>
			<th>RefPhone</th>
			<th>RefFax</th>-->
		</tr>
	</thead>
	<tbody>
		<?php foreach($record as $row): ?>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo $row->lastname; ?></td>
			<td><?php echo $row->firstname; ?></td>
			<td><?php echo stripslashes($row->street_addr); ?></td>
			<td><?php echo stripslashes($row->suburb); ?></td>
			<td><?php echo $row->postcode; ?></td>
			<td><?php echo ($row->dob != '0000-00-00')?date('d/m/Y', strtotime($row->dob)):''; ?></td>
			<td><?php echo $row->medicare_number; ?></td>
			<td><?php echo $row->medicare_ref; ?></td>
			<td></td>			
			<td><?php echo $row->dva?'Yes':'No'; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td></td>
			<td><?php echo $row->mobile_no; ?></td>
			<td><?php echo ($row->gender=='M')?'Male':'Female'; ?></td>
			<td><?php echo $this->Commonmodel->getState($row->postcode); ?></td>
			<td><?php echo $row->medicare_expiry; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>