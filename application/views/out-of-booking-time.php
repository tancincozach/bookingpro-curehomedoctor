
<div class="jumbotron clearfix text-center" >
    <div class="col-lg-6 col-sm-8 center-block float-none">
    	<h4>Advice Caller: </h4>

    	<br />
                
        <?php echo form_open('dashboard/custom_submit', 'class="" id="dashboard_form" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()" '); ?>
            
             
            <input type="hidden" name="custom_submit_type" value="booking-out-call">
            <input type="hidden" name="tran_type" value="Out of booking time">

            <?php if( $msg =='Mon-Frid 7am-8am' ): ?>
            
                <h4>I am sorry but we will not be able to see you before 8am, 
                therefore, as per the Dept of Health we can only see you "after hours".  
                You will need to visit your regular GP or you can call us back after 4pm today 
                and we will be able to book you in with our evening doctor who starts at 6pm.</h4>

                <input type="hidden" name="advice_caller" value="7am-8am Booking - Cannot be seen before 8am - Advised to see Regular GP">                
            
            <?php elseif( $msg =='Mon-Frid 8am-4pm' ): ?>
            
                <!-- <h4>We cannot take bookings until after 4pm, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->
                    
                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5">
                    
                    <strong>Booking Times</strong> <br /> <br /> 
                    <!-- Mon - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>                


                <input type="hidden" name="advice_caller" value="Advice caller to call back after  4pm">
            
            <?php elseif( $msg =='Saturday 8am-10am' ): ?>

                <!-- <h4>We cannot take bookings until after 10am, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->

                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5"> 
                     
                    <strong>Booking Times</strong> <br /><br />
                    <!-- Mon - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>    


                <input type="hidden" name="advice_caller" value="Advice caller to call back after  10am">
            
            <?php elseif( $msg =='Sunday night - Thursday night -  NO BOOKINGS between midnight & 4pm' ): ?>

                <!-- <h4>We cannot take bookings until after 10am, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->

                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5"> 
                     
                    <!-- <strong>Booking Times</strong> <br /><br />
                    Mon - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->
                     
                    <strong>Booking Times</strong> <br /><br />
                    <!-- Mon - Frid after 4pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>    


                <input type="hidden" name="advice_caller" value="Advice caller: Sunday night - Thursday night -  NO BOOKINGS between midnight & 4pm">

            <?php elseif( $msg =='Sunday night - Thursday night -  NO BOOKINGS between midnight & 6pm' ): ?>

                <!-- <h4>We cannot take bookings until after 10am, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->

                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5"> 
                     
                    <!-- <strong>Booking Times</strong> <br /><br />
                    Mon - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->
                     
                    <strong>Booking Times</strong> <br /><br />
                    <!-- Mon - Frid after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>    


                <input type="hidden" name="advice_caller" value="Advice caller: Sunday night - Thursday night -  NO BOOKINGS between midnight & 6pm">
            
            <?php //elseif( $msg =='Friday night - NO BOOKINGS between midnight & 10am Sat' ): ?>
            <?php elseif( $msg =='Friday night - NO BOOKINGS between midnight & 12pm Sat' ): ?>

                <!-- <h4>We cannot take bookings until after 10am, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->

                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5"> 
                     
                    <strong>Booking Times</strong> <br /><br />
                    <!-- Mon - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>    


                <input type="hidden" name="advice_caller" value="Advice caller: Friday night - NO BOOKINGS between midnight & 12pm Sat">
            
            <?php elseif( $msg =='Saturday night - NO BOOKINGS between midnight & 6am Sun' ): ?>

                <!-- <h4>We cannot take bookings until after 10am, 
                please call us back then if needed and we will be happy to make a booking and have a doctor out to see you</h4> -->

                <h4>
                    We cannot take bookings at the moment, please call us back in booking times and we
                    will be happy to make a booking and have a doctor come and see you?                    
                </h4>

                <div class="bg-info pad_5"> 
                     
                    <strong>Booking Times</strong> <br /><br />
                    M<!-- on - Frid after 4pm <br />
                    Saturday after 10am <br />
                    Sunday & Pub Hols - after 6am <br /> -->

                    Mon - Frid - METRO - after 4pm <br /> 
                    Mon - Frid - REGIONAL - after 6pm <br />
                    Saturday after 12pm <br />
                    Sunday & Pub Hols - after 6am<br />

                </div>    


                <input type="hidden" name="advice_caller" value="Advice caller: Saturday night - NO BOOKINGS between midnight & 6am Sun">

            <?php endif; ?>

            <?php
                foreach($collection as $field=>$value): 

                    if( $field == 'Suburb' ){
                        foreach( $value as $suburb_field=>$suburb_val ){
                            echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                        }
                    }else
                        echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
            
                endforeach; 
            ?>
            
            <br />
            <button class="btn btn-primary" type="submit" name="btn-service">Submit</button>

            <?php if( $_SERVER['SERVER_NAME'] == 'bookingpro-curehomedoctor-test.welldone.net.au' ): ?>
                <br />
                <br />
                <br />
                <br />
                <?php $uri = http_build_query($_GET);  ?>
                <a style="" class="btn btn-warning" href="dashboard/?<?php echo $uri; ?>&schedule_bypass=1">Continue on flow (for training purposes only)</a>
            <?php endif; ?>
        </form>
      
    </div>
</div>