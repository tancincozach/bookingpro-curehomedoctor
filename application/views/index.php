<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo BRAND_NAME; ?></title>

<base href="<?php echo base_url(); ?>" />

<!-- jQuery UI -->
<link href="assets/css/redmond/jquery-ui-1.10.4.custom.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->

<?php echo @$css_file; ?>

<script type="text/javascript">
    
    var hcd_globals = {        
        agent_name:"<?php echo $this->agent_name;?>",
        user_id:"<?php echo $this->user_id;?>",
        cookie_name:"<?php echo $this->config->item('sess_cookie_name'); ?>",
        n:'<?php echo $this->security->get_csrf_token_name(); ?>',
        h:'<?php echo $this->security->get_csrf_hash(); ?>',
        base_url: '<?php echo base_url(); ?>',
        timezone: '<?php echo $this->default_timezone; ?>'
    }

</script>

</head>
<body>

<div class="<?php echo @$container; ?>">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-md-3 col-lg-3">
                    <img alt="Cure Logo" class="img-responsive" src="<?php echo BRAND_CLIENT_LOGO_PATH; ?>" style="height: 51px" />
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <div class="row"  >
                        <div class="col-md-12 top-schedule">                            
                            <ol class="breadcrumb">
                                <!-- <li>Mon-Fri between 4pm-8am</li>
                                <li>Saturday FROM 10am</li>
                                <li>Sun & Pub Holiday - All Day</li> -->
                                
                                <li>Mon-Fri between 6pm-8am</li>
                                <li>Saturday FROM 12pm</li>
                                <li>Sun & Pub Holiday - All Day</li>
                            </ol>
                        </div>
                        
                        <div class="col-md-12 top-clock">
                            <span id="clock_div"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3 clearfix top-brand-logo">
                    <img alt="BookingPRO Logo" class="img-responsive pull-right" src="<?php echo BRAND_LOGO_PATH; ?>" />
                </div>                
            </div>
            <nav class="navbar navbar-default navbar-static-top" role="navigation" id="custom-bootstrap-menu">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span><span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo BRAND_NAME; ?></a>
                </div>
                
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav">
                        <?php if( $this->user_lvl != 5 && $this->user_lvl != 6): ?>
                        <li class="<?php echo (@$menu_active=='dasbhoard')?'active':''; ?>"><a href="dashboard">Dashboard</a></li>                        
                        <li class="<?php echo (@$menu_active=='callcenter')?'active':''; ?>"><a href="callcenter">Booking Register</a></li>                                                
                        <li class="<?php echo (@$menu_active=='call-register')?'active':''; ?>"><a href="dashboard/call_register">Call Register</a></li>                                                
                        <li class="<?php echo (@$menu_active=='outbound-register')?'active':''; ?>"><a href="dashboard/outbound_register">Outbound Register</a></li>                                                
                        <li class="<?php echo (@$menu_active=='outbound-call')?'active':''; ?>"><a href="dashboard/outbound_call">Outbound Call</a></li>                                                
                        <!-- <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Call<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Register</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Outbound</a></li>                                 
                            </ul>
                        </li> -->
                        <?php endif; ?>


                        <?php if( $this->user_lvl == 6): ?>
                        <li class="<?php echo (@$menu_active=='dasbhoard')?'active':''; ?>"><a href="dashboard">Dashboard</a></li>                        
                        <li class="<?php echo (@$menu_active=='callcenter')?'active':''; ?>"><a href="callcenter">Booking Register</a></li>                                                
                                               
                        <!-- <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Call<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Register</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Outbound</a></li>                                 
                            </ul>
                        </li> -->
                        <?php endif; ?>



                     <?php if( $this->user_lvl.$this->user_sub_lvl==53): ?>
                        <li class="<?php echo (@$menu_active=='dasbhoard')?'active':''; ?>"><a href="dashboard">Dashboard</a></li>                        
                        <li class="<?php echo (@$menu_active=='callcenter')?'active':''; ?>"><a href="callcenter">Booking Register</a></li>                                                
                        <li class="<?php echo (@$menu_active=='call-register')?'active':''; ?>"><a href="dashboard/call_register">Call Register</a></li>                                                
                        <!-- <li class="<?php echo (@$menu_active=='outbound-register')?'active':''; ?>"><a href="dashboard/outbound_register">Outbound Register</a></li>                                                 -->
                        <li class="<?php echo (@$menu_active=='outbound-call')?'active':''; ?>"><a href="dashboard/outbound_call">Outbound Call</a></li>                                                
                        <!-- <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Call<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Register</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Outbound</a></li>                                 
                            </ul>
                        </li> -->
                        <?php endif; ?>
                        <?php if( ($this->user_lvl != 5 && $this->user_lvl != 6) OR ($this->user_lvl.$this->user_sub_lvl == 51 || $this->user_lvl.$this->user_sub_lvl == 52 || $this->user_lvl.$this->user_sub_lvl == 53) ): ?>
                        <li class="<?php echo (@$menu_active=='chaperone')?'active':''; ?>"><a href="chaperone">Chaperone</a></li>
                        <?php endif; ?>

                       <?php if( ($this->user_lvl != 5 && $this->user_lvl != 6)  OR ($this->user_lvl.$this->user_sub_lvl == 52 || $this->user_lvl.$this->user_sub_lvl == 53) ): ?>
                        <li class="<?php echo (@$menu_active=='doctor')?'active':''; ?>"><a href="doctor">Doctor</a></li>
                        <?php endif; ?>

          <!--                <?php if( ($this->user_lvl != 5) OR ($this->user_lvl.$this->user_sub_lvl == 53) ): ?>
                        <li class="<?php echo (@$menu_active=='clientadmin')?'active':''; ?>"><a href="clientadmin">Client Admin</a></li>
                        <?php endif; ?> -->

                        <?php if( $this->user_lvl != 5): ?>
                        <li id="general_notices_li">
                            <a onclick="hcd.notification.view(1)" title="" data-placement="bottom" data-toggle="tooltip" class="showToolTip" href="javascript:void(0)" data-original-title="General Notices">                           

                            <?php
                                $class = '';
                                if($this->total_notices_gen > 0)
                                {
                                    $class = 'myblink';
                                }
                            ?>
                                <div class="<?php echo @$class;?>">
                                <?php echo (@$this->total_notices_gen > 0)?'<span class="badge notice-count" >'.@$this->total_notices_gen.'</span>':''; ?>

                                    <span style=" " class="glyphicon glyphicon-bell"></span>
                                </div>
                            </a>
                        </li> 
                        <?php endif; ?>
                                                

                       <!--  <li><a href="#">FAQ's</a></li> -->

                        <?php if( $this->user_lvl != 5 AND count($this->header_contacts) > 0): ?>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                            <?php 
                                foreach( $this->header_contacts as $key=>$name ){

                                    echo '<li><a href="dashboard/contact/'.$key.'" '.($this->user_lvl==5?'':' target="_blank"').' >'.$name.'</a></li>';
                                }
                            ?>
                            </ul>
                        </li>
                        <?php endif; ?>

                        <?php if( $this->user_lvl != 5 && $this->user_lvl != 6): ?>
                        <li class="<?php echo (@$menu_active=='report')?'active':''; ?>">
                            <a href="report" class="dropdown-toggle" data-toggle="dropdown">Reports <strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="report/daily">Daily</a></li>
                                <li><a href="report/cancellation">Cancellation</a></li>
                                <li><a href="report/wyhau">WYHAU</a></li>
                                <li><a href="report/appointment">Appointment</a></li>
                                <li><a href="report/acss">ACSS Input</a></li>
                            </ul>
                        </li>
                        <?php endif; ?>

                        <li class="<?php echo (@$menu_active=='trainingvideos')?'active':''; ?>"><a href="dashboard/trainingvideos">Training Videos</a></li>             
                        
                       <?php if( $this->user_lvl.$this->user_sub_lvl == 51  && $this->user_lvl != 6): ?>
                        <li class="<?php echo (@$menu_active=='report')?'active':''; ?>"><a href="report/chaperone">Report</a></li>             
                        <?php endif; ?>

                         <?php if($this->user_lvl.$this->user_sub_lvl == 53  && $this->user_lvl != 6): ?>
                        <li class="<?php echo (@$menu_active=='report')?'active':''; ?>"><a href="report/client_admin">Report</a></li>             
                        <?php endif; ?>
                        <li>
                                    <a href="javascript:void(0)" class="showToolTip" data-toggle="tooltip" data-placement="bottom" title="Open&nbsp;Notes" onClick="StickyNotes.activate('activate')"><span class="glyphicon glyphicon-file" style=""></span></a>
                                </li>

                    </ul>

                     
                    <ul class="nav navbar-nav navbar-right right-menu">
                        <?php if( $this->user_lvl != 6): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maintenance<strong class="caret"></strong></a>
                            <?php $this->load->view('maintenance/sidebar', array('sidebar_class'=>'dropdown-menu')); ?>                          
                        </li>
                        <?php endif;?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setting<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a><?php echo $this->agent_name; ?></a></li>

                                <!-- Chaperone able to switch area/doctor -->
                                <?php if( $this->user_lvl.$this->user_sub_lvl == 51 ): ?>
                                    <li><a href="dashboard/chaperone_switch_area_doctor">Switch Area/Doctor</a></li>
                                <?php endif; ?>

                                <li>
                                    <a href="logout">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </nav>
        </div>
    </div>
    <div id="ribbon"></div>
    <div class="row">        
        <div class="col-lg-12 col-md-12 main-content">                    
            
            <?php $this->load->view($view_file, @$data); ?>
        
        </div>
    </div>
    <div class="row">
        <br />
        <div class="col-lg-12 text-center">
            <p><?php echo SITE_FOOTER; ?></p>
        </div>
    </div>    
  
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery-1.11.3.min.js"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/moment/moment.min.js"></script>
<script src="assets/plugins/moment/en-au.js"></script>
<script src="assets/plugins/moment/moment-timezone-with-data-2010-2020.js"></script>
<script src="assets/plugins/moment/moment-timezone.min.js"></script>

<script src="assets/plugins/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script src="assets/js/apps.js"></script>
<script src="assets/js/clock.js"></script>
<script src="assets/js/jquery.cookie.js"></script>

<?php echo @$js_file; ?>

</body>
</html>