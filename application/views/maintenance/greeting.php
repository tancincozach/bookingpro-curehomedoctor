 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Greeting</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is update the greeting on the dashboard
                </dd>
                <dt>
                    Feature(s)
                </dt>
                <dd>
                    <ul>
                        <li>Able to update, change colors and backgrounds</li>                        
                        <li>There are pre-defined styles on formats toolbars</li>                        
                    </ul>                
                </dd> 
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
    
    <div class="row">    
        <div class="col-md-12">

            <p>You can customize the greeting you see on the dashboard.</p>

            <?php echo form_open('maintenance/greeting', 'class="form-inline" id="newAreaForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                <input type="hidden" name="id" value="<?php echo @$greeting->id; ?>" />
                <input type="hidden" name="formtype" value="greetingupdate" />
                 
                <textarea class="input-block-level" id="summernote" name="content_html" ><?php echo @stripslashes($greeting->set_value); ?></textarea>

                <br />

                <button type="submit" class="btn btn-primary"><?php echo (@$formtype=='new')?'Submit':'Update'; ?></button>
                
                <br />

            </form>

        </div>
    </div> <!-- end row -->    

     <br />

</div>