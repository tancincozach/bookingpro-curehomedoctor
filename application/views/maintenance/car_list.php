
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Car List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif;   ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open('maintenance/car/', 'class="form-inline box_border pad_5 bg-info" id="carListForm" method="post"');?>         
            <input type="hidden" name="car" value="<?php echo @$car_details->car_id;?>"/>
            <input type="hidden" name="formtype" value="<?php echo (isset($car_details) ? 'update':'new');?>"/>
        <div class="form-group ">
                    <div class="input-group">
                    <input type="text" name="search_car" class="form-control" placeholder="Search Car">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary  search">Search</button>                        
                    </div>
                    <div class="btn-group">
                       <a  href="maintenance/cars/" class="btn btn-default ">Reset</a>                   
                    </div>

                      <div class="btn-group">
                        <a  href="maintenance/car_form/" class="btn btn-success ">Add Car</a>
                    </div> 
            </div>
      <div class="btn-group pull-right">
                 <a class="btn btn-primary" href="maintenance/cars/?filter=all">All</a>  
               <a class="btn btn-success" href="maintenance/cars/">Active</a>  
                <a class="btn btn-danger" href="maintenance/cars/?filter=inactive">Inactive</a>         
            </div> 
            </form>
        </div>


        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Name</th>                                            
                        <th>Plate No</th>                                                                    
                        <th>Email</th>                                                                    
                        <th >
                            <!-- <input type="checkbox" name="check_all_email"/>&nbsp;
                            <button class="btn btn-xs btn-default send-btn" alt="email"  >Send Email</button> -->
                            Send Appt
                        </th>                  
                        <th>Mobile</th>                                                                    
                        <th >
                          <!--       <input type="checkbox" name="check_all_sms"/>&nbsp;
                                <button class="btn btn-xs btn-default send-btn"  alt="sms">Send SMS</button> -->
                            Send Appt
                        </th>                  
                        <th>Area</th>                  
                        <th>Date Updated</th>                                            
                        <th>Status</th>                                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo $row->car_id; ?>">        
                                 <td><?php   echo $row->car_name ;   ?></td>         
                                  <td><?php echo $row->car_plateno ;?></td>                          
                                  <td><?php echo $row->car_email ;?></td>    
                                      <td class="text-center">
                                    <!-- <input type="checkbox"  name="email" value="<?php echo $row->car_id; ?>" <?php echo isset($row->send_email) && intval($row->send_email)==1 ? "CHECKED":"";?>/> -->

                                        <?php echo isset($row->send_email) && intval($row->send_email)==1 ? "Yes":"No";?>
                                  </td>
                                  <td><?php echo $row->car_mobile ;?></td>    
                                  <td class="text-center">
                                        <!-- <input type="checkbox" name="sms" value="<?php echo $row->car_id; ?>"    <?php echo isset($row->send_text) && intval($row->send_text)==1 ? "CHECKED":"";?>/> -->
                                        <?php echo isset($row->send_text) && intval($row->send_text)==1 ? "Yes":"No";?>
                                  </td>      
                                  <td><?php echo $row->area_name ;?></td>    
                                  
                                  <td><?php echo $row->car_updated;?></td>    
                                  <td><span class="label  label-<?php echo ($row->car_deleted==0 ? 'success':'danger'); ?>"><?php echo ($row->car_deleted==0 ? 'active':'inactive'); ?></span></td>                                                                        
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/car_form/?car=<?php echo $row->car_id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/car/?formtype=delete&car=<?php echo $row->car_id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                                                                   
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="7">No car(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>