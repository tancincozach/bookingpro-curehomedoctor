
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>

<style type="text/css">
    
    #consult_note_container .panel-heading, 
    #consult_note_container .panel-body, 
    #consult_note_container .panel-footer {
        padding-left: 5px;
        padding-right: 5px;
    }

    .consult-list .tools{
        display: none;
    }

    .consult-list > li:hover .tools {
        display: inline-block;
    }

    .add_dropdown_item {
        padding-top: 5px;
        padding-bottom: 5px;
    }
</style>

<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Consult Note Dropdown</h3>
 

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row" id="consult_note_container">
        
        <div class="col-sm-12">
            <p class="bg-danger">
                Please don't include "Other" system will taker on it.
            </p>
        </div>

        <div class="col-sm-12 clearfix ">
            <div class="card-columns  clearfix">
                <?php foreach ($items as $dropdown => $item_data): 
                    $panel_title = ucwords(str_replace('_', ' ', $dropdown));

                    if( $panel_title == 'Social History' )
                        //$panel_title = 'Social / Family History / Smoker / Alcohol';
                        $panel_title = 'Social / Family History';
                ?>           
               


                <div class="card ">
                    <div class="box" id="dropdown-<?php echo $dropdown; ?>" data-id="<?php echo $dropdown; ?>"> 
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <h5 class="pull-left panel-title"><?php echo $panel_title; ?></h5>
                                <div class="pull-right">
                                    <button type="button" onclick="consult.add(this, event)"  class="btn btn-success btn-xs" aria-label="New" title="Add new dropdown">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button> 
                                </div>
                            </div>
                            <div class="panel-body">
                           

                                <ul class="list-group consult-list">
                                    <?php foreach($item_data as $row): ?>
                                    <li class="list-group-item clearfix" data-id="<?php echo $row->dropdown_id; ?>">
                                        <div class="item" >
                                            <span class="text pull-left"><?php echo stripslashes($row->dropdown_value); ?></span>
                                            <span class="tools pull-right">
                                                <a href="#" onclick="consult.edit_item(this, event)" class="text-primary edit-item"><i class="glyphicon glyphicon-edit "></i></a>
                                                <a href="#" onclick="consult.delete_item(this, event)" class="text-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                            </span>
                                        </div>
                                        <div class="edit" style="display: none">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control">
                                                <span class="input-group-btn">
                                                    <button type="button" onclick="consult.save_item(this, event)" class="btn btn-success btn-flat" title="Save"><i class="glyphicon glyphicon-save "></i></button>
                                                    <button type="button" onclick="consult.cancel_item(this, event)" class="btn btn-danger btn-flat" title="Cancel"><i class="glyphicon glyphicon-triangle-right "></i></button>
                                                </span>
                                          </div>                      
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>
           
                 
      
    </div> <!-- end row -->    

</div>