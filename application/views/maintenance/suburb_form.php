 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Suburb Form</h3>

    <?php if( SHOW_MILESTONE ): ?>


    

    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>
<?php echo ($this->session->flashdata('error') != '')?'<p class="flash-mesg alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>

   
    <div class="row">

        <div class="col-md-12 ">       

            <?php echo form_open('maintenance/suburb', ' data-toggle="validator"  role="form"  id="patient-form" method="post" '); ?>
            
                    <div class="row">
                        <div class="form-group col-sm-12  col-md-8">
                            <label for="suburb_name" class="control-label">Suburb Name*</label>
                            <input type="text" placeholder="" class="form-control" id="suburb_name" name="suburb_name" value="<?php echo @$suburb[0]->suburb_name;?>" required>
                        </div>
                        <div class="form-group col-sm-12  col-md-8">
                            <label for="suburb_postcode" class="control-label">Suburb PostCode*</label>
                            <input type="text" placeholder=" " class="form-control" id="suburb_postcode" name="suburb_postcode" value="<?php echo @$suburb[0]->suburb_postcode;?>" required>
                        </div>    
                        <div class="form-group col-sm-12  col-md-8">
                            <label for="suburb_status" class="control-label">Suburb Status*</label>
                            <select class="form-control" id="suburb_status" name="suburb_status">
                                    <option value="1" <?php isset($suburb[0]->suburb_status) && $suburb[0]->suburb_status==1 ? 'SELECTED':'';?> >Active</option>
                                    <option value="0" <?php isset($suburb[0]->suburb_status) && $suburb[0]->suburb_status==0 ? 'SELECTED':'';?>>Inactive</option>
                            </select>
                        </div>    
                    </div>
                    
                     <div class="form-group">

                       <input type="hidden" id="formtype" name="formtype" value="" />    
    					<input type="hidden" name="area" value="<?php echo @$area_id;?>" />   
    

                            <?php if(!isset($suburb)){?>                                
                                <button type="submit"  alt="new" class="btn btn-success action-button ">Add</button>
                                <a href="maintenance/suburblist?area=<?php echo @$area_id;?>"  class="btn btn-primary action-button cancel">Cancel</a>
                            <?php
                            }else{
                                ?>                              
                              
                                <input type="hidden" name="suburb_id" value="<?php echo @$suburb[0]->suburb_id;?>" />     
                                <button type="submit"  alt="update" class="btn btn-success action-button">Update</button>
                                <button alt="delete" onclick="return confirm('Are you sure you want to delete this recored ?')"  class="btn btn-danger action-button delete" >Delete</button>
                                <button alt="cancel" class="btn btn-primary action-button cancel">Cancel</button>
                                <?php
                            }
                            ?>
                     </div>
            </form>
            </div>
        </div>
    </div> <!-- end row -->    