 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Contacts</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is update the greeting on the dashboard
                </dd>
                <dt>
                    Feature(s)
                </dt>
                <dd>
                    <ul>
                        <li>Able to update, change colors and backgrounds</li>                        
                        <li>There are pre-defined styles on formats toolbars</li>                        
                    </ul>                
                </dd> 
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
    
    <div class="row">    
        <div class="col-md-12">

            


            <?php echo form_open('maintenance/contact_form', 'class="form-inline" id="newAreaForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>

                    
                        <input type="hidden" value="<?php echo @$contact->id;?>" name="contact">
                        <input type="hidden" value="<?php echo (isset($contact->id) ?'update':'new');?>" name="formtype">

                        <div class="form-group" style="margin-bottom:10px">
                            <label for="contact_title">Title</label>
                            <input type="text" value="<?php echo @$contact->contact_title;?>" required="required" name="contact_title" class="form-control" style="width: 300px"> 
                        </div>
<br/>
<div class="clearfix"></div>
                        <div class="form-group" style="margin-bottom:10px">
                            <label for="contact_html">Html Content</label>
                                            <textarea class="input-block-level" id="contact-msg" name="content_html" ><?php echo @stripslashes(@$contact->content_html); ?></textarea>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="form-group" style="margin-bottom:10px">
                            <label for="em_status">Status</label>
                            <select style="width: 200px" name="contact_status" class="form-control">
                                    <option value="1" <?php echo (isset($contact->contact_status) && $contact->contact_status==1 ? 'selected':'')?>>Active</option>
                                    <option value="0" <?php echo (isset($contact->contact_status) && $contact->contact_status==0 ? 'selected':'')?>>Inactive</option>
                                        
                            </select>
                             
                        </div>

                        <div class="clearfix"></div>
                        <br>
               
                             
                              <button class="btn btn-primary" type="submit">Submit</button>
                               <button onclick=" window.location = 'maintenance/contacts'; " class="btn btn-default" type="button">Cancel</button>
                     
                    </form>

        </div>
    </div> <!-- end row -->    

     <br />

</div>