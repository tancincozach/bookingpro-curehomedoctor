 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Add/Edit Notice</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is update the greeting on the dashboard
                </dd>
                <dt>
                    Feature(s)
                </dt>
                <dd>
                    <ul>
                        <li>Able to update, change colors and backgrounds</li>                        
                        <li>There are pre-defined styles on formats toolbars</li>                        
                    </ul>                
                </dd> 
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
    
    <div class="row">    
        <div class="col-md-12">
            <?php echo form_open('maintenance/notice_form', 'class="form-inline" id="newNoticeForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>

                    
                        <input type="hidden" value="<?php echo @$notice->id;?>" name="notice">
                        <input type="hidden" value="<?php echo (isset($notice->id) ?'update':'new');?>" name="formtype">
                        <input type="hidden" value="1" name="group">

                        <div class="form-group" style="width:400px"> 
                            <label for="content_html">Content</label>
                            <textarea class="input-block-level" name="content" style="width: 100%" rows="5"><?php echo @$notice->content_html;?></textarea>
                        </div> 

                        <div class="clearfix" style="margin-bottom:10px"></div>
                    <div class="form-group" >
                                                <label for="text_notes">Expiry Date</label><br/>
                                                 <div class="input-group date"  id="expire_date">
                                                 <input type="text" class="form-control" name="expiry_date" value="<?php echo @$notice->expire_date;?>" />
                                                 <span class="input-group-addon">
                                                 <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                        </div>
                        <!-- <div class="form-group" style="margin-bottom:10px">
                            <label for="notice_title">Expire Date</label><br/>
                            <input type="text" value="<?php echo @$notice->expire_date;?>" id="expire_date" required="required" name="expire" class="form-control" > 
                        </div>
 -->
                        <div class="clearfix"></div>

                        <div class="form-group" >
                            <label for="em_status">Status</label><br/>
                            <select  name="status" class="form-control" style=" width: 197px;">
                                    <option value="1" <?php echo (isset($notice->notice_status) && $notice->notice_status==1 ? 'selected':'')?>>Active</option>
                                    <option value="0" <?php echo (isset($notice->notice_status) && $notice->notice_status==0 ? 'selected':'')?>>Inactive</option>
                                        
                            </select>
                             
                        </div>

                        <div class="clearfix"></div>
                        <br>
               
                             
                              <button class="btn btn-primary" type="submit">Submit</button>
                               <button onclick=" window.location = 'maintains/notices'; " class="btn btn-default" type="button">Cancel</button>
                     
                    </form>

        </div>
    </div> <!-- end row -->    

     <br />

</div>