 
<h4 class="page-header">Training Videos</h4>

<!-- search -->
<div> 

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success text-bold"><label>'.$this->session->flashdata('fmesg').'</label></div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger text-bold"><label>'.$this->session->flashdata('error').'</label></div>':''; ?>    

    <?php echo form_open_multipart('', 'class="form-inline"'); ?>
    <input type="hidden" name="upload_training" value="hahaha">
        <div class="form-group">
            <label for="search" class="">Upload Training Video</label>
            <input type="file" class="form-control input-sm"  name="t_video" >
        </div>        
              <input class="btn btn-success btn-sm" name="video_upload" value="Upload Video" type="submit" />

    </form>
</div>
<br />

<?php 
	if(isset($dir)): 
		if( count($dir) ):
?>
<div class="col-md-6">

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>File Name</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($dir as $key=>$file):?>
			<tr>
				<td><a href="dashboard/trainingvideos/?vid=<?php echo urlencode($file); ?>"><?php echo $file; ?></a></td>
				<td><a  href="maintenance/training_videos/?vid=<?php echo urlencode($file); ?>&del=true" class="delete_video" >Delete File</a></td>	
			</tr>
			<?php endforeach;  ?>
			
		</tbody>
			
	</table>
	
</div>
<?php 
		else:
			echo '<h5>No traning videos Available</h5>';
		endif;	
	endif; 
?>


<?php if( isset($vid_file) ): ?>

<div>
	<video width="100%" height="auto" autoplay>
		<source src="<?php echo $vid_file; ?>" type="video/mp4">
		Your browser does not support the video tag.
	</video>
</div>

<div>
<a href="dashboard/trainingvideos" class="btn btn-info btn-xs">Back</a>
</div>

<?php endif; ?>
