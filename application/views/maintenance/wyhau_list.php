 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Where you heard about us</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    Status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>

    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open('maintenance/wyhau', 'class="form-inline box_border pad_5 bg-info" id="newAreaForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                <input type="hidden" name="formtype" value="<?php echo isset($wyhau->dropdown_id) ? 'update':'new';?>" />
                <input type="hidden" name="wyhau" value="<?php echo @$wyhau->dropdown_id;?>" />
                <input type="hidden" name="dropdown_group" value="WYHAU" />
                <div class="form-group">
                    <label for="dropdown_value">
                    <?php if(isset($wyhau->dropdown_id)){?>
                     Update 
                    <?php  }else{?>
                   Create New  
                    <?php  }?>item
                    </label>
                    <div class="input-group">                      
                        <input type="text" name="dropdown_value" class="form-control" value="<?php echo @$wyhau->dropdown_value;?>" placeholder="Input here" required>                        
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            </form>
        </div>

        <div class="col-md-12">        
            <table id="table-wyhau" class="table table-hover table-condensed table-row-order">
                <thead>
                    <tr>
                        <th>Name</th>                                            
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($wyhaulist as $row): ?>
                    <tr id="tr-<?php echo $row->dropdown_id; ?>">        
                        <td><?php echo $row->dropdown_label; ?></td>                         
                        <td>
                            <?php $status = $row->dropdown_status; ?>
                            <div class="btn-group">
                                <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs" style="width: 65px"><?php echo ($status)?'Active':'Inactive' ?></button>
                                
                                <?php if(in_array('edit', $this->user_perms)): ?>
                                <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                               
                                <ul class="dropdown-menu">
                                    <li><a href="maintenance/wyhau?id=<?php echo $row->dropdown_id; ?>&active=<?php echo ($status==0)?1:0 ?>" onclick="return confirm('Confirm?')"><?php echo (!$status)?'Activate':'Disable' ?></a></li>

                                </ul>
                                <?php endif; ?>
                            </div>

                            <?php if(in_array('edit', $this->user_perms)): ?>
                            <a href="maintenance/wyhaulist?id=<?php echo $row->dropdown_id;?>" class="btn btn-info btn-xs" role="button" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <?php endif; ?>
                        </td>                  
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>