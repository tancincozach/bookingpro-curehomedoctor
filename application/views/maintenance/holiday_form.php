
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Add/Edit Public Holiday</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">
        
        <div class=" col-lg-6 col-sm-6 col-md-6">
            <?php echo form_open('maintenance/user', ' id="UserForm" class="form-horizontal"  style="padding:10px" method="post"');?>         
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Name</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="user_fullname" name="user_fullname" value="<?php echo @$user->user_fullname;?>" required> 
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Username</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="user_name" name="user_name" value="<?php echo @$user->user_name;?>" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Password</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="user_pass" name="user_pass"  <?php if(!isset($user->user_id)){?>required <?php }?>>                     
                            <?php if(isset($user->user_id)){?>
                                <label class="alert alert-warning" style="margin-top:10px;">Leave Password blank if not updating</label>
                            <?php }?>
                    </div>
                </div>
            
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">User Level*</label>
                    <div class="col-lg-5">
                         <select name="user_level"  class="form-control" >
                                <option value="0" <?php echo (isset($user->user_level) && $user->user_level==0 ? "selected":'');?>>Operator</option>
                                <option value="1" <?php echo (isset($user->user_level) && $user->user_level==1 ? "selected":'');?>>Supervisor</option>
                                <option value="2" <?php echo (isset($user->user_level) && $user->user_level==2 ? "selected":'');?>>Support/Teamleader</option>
                                <option value="3" <?php echo (isset($user->user_level) && $user->user_level==3 ? "selected":'');?>>Ccadmin</option>
                                <option value="5" <?php echo (isset($user->user_level) && $user->user_level==5 ? "selected":'');?>>Client</option>
                                </select>
                            <?php

                                $sub_level_class = (isset($user->user_sublevel) &&  intval($user->user_sublevel)!=0 ? '':'hide');
                                
                            ?>
                    </div>
                </div>

             <div class="form-group sub_level <?php echo $sub_level_class;?>">
                <label class="control-label col-lg-2" style="text-align:left">Area</label>
                <div class="col-lg-5">
                    <select name="area_id" class="form-control sub_level  <?php echo $sub_level_class;?>"  required>  
                        <?php foreach($area as $row):?>    
                        <option value="<?php echo $row->area_id; ?>"><?php echo $row->area_name; ?></option>                         
                        <?php endforeach;?>                             
                    </select>
                </div>
             </div>
            <div class="form-group sub_level <?php echo $sub_level_class;?>">
                <label class="control-label col-lg-2" style="text-align:left">User Sub Level*</label>
                <div class="col-lg-5">
                    <select name="user_sublevel" class="form-control sub_level  <?php echo $sub_level_class;?>"  required>
                                <option value="1" <?php echo (isset($user->user_sublevel) && $user->user_sublevel==1 ? "selected":'');?>>Chaperone</option>
                                <option value="2" <?php echo (isset($user->user_sublevel) && $user->user_sublevel==2 ? "selected":'');?>>Doctor</option>    
                            </select>
                </div>
             </div>
            <div class="form-group sub_level <?php echo $sub_level_class;?>">
                <label class="control-label col-lg-2" style="text-align:left">User Options*</label>
                <div class="col-lg-5">
                     <div class="checkbox sub_level <?php echo $sub_level_class;?>" >
                              <label><input type="checkbox" name="user_opts[]" value="edit" <?php echo (isset($user->user_opts) && strpos($user->user_opts,'edit')!== false ? "checked":'');?>>Add/Edit</label>
                            </div>
                            <div class="checkbox sub_level <?php echo $sub_level_class;?>">
                              <label><input type="checkbox" name="user_opts[]" value="delete" <?php echo (isset($user->user_opts) && strpos($user->user_opts,'delete')!== false ? "checked":'');?>>Delete</label>
                            </div>
                </div>
             </div>


            <div class="form-group">
                <label class="control-label col-lg-2 " style="text-align:left">Contact Email</label>
                <div class="col-lg-5">
                     <input type="text" placeholder="" class="form-control" id="contact_email" name="contact_email" value="<?php echo @$user->contact_email;?>" required>
                </div>
             </div>

             <div class="form-group">
                <label class="control-label col-lg-2 " style="text-align:left">Contact Mobile</label>
                <div class="col-lg-5">
                     <input type="text" placeholder="" class="form-control" id="contact_mobile" name="contact_mobile" value="<?php echo @$user->contact_mobile;?>" required>
                </div>
             </div>
          <!--    <div class="form-group">
                
                            <label for="user_fullname" class="control-label">Name*</label>
                            <input type="text" placeholder="" class="form-control" id="user_fullname" name="user_fullname" value="<?php echo @$user->user_fullname;?>" required> 
                            <label for="user_name" class="control-label">Username*</label>
                            <input type="text" placeholder="" class="form-control" id="user_name" name="user_name" value="<?php echo @$user->user_name;?>" required>
                            <label for="user_pass" class="control-label">Password*</label>
                            <input type="text" placeholder="" class="form-control" id="firstname" name="user_pass"  <?php if(!isset($user->user_id)){?>required <?php }?>>                     
                             <?php if(isset($user->user_id)){?>
                                <label class="alert alert-warning">Leave Password blank if not updating</label>
                            <?php }?>
                            <label for="first-name" class="control-label">User Level*</label>
                            <select name="user_level"  class="form-control" >
                                <option value="0" <?php echo (isset($user->user_level) && $user->user_level==0 ? "selected":'');?>>Operator</option>
                                <option value="1" <?php echo (isset($user->user_level) && $user->user_level==1 ? "selected":'');?>>Supervisor</option>
                                <option value="2" <?php echo (isset($user->user_level) && $user->user_level==2 ? "selected":'');?>>Support/Teamleader</option>
                                <option value="3" <?php echo (isset($user->user_level) && $user->user_level==3 ? "selected":'');?>>Ccadmin</option>
                                <option value="5" <?php echo (isset($user->user_level) && $user->user_level==5 ? "selected":'');?>>Client</option>
                                </select>
                            <?php

                                $sub_level_class = (isset($user->user_sublevel) &&  intval($user->user_sublevel)!=0 ? '':'hide');
                                
                            ?>
                            <label for="sub-user-level" class="control-label sub_level <?php echo $sub_level_class;?>">User Sub Level*</label>
                            <select name="user_sublevel" class="form-control sub_level  <?php echo $sub_level_class;?>"  required>
                                <option value="1" <?php echo (isset($user->user_sublevel) && $user->user_sublevel==1 ? "selected":'');?>>Doctor</option>
                                <option value="2" <?php echo (isset($user->user_sublevel) && $user->user_sublevel==2 ? "selected":'');?>>Chaperone</option>    
                            </select><br/>
                            <label for="sub-user-level" class="control-label sub_level <?php echo $sub_level_class;?>">User Options*</label>                            
                            <div class="checkbox sub_level <?php echo $sub_level_class;?>" >
                              <label><input type="checkbox" name="user_opts[]" value="edit" <?php echo (isset($user->user_opts) && strpos($user->user_opts,'edit')!== false ? "checked":'');?>>Add/Edit</label>
                            </div>
                            <div class="checkbox sub_level <?php echo $sub_level_class;?>">
                              <label><input type="checkbox" name="user_opts[]" value="delete" <?php echo (isset($user->user_opts) && strpos($user->user_opts,'delete')!== false ? "checked":'');?>>Delete</label>
                            </div>

                            <label for="contact_email" class="control-label">Contact Email</label>
                            <input type="text" placeholder="" class="form-control" id="contact_email" name="contact_email" value="<?php echo @$user->contact_email;?>" required>
                            <label for="contact_mobile" class="control-label">Contact Mobile</label>
                            <input type="text" placeholder="" class="form-control" id="contact_mobile" name="contact_mobile" value="<?php echo @$user->contact_mobile;?>" required>
                            


                </div> -->
             <div class="form-group">
                    <input type="hidden" id="formtype" name="formtype" value="" />   
                <?php
                   if(isset($user->user_id))
                    {
                    ?>  
                    <input type="hidden" name="user" value="<?php echo @$user->user_id;?>" />  
                    <button type="submit"  alt="update" class="btn btn-success action-button">Update</button>
                    <button alt="delete" class="btn btn-danger action-button delete" >Delete</button>
                    <?php
                    }
                    else
                    {
                     ?>
                       <button type="submit"  alt="new" class="btn btn-success action-button ">Add</button>
                     <?php

                    }
                ?>                            
                <button class="btn btn-primary action-button cancel">Cancel</button>
             </div>
                </form>
        </div>
    </div> <!-- end row -->    

</div>