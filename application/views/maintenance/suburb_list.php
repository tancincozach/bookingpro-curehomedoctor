 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header"><?php echo $areas->area_name.' / ';?>Suburb</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to setup areas, suburb on area, add cars, add chaperone
                </dd>
                <dt>
                    Features:
                </dt>
                <dd>                    
                    <ul>
                        <li>Suburb button
                            <ul>
                                <li>Show popup Suburb</li>                                
                                <li>Add/Edit</li>                                
                            </ul>
                        </li>
                        <li>Able upload suburb from csv</li>
                    </ul>
                </dd>
                <dt>
                    Status:
                </dt>
                <dd>
                    Partial
                </dd>
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>
    
    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open('maintenance/suburb', 'class="form-inline box_border pad_5 bg-info" id="suburbForm" method="post"'); ?>
                <input type="hidden" name="formtype" value="new" />
                <input type="hidden" name="area_id" value="<?php echo @$area_id?>" />
                <div class="form-group">
                 <div class="input-group">
                    <input type="text" name="search_suburb" class="form-control" placeholder="Search Suburb">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary search">Search</button>                        
                </div>
                </div>
                      <div class="btn-group">
               <a class="btn btn-primary" href="maintenance/suburb_form/?area=<?php echo @$area_id?>">Add New Suburb</a>  
            </div>
                <div class="btn-group pull-right">
                 <a class="btn btn-primary" href="maintenance/suburbs/?area=<?php echo @$area_id?>&filter=all">All</a>  
               <a class="btn btn-success" href="maintenance/suburbs/?area=<?php echo @$area_id?>">Active</a>  
                <a class="btn btn-danger" href="maintenance/suburbs/?area=<?php echo @$area_id?>&filter=inactive">Inactive</a>         
            </div>  
            </form>
        </div>

        <div class="col-md-12">        
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Name</th>                     
                        <th>Status</th>                     
                        <th>Options</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($record) &&  count($record) > 0) :
                     foreach($record as $row): ?>



                    <tr>
                        <td><?php echo @$row->suburb_name;?></td>
                        <td>
                        <!-- <span class="label  <?php  echo ($row->suburb_status==1 ? "label-success":"label-danger");?>"><?php echo ($row->suburb_status==1 ? "Active":"Inactive"); ?></span> -->
                        <?php $status = $row->suburb_status;
                      /*  echo $status;

                        exit();*/
                         ?>
                        <div class="btn-group">
                            <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs" style="width: 65px"><?php echo ($status)?'Active':'Inactive' ?></button>
                            <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                               <li><a href="maintenance/suburbs/?area=<?php echo  @$row->area_id;?>&suburb_id=<?php echo @$row->suburb_id?>&status=<?php echo (!$status)?'activate':'disable' ?>" onclick="return confirm('Confirm?')"><?php echo (!$status)?'Activate':'Disable' ?></a></li>
                            </ul>
                        </div>
                        </td>
                        <td >
                            <a href="maintenance/suburb_form/?area=<?php echo  @$row->area_id;?>&suburb_id=<?php echo @$row->suburb_id?>" class="btn btn-info btn-xs" role="button" title="Edit">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="maintenance/suburb/?area=<?php echo  @$row->area_id;?>&suburb_id=<?php echo @$row->suburb_id?>&formtype=delete" onclick="return confirm('Are you sure you want to delete this recored ?')" class="btn btn-info btn-xs" role="button" title="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                        </td>                  

                    </tr>
                    <?php endforeach;
                        else : ?>
                        <tr> <td  colspan="2">No suburb(s) found.</td></tr>
                    <?php endif;
                        if(isset($links) && $links!=''):
                    ?>
                        <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                        </tr>
                    <?php
                        endif;
                    ?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>