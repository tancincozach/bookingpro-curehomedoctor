
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Practices List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">                       
<?php echo form_open('maintenance/practices_actions'.(isset($practices[0]->id) ? '?practice='.$practices[0]->id:''), 'class="form-inline box_border pad_5 bg-info" id="PracticesListForm" method="post"'); ?>
                <input type="hidden" name="formtype" value="<?php if(count(@$practices)==0) :?>new<?php else:?>update<?php endif;?>" />
                <input type="hidden" name="area" value="<?php echo @$practices->id?>" />
                <div class="form-group">
                    <label for="practice_name"><?php if(count(@$practices[0])==0) :?>Create New Practice<?php else:?>Update Practice<?php endif;?></label>
                    <div class="input-group">                      
                        <input type="text" name="practice_name" class="form-control" placeholder="Type New Practice" value="<?php echo @
                        $practices[0]->practice_name?>">  
                    
                    </div>
                    <div class="input-group">     
                    <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo @
                        $practices[0]->email?>">  
                        </div>
             
                </div>
            <div class="btn-group ">
                 <button type="submit" class="btn btn-primary ">Submit</button>               
            </div>
           <div class="form-group ">
                    <div class="input-group">
                    <input type="text" name="search_practice" class="form-control" placeholder="Search Practice">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary  search">Search</button>                        
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-default  cancel">Reset</button>
                    </div>
                          
            </div>
            </form>
        </div>


        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>                        
                        <th>Practice Name</th>                                            
                        <th>Email</th>                                                
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo $row->id; ?>">        
                                 <td><?php echo $row->practice_name ;   ?></td>         
                                  <td><?php echo $row->email ;?></td>                                                            
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/practices/?practice=<?php echo $row->id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/practices_actions/?formtype=delete&practice=<?php echo $row->id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                                                                   
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No practice(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>