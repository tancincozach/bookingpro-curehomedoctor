    <!-- MODAL-->
    <div class="modal fade" id="areas-availability-modal" tabindex="-1" role="dialog" aria-labelledby="AreasAvailabilityModal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Set Availability Status</h4>
                </div>
                <div class="modal-body">
                <?php //print_r($record); ?>
                <?php //print_r($availability); ?>
                    <h4>AREA: <strong class="text-primary"><?php echo $record->area_name; ?></strong></h4>
                    <div class="area_availability_form">
                        <div class="col-sm-11" style="border: 1px solid #ccc; padding: 5px;">
                            <form method="post" name="form_set_availability" onsubmit="return areas_availability.on_submit_setAvailability2(this);">
                            
                                <input type="hidden" name="area_id" value="<?php echo $area_id; ?>"> 
                                <input type="hidden" name="agent_name" value="<?php echo $agent_name; ?>"> 
                                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>"> 

                                <table class="" >
                                    <tr>
                                        <td style="width: 300px">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="is_open" onclick="areas_availability.on_click_radio_reopen_dt_range(1)" value="1" required=""> Open
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="is_open" onclick="areas_availability.on_click_radio_reopen_dt_range(0)" value="0" required=""> Closed
                                                </label>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>                                  
                                     <tr>
                                        <td style="vertical-align: top">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="is_open" onclick="areas_availability.on_click_radio_reopen_dt_range(2)" value="0" required=""> Quick Close - this will close the area &amp; re-open next available booking slot EG: 6pm tonight
                                                </label>

                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             
                                            <div id="closed_reopen_dt" style="display: none">
                                                
                                                <div class="col-sm-12" style="border-bottom: 1px dotted #000">
                                                    <strong>RE-OPEN</strong>
                                                </div>                                                


                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#close_for_period" aria-controls="close_for_period" role="tab" data-toggle="tab">CLOSE FOR PERIOD & SCRIPT CHANGE</a></li>
                                                    <li role="presentation"><a href="#close_for_evening" aria-controls="close_for_evening" role="tab" data-toggle="tab">CLOSE FOR EVENING</a></li>
                                                </ul>


                                                  <!-- Tab panes -->
                                                <div class="tab-content">

                                                    <div role="tabpanel" class="tab-pane active" id="close_for_period">
                                                        <!-- <strong>CLOSE FOR PERIOD & SCRIPT CHANGE</strong> -->
                                                        <br />
                                                        <br />

                                                        <div> 
                                                            <span style="width: 40px; float: left;"><strong>Start</strong> </span>
                                                            <input type="text"  name="reopen_dt_start" width="150px" />
                                                        </div> 
                                                        <div class="form-group"> 
                                                            <span style="width: 40px; float: left"><strong>End</strong> </span>
                                                            <input type="text"  name="reopen_dt_end" width="150px">
                                                        </div>
                                                        <p class="text-info">
                                                            Hint: <br />                                                    
                                                            End date/time is optional, it can only be used to close for a certain date/time range. <br />
                                                        </p>
         
                                                        <div>
                                                            <strong>Default script is</strong>
                                                            <p>We do service your suburb but due to high number of bookings today our doctors will not be able to see you. 
                                                                You will need to call back tomorrow to make a booking    
                                                            </p>
                                                            <br />
                                                            <strong>To change script, manually enter script to display below</strong>
                                                            <textarea class="form-control" name="more_info"></textarea>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="close_for_evening">
                                                        <!-- <strong>CLOSE FOR EVENING</strong> -->
                                                        <br />
                                                        <br />

                                                        <div> 
                                                            <span style="width: 40px; float: left;"><strong>Start</strong> </span>
                                                            <input type="text"  name="reopen_dt_start" width="150px" required="" />
                                                        </div> 
                                                        <div class="form-group"> 
                                                            <span style="width: 40px; float: left"><strong>End</strong> </span>
                                                            <input type="text"  name="reopen_dt_end" width="150px" required="">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div> 

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <br />
                                            <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </form>
                            <br />
                        </div>
                        <div class="col-sm-6">
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <h4 class="text-info">NOTE: The latest added status will be the current active availability status. History is sorted from latest </h4>

                    <h3>Availability History</h3>
                    <div class="area_availability_history_div" style="max-height: 300px; overflow-y: scroll;">
                        <table class="table table-condensed" id="table_area_availability_history">
                            <thead>
                                <tr>
                                    <th>Status</th>                                    
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Added By</th>
                                    <th>Date Created</th>
                                    <th>Advice Caller Script</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>                                
                            <?php foreach($availability as $available): ?>
                                <tr>
                                    <td><?php echo ($available->is_open)?'<span class="text-success">OPEN</span>':'<span class="text-danger">CLOSED</span>'; ?></td>
                                    <td><?php echo (@$available->reopen_dt_start != '' AND $available->reopen_dt_start != '0000-00-00 00:00:00')?date('d/m/Y H:i', strtotime($available->reopen_dt_start)):''; ?></td>
                                    <td><?php echo (@$available->reopen_dt_end != '' AND $available->reopen_dt_end != '0000-00-00 00:00:00')?date('d/m/Y H:i', strtotime($available->reopen_dt_end)):''; ?></td>                                
                                    <td><?php echo $available->agent_name; ?></td>                                    
                                    <td><?php echo $available->area_avail_created; ?></td>
                                    <td><?php echo stripslashes($available->more_info); ?></td>
                                    <td></td>                                    
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>