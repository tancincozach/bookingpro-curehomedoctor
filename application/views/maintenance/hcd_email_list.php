
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">HCD Email List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">                       
<?php echo form_open('maintenance/hcd_emails'.(isset($hcd_email->id) ? '?id='.$hcd_email->id:''), 'class="form-inline box_border pad_5 bg-info" id="hcd_emailListForm" method="post"'); ?>
                <input type="hidden" name="formtype" value="<?php echo (isset($hcd_email->id) ? 'update':'new');?>" />
                <input type="hidden" name="set_id" value="<?php echo @$hcd_email->id?>" />
               
                <div class="form-group">
                    <label for="email_name"><?php if(count(@$hcd_email)==0) :?>Create New Email<?php else:?>Update Email<?php endif;?></label>
                    <div class="input-group">                      
                        <input type="email" name="content_html" class="form-control" placeholder="Type New Email" value="<?php echo @
                        $hcd_email->set_value?>" required>  
                    
                    </div>
             
                </div>
            <div class="btn-group ">
                 <button type="submit" class="btn btn-primary ">Submit</button>               
            </div>
           <div class="form-group ">
                    <div class="input-group">
                    <input type="email"  name="search_email" class="form-control" placeholder="Search Email">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary  search">Search</button>                        
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-default  cancel">Reset</button>
                    </div>
                          
            </div>
            </form>
        </div>


        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>                        
                        <th>Email</th>                                            
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo $row->id; ?>">        
                                 <td><?php echo $row->set_value ;   ?></td>                                           
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/hcd_emails/?&id=<?php echo $row->id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/hcd_emails/?delete=<?php echo $row->id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                                                                   
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No email(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>