 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Area - End Script</h3>
 

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg">'.$this->session->flashdata('fmesg').'</p>':''; ?>
    
    <div class="row">    
        <div class="col-md-12">

            <p>You can customize the form End Script, by updating below</p>

            <?php echo form_open('maintenance/area_form_endscript/<?php echo @$area_id; ?>', 'class="form-inline" id="newAreaForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                <input type="hidden" name="area_id" value="<?php echo @$area_id; ?>" /> 
                <input type="hidden" name="area_name" value="<?php echo @$area->area_name; ?>" /> 
                 
                <textarea class="input-block-level" id="summernote" name="content_html" ><?php echo stripslashes($area->form_end_script); ?></textarea>

                <br />

                <button type="submit" class="btn btn-primary">Save</button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-warning" href="maintenance/areas">Cancel</a>
                
                <br />

            </form>

        </div>
    </div> <!-- end row -->    

     <br />

</div>