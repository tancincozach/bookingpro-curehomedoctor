
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">CMA Settings</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php

    ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">
                
           
                <div class="col-md-6">
                      <?php echo form_open('maintenance/cma_setting/live', ' class="form-horizontal"  style="padding:10px" method="post"');?>    
                      <input type="hidden" name="set_name" value="CMA_LIVE">
                  <table class="table table-hover table-bordered">
                     <thead>
                                <th colspan="2">Live</th>
                            </thead>
                            <tbody>
                                <tr>
                                <td>CMA_DB</td>
                                <td>
                                <select name="cma_db" class="form-control">
                                    <option value="cma_nowra" <?php echo  (isset($cma_live->cma_db) && $cma_live->cma_db=='cma_nowra' ? 'selected':'');  ?>>Nowra</option>        
                                    <option value="cma_international" <?php echo  (isset($cma_live->cma_db) && $cma_live->cma_db=='cma_international' ? 'selected':'');  ?>>International</option>        
                                    <option value="cma_melbourne" <?php echo  (isset($cma_live->cma_db) && $cma_live->cma_db=='cma_melbourne' ? 'selected':'');  ?>>Melbourne</option>        
                                    <option value="cma_site4" <?php echo  (isset($cma_live->cma_db) && $cma_live->cma_db=='cma_site4' ? 'selected':'');  ?>>Site 4</option>        
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>SITE_ID</td>
                                <td> <select name="site_id" class="form-control">
                                    <option  value ="1"  <?php echo  (isset($cma_live->site_id) && $cma_live->site_id==1 ? 'selected':'');  ?>>Nowra</option>        
                                    <option value  ="2"  <?php echo  (isset($cma_live->site_id) && $cma_live->site_id==2 ? 'selected':'');  ?>>International</option>        
                                    <option value ="3"  <?php echo  (isset($cma_live->site_id) && $cma_live->site_id==3 ? 'selected':'');  ?>>Melbourne</option>        
                                    <option value ="4"   <?php echo  (isset($cma_live->site_id) && $cma_live->site_id==4 ? 'selected':'');  ?>>Site 4</option>        
                                </select></td>
                            </tr>
                             <tr>
                                <td>CMA_ID</td>
                                <td><input type="text" class="form-control" name="cma_id" value="<?php echo @$cma_live->cma_id;?>"></td>
                            </tr>
                            <tr>
                                <td>CUST_ID</td>
                                <td><input type="text" class="form-control" name="cust_id" value="<?php echo @$cma_live->cust_id;?>"></td>
                            </tr>
                            <tr>
                                <td>CONTACT_ID ISSUE</td>
                                <td><input type="text" class="form-control" name="contact_id_issue" value="<?php echo @$cma_live->contact_id_issue;?>" ></td>
                            </tr>
                            <tr>
                                <td>CUST_NAME</td>
                                <td><input type="text" class="form-control" name="cust_name" value="<?php echo @$cma_live->cust_name;?>" ></td>
                            </tr>
                            <tr>
                                <td>AGENT_NAME</td>
                                <td><input type="text" class="form-control" name="agent_name" value="<?php echo @$cma_live->agent_name;?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" class="btn btn-primary"  value="Update"/>
                                </td>
                            </tr>
                            </tbody>
                            
                        </table>
                        </form>
                </div>
                <!--
                <div class="col-md-6">
                <?php echo form_open('maintenance/cma_setting/test', '  class="form-horizontal"  style="padding:10px" method="post"');?>         
                <input type="hidden" name="set_name" value="CMA_TEST">
                     <table class="table table-hover table-bordered">
                     <thead>
                                <th colspan="2">Live</th>
                            </thead>
                            <tbody>
                                <tr>
                                <td>CMA_DB</td>
                                <td>
                                <select name="cma_db" class="form-control">
                                    <option value="cma_nowra" <?php echo  (isset($cma_test->cma_db) && $cma_test->cma_db=='cma_nowra' ? 'selected':'');  ?>>Nowra</option>        
                                    <option value="cma_international" <?php echo  (isset($cma_test->cma_db) && $cma_test->cma_db=='cma_international' ? 'selected':'');  ?>>International</option>        
                                    <option value="cma_melbourne" <?php echo  (isset($cma_test->cma_db) && $cma_test->cma_db=='cma_melbourne' ? 'selected':'');  ?>>Melbourne</option>        
                                    <option value="cma_site4" <?php echo  (isset($cma_test->cma_db) && $cma_test->cma_db=='cma_site4' ? 'selected':'');  ?>>Site 4</option>        
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>SITE_ID</td>
                                <td> <select name="site_id" class="form-control">
                                    <option  value ="1"  <?php echo  (isset($cma_test->site_id) && $cma_test->site_id==1 ? 'selected':'');  ?>>Nowra</option>        
                                    <option value  ="2"  <?php echo  (isset($cma_test->site_id) && $cma_test->site_id==2 ? 'selected':'');  ?>>International</option>        
                                    <option value ="3"  <?php echo  (isset($cma_test->site_id) && $cma_test->site_id==3 ? 'selected':'');  ?>>Melbourne</option>        
                                    <option value ="4"   <?php echo  (isset($cma_test->site_id) && $cma_test->site_id==4 ? 'selected':'');  ?>>Site 4</option>        
                                </select></td>
                            </tr>
                             <tr>
                                <td>CMA_ID</td>
                                <td><input type="text" class="form-control" name="cma_id" value="<?php echo @$cma_test->cma_id;?>"></td>
                            </tr>
                            <tr>
                                <td>CUST_ID</td>
                                <td><input type="text" class="form-control" name="cust_id" value="<?php echo @$cma_test->cust_id;?>"></td>
                            </tr>
                            <tr>
                                <td>CONTACT_ID ISSUE</td>
                                <td><input type="text" class="form-control" name="contact_id_issue" value="<?php echo @$cma_test->contact_id_issue;?>" ></td>
                            </tr>
                            <tr>
                                <td>CUST_NAME</td>
                                <td><input type="text" class="form-control" name="cust_name" value="<?php echo @$cma_test->cust_name;?>" ></td>
                            </tr>
                            <tr>
                                <td>AGENT_NAME</td>
                                <td><input type="text" class="form-control" name="agent_name" value="<?php echo @$cma_test->agent_name;?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" class="btn btn-primary"  value="Update"/>
                                </td>
                            </tr>
                            </tbody>
                            
                        </table>
                </form>
                </div>
                -->
      
    </div> <!-- end row -->    

</div>