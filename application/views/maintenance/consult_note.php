
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Upload Consult Note</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">
  
        <div class=" col-lg-6 col-sm-6 col-md-6">
                <?php echo form_open('', ' id="carForm" class="form-horizontal"   enctype="multipart/form-data"  style="padding:10px" method="post"');?>                                         
                                    
                                    <?php if($this->user_lvl.$this->user_sub_lvl!=52):?>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2" style="text-align:left">Doctor</label>
                                        <div class="col-lg-5">
                                            <select name="doctor" class="form-control">

                                            <?php foreach($doctors as $id=>$name):?>
                                            <option value="<?php echo $id;?>"><?php echo $name;?></option>
                                            <?php endforeach;?>
                                                    
                                            </select>
                                        </div>
                                    </div> 
                                <?php else:?>
                                    <input type="hidden" name="doctor" value="<?php echo $this->user_id;?>">
                                <?php endif;?>
                                 <div class="form-group">
                                        <label class="control-label col-lg-2" style="text-align:left"></label>
                                        <div class="col-lg-5">
                                            <input name="consult_note" type="file">
                                        </div>
                                    </div>
                                 <div class="form-group">
                                        <label class="control-label col-lg-2" style="text-align:left"></label>
                                        <div class="col-lg-5">
                                           <button class="btn btn-success  btn-sm">Submit</button>
                                    </div>
                 </form>

        </div>
    </div> <!-- end row --> 
    <div class="row">
          <div class="col-md-12">
              <div class="alert alert-info">
                  <strong>Please follow the column format below .</strong> 
                </div>
          </div>  
    </div>  
    <div class="row">

        <div class="col-md-12" >
            
     
                 <table class="table table-condensed table-bordered call_notes_tbl" style="margin:10px">
                        <thead>
                          <tr>
                            <th>Illness</th>
                            <th>Call Centre/Triage Note</th>
                            <th>Present Complaint</th>
                            <th>Past Medical History</th>
                            <th>Current Medications</th>
                            <th>Immunisations</th>
                            <th>Allergies</th>
                            <th>Social/Family History</th>
                            <th>Observations</th>
                            <th>Diagnosis</th>
                            <th>Plan</th>
                            <th>Medication Administered</th>
                            <th>Mediated Prescribed</th>
                            <th>Followup with GP</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($consult_note) && count($consult_note) > 0 ):

                            foreach ($consult_note as $row) :
                                
                        ?>
                        <tr>
                            <td><?php echo @$row->prognosis;?></td>
                            <td><?php echo @$row->call_center_triage;?></td>
                            <td><?php echo @$row->present_complain;?></td>
                            <td><?php echo @$row->past_med_history;?></td>
                            <td><?php echo @$row->current_medication;?></td>
                            <td><?php echo @$row->immunisations;?></td>
                            <td><?php echo @$row->allergies;?></td>
                            <td><?php echo @$row->family_history;?></td>
                            <td><?php echo @$row->observations;?></td>
                            <td><?php echo @$row->diagnosis;?></td>
                            <td><?php echo @$row->plan;?></td>
                            <td><?php echo @$row->medication_administired;?></td>
                            <td><?php echo @$row->mediated_perscribed;?></td>
                            <td><?php echo @$row->follow_up_with_gp;?></td>
                        </tr>
                        <?php
                            endforeach;
                         endif;

                         ?>
        
                        </tbody>
                  </table>


        </div>
        
    </div>

</div>
