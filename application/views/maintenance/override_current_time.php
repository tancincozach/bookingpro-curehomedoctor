
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Overide Current Time (Only works on test)</h3>

 

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">
                
           
        <div class="col-md-6">
            <?php 
            
            echo form_open('maintenance/override_current_time', ' class="form-horizontal"  style="padding:10px" method="post"');?>    
                
                <table class="table table-hover table-bordered">            
                    <tbody>                       
                        <tr>
                            <td>Format</td>
                            <td>2018-03-01 18:00:00</td>
                        </tr>                      
                        <tr>
                            <td>DATE TIME</td>
                            <td><input type="text" class="form-control" name="dt_val" value="<?php echo @$oct;?>"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" class="btn btn-primary"  value="Update"/>
                            </td>
                        </tr>
                    </tbody>
                    
                </table>
            </form>
        </div>
                
      
    </div> <!-- end row -->    

</div>