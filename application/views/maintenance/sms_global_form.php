 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">SMS Global - Test</h3>
 

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>
    <?php echo ($this->session->flashdata('error') != '')?'<p class="flash-mesg alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>

   
    <div class="row">

        <div class="col-md-12 ">       

            <?php echo form_open('maintenance/smsglobal', ' role="form"  id="smsglobal-form " method="post" class="bg-grey form-horizontal" '); ?>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="sms-to" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sms-to" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="sms-msg" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Send SMS</button>
                        </div>
                    </div>                    
               </div>     
            </form>
            </div>
        </div>
    </div> <!-- end row -->    