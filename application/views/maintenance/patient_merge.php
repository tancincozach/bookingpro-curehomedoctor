 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Compare Patient</h3>

    <?php if( SHOW_MILESTONE ): ?>

    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>


<?php 


?>

    <div class="row">
    	<div class="col-md-6">    		                         
                    <div class="row">
                        <div class="col-sm-6  col-md-8">
                            <label  >First Name :</label>
                            <span><?php echo @$patient['firstname'];?></span>
                        </div>
                        <div class="col-sm-6  col-md-8">
                            <label >Last Name :</label>
                            <span><?php echo @$patient['lastname'];?></span>                            
                        </div>    
                        <div class="col-sm-6  col-md-8">
                            <label for="dob" >Date of Birth :</label>                            
                            <span><?php echo @$patient['dob'];?></span>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <label >Mobile Phone :</label>                            
                            <span><?php echo @$patient['phone'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Email :</label>                            
                            <span><?php echo @$patient['email'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Street Address :</label>                            
                            <span><?php echo @$patient['street_addr'];?></span>
                        </div>
                       <div class="col-sm-6 col-md-8">
                            <label >Suburb :</label>                            
                            <span><?php echo @$patient['suburb_name'];?></span>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <label >PostCode :</label>                            
                            <span><?php echo @$patient['postcode'];?></span>
                        </div>
                            <div class="col-sm-6 col-md-8">
                            <label >Medicare Number :</label>                            
                            <span><?php echo @$patient['medicare_number'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Medicare Ref :</label>                            
                            <span><?php echo @$patient['medicare_ref'];?></span>
                        </div>
                    
                        <div class="col-sm-6 col-md-8">
                            <label >Medicare Expiry :</label>                            
                            <span><?php echo @$patient['medicare_expiry'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >DVA :</label>                            
                            <span><?php echo @$patient['dva'];?></span>
                        </div>                      
                    </div>
                    <div class="row">
                      <div class="col-sm-12 col-md-12">
                               <table class="table table-hover  table-row-order table-patient">
                                <thead>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Area</th>
                                    <th>Symptoms</th>                                
                                    <th>Doctor</th>  
                                    <th>Chaperone</th>  
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>tes</td>
                                        <td>tes</td>
                                        <td>test</td>
                                        <td>test</td>
                                        <td>test</td>
                                        <td>test</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                    </div>
    	</div>
    	   <div class="col-md-6">  
           <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
           <?php  if($duplicates!=0 ) {


            $ctr=1;
                    foreach(@$duplicates as $patient){

                ?>
                  <div class="panel panel-default">
                    <div class="panel-heading panel-heading-hcd" role="tab" id="patient<?php echo $ctr?>">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $ctr?>" aria-expanded="true" aria-controls="collapse<?php echo $ctr?>">
                          Record # <?php echo $patient['patient_id'];?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse<?php echo $ctr?>" class="panel-collapse collapse <?php if($ctr==1){?>in<?php }?>" role="tabpanel" aria-labelledby="patient<?php echo $ctr?>">
                      <div class="panel-body">
                         <div class="row">  
                          <div class="col-sm-6  col-md-8">
                            <label for="dob" >Firstname :</label>                            
                            <span><?php echo @$patient['firstname'];?></span>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <label >Lastname:</label>                            
                            <span><?php echo @$patient['lastname'];?></span>
                        </div>
                        <div class="col-sm-6  col-md-8">
                            <label for="dob" >Date of Birth :</label>                            
                            <span><?php echo @$patient['dob'];?></span>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <label >Mobile Phone :</label>                            
                            <span><?php echo @$patient['phone'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Email :</label>                            
                            <span><?php echo @$patient['email'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Street Address :</label>                            
                            <span><?php echo @$patient['street_addr'];?></span>
                        </div>
                       <div class="col-sm-6 col-md-8">
                            <label >Suburb :</label>                            
                            <span><?php echo @$patient['suburb_name'];?></span>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <label >PostCode :</label>                            
                            <span><?php echo @$patient['postcode'];?></span>
                        </div>
                            <div class="col-sm-6 col-md-8">
                            <label >Medicare Number :</label>                            
                            <span><?php echo @$patient['medicare_number'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >Medicare Ref :</label>                            
                            <span><?php echo @$patient['medicare_ref'];?></span>
                        </div>
                    
                        <div class="col-sm-6 col-md-8">
                            <label >Medicare Expiry :</label>                            
                            <span><?php echo @$patient['medicare_expiry'];?></span>
                        </div>
                         <div class="col-sm-6 col-md-8">
                            <label >DVA :</label>                            
                            <span><?php echo @$patient['dva'];?></span>
                        </div>
                      
                    </div>
                      <div class="row">
                      <div class="col-sm-12 col-md-12">
                                      <table class="table table-hover  table-row-order table-patient">
                                <thead>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Area</th>
                                    <th>Symptoms</th>                                
                                    <th>Doctor</th>  
                                    <th>Chaperone</th>  
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>tes</td>
                                        <td>tes</td>
                                        <td>test</td>
                                        <td>test</td>
                                        <td>test</td>
                                        <td>test</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                    </div>
                      </div>
                    </div>
                  </div>
                <?php 
                    $ctr++;
                       }
                    }

                ?>
                 
                </div>                                 
                   
        </div>
    </div>

        </div>
