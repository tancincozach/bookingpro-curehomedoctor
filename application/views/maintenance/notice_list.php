 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">General Notices</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

     
            
          

   

        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Content</th>                                                                    
                        <th>Status</th>                           
                        <th>Agent Name</th>                           
                        <th>Expiry</th>                           
                        <th>Date Updated</th>                           
                        <th>Action</th>        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($general) &&  count($general) > 0) 
                        {
                       foreach($general as $row){ ?>
                            <tr id="tr-<?php echo @$row->id;  ?>">                                          
                                  <td><?php echo @$row->content_html; ?></td>   
                                  <td><?php echo (isset($row->notice_status) && $row->notice_status==1 ? 'Active':'Inactive'); ?></td>   
                                  <td><?php echo @$row->agent_name; ?></td>   
                                  <td><?php echo @$row->expiry; ?></td>   
                                  <td><?php echo @$row->date_updated; ?></td>   
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/notice_form/?notice=<?php echo @$row->id; ?>" >Edit</a></li>                                            
                                            <li><a  onclick="javascript:contact.view_contact(<?php echo @$row->id; ?>);" >View</a></li>                                            
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No General Notice(s) found.</td>
                        </tr>
                    <?php
                    }

                    ?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    
  <!--   <h3 class="page-header">Roster Notices</h3>
    <div class="row">
        <div class="col-md-12">
                        <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Content</th>                                                                    
                        <th>Status</th>                           
                        <th>Agent Name</th>                           
                        <th>Expiry</th>                           
                        <th>Date Updated</th>                           
                        <th>Action</th>        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($roster) &&  count($roster) > 0) 
                        {
                       foreach($roster as $row){ ?>
                            <tr id="tr-<?php echo @$row->id;  ?>">                                            
                                  <td><?php echo @$row->content_html; ?></td>   
                                  <td><?php echo (isset($row->notice_status) && $row->notice_status==1 ? 'Active':'Inactive'); ?></td>   
                                  <td><?php echo @$row->expiry; ?></td>   
                                  <td><?php echo @$row->agent_name; ?></td>   
                                  <td><?php echo @$row->date_updated; ?></td>   
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/contact_form/?contact=<?php echo @$row->id; ?>" >Edit</a></li>                                            
                                            <li><a  onclick="javascript:contact.view_contact(<?php echo @$row->id; ?>);" >View</a></li>                                            
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No Roster notice(s) found.</td>
                        </tr>
                    <?php
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div> -->

</div>