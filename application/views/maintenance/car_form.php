
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Add/Edit Car</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">
  
        <div class=" col-lg-6 col-sm-6 col-md-6">
            <?php echo form_open('maintenance/car', ' id="carForm" class="form-horizontal"  style="padding:10px" method="post"');?>         
            <input type="hidden" name="area" value="<?php echo @$area_id;?>"/>
            <input type="hidden" name="car" value="<?php echo @$car_details->car_id;?>"/>
            <input type="hidden" name="formtype" value="<?php echo (isset($car_details) ? 'update':'new');?>"/>
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Name</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="user_fullname" name="car_name" value="<?php echo @$car_details->car_name;?>" required> 
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Plate No.</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="car_plateno" name="car_plateno" value="<?php echo @$car_details->car_plateno;?>" required>
                    </div>
                </div>
                
             <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Email.</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="car_email" name="car_email" value="<?php echo @$car_details->car_email;?>" required>
                    </div>
             </div>      

              <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Send Appts</label>
                    <div class="col-lg-5">
                            <select name="send_email" class="form-control">
                                <option value ="">Select</option>
                                <option value="1" <?php echo isset($car_details->send_email) && intval($car_details->send_email)==1 ? 'SELECTED':'';?>>Yes</option>
                                <option value="0" <?php echo isset($car_details->send_email) && intval($car_details->send_email)==0 ? 'SELECTED':'';?>>No</option>
                            </select>                     
                    </div>
             </div>
                
             <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Mobile</label>
                    <div class="col-lg-5">
                        <input type="text" placeholder="" class="form-control" id="car_plateno" name="car_mobile" value="<?php echo @$car_details->car_mobile;?>" required>
                    </div>
            </div>

             <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Send Appts</label>
                    <div class="col-lg-5">
                            <select name="send_sms" class="form-control">
                                <option value="">Select</option>
                                <option value="1" <?php echo isset($car_details->send_text) && intval($car_details->send_text)==1 ? 'SELECTED':'';?>>Yes</option>
                                <option value="0" <?php echo isset($car_details->send_text) && intval($car_details->send_text)==0 ? 'SELECTED':'';?>>No</option>
                            </select>                     
                    </div>
             </div>

                    <div class="form-group">
                    <label class="control-label col-lg-2" style="text-align:left">Area</label>
                    <div class="col-lg-5">
                        <select class="form-control" name="area_id">
                        <option value="">SELECT AREA</option>
                        <?php 
                        if(isset($areas) && count($areas) > 0){
                             foreach($areas as $area_id=>$area_name) {                            
                            ?>
                            <option value="<?php echo $area_id;?>" <?php echo (@$car_details->area_id==$area_id ? 'SELECTED':'') ?>><?php echo $area_name;?></option>
                         <?php }
                        }?>
                        </select>
                    </div>
                </div>

          <!--       <div>
                    <label class="control-label" style="text-align:left">Drop Box Login Detail</label>

                    <div class="form-group">
                        
                        <div class="col-md-7">
                            <input type="text" placeholder="Email" class="form-control" id="dropbox_email" name="dropbox_email" value="<?php echo @$car_details->dropbox_email;?>" required>
                            <input type="text" placeholder="Username" class="form-control" id="dropbox_login" name="dropbox_login" value="<?php echo @$car_details->dropbox_login;?>" required>
                            <input type="text" placeholder="Password" class="form-control" id="dropbox_pass" name="dropbox_pass" value="<?php echo @$car_details->dropbox_pass;?>" required>
                            <input type="text" placeholder="Secret" class="form-control" id="dropbox_secret" name="dropbox_secret" value="<?php echo @$car_details->dropbox_secret;?>" required>
                            <input type="text" placeholder="Api Key" class="form-control" id="dropbox_key" name="dropbox_key" value="<?php echo @$car_details->dropbox_key;?>" required>
                        </div>
                    </div>
                </div> -->
             <div class="form-group">
                    <input type="hidden" id="formtype" name="formtype" value="" />   
                <?php
                   if(isset($car_details->car_id))
                    {
                    ?>  
                    <input type="hidden" name="user" value="<?php echo @$user->user_id;?>" />  
                    <button type="submit"  alt="update" class="btn btn-success action-button">Update</button>
                    <button alt="delete" class="btn btn-danger action-button delete" >Delete</button>
                    <?php
                    }
                    else
                    {
                     ?>
                       <button type="submit"  alt="new" class="btn btn-success action-button ">Add</button>
                     <?php

                    }
                ?>                            
                <button class="btn btn-primary action-button cancel">Cancel</button>
             </div>
                </form>
        </div>
    </div> <!-- end row -->    

</div>