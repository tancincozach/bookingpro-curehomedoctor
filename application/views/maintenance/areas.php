
<script>

    var  misc_data = { 
                        user_id:<?php echo $misc_data['user_id']?>,
                        agent_name:'<?php echo $misc_data['agent_name']?>'
                     }


</script> 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Areas</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to setup areas, suburb on area, add cars, add chaperone
                </dd>
                <dt>
                    Features:
                </dt>
                <dd>                    
                    <ul>
                        <li>Suburb button
                            <ul>
                                <li>Show popup Suburb</li>                                
                                <li>Add/Edit</li>                                
                            </ul>
                        </li>
                        <li>Able upload suburb from csv</li>
                    </ul>
                </dd>
                <dt>
                    Status:
                </dt>
                <dd>
                    Partial
                </dd>
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>

    
    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    

    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open('maintenance/area', 'class="form-inline box_border pad_5 bg-info" id="newAreaForm" method="post"'); ?>
                <input type="hidden" name="formtype" value="<?php if(count($area)==0) :?>new<?php else:?>update<?php endif;?>" />
                <input type="hidden" name="area" value="<?php echo @$area[0]->area_id?>" />
                <div class="form-group">
                    <label for="area_name"><?php if(count($area)==0) :?>Create New Area<?php else:?>Update Area<?php endif;?></label>
                    <div class="input-group">                      
                        <input type="text" name="area_name" class="form-control" placeholder="Type New Area" value="<?php echo @$area[0]->area_name?>">  
                    </div>
                    <div class="input-group">                      
                    <select class="form-control" name="timezone">
                    <option selected="selected" value="">Select Timezone</option>
                        <option value="Australia/Adelaide" <?php echo ( @$area[0]->timezone=='Australia/Adelaide' ? "selected":""); ?> >Australia/Adelaide</option>
                        <option value="Australia/Broken_Hill" <?php echo ( @$area[0]->timezone=='Australia/Broken_Hill' ? "selected":""); ?>>Australia/Broken_Hill</option>
                        <option value="Australia/Hobart" <?php echo ( @$area[0]->timezone=='Australia/Hobart' ? "selected":""); ?>>Australia/Hobart</option>
                        <option value="Australia/Lord_Howe" <?php echo ( @$area[0]->timezone=='Australia/Lord_Howe' ? "selected":""); ?>>Australia/Lord_Howe</option>
                        <option value="Australia/Melbourne" <?php echo ( @$area[0]->timezone=='Australia/Melbourne' ? "selected":""); ?>>Australia/Melbourne</option>
                        <option value="Australia/NSW" <?php echo ( @$area[0]->timezone=='Australia/NSW' ? "selected":""); ?>>Australia/NSW</option>
                        <option value="Australia/Perth" <?php echo ( @$area[0]->timezone=='Australia/Perth' ? "selected":""); ?>>Australia/Perth</option>
                        <option value="Australia/Queensland" <?php echo ( @$area[0]->timezone=='Australia/Queensland' ? "selected":""); ?>>Australia/Queensland</option>
                        <option value="Australia/Sydney" <?php echo ( @$area[0]->timezone=='Australia/Sydney' ? "selected":""); ?>>Australia/Sydney</option>
                        <option value="Australia/Victoria" <?php echo ( @$area[0]->timezone=='Australia/Victoria' ? "selected":""); ?>>Australia/Victoria</option>
                        <option value="Asia/Manila" <?php echo ( @$area[0]->timezone=='Asia/Manila' ? "selected":""); ?>>Asia/Manila</option>
                    </select></div>
                </div>
                <?php
                    $custom_sched_closed = (in_array(@$area[0]->custom_sched_closed, array('', ' ', '00:00:00')))?'': $area[0]->custom_sched_closed;                    
                ?>
                <!-- <div class="form-group">
                    <label for="custom_sched_closed">(Optional) HH:mm:ss</label>   
                    <input type="text" name="custom_sched_closed" id="custom_sched_closed" value="<?php echo $custom_sched_closed; ?>" style="width: 85px !important" class="form-control" placeholder="Custom Schedule Closed" title="Custom Schedule Closed aside from default midnight" >  
                </div> -->
                <button type="submit" class="btn btn-primary ">Submit</button>         

            <div class="btn-group pull-right">
                 <a class="btn btn-primary" href="maintenance/areas/?filter=all">All</a>  
               <a class="btn btn-success" href="maintenance/areas">Active</a>  
                <a class="btn btn-danger" href="maintenance/areas/?filter=inactive">Inactive</a>         
            </div>       
            </form>
        </div>

        <div class="col-md-12">        
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Name</th>                     
                        <th>Options</th>                        
                        <th>Status</th>
                        <th>Re-Open</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
            <?php if(isset($record) &&  count($record) > 0) :?>

                    <?php foreach($record as $row): ?>
                    <tr>
                        <td><?php echo $row->area_name; ?></td>
                        <td>                        
                            <div class="btn-group">
                                <button type="button" class="btn btn-<?php if($row->suburb_count > 0):?>primary <?php else:?>danger<?php endif;?> btn-xs ">Suburb <span class="badge "><?php echo $row->suburb_count;?></span></button>
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" alt="<?php echo $row->area_id?>">
                                    <li><a class="add-suburb"  alt="add"  style="cursor:pointer">Manual Add Suburb</a></li>
                                    <li><a class="add-suburb"  alt="upload" style="cursor:pointer" >Upload Suburb</a></li> 
                                    <li><a class="add-suburb" alt="view" style="cursor:pointer">View Suburb</a></li> 
                                </ul>
                            </div>
                            <button type="button" onclick="javascript: areas.view_chaperone(<?php echo $row->area_id?>);" class="btn btn-<?php if($row->chaperone_count > 0):?>primary <?php else:?>danger<?php endif;?>  btn-xs" >Chaperone <span class="badge "><?php echo $row->chaperone_count;?></span></button>


                            <!-- <button type="button" class="btn btn-<?php if($row->car_count > 0):?>primary <?php else:?>danger<?php endif;?>  btn-xs" onclick="window.location='maintenance/cars/?area=<?php echo $row->area_id;?>'" >Cars&nbsp;<span class="badge "><?php echo $row->car_count;?></span></button> -->
                             
                            <!-- discontinue 20180124 -->
                            <!-- <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs">Availability </button>
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" alt="<?php echo $row->area_id?>">

                                <?php if($row->open_status!=0):?>     
                                    <li><a onclick="areas_availability.setAvailability(<?php echo $row->area_id?>);"  style="cursor:pointer">Set</a></li>
                                <?php endif;?>

                                <?php if($row->open_status==0):?>                                        
                                    <li><a onclick="areas_availability.reopen_area_activity(<?php echo $row->area_avail_id?>);" style="cursor:pointer">Re-Open </a></li> 
                                <?php endif;?>

                                    <li><a href="maintenance/availability_history/?area=<?php echo $row->area_id ?>" style="cursor:pointer">View History</a></li> 
                                </ul>
                            </div> -->

                             <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs"><?php echo isset($row->timezone) ? $row->timezone:'<strong class="text-danger">No Timezone Set</strong>' ?> </button>
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" alt="<?php echo $row->area_id?>">

                                    <li>
                                        <a  style="cursor:pointer" onclick="areas_availability.set_area_timezone(<?php echo $row->area_id?>)">
                                        <?php echo isset($row->timezone) ? 'Change Timezone':'Asign Timezone';?>                                            
                                        </a>
                                    </li>                                                                    
                                </ul>

                            </div>
                            
                            <a href="maintenance/area_form_endscript/<?php echo $row->area_id; ?>" class="btn btn-primary btn-xs"> 
                                <span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Script
                            </a>

                             <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs"><?php echo isset($row->region_type) ? $row->region_type:'<strong class="text-danger">Please set Region Type</strong>' ?> </button>
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" alt="<?php echo $row->region_type?>">

                                    <?php if( $row->region_type != 'METRO' ): ?>
                                    <li>
                                        <a  style="cursor:pointer" onclick="areas.set_region_type(<?php echo $row->area_id?>, 'METRO', '<?php echo $this->agent_name; ?>', '<?php echo $this->user_id; ?>')"> Change to <strong>METRO</strong> </a>
                                    </li>  
                                    <?php endif; ?>

                                    <?php if( $row->region_type != 'REGIONAL' ): ?>
                                    <li>
                                        <a  style="cursor:pointer" onclick="areas.set_region_type(<?php echo $row->area_id?>, 'REGIONAL', '<?php echo $this->agent_name; ?>', '<?php echo $this->user_id; ?>')"> Change to <strong>REGIONAL</strong> </a>
                                    </li>                                                                    
                                    <?php endif; ?>
                                </ul>

                            </div>

                            <?php if( !in_array(@$row->custom_sched_closed, array('', ' ', '00:00:00')) ): ?>
                            <span class="badge">Closing Times: <?php echo $row->custom_sched_closed; ?></span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <!-- <span class="label  <?php  echo ($row->open_status==1 ? "label-success":"label-danger");?>"><?php echo ($row->open_status==1 ? "Open":"Closed"); ?></span> -->

                            
                            <button type="button" class="btn <?php echo ($row->open_status==1 ? "btn-success":"btn-danger"); ?> btn-xs" onclick="areas_availability.setAvailability2(<?php echo $row->area_id?>, '<?php echo $this->agent_name; ?>', '<?php echo $this->user_id; ?>')" >                                
                                <?php echo ($row->open_status==1 ? "Open":"Closed"); ?>
                            </button>

                        </td>
                        <td>
                            <?php

                                if( !$row->open_status ) {
                                    echo ($row->reopen_dt_start != '' AND $row->reopen_dt_start != '0000-00-00 00:00:00')?date('d/m/Y H:i', strtotime($row->reopen_dt_start)):'';
                                    echo ($row->reopen_dt_end != '' AND $row->reopen_dt_end != '0000-00-00 00:00:00')?' - '.date('d/m/Y H:i', strtotime($row->reopen_dt_end)):'';

                                }

                            ?>
                        </td>
                        <td align="right">

                            <?php $status = $row->area_status; ?>
                         <!--    <label class="label <?php echo ($status)?'label-success':'label-danger' ?>"><?php echo ($status)?'Active':'Inactive' ?></label> -->

                            
                            <div class="btn-group">
                                <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs" style="width: 65px"><?php echo ($status)?'Active':'Inactive' ?></button>
                                <button type="button" class="btn <?php echo ($status)?'btn-success':'btn-danger' ?> btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="maintenance/area/?<?php echo (!$status)?'activate':'disable' ?>=<?php echo $row->area_id ;?>" onclick="return confirm('Confirm?')"><?php echo (!$status)?'Activate':'Disable' ?></a></li>
                                </ul>
                            </div>
                            
                            <a href="maintenance/areas/?area=<?php echo $row->area_id ;?>"class="btn btn-info btn-xs" role="button" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            
                        </td>                  
                    </tr>
                    <?php endforeach;
                    else:?>
                        <tr>
                            <td colspan="3">No area(s) found.</td>
                        </tr>                        
                    <?php 
                    endif;
                 ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>
<input type="hidden" id="current_date" value="<?php echo $current_date;?>"/>
<input type="hidden" id="current_date_formatted" value="<?php echo date('d/m/Y',strtotime($current_date));?>"/>