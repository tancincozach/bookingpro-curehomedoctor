 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Patient List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open_multipart('maintenance/import_patient', 'class="form-inline box_border pad_5 bg-info" id="PatientListForm" method="post"');?>         
         <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary btn-file">
                        Browse… <input type="file"  name="patient" multiple="">
                    </span>
                </span>
                <input type="text" readonly="" class="form-control" placeholder="Import Patient" >
            </div>
            <div class="btn-group ">
                 <input type="submit" class="btn btn-default" value="Upload" >
           
                
            </div>
        <div class="form-group " >
                    <div class="input-group">
                    <input type="text" name="search_patient" class="form-control" placeholder="Search Patient">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary search">Search</button>                        
                    </div>
                    <div class="btn-group">                        
                        <a href="maintenance/patients/" class="btn btn-default ">Reset</a>
                    </div>
                          
            </div>
            <div class="btn-group pull-right">
               <a class="btn btn-primary" href="maintenance/patient_form/">Add patient</a>  
                <a class="btn btn-success" href="maintenance/patients/?filter=show_duplicates">Show Duplicates</a>         
                <!-- <a class="btn btn-info" href="maintenance/patients/?download=true">Download Patient(s)</a>          -->
                <!-- Split button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-info">Download</button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>


                  <ul class="dropdown-menu">
                    <li><a href="maintenance/patients/?download=true">All</a></li>
                    <?php foreach($areas as $row):?>
                        <li><a href="maintenance/patients/?download=true&dl=<?php echo $row->area_id;?>"><?php echo $row->area_name;?></a></li>
                        <li role="separator" class="divider"></li>
                   <?php endforeach;?> 


                  </ul>
                </div>
            </div>
            </form>
        </div>


        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Patient #</th>                                            
                        <th>Name</th>                                            
                        <th>Address</th>                                            
                        <th>Suburb</th>                                            
                        <th>Landline</th>                                            
                        <th>Mobile</th>                                            
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo $row->patient_id; ?>">        
                                  <td><?php echo $row->patient_ext_id;?></td>                         
                                  <td><?php echo $row->firstname .' '.  $row->lastname; ?></td>                         
                                  <td><?php echo $row->street_addr; ?></td>    
                                  <td><?php echo $row->suburb; ?></td>    
                                  <td><?php echo $row->phone; ?></td>    
                                  <td><?php echo $row->mobile_no; ?></td>    
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/patient_form/?&patient=<?php echo $row->patient_id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/patient/?formtype=delete&patient=<?php echo $row->patient_id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                       
                                            <li><a href="maintenance/patient_merge/?patient=<?php echo $row->patient_id; ?>" >Compare Duplicates</a></li>                                                                                       

                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No patient(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>