 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Patient Form</h3>

    <?php if( SHOW_MILESTONE ): ?>


    

    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<p class="flash-mesg alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>
<?php echo ($this->session->flashdata('error') != '')?'<p class="flash-mesg alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('fmesg').'</p>':''; ?>

   
    <div class="row">

        <div class="col-md-12 ">       

            <?php echo form_open('maintenance/patient', ' role="form"  id="patient-form" method="post" class="bg-grey" '); ?>
            
                    <div class="row">
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="first-name" class="control-label">First Name*</label>
                            <input type="text" placeholder="" class="form-control" id="firstname" name="firstname" value="<?php echo @$patient_info->firstname;?>" required>
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="last-name" class="control-label">Last Name*</label>
                            <input type="text" placeholder=" " class="form-control" id="lastname" name="lastname" value="<?php echo @$patient_info->lastname;?>" required>
                        </div>    
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="dob" class="control-label ">Date of Birth*</label> <br />
                            <input type="text" placeholder="" class="form-control datepicker" id="dob" name="dob" value="<?php echo date('d/m/Y',strtotime(@$patient_info->dob));?>" data-format="DD/MM/YYYY" data-template="D MMM YYYY">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="phone">Landline Phone*</label>
                            <input type="text" placeholder="" class="form-control" id="phone" name="phone" value="<?php echo @$patient_info->phone;?>" required>
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="mobile_no">Mobile Phone*</label>
                            <input type="text" placeholder="" class="form-control" id="mobile_no" name="mobile_no" value="<?php echo @$patient_info->mobile_no;?>" required>
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" placeholder="" class="form-control" id="email" name="email" value="<?php echo @$patient_info->email;?>"> 
                        </div>    
                    </div>
                    <div class="row">
                         <div class="form-group col-sm-12  col-md-4">
                            <label for="street_addr">Street Address</label>
                            <input type="text" placeholder="" class="form-control" id="street_addr" name="street_addr" value="<?php echo @$patient_info->street_addr;?>">
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="last-name">Suburb</label>
                           <select id="suburb" name="suburb" class="form-control">
                                <option value="">Select</option>    
                                <?php foreach( $suburb as $rowsuburb){?>
                                <option value="<?php echo $rowsuburb->suburb_name?>" <?php echo (isset($patient_info->suburb) &&  $rowsuburb->suburb_name==$patient_info->suburb) ? "selected":""?>><?php echo $rowsuburb->suburb_name?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="postcode">PostCode</label>
                            <input type="text" placeholder="" class="form-control" id="postcode" name="postcode" value="<?php echo @$patient_info->postcode;?>">
                        </div>  
                       
                    </div>
               
                    <div class="row">
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="medicare_number">Medicare Number</label>
                        <input type="text"  maxlength="10" placeholder="" class="form-control" id="medicare_number" name="medicare_number" value="<?php echo @$patient_info->medicare_number;?>" required>
                         </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="medicare_ref">Medicare Ref</label>
                            <input type="text" placeholder="" class="form-control" id="medicare_ref" name="medicare_ref" value="<?php echo @$patient_info->medicare_ref;?>">
                        </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="medicare_expiry">Medicare Expiry</label>
                            <input type="text" placeholder="" class="form-control" id="medicare_expiry" name="medicare_expiry" value="<?php echo @$patient_info->medicare_expiry;?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12  col-md-4">                
                            <label for="dva">DVA <br/>
                              <input type="checkbox" name="dva" class="form-control"  value="0"  <?php echo (isset($patient_info->dva)  && $patient_info->dva > 0 ? 'checked':'');?>  />
                            </label>
                            
                        </div>
                        <div class="form-group col-sm-12 col-md-8">
                            <label for="wdyhau">Where did you hear about us</label>
                            <select id="hear-about-us" name="wdyhau" class="form-control">
                                <option value="" >Select</option>
                                 <?php foreach( $wyhaulist as $wyhau){?>
                        <option value="<?php echo $wyhau->dropdown_id ?>"  <?php echo (isset($patient_info->wdyhau) &&  $wyhau->dropdown_id=$patient_info->wdyhau) ? "selected":""?>><?php echo $wyhau->dropdown_label ;?></option>
                                <?php }?>
                            </select>
                        </div>      
                    </div>
                      <div class="row">
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="health_insurance_provider">Private Health Insurance Provider</label>
                        <input type="text"  placeholder="" class="form-control" id="health_insurance_provider" name="health_insurance_provider" value="<?php echo @$patient_info->health_insurance_provider;?>" required>
                         </div>
                        <div class="form-group col-sm-12  col-md-4">
                            <label for="health_fund_number">Private Health Fund Number</label>
                            <input type="text" placeholder="" class="form-control" id="health_fund_number" name="health_fund_number" value="<?php echo @$patient_info->health_fund_number;?>">
                        </div>

                    </div>
               <div class="row">
                       <div class="form-group col-sm-4  col-md-4 ">    
                        <label for="dva">Notes <br/>                        
                              <textarea name="patient_notes"  rows="10" cols="53"  id="patient_notes" style="background:#F2DEDE"   class="form-control"><?php echo @$patient_info->patient_notes;?> </textarea> 
                            </label>
                        </div>
                        <div class="form-group col-sm-6  col-md-6 ">                            
                                <label for="em_contact_name pull-left">Emergency Contact Name <br/>                        
                              <input name="em_contact_name"  rows="10" cols="53"  id="em_contact_name" value="<?php echo @$patient_info->em_contact_name;?> " class="form-control">
                            </label>
                            <label for="em_contact_phone pull-left"> Emergency Contact Phone <br/>                        
                              <input name="em_contact_phone"  rows="10" cols="53"  id="em_contact_phone"   class="form-control" value="<?php echo @$patient_info->em_contact_phone;?> ">
                            </label> <br/>   
                            <label for="em_contact_phone pull-left">Relationship                      
                             <input type="text" class="form-control" name="em_relationship"  value="<?php echo @$patient_info->em_relationship; ?>" placeholder="Search by name" required >
                            </label> 
                        </div>
                        <div class="form-group col-sm-6  col-md-6 ">                            
                                <label for="next_kin pull-left">Next of Kin <br/>                        
                              <input name="next_kin"  rows="10" cols="53"  id="next_kin" value="<?php echo @$patient_info->next_kin;?> " class="form-control">
                            </label>
                            <label for="next_kin_contact_phone pull-left"> Next of Kin Contact Phone <br/>                        
                              <input name="next_kin_contact_phone"  rows="10" cols="53"  id="next_kin_contact_phone"   class="form-control" value="<?php echo @$patient_info->next_kin_contact_phone;?> ">
                            </label>  
                        </div>
                        <div class="form-group col-sm-6  col-md-6 ">                            
  
                            <label for="em_contact_phone pull-left">Search Regular Practice                      
                             <input type="text" class="form-control" name="regular_practice" id="practice_autocomplete" value="<?php echo @$patient_info->regular_practice; ?>" placeholder="Search by name" required >
                            </label>  

                        </div>
                    </div>
                  <div class="row">
                       <div class="form-group col-sm-4  col-md-4">   
                           <div class="table-responsive">                               
                               <table class="table">
                                   <tr>
                                       <td>
                                           <select name="banned" class="form-control">                                          
                                            <option value="No" <?php echo (isset($patient_info->banned) &&  $patient_info->banned=='No') ? "selected":""?>>No</option>
                                            <option value="Yes" <?php echo (isset($patient_info->banned) &&  $patient_info->banned=='Yes') ? "selected":""?>>Yes</option>
                                        </select>
                                       </td>
                                       <td><label>Banned Patient</label></td>
                                       <td><input type="text" class="form-control" name="banned_reason" placeholder="Reason" value="<?php echo @$patient_info->banned_reason; ?>" <?php echo isset($patient_info->banned_reason) && $patient_info->banned_reason!="" ? "":"disabled"?>></td>
                                   </tr>
                                   <tr>
                                       <td> <select name="deceased" class="form-control">                                            
                                            <option value="No" <?php echo (isset($patient_info->deceased) &&  $patient_info->deceased=='No') ? "selected":""?>>No</option>
                                            <option value="Yes" <?php echo (isset($patient_info->deceased) &&  $patient_info->deceased=='Yes') ? "selected":""?>>Yes</option>
                                        </select></td>
                                       <td><label>Deceased Patient</label></td>
                                       <td>&nbsp;</td>
                                   </tr>
                               </table>         
                           </div>
                        </div>                           
                    </div>
                     <div class="form-group">

                       <input type="hidden" id="formtype" name="formtype" value="" />    


                            <?php if(!isset( $patient_info)){?>                                
                                <button type="submit"  alt="new" class="btn btn-success action-button ">Add</button>
                                <button alt="cancel" class="btn btn-primary action-button cancel">Cancel</button>
                            <?php
                            }else{
                                ?>                              
                                <input type="hidden" name="patient" value="<?php echo @$patient_info->patient_id;?>" />     
                                <button type="submit"  alt="update" class="btn btn-success action-button">Update</button>
                                <button alt="delete" class="btn btn-danger action-button delete" >Delete</button>
                                <button alt="cancel" class="btn btn-primary action-button cancel">Cancel</button>
                                <?php
                            }
                            ?>
                     </div>
            </form>
            </div>
        </div>
    </div> <!-- end row -->    