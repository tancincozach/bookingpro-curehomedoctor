 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Import Doctor</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

         <div class="col-md-12">        
                     
			<?php echo form_open_multipart('','method="post"');?>         

				<input type="file" name="doctors">
				<input type="submit" class="btn btn-primary"  value="upload"/>
			</form>


			<?php

				if(isset($existing_doctors) && count($existing_doctors) > 0)
				{

					?>
					<table class="table table-striped">
								
								<thead>
								   <th>User ID</th>
								   <th>Username</th>
								   <th>Fullname</th>								   
								</thead>
								<tbody>
								<?php 
									foreach($existing_doctors as $value)
									{
									?>
									 <tr>
									 	<td><?php echo $value['user_id']; ?></td>
									 	<td><?php echo $value['user_name']; ?></td>
									 	<td><?php echo $value['user_fullname'];?></td>									 	
									 </tr>
									 <?php
									 }
									 ?>
								</tbody>

						</table>
					<?php
				}

			?>
        </div>
    </div> <!-- end row -->    

</div>