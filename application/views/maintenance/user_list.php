
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">User List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open_multipart('#', 'class="form-inline box_border pad_5 bg-info" id="userListForm" method="post"');?>         
        <div class="form-group ">
                    <div class="input-group">
                    <input type="text" name="search_user" class="form-control" placeholder="Search User">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary btn-sm search">Search</button>                        
                    </div>
                    <div class="btn-group">
                         <a class="btn btn-default " href="maintenance/users/">Reset</a>
                    </div>
                          
            </div>

            <div class="btn-group pull-right ">
               <a class="btn btn-primary" href="maintenance/user_form/">Add User</a>              
            </div>

            </form>
        </div>


        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Name</th>                                            
                        <th>Username</th>                                            
                        <th>User Level</th>                        
                        <th>Provider #</th>                        
                        <th>Date Updated</th>                                            
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo $row->user_id; ?>">        
                                 <td><?php echo @$row->user_fullname ;   ?></td>         
                                  <td><?php echo @$row->user_name ;?></td>                          
                                  <td><?php echo @$row->user_lvl. '' .(isset($row->sub_level) ? ' / '.$row->sub_level :'')?></td>    
                                   <td><?php echo @$row->prov_num;?></td>    
                                  <td><?php echo @$row->user_updated; ?></td>                                                                        
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">
                                            <li><a href="maintenance/user_form" >Add</a></li>
                                            <li><a href="maintenance/user_form/?&user=<?php echo $row->user_id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/user/?formtype=delete&user=<?php echo $row->user_id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                                                                   
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No users found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>