
<?php if( empty($sidebar_class) ): ?>
<div class="col-xs-2 col-md-2 col-lg-2 ">
    <div class="row nav-sidebar">
<?php endif; ?>
		<ul class="<?php echo @$sidebar_class; ?>">
            <?php if( $this->user_lvl != 5 ): ?>
		    <li class="<?php echo @($sidebar_active=='general')?'active':''; ?>"><a href="maintenance/general_settings">General Settings</a></li>					 			
			<li class="<?php echo @($sidebar_active=='notice')?'active':''; ?>"><a href="maintenance/notices">Notices</a></li>		
			<li class="<?php echo @($sidebar_active=='contact')?'active':''; ?>"><a href="maintenance/contacts">Contacts</a></li>					 
				 
			<li class="<?php echo @($sidebar_active=='areas')?'active':''; ?>"><a href="maintenance/areas">Areas/Suburb</a></li>
			<li class="<?php echo @($sidebar_active=='cars')?'active':''; ?>"><a href="maintenance/cars">Cars</a></li>
			<li class="<?php echo @($sidebar_active=='patient')?'active':''; ?>"><a href="maintenance/patients">Patients</a></li>	 
			<li class="<?php echo @($sidebar_active=='users')?'active':''; ?>"><a href="maintenance/users">Users</a></li>
			<li class="<?php echo @($sidebar_active=='practices')?'active':''; ?>"><a href="maintenance/practices">Practices</a></li>	
		 	<li class="<?php echo @($sidebar_active=='public_holidays')?'active':''; ?>"><a href="maintenance/public_holidays">Public Holidays</a></li>	
			<!-- <li class="<?php echo @($sidebar_active=='schedules')?'active':''; ?>"><a href="maintenance/schedules">Schedules</a></li> -->	

			<li class="<?php echo @($sidebar_active=='cma_setting')?'active':''; ?>"><a href="maintenance/cma_setting">CMA Setting</a></li>	
			
			<li class="divider"></li>

			<li class="<?php echo @($sidebar_active=='consult-note-admin')?'active':''; ?>"><a href="maintenance/consult_notes">Consult Notes</a></li>	
			
			<li class="divider"></li>

			<li class="<?php echo @($sidebar_active=='consult-note-dropdown')?'active':''; ?>"><a href="maintenance/consultnotesdropdown">Consult Notes Dropdown</a></li>	
			<li class="divider"></li>
						 
			<li class="<?php echo @($sidebar_active=='wyhau')?'active':''; ?>"><a href="maintenance/wyhaulist">Where you heard about us</a></li>
			<li class="divider"></li>
			<li class="<?php echo @($sidebar_active=='training_vid')?'active':''; ?>"><a href="maintenance/training_videos">Training Videos</a></li>
			
			<?php if( in_array($_SERVER['SERVER_NAME'], array('127.0.0.1', 'localhost', 'bookingpro-curehomedoctor-test.welldone.net.au')) ): ?>	 
				<li class="divider"></li>
				<li class="<?php echo @($sidebar_active=='override_current_time')?'active':''; ?>"><a href="maintenance/override_current_time">Overide Current Time</a></li>	 				
			<?php endif; ?>
		<?php elseif($this->user_lvl.$this->user_sub_lvl==53):?>		
			<li class="<?php echo @($sidebar_active=='areas')?'active':''; ?>"><a href="maintenance/areas">Areas/Suburb</a></li>
		<?php elseif($this->user_lvl.$this->user_sub_lvl==52):?>		
			<li class="<?php echo @($sidebar_active=='consult-notes')?'active':''; ?>"><a href="maintenance/consult_notes">Consult Notes</a></li>
			<li class="<?php echo @($sidebar_active=='patient')?'active':''; ?>"><a href="maintenance/patients">Patients</a></li>
		<?php else:?>
			<li class="<?php echo @($sidebar_active=='patient')?'active':''; ?>"><a href="maintenance/patients">Patients</a></li>	 
		<?php endif;?>
			
			<li class="divider"></li>
		</ul>

<?php if( empty($sidebar_class) ): ?>	
    </div>
</div>
<?php endif; ?>


