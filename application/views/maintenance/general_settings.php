
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">General Settings</h3>
    
    <div class="row">

        <div class="col-md-12 ">     
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Greeting</a></li>
                <li><a data-toggle="tab" href="#email">ADMIN Email</a></li>
                <li><a data-toggle="tab" href="#sms">Send Email / SMS Config</a></li>
                <li><a data-toggle="tab" href="#max_booking">Chaperone Maximum Booking</a></li>
                <li><a data-toggle="tab" href="#sched_filter">Schedule Filter</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active" style="padding:10px">

                  <?php echo form_open('maintenance/general_settings', 'class="form-inline pad_5" id="newAreaForm" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                        <input type="hidden" name="genaral_id" value="<?php echo @$greeting->id; ?>" />
                        <input type="hidden" name="action" value="greeting" />
                        
                         
                        <textarea class="input-block-level" id="summernote" name="content_html" ><?php echo @stripslashes($greeting->set_value); ?></textarea>

                        <br />

                        <button type="submit" class="btn btn-primary"> Submit</button>
                        
                        <br />             
                    </form>
                </div>
                <div id="email" class="tab-pane fade" style="padding:10px">                            

                    <input type="hidden" name="hcd_email_id" value="<?php echo @$hcd_email->id; ?>" />
                    <input type="hidden" name="action" value="<?php echo @$value->set_name;?>" />
                    <div class="alert alert-info" role="alert"><strong>Note: Separate emails by semi colon eg  ( sample@gmail.com;sample2@gmail.com ) .</strong></div>
                    <?php
                    if(count($email) > 0) {

                        foreach( $email as $key=>$value){
                    ?>
                        <div class="row">

                            <div class="col-md-2">
                                <label> <?php echo str_replace('_',' ',@$value['set_name'])?></label>
                            </div>  

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="<?php echo @$value->set_name; ?>" value="<?php echo @$value['set_value']; ?>" >
                            </div>

                            <div class="col-md-2"> 
                                <button class="btn btn-success submit-email" alt="<?php echo @$value['set_name'];?>">Update</button>
                            </div>

                        </div>
                        <br/>
                    <?php
                        }
                    }
                    ?>

                    <br/>                      
                </div>

                <div id="sms" class="tab-pane fade" style="padding:10px">
                    <div class="row">
                        <div class="col-md-1">
                            <label> Send Email ? </label>
                        </div>
                        <div class="col-md-10">        
                            <input  id="send_email" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" type="checkbox">                    
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-1">
                            <label> Send SMS ? </label>  
                        </div>
                        <div class="col-md-10">        
                            <input  id="send_sms" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" type="checkbox">
                        </div>
                    </div>
                </div>
                
                <div id="max_booking" class="tab-pane fade" style="padding:10px">

                    <?php echo form_open('maintenance/general_settings', 'class="form-inline pad_5" id="max_form_booking_form" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="MAX_BOOKING">MAX BOOKING</label>
                            <input type="number" name="MAX_BOOKING" id="general_chap_max_booking" onkeyup="this.value=this.value.replace(/[^\d]/,'')" class="form-control" value="<?php echo @$max_booking->set_value; ?>" />
                        </div>
                        
                        <button type="button" class="btn btn-success" onclick="general.set_chaperone_max_booking()">Update</button>
                    </form>
                </div>

                <div id="sched_filter" class="tab-pane fade" style="padding:10px">
                    <div class="row">
                        <div class="col-md-3">
                            <label> Remove Schedule filtering</label>
                        </div>
                        <div class="col-md-7">        
                            <input  id="override_sched_filter" <?php echo (@$override_sched_filter->set_value)?'checked':'' ?>  data-toggle="toggle" data-onstyle="success" data-offstyle="danger" type="checkbox">                    
                        </div>
                    </div>                     
                </div>

            </div>
        </div>
    </div>
</div>