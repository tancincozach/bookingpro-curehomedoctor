 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Schedules</h3>
  
    
    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?> 

    <div class="row">
 
        <div class="col-md-12">        
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                                      
                        
                        <th>Mon</th>
                        <th>Tue</th>
                        <th>Wed</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                        <th>Sun</th>
                        <th>Replace Time</th>
                        <th>Timezone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($schedules as $row): ?>
                    <tr> 
                        
                        <td><span class="glyphicon <?php echo ($row->day_mon)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_tue)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_wed)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_thu)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_fri)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_sat)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><span class="glyphicon <?php echo ($row->day_sun)?'glyphicon-ok text-success':'glyphicon-remove text-danger'; ?>" aria-hidden="true"></span></td>
                        <td><?php echo (trim($row->update_time_to_tz)!='')?date('H:i', strtotime($row->update_time_to_tz)):''; ?></td>
                        <td><?php echo $row->timezone; ?></td>
                        <td></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>
 