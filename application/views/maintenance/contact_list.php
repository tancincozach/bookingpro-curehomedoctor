 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Contact List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

     
         <div class="col-md-12">
         <?php echo form_open('maintenance/contacts', 'class="form-inline box_border pad_5 bg-info" id="contactSearchForm" method="post" '); ?>
         <input type="hidden" value="" name="car">
            <input type="hidden" value="new" name="formtype">
             <div class="form-group ">
                    <div class="input-group">
                    <input type="text" placeholder="Search Contact" class="form-control" name="search_contact">              
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-primary  search">Search</button>                        
                    </div>
                    <div class="btn-group">
                       <a class="btn btn-default " href="maintenance/contacts/">Reset</a>                   
                    </div>

                      <div class="btn-group">
                        <a class="btn btn-success " href="maintenance/contact_form/">Add Contact</a>
                    </div> 
            </div>
      <div class="btn-group pull-right">
                 <a href="maintenance/contacts/?filter=all" class="btn btn-primary">All</a>  
               <a href="maintenance/contacts/" class="btn btn-success">Active</a>  
                <a href="maintenance/contacts/?filter=inactive" class="btn btn-danger">Inactive</a>         
            </div> 
            </form>
         </div>
            
          

   

        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Title</th>                                                                    
                        <th>Action</th>                           
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        {
                       foreach($record as $row){ ?>
                            <tr id="tr-<?php echo @$row->contact_id;  ?>">        
                                  <td><?php echo @$row->contact_title; ?></td>                                                           
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/contact_form/?contact=<?php echo @$row->id; ?>" >Edit</a></li>                                            
                                            <li><a  onclick="javascript:contact.view_contact(<?php echo @$row->id; ?>);" >View</a></li>                                            
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No contacts(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>