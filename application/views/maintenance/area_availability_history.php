 
<?php $this->load->view('maintenance/sidebar', @$sidebar); ?>
 
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Area Availability History</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to setup areas, suburb on area, add cars, add chaperone
                </dd>
                <dt>
                    Features:
                </dt>
                <dd>                    
                    <ul>
                        
                    </ul>
                </dd>
                <dt>
                    Status:
                </dt>
                <dd>
                    Partial
                </dd>
                
            </dl>        
        </div> 
    </div>
    <?php endif;
/*        echo "<pre>";
        print_r($record);
        echo "<pre/>";*/
     ?>
    
    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">


        <div class="col-md-12">        
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Effective Date</th>                     
                        <th>Status</th>                     
                        <th>Agent</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($record['message']) &&  count($record['message']) > 0) :

                     foreach($record['message'] as $row):


                      ?>
                    <tr>
                        <td><?php echo @$row['effective_date'];?></td>
                        <td><span class="label  <?php  echo (@$row['is_open']==1 ? "label-success":"label-danger");?>"><?php echo (@$row['is_open']==1) ? 'Open':'Closed'; ?></span></td>
                        <td > <?php echo @$row['agent_name']; ?></td>                  

                    </tr>
                    <?php 

                    endforeach;
                        else : ?>
                        <tr> <td  colspan="2">No suburb found.</td></tr>
                    <?php endif;
                        if(isset($links) && $links!=''):
                    ?>
                        <tr> 
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                        </tr>
                    <?php
                        endif;
                    ?>
                </tbody>
            </table>            
        </div>
    </div> <!-- end row -->    

</div>