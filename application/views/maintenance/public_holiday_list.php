 
<?php  $this->load->view('maintenance/sidebar', @$sidebar); ?>
<div class="col-xs-10 col-md-10 col-lg-10 maintain-content">

    <h3 class="page-header">Public Holiday List</h3>

    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   Purpose for this page is to manage wyhau list
                </dd>
                <dt>
                    Features
                </dt>
                <dd>
                    <ul>
                        <li>Edit</li>                                
                        <li>Add - done</li>                                
                        <li>Sortable drag/drop - done</li>                         
                    </ul>                
                </dd>
                <dt>
                    status
                </dt>
                <dd>
                    Partial                
                </dd>
            </dl>
            
        </div>
    </div>
    <?php endif; ?>

    <?php echo ($this->session->flashdata('fmesg') != '')?'<div class="flash-mesg alert alert-success">'.$this->session->flashdata('fmesg').'</div>':''; ?>    
    <?php echo ($this->session->flashdata('error') != '')?'<div class="flash-mesg alert alert-danger">'.$this->session->flashdata('error').'</div>':''; ?>    
    
    <div class="row">

        <div class="col-md-12 ">               
            <?php echo form_open_multipart('maintenance/public_holiday_actions', 'class="form-inline box_border pad_5 bg-info" id="HolidayListForm" method="post"');?>         
                <input type="hidden" name="formtype" value="<?php if(count(@$holiday)==0) :?>new<?php else:?>update<?php endif;?>" />
                <input type="hidden" name="holiday" value="<?php echo @$holiday->holiday_id?>" />


                <div class="form-group">
                    <label for="holiday_name"><?php if(count(@$holiday)==0) :?>Create New Holiday <?php else:?>Update Holiday<?php endif;?></label>
                    <div class="input-group">                      
                        <input type="text" name="holiday_name" class="form-control" placeholder="Type New Holiday " value="<?php echo @
                        $holiday->holiday_name?>">  
                    
                    </div>
                       <!--  <div class="btn-group btn-sm">
                            <button type="button" class="btn btn-default btn-sm">Areas</button>
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                            <?php
                                                             

                                  $selected_area  =   isset($holiday->area_ids) ? explode(',',$holiday->area_ids): array();
                                    

                                foreach($area as $key=>$val):
                                    $is_check = '';
                                    if( in_array($key, $selected_area))  {
                                        $is_check = "checked";
                                        $s_area[] = $val;
                                    }
                                    
                            ?>
                                <li>
                                    <a href="return false;" class="small noclose" data-value="<?php echo $key; ?>" tabIndex="-1">
                                        <label for="area[]"><input type="checkbox" name="area[]" value="<?php echo $key; ?>" <?php echo $is_check; ?>  />&nbsp;<?php echo $val; ?></label>
                                    </a>
                                </li>
                            <?php endforeach; ?> 
                            </ul>
                        </div>   -->   
                    <div class="input-group">     
                        <input type="text" name="effective_date" class="form-control datepicker" placeholder="Effective Date" value="<?php echo (@$holiday->effective_date != '')?date('d/m/Y',strtotime(@$holiday->effective_date)):'';?>">  
                    </div>
             
                </div>
                   <div class="btn-group ">
                 <button type="submit" class="btn btn-primary ">Submit</button>               
            </div>

         <!--     <div class="form-group  pull-right" >
                    <div class="input-group">
                    <input type="text" name="search_holiday" class="form-control" placeholder="Search Holiday">              
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-primary  search">Search</button>                        
                    </div>
                    <div class="btn-group">
                        <button  class="btn btn-default ">Reset</button>
                    </div>

                          
            </div>  
            -->
            </form>
        </div>

   

        <div class="col-md-12">        
            <table class="table table-hover  table-row-order table-patient ">
                <thead>
                    <tr>
                        <th>Effective Date</th>                                            
                        <th>Holiday</th>                                            
                        <!-- <th>Area</th>   -->                                      
                        <th>Action</th>                           
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if(isset($record) &&  count($record) > 0) 
                        { 

                       foreach($record as $row){ 

                        $areas = $this->holiday->get_concat_areas(array('where'=>"public_holiday.holiday_id={$row->holiday_id}"));
                      // $areas[0]->areas; 
                        ?>
                            <tr id="tr-<?php echo @$row->holiday_id;  ?>">        
                                  <td><?php echo date('d/m/Y', strtotime($row->effective_date)); ?></td>                         
                                  <td><?php echo $row->holiday_name; ?></td>                                        
                                <td>                            
                                     <div class="btn-group">
                                    
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                       
                                        <ul class="dropdown-menu">                                            
                                            <li><a href="maintenance/public_holidays/?&holiday=<?php echo @$row->holiday_id; ?>" >Edit</a></li>
                                            <li><a href="maintenance/public_holiday_actions/?formtype=delete&holiday=<?php echo @$row->holiday_id; ?>"  onclick="return confirm('Are you sure you want to delete this record ?')">Delete</a></li>                                                                                                                                   
                                        </ul>
                                    
                                    </div>

                                    
                                </td>                  
                            </tr>
                        <?php 
                            }
                }else{
                        ?>
                        <tr>
                          <td  colspan="6">No holiday(s) found.</td>
                        </tr>
                    <?php
                    }

                    if(isset($links) && $links!='')
                    {
                    ?>
                    <tr>
                        <td colspan="6" class="text-center"><?php echo @$links; ?></td>
                    </tr>
                    <?php
                    }?>
                </tbody>
            </table>            
        </div>

        <div class="col-md-12 ">
            

                            

                              <h4>Public Holidays (NSW) via common holiday api</h4> 


                            
                              <hr>
                            <table class="table table-hover  table-row-order table-patient ">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Holiday</th>                                
                                            <th>Info</th>                                
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    if(isset($holiday_nsw)):
                                        
                                        foreach( $holiday_nsw as $key=>$row):
                                    ?>
                                        <tr id="holidayitem-<?php echo @$row->id;?>">
                                            <td><?php echo @$row->holiday_date;?></td> 
                                            <td><?php echo stripslashes(@$row->holiday_name);?></td>
                                            <td><?php echo stripslashes(@$row->info);?></td>                                 
                                        </tr>
                                        
                                     <?php
                                        endforeach;
                                     else:
                                        ?>
                                        <tr>
                                           <td colspan="3">No response found.</td> 
                                        </tr>
                                        <?php
                                     endif;
                                    ?>
                                    </tbody>
                                </table>

        </div>

    </div> <!-- end row -->    

</div>