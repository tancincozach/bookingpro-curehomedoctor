 

<?php if( $getvars['step'] == 1 ): ?> 

    <div class="text-center">
        
        <!-- <h3>Thank you for calling House Call Doctor </h3>
        <h4>Please be advised that this call will be recorded for quality and training purposes</h4>
        <h4>This is .........</h4> -->

        <?php echo stripslashes(@$getvars['greeting']->set_value); ?>
        <?php //echo stripslashes(@$this->Commonmodel->get_row_custom_contact(array('contact_title'=>'XMAS ROSTER'))->content_html); ?>

        <hr />

        <div class="jumbotron clearfix" style="padding: 5px">
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard', 'class="" name="dashboard_form" method="get"'); ?>

                    <input type="hidden" name="step" value="2">

                    <p>Can I please have your name</p>
                    <p>
                        <input type="text" name="collection[First_Name]" value="" placeholder="Enter First Name" required/>
                        <input type="text" name="collection[Last_Name]" value="" placeholder="Enter Last Name"/> 
                    </p>            

                    <p>

                    <?php 
                    if( isset($getvars['phone']) && @$getvars['phone'] != '' ){
                        echo 'Can I confirm the number you are calling from is your best contact number';
                    }else{
                        echo 'Can I please have your phone number';
                    }

                    ?>
                    </p>

                    <p><input type="text" name="collection[Phone]" value="<?php echo @$getvars['phone']; ?>" placeholder="Enter phone" required/> </p>

                    <p>Would you like to make a booking ?</p>
                    <p>                         
                        <!-- <a class="btn btn-primary btn-large" href="dashboard">Yes</a>
                        <a class="btn btn-primary btn-large" href="#">No</a> -->
                        <input class="btn btn-primary" type="submit" name="bookingtype" value="Yes">
                        &nbsp;&nbsp;&nbsp;
                        <input class="btn btn-primary" type="submit" name="bookingtype" value="No">

                    </p>

                </form> 
                
                <hr style="border: 1px solid red" />

                <?php echo form_open('dashboard/callcancelled', 'class="" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()"'); ?>

                    <div class="text-center">
                        <input class="btn btn-danger" type="submit" name="calltype" value="Disconnect on Greeting">
                        <input class="btn btn-danger" type="submit" name="calltype" value="Wrong Number">
                    </div>  
                </form> 

            </div>
        </div>

    </div>

 

<?php elseif( $getvars['step'] == 2 ): ?>

    <!-- Move to booking-dashboard/v2-booking-yes.php -->
    <?php if($getvars['bookingtype']=='Yes'): ?>


    <?php if( SHOW_MILESTONE ): ?>
    <div class="alert alert-warning alert-dismissible milestone-box">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="milestone-body">
            <dl>
                <dt>
                    Description:
                </dt>
                <dd>
                   This is for to check if booking is available on a area.
                </dd>
                <dt>
                    Features/Statuses:
                </dt>
                <dd>                    
                    <ul>                        
                        <li>Searching suburbs with autocomplete dropdown is working</li>
                        
                        <li>Buttons
                            <ul>
                                <li>Serviced YES - Bookings Available -> not working, redirect only</li>
                                <li>Serviced YES - No Booking -> not working, redirect only</li>
                                <li>Serviced NO -> redirect only on advice caller page</li>
                            </ul>
                        </li> 
                    </ul>
                </dd>
                 <dt>
                    Plan:
                </dt>
                <dd>
                    Only show buttons if meet the criteria,<br />
                    <strong>Booking Available</strong> - show on if service is available and status is open <br />
                    <strong>No Booking</strong> - on show if serivice is available but status is close <br />
                    <strong>Service No</strong> - only show if no service on area
                </dd>
                
            </dl>        
        </div> 
    </div>
    <?php endif; ?>

    <div class="jumbotron clearfix text-center" >
        <div class="col-lg-12 col-md-12">

            <?php echo form_open('dashboard', 'class="" id="dashboard_form" name="dashboard_form" method="get"'); ?>

                <input type="hidden" name="step" value="3">

                <?php foreach($getvars['collection'] as $field=>$value): ?>
                    <input type="hidden" name="collection[<?php echo $field; ?>]" value="<?php echo $value; ?>" />   
                <?php endforeach; ?>
                
                <input type="hidden" name="collection[Suburb][id]" value="" />   
                <input type="hidden" name="collection[Suburb][area]" value="" />   
                <input type="hidden" name="collection[Suburb][postcode]" value="" /> 
                <input type="hidden" name="collection[Suburb][area_id]" value="" /> 

                <input type="hidden" name="collection[area_open_status]" value="" />   
                <input type="hidden" name="collection[area_avail_id]" value="" />   

                <div class="row">
                    <div class="col-sm-8 col-md-8 col-lg-8">

                        <h4>Can I please have the suburb you are calling from?</h4>
                           
                        <div class="col-lg-5 center-block" style="float:none">  
                            <span class="text-primary bg-primary" style="padding: 2px">Search by SUBURB or POSTCODE</span>
                            <input type="text" class="suburb form-control" id="collection_suburb_suburb"  name="collection[Suburb][suburb]" value="" onblur="this.value=(this.value).trim()" placeholder="Search by SUBURB or POSTCODE" autofocus  /> 
                        </div> 

                        <br />
                        <br />

                        <div style="display:none" id="btn-service-1" class="btn-service-cls">
                            <button class="btn btn-primary" type="submit" name="btn-service" value="1">Serviced YES - Bookings Available</button>
                        </div>

                        <div style="display:none"  id="btn-service-2" class="btn-service-cls">
                            <button  class="btn btn-info" type="submit" name="btn-service" value="2">Serviced YES - No Booking</button>
                        </div>

                        <div style="display:none" id="btn-service-3" class="btn-service-cls">
                            <p>ENSURE the FULL suburb name is entered as this willl log to "suburb not on list", THEN click <strong>"Service NO"</strong> BUTTON to complete call</strong></p>
                            <button class="btn btn-warning" type="submit" name="btn-service" value="3">Serviced NO</button>
                        </div>

                        <div id="suburb-not-display-note">
                            <br />
                            <p><strong>If SUBURB does not display</strong> "DOUBLE CHECK" by starting with 2 characters</p>
                            <p>if still no match</p>
                            <p>1. CLICK on <stong style="color: blue">"no match found-press table or hit enter".</stong></p>
                        </div>

                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4 overnight-staff-options" style="display:none">

                        <h4>Overnight Staff Options  (After Midnight)</h4>

                        <button class="btn btn-danger" type="submit" name="btn-service" value="Mon - Frid after 6pm">Mon - Frid after 6pm</button>
                        <br />
                        <br />
                        <button class="btn btn-danger" type="submit" name="btn-service" value="Saturday after 12pm">Saturday after 12pm</button>
                        <br />
                        <br />
                        <button class="btn btn-danger" type="submit" name="btn-service" value="Sunday & public holiday - anytime">Sunday & public holiday - anytime</button>

                    </div>

                </div>

                <div class="clearfix">
                    <hr style="border: 1px solid red" />
                    <a class="btn btn-danger" href="dashboard/did_not_complete/?<?php echo $getvars['uri_not_complete']; ?>" >DID NOT COMPLETE</a>
                </div>

            </form> 

        </div>
    </div>
    <?php endif; ?> <!-- endif bookingtype Yes -->


    <!-- Move to booking-dashboard/v2-booking-no.php -->
    <?php if($getvars['bookingtype']=='No'): ?>

        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                       This is for Non Booking
                    </dd>
                    <dt>
                        Features:
                    </dt>
                    <dd>                    
                        <ul>                        
                            <li>Links are not yet funtional yet</li>
                        </ul>
                    </dd>
                    
                    
                </dl>        
            </div> 
        </div>
        <?php endif; ?>

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard', 'class="" name="dashboard_form" method="get"'); ?>
                    
                    <input type="hidden" name="step" value="3">

                    <?php foreach($getvars['collection'] as $field=>$value): ?>
                        <input type="hidden" name="collection[<?php echo $field; ?>]" value="<?php echo $value; ?>" />   
                    <?php endforeach; ?>

                    <h4>How can I help you?</h4>
                       
                    <div class="row boxes-center center-block"> 
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="callcenter/?stype=cancel&search2=<?php echo @$getvars['search_more']; ?>">Cancel Booking</a></div>
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="callcenter/?stype=eta&search2=<?php echo @$getvars['search_more']; ?>">Wanting ETA</a></div>
                    </div>

                    <div class="row boxes-center center-block">  
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/call_from_doctor_car/?search2=<?php echo @$getvars['search_more']; ?>">Call from Doctor / Car</a></div>
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/call_from_pharmacy/?search2=<?php echo @$getvars['search_more']; ?>">Call from Pharmacy</a></div>
                        
                    </div>

                     <div class="row boxes-center center-block">
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/general_message_for_admin/?search2=<?php echo @$getvars['search_more']; ?>">General Message for Admin</a></div>                
                     </div>

                     <div class="row boxes-center center-block">
                        <div class="col-xs-10 col-sm-3 col-md-2 col-lg-2 boxes-category"><a href="dashboard/general_call/no_action_required/?search2=<?php echo @$getvars['search_more']; ?>">No Action Required</a></div>                
                     </div>

                </form> 

            </div>
        </div>
    <?php endif; ?> <!-- endif bookingtype No -->


<!-- used for MON-FRID Midnight - 8am etc -->
<?php elseif( $getvars['step'] == 31 ): ?>

    <div class="jumbotron clearfix text-center" >
        <div class="col-lg-12 col-md-12">

            <?php if( in_array($getvars['btn-service'], array('Mon - Frid after 4pm', 'Saturday after 10am', 'Mon - Frid after 6pm', 'Saturday after 12pm') ) ): ?> 
                <?php echo form_open('dashboard/custom_submit', 'class="" id="dashboard_form" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()" '); ?>
                    
                    <input type="hidden" name="step" value="3">
                    <!-- <input type="hidden" name="collection[tran_type]" value="After midnight - booked out - later appt book"> -->
                    
                    <input type="hidden" name="custom_submit_type" value="booking-out-call">

                    <?php
                        foreach($getvars['collection'] as $field=>$value): 

                            if( $field == 'Suburb' ){
                                foreach( $value as $suburb_field=>$suburb_val ){
                                    echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                }
                            }else
                                echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                    
                        endforeach; 
                    ?>

                    <?php if( $getvars['btn-service'] ==  'Mon - Frid after 4pm'): ?>
                        <input type="hidden" name="tran_type" value="After midnight - booked out">
                        <input type="hidden" name="advice_caller" value="Advice caller to call back after 4pm">
                        <h4>
                        We do service your suburb but we are currently booked out. <br />
                        Our Doctors will be on again from 6pm tonight<br />
                        If needed you can call us back on <strong class="text-danger">13 28 73 (13 CURE)</strong> <br />
                        after 4pm and we will be happy to book you in.
                        </h4>
                    <?php endif; ?>

                    <?php if( $getvars['btn-service'] ==  'Saturday after 10am'): ?>
                        <input type="hidden" name="tran_type" value="After midnight - booked out">
                        <input type="hidden" name="advice_caller" value="Advice caller to call back after 10am">
                        <h4>
                            We do service your suburb but we are currently booked out. <br />
                            Our Doctors will be on again from midday today.  <br /> 
                            If needed you can call us back on <strong class="text-danger">13 28 73 (13 CURE)</strong> <br />
                            after 10am  and we will be happy to book you in.
                        </h4>
                    <?php endif; ?>

                    <?php if( $getvars['btn-service'] ==  'Mon - Frid after 6pm'): ?>
                        <input type="hidden" name="tran_type" value="After midnight - booked out">
                        <input type="hidden" name="advice_caller" value="Advice caller to call back after 6pm">
                        <h4>
                        We do service your suburb but we are currently booked out. <br />
                        Our Doctors will be on again from 6pm tonight<br />
                        If needed you can call us back on <strong class="text-danger">13 28 73 (13 CURE)</strong> <br />
                        after 4pm and we will be happy to book you in.
                        </h4>
                    <?php endif; ?>

                    <?php if( $getvars['btn-service'] ==  'Saturday after 12pm'): ?>
                        <input type="hidden" name="tran_type" value="After midnight - booked out">
                        <input type="hidden" name="advice_caller" value="Advice caller to call back after 12pm">
                        <h4>
                            We do service your suburb but we are currently booked out. <br />
                            Our Doctors will be on again from midday today.  <br /> 
                            If needed you can call us back on <strong class="text-danger">13 28 73 (13 CURE)</strong> <br />
                            after 10am  and we will be happy to book you in.
                        </h4>
                    <?php endif; ?> 


                    <br />
                    <br />
                    <input class="btn btn-primary" type="submit" name="submit_btn" value="Submit">
                </form>

            <?php elseif( $getvars['btn-service'] ==  'Sunday & public holiday - anytime' ): ?>

                <h4>
                    

                    <h4>
                        We do service your suburb but we are currently booked out. <br />Our Doctors will be on again in the morning, <br />would you like me to a make a booking for then?
                    </h4>
                   
                </h4>

                <div class="col-lg-6 text-right" >
                    <?php echo form_open('dashboard', 'class="" id="dashboard_form" name="dashboard_form" method="get" onsubmit="return hcd.common.confirm(\'Proceeds to booking form?\')" '); ?>
                        
                        <input type="hidden" name="step" value="3">
                        <input type="hidden" name="collection[tran_type]" value="After midnight - booked out">
                        
                         
                        <?php
                            foreach($getvars['collection'] as $field=>$value): 

                                if( $field == 'Suburb' ){
                                    foreach( $value as $suburb_field=>$suburb_val ){
                                        echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                    }
                                }else
                                    echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                        
                            endforeach; 
                        ?>

                        <button class="btn btn-primary" type="submit" name="btn-service" value="1">Yes</button>
                    </form>
                </div>

                <div class="col-lg-6 text-left" >
                    <?php echo form_open('dashboard/custom_submit', 'class="" id="dashboard_form" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm(\'Log as After midnight - booked out\')" '); ?>
                        
                        <input type="hidden" name="step" value="3">
                        <!-- <input type="hidden" name="collection[tran_type]" value="After midnight - booked out - later appt book"> -->
                        
                        <input type="hidden" name="custom_submit_type" value="booking-out-call">
                        <input type="hidden" name="tran_type" value="After midnight - booked out">
                        <input type="hidden" name="advice_caller" value="Advice caller to call 13 CURE if need assistance in future">
                        
                        <?php
                            foreach($getvars['collection'] as $field=>$value): 

                                if( $field == 'Suburb' ){
                                    foreach( $value as $suburb_field=>$suburb_val ){
                                        echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                    }
                                }else
                                    echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                        
                            endforeach; 
                        ?>
                        
                        &nbsp;
                        <button class="btn btn-primary" type="submit" name="btn-service" value="Sunday & public holiday - anytime">No</button>

                    </form>
                </div>
            <?php endif; ?>


        </div>
    </div>
<!-- endif $getvars['step'] 21 -->

<?php elseif( $getvars['step'] == 3 ): ?>

    <?php if($getvars['btn-service']=='1'): ?>

        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Log to: "Booking Made"
                    </dd>
                     <dt>
                        Status:
                    </dt>
                    <dd>
                        Submit not working yet
                    </dd>
                </dl>        
            </div> 
        </div>
        <?php endif; ?>

        Open TODAY Appointment Book Operator Completes booking

    <?php endif; ?>

    <?php if($getvars['btn-service']=='2'): ?>

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard/custom_submit', 'class="" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()" '); ?>
                    
                    <input type="hidden" name="step" value="4">
                    <input type="hidden" name="custom_submit_type" value="booking-out-call">
                    <input type="hidden" name="tran_type" value="Booked out - advised to call back tomorrow">
                    <?php 
                        foreach($getvars['collection'] as $field=>$value): 

                            if( $field == 'Suburb' ){
                                foreach( $value as $suburb_field=>$suburb_val ){
                                    echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                }
                            }else
                                echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                    
                        endforeach; 
                    ?> 

                    <h4>ADVICE CALLER</h4>
                    <h4>                    

                        <?php if( @$getvars['advice_caller_custom_script'] != '' ): ?>
                            <?php echo stripslashes($getvars['advice_caller_custom_script']); ?>
                        <?php else: ?>
                           <!--  We do service your suburb but due to high number of bookings today our doctors will not be able to see you. <br />
                            You will need to call back tomorrow to make a booking after -->
                            
                            We cannot take bookings at the moment, please call us back in booking times and we will be <br /> 
                            happy to make a booking and have a doctor come and see you?


                        <?php endif; ?>
                    </h4>
                    
                    <br />

                    <div class="bg-info"> 
                        <!-- Mon - Frid after 4pm <br />
                        Saturday after 10am <br />
                        Sunday & public holiday - anytime <br /> -->

                        <strong>Booking Times</strong> <br /> <br />  
                        Mon - Frid - METRO - after 4pm <br /> 
                        Mon - Frid - REGIONAL - after 6pm <br />
                        Saturday after 12pm <br />
                        Sunday & Pub Hols - after 6am<br />


                    </div>

                    <br />

                   <!--  <h4>Would you like to make a booking tomorrow?</h4>
                    
                    <br />
                    <br /> -->

                    <input class="btn btn-primary" type="submit" name="submit_btn" value="Submit">
                    <!-- <input class="btn btn-primary" type="submit" name="btn-book-next-day" value="Yes"> -->
                    <!-- <input class="btn btn-primary" type="submit" name="btn-book-next-day" value="No"> -->

                </form>
            </div>
        </div>

    <?php endif; ?> <!-- end btn-service 2 -->



    <?php if($getvars['btn-service']=='3'): ?>

        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Log to: "Suburb not serviced" Record <br />
                        1. name <br />
                        2. phone <br />
                        3. suburb <br />
                    </dd>
                     <dt>
                        Status:
                    </dt>
                    <dd>
                        Submit not working yet
                    </dd>
                </dl>        
            </div> 
        </div>
        <?php endif; ?>

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard/non_booking_submit', 'class="" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                                        
                    <?php 
                        foreach($getvars['collection'] as $field=>$value): 

                            if( $field == 'Suburb' ){
                                foreach( $value as $suburb_field=>$suburb_val ){
                                    echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                }
                            }else
                                echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                    
                        endforeach; 
                    ?>          

                    <h4>ADVICE CALLER</h4>
                    <h4>
                        I am sorry we do not service this suburb at the moment. <br />
                        We will let you know if we service this suburb in the future
                    </h4>
                     <h4>Thank you for your call.</h4>
                    <br />
                    <br />
                    <button class="btn btn-primary" type="submit" name="submit" value="suburb-not-serviced">Submit Log</button>

                </form>
            </div>
        </div>

    <?php endif; ?> <!-- end btn-service 3 -->



    <?php if($getvars['btn-service']=='midnight-bookedout-no'): ?>

        

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard/non_booking_submit', 'class="" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                                        
                    <?php 
                        foreach($getvars['collection'] as $field=>$value): 

                            if( $field == 'Suburb' ){
                                foreach( $value as $suburb_field=>$suburb_val ){
                                    echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                }
                            }else
                                echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                    
                        endforeach; 
                    ?>          

                    <h4>ADVICE CALLER</h4>
                    <h4>
                        Thank you for your call, 
                        <br />                        
                        If we can be assistance in the future please do not hesitate to call us on <strong>13 28 73 (13 CURE)</strong>,
                        <br />
                        have a nice day
                    </h4>
                     
                    <br />
                    <br />
                    <button class="btn btn-primary" type="submit" name="submit" value="midnight-bookedout-no">Submit Log</button>

                </form>
            </div>
        </div>

    <?php endif; ?> <!-- end btn-service 3 -->

<?php elseif( $getvars['step'] == 4 ): ?>

    <?php if($getvars['btn-book-next-day']=='Yes'): ?>

        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>
                        Log to: "Booked Out - Next Day Booked"              
                    </dd>
                     <dt>
                        Status:
                    </dt>
                    <dd>
                        Submit not working yet
                    </dd>
                </dl>        
            </div> 
        </div>
        <?php endif; ?>

        Open NEXT DAY Appointment Book operator completes booking

    <?php endif; ?> <!-- end btn-book-next-day Yes -->


    <?php if($getvars['btn-book-next-day']=='No'): ?>

        <?php if( SHOW_MILESTONE ): ?>
        <div class="alert alert-warning alert-dismissible milestone-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="milestone-body">
                <dl>
                    <dt>
                        Description:
                    </dt>
                    <dd>

                        Log to: "Booked Out - Declined Next Day Booking" <br />
                        Record <br />
                        1. name  <br />
                        2. phone <br />
                        3. suburb              
                    </dd>
                     <dt>
                        Status:
                    </dt>
                    <dd>
                        Submit not working yet
                    </dd>
                </dl>        
            </div> 
        </div>
        <?php endif; ?>

        <div class="jumbotron clearfix text-center" >
            <div class="col-lg-12 col-md-12">

                <?php echo form_open('dashboard/non_booking_submit', 'class="" name="dashboard_form" method="post" onsubmit="return hcd.common.confirm()"'); ?>
                 
                    <?php 
                        foreach($getvars['collection'] as $field=>$value): 

                            if( $field == 'Suburb' ){
                                foreach( $value as $suburb_field=>$suburb_val ){
                                    echo  '<input type="hidden" name="collection[Suburb]['.$suburb_field.']" value="'.$suburb_val.'" />';
                                }
                            }else
                                echo  '<input type="hidden" name="collection['.$field.']" value="'.$value.'" />';
                    
                        endforeach; 
                    ?> 

                    <h4>ADVICE CALLER</h4>
                    <h4>
                        Thank you for your call, if we can be of assistance in <br />
                        the future please do not hesitate to call us on <strong>13 28 73 (13 CURE)</strong>, <br />
                        have a nice day
                    </h4>
                     
                    <br />
                    <br />
                    <button class="btn btn-primary" type="submit" name="submit" value="btn-next-day-declined">Submit Log</button>

                </form>
            </div>
        </div>

    <?php endif; ?> <!-- end btn-book-next-day Yes -->

<?php endif; ?>


<?php if( $getvars['step'] == 5 ): ?> 

    <?php if($getvars['btn-service']=='advice-caller-bookout'): ?>

            <div class="jumbotron clearfix text-center" >
                <div class="col-lg-12 col-md-12">

             
                        <h4>ADVICE CALLER</h4>

                        <h4>
                            Thank you for your call, if we can be of assistance in the future please do not hesitate to call us on <strong class="text-danger">13 28 73 (13 CURE)</strong>.
                        </h4>
                        
                        <br />
                        <br />

                        <a class="btn btn-primary" href="dashboard">Go to Dashboard</a>

                    </form>
                </div>
            </div>

    <?php endif; ?>
<?php endif; ?>