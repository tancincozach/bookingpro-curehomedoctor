<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	
	public $logged_in;
 	
 	public $view_data = array('container'=>'container-fluid', 'view_file'=>'default-content', 'js_file'=>'', 'data'=>'');

 	public $user_perms = array('edit'); //user_opts
 	public $user_lvl = '';
 	public $user_sub_lvl = '';
 	public $user_id = 1;
 	public $agent_name = 'Default User';
 	public $agent_id = 60;
 	 	 
 	public $user_extra = array();

 	//public $booking_statuses = array(0=>'CLOSED', 1=>'CANCELLED', 2=>'OPEN', 3=>'BOOKED', 4=>'VISIT');
 	public $booking_statuses = array(
 			BOOK_STATUS_CLOSED=>'COMPLETE', 
 			BOOK_STATUS_CANCELLED=>'CANCELLED', 
 			BOOK_STATUS_UNSEEN=>'UNSEEN', 
 			BOOK_STATUS_VISIT=>'VISIT', 
 			BOOK_STATUS_BOOKED=>'BOOKED', 
 			BOOK_STATUS_OPEN=>'OPEN'
 		);

 	public $header_contacts = array();

 	//updating default time please update ajax controller
 	//public $default_timezone = 'Australia/Queensland';
 	public $default_timezone = 'Australia/Sydney';

	function __construct()
	{
		
		parent::__construct();
 		
		if( !$this->session->userdata('logged_in')){
			redirect(base_url().'login');
		}
		
		$this->logged_in 	= $this->session->userdata('logged_in');

		$this->user_id 		= $this->session->userdata('user_id'); 		
		$this->agent_name 	= $this->session->userdata('agent_name'); 		
		$this->user_lvl 	= $this->session->userdata('user_lvl'); 		
		$this->user_sub_lvl = $this->session->userdata('user_sub_lvl');
		//$this->user_perms 	= json_decode($this->session->userdata('user_perms'));

		if( $this->session->userdata('agent_id') ){
			$this->agent_id 	= $this->session->userdata('agent_id');
		}
		
		if( $this->user_lvl.$this->user_sub_lvl == 51 ){

		 	if( $this->uri->segment(2) != 'register_doctor' ){

				$uri = $this->uri->segment(2);

				$this->user_extra['areas'] 	= $this->session->userdata('user_extra_areas');
				$this->user_extra['doctor'] = $this->session->userdata('user_extra_doctor');
				$this->user_extra['car'] 	= $this->session->userdata('user_extra_car');


				if( ($this->user_extra['areas']  == '' OR $this->user_extra['doctor'] == '' ) AND $uri != 'chaperone_switch_area_doctor'    AND  $uri != 'patients'   AND  $uri != 'patient_form' ){					
		
					redirect(base_url().'dashboard/chaperone_switch_area_doctor');
				}
			
		 	}
		}

		$this->load->model('Commonmodel');
		
		$this->header_contacts = $this->Commonmodel->contact_header();


		$this->total_notices_gen  = $this->Commonmodel->count_notice_open(1); //notices


		//print_r($this->user_perms);
		//echo $this->user_id.' - '.$this->agent_name.' - '.$this->user_lvl.' - '.$this->user_sub_lvl;

		/*try {
			
			//print_r($this->view_data);

		} catch (Exception $e) {
			 
		}*/

	}
 
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */