<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportmodel extends CI_Model{ 

	/*function dailyreport($date){

		$this->db->where("area_status", '1');

		$this->db->select('`area_name`, COUNT(`appointment`.`appt_id`) AS all_booking, SUM(IF(book_status < 8, 1, 0)) AS booked, SUM(IF(book_status=1, 1, 0)) AS cancelled_count ');
		
		$this->db->join('appointment', "appointment.area_id = areas.area_id AND DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') = '$date' ", 'LEFT OUTER');
		
		$this->db->group_by('`areas`.area_id');

		$query = $this->db->get('areas');

		return $query->result();
	}*/

	function dailyreport($from, $to){

		/*$this->db->select("DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', 'Australia/Queensland'), '%Y-%m-%d') AS appt_created_tz,
		`area_name`,
		COUNT(`appointment`.`appt_id`) AS all_booking,

		SUM(IF(book_status = 0, 1, 0)) AS book_completed, 
		SUM(IF(book_status = 8, 1, 0)) AS book_open, 
		SUM(IF(book_status = 4, 1, 0)) AS book_visit,

		SUM(IF(book_status < 8, 1, 0)) AS booked, 
		SUM(IF(book_status=1, 1, 0)) AS cancelled_count
		");*/
		$this->db->select("DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', 'Australia/Queensland'), '%Y-%m-%d') AS appt_created_tz,
		`area_name`,
		COUNT(`appointment`.`appt_id`) AS all_booking,

		SUM(IF(book_status = ".BOOK_STATUS_CLOSED.", 1, 0)) AS book_completed, 
		SUM(IF(book_status = ".BOOK_STATUS_OPEN.", 1, 0)) AS book_open, 
		SUM(IF(book_status = ".BOOK_STATUS_VISIT.", 1, 0)) AS book_visit,

		SUM(IF(book_status < ".BOOK_STATUS_OPEN.", 1, 0)) AS booked, 
		SUM(IF(book_status = ".BOOK_STATUS_UNSEEN.", 1, 0)) AS unseen,
		SUM(IF(book_status = ".BOOK_STATUS_CANCELLED.", 1, 0)) AS cancelled_count
		");

		$this->db->where(" DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'");
		$this->db->where("area_status", '1');

		$this->db->join('`areas`', '`areas`.`area_id`  = `appointment`.`area_id`', 'LEFT OUTER');

		$this->db->group_by("`appointment`.`area_id`, DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', 'Australia/Queensland'), '%Y-%m-%d')");
		$this->db->order_by('appt_created_tz ASC, area_name ASC');

		$query = $this->db->get('appointment');

		return $query->result();
	}

	function cancellation($from, $to){

		$this->db->where(" DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'");
		$this->db->where('book_status', BOOK_STATUS_CANCELLED);
		$this->db->where("area_status", '1');

		$this->db->join('`patient`', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('`areas`', "`areas`.`area_id` = `appointment`.`area_id`", 'LEFT OUTER');
		$query = $this->db->get('appointment');

		return $query->result();
	}
 
 	function wyhau($from, $to){

 		$collection = array();

 		$this->db->where('area_name != ', '');
 		$areas_query = $this->db->get('areas');
 
  		$area_cols = array();
 		foreach( $areas_query->result() as $area ) {
 			$area_cols[$area->area_id]  = $area->area_name;
		}
		$areas_query->free_result();

 		$query = "SELECT  `appointment`.`area_id`, latest_wdyhau, COUNT(appt_id) as total
				FROM appointment
				LEFT OUTER JOIN `areas` ON `areas`.`area_id` = `appointment`.`area_id`
				WHERE 
					`areas`.`area_status` = 1
					AND latest_wdyhau != '' 
					AND DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'
				GROUP BY `appointment`.`area_id`, latest_wdyhau
			";

 		$appt_query = $this->db->query($query);

		$list_cols = array(); 		
 		foreach( $appt_query->result() as $row ) {
 			$list_cols[$row->latest_wdyhau][$row->area_id] = $row->total; 
		} 

		$appt_query->free_result();

		$this->db->select('dropdown_value');
		$this->db->where('dropdown_group', 'WYHAU');
		$this->db->order_by('dropdown_value', 'asc');
 		$wdyhau_query = $this->db->get('dropdown');
 		
 		$collection[] = array_values($area_cols);
 		foreach($wdyhau_query->result() as $wdyhau) {

 			//echo $wdyhau->dropdown_value.'<br />';
 			
 			foreach($area_cols as $area_id=>$val){
 				$collection[$wdyhau->dropdown_value][$val] = @$list_cols[$wdyhau->dropdown_value][$area_id];
 			}

 		}

 		$wdyhau_query->free_result();
 		
 		

 		return $collection;
 	}

 	function appointment($from, $to,$params=array()){


 		//$this->db->where("DATE_FORMAT(appt_start, '%Y-%m-%d') BETWEEN '$from' AND '$to'", NULL, false);
		
		$this->db->where("DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'", NULL, false);
		$this->db->where("`areas`.`area_status`", '1');

		if(isset($params['where_str']) && $params['where_str']!=''){



			$this->db->where($params['where_str'], null, false);


		}

		if(isset($params['where']) && $params['where']!=''){



			$this->db->where($params['where']);


		}

 		$select = "transaction.*, patient.*, appointment.*,appointment_consult_notes.*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ";
		
		//$select .= ", IF( appt_start_queue IS NULL , TIMEDIFF(NOW(),appt_created), TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";

		

		$select .= ", (CASE 
					WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
					WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
					WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
					ELSE TIMEDIFF(appt_start_queue, appt_created) 
				END)  AS in_queue ";


		$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name , doctor.prov_num as doctor_prov_num,(patient.patient_id + 1000)  as patient_ext_id";
		$select .= ", `areas`.`area_name`";		
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."') AS appt_created_tz ";
		$select .= ", CONVERT_TZ(FROM_UNIXTIME(consult_start), 'Australia/Sydney', '".$this->default_timezone."') AS 'consult_start_tz' ";
		$select .= ", CONVERT_TZ(FROM_UNIXTIME(consult_end), 'Australia/Sydney', '".$this->default_timezone."') AS 'consult_end_tz' ";
		$select .= ", `areas_cars_link`.`car_name`";

 		$this->db->select($select);


 		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');		
		$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
		$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
		$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');
		$this->db->join('`areas_cars_link`', '`areas_cars_link`.`car_id` = `appointment`.`car_id`', 'LEFT OUTER');
		$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`', 'LEFT OUTER');


		$query = $this->db->get('appointment');


		//echo $this->db->last_query();

		//exit();

		return $query->result();
 	}

 	function chaperone($from, $to, $chaperone_id){


 		//$this->db->where("DATE_FORMAT(appt_start, '%Y-%m-%d') BETWEEN '$from' AND '$to'", NULL, false);
 		$this->db->where("DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'", NULL, false);

 		$this->db->where("`areas`.`area_status`", '1');

 		if( $this->user_lvl == 5 )
 			$this->db->where('chaperone_id', $chaperone_id );

 		$select = "transaction.*, patient.*, appointment.*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ";
		//$select .= ", IF( appt_start_queue IS NULL , TIMEDIFF(NOW(),appt_created), TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
		
		$select .= ", (CASE 
					WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
					WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
					WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
					ELSE TIMEDIFF(appt_start_queue, appt_created) 
				END)  AS in_queue ";		

		$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
		$select .= ", `areas`.`area_name`";		
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."') AS appt_created_tz ";
		$select .= ", CONVERT_TZ(FROM_UNIXTIME(consult_start), 'Australia/Sydney', '".$this->default_timezone."') AS 'consult_start_tz' ";
		$select .= ", CONVERT_TZ(FROM_UNIXTIME(consult_end), 'Australia/Sydney', '".$this->default_timezone."') AS 'consult_end_tz' ";
		$select .= ", `areas_cars_link`.`car_name`";

 		$this->db->select($select);


 		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
		$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
		$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');
		$this->db->join('`areas_cars_link`', '`areas_cars_link`.`car_id` = `appointment`.`car_id`', 'LEFT OUTER');
		$query = $this->db->get('appointment');

		return $query->result();
 	}

 	function call_register($from, $to){

 		$this->db->where("DATE_FORMAT(CONVERT_TZ(tran_created, 'Australia/Sydney', '".$this->default_timezone."'), '%Y-%m-%d') BETWEEN '$from' AND '$to'", NULL, false);
 		//$this->db->where("`areas`.`area_status`", '1');

		$select = "*";
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."') AS 'appt_created_tz' ";
		$select .= ", CONVERT_TZ(tran_created, 'Australia/Sydney', '".$this->default_timezone."') AS 'tran_created_tz' ";

		$this->db->select($select); 

		$this->db->join('`appointment`', '`appointment`.`tran_id` = `transaction`.`tran_id`', 'LEFT OUTER');
		$this->db->join('`patient`', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		//$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
		$query = $this->db->get('transaction');
		
		return $query->result();

 	}

 	public function get_practice_email( $practice ){

 			$this->db->select('email');
 			$this->db->where('practice_name',$practice);
 			$qry = $this->db->get('practices');

 			$row_result = $qry->row();

 			return @$row_result->email;

 	}

}