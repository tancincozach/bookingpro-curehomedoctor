<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model
{ 

	
	function set_preset($user_id, $set){
		
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('user_preset')->row();
		//$query->free_result();		 
		
		$return = 0; 

		if( count($query) > 0 ){
			//update
			$this->db->where('user_id', $user_id);
			$return = $this->db->update('user_preset', $set)?$user_id:0;

		}else{
			//insert	
			$set['user_id'] = $user_id;		
			$this->db->where('user_id', $user_id);
			if( $this->db->insert('user_preset', $set) ){

				$query = $this->db->get_where('user_preset', array('id'=>$this->db->insert_id()))->row();
				$return = $query->user_id;

			}else{

				$return = 0;

			}

		}

		return $return;
	}

	function get_preset($user_id){
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('user_preset');

		return $query->row();
	}

	function get_doctors( $where=array(), $result_type='dropdown', $select='' ){

		if(!empty($where)){
			$this->db->where($where);
		}

		if(!empty($select)){
			$this->db->select($select);
		}

		$this->db->where('user_sublevel', 2);
		$this->db->where('user_level', 5);
		$this->db->where('deleted', 0);

		$this->db->order_by('user_fullname', 'asc');

		$query = $this->db->get('user');

		$results = $query->result();

		if( $result_type == 'dropdown' ){

			$tmp = array(''=>'---select---');
			foreach($results as $row) {
				$tmp[$row->user_id] = $row->user_fullname;
			}

			$results = $tmp;
		}

		return $results;
	}

	function get_chaperones( $where=array(), $result_type='dropdown',  $select='' ){

		if(!empty($where)){
			$this->db->where($where);
		}
		
		if(!empty($select)){
			$this->db->select($select);
		}

		$this->db->where('user_sublevel', 1);
		$this->db->where('user_level', 5);
		$this->db->where('deleted', 0);
		$query = $this->db->get('user');

		$results = $query->result();

		if( $result_type == 'dropdown' ){

			$tmp = array(''=>'---select---');
			foreach($results as $row) {
				$tmp[$row->user_id] = $row->user_fullname;
			}

			$results = $tmp;
		}

		return $results;

	}
	
	/**
	 * Areas
	 */
	function add( $set )
	{

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
		
		try
		{
			if(empty($user_id) && $user_id=='')

			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			if(!$this->db->insert('user', $set))

			throw new Exception("Error: Unable to add a user");					

			$last_id = $this->db->insert_id();

					$this->insert_audit_trail(
							array(
							'target_table'=>'user',
							'table_ref_col'=>'user_id',
							'table_action'=>'Add',
							'table_ref_id'=> $last_id,
							'user_id'=>$user_id,
							'agent_name'=>$agent_name,
							'message'=> json_encode($set)
							)
	  				);			

			return $last_id;


		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}
	}


	function update( $id, $set )
	{

		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
	
     	try {
			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	
			

			$this->db->where('user_id', $id);

 			
			if(!$this->db->update('user',  $set))
			
			throw new Exception("Error: Unable to update ");	

			$this->insert_audit_trail(
									array(
									'target_table'=>'user',
									'table_ref_col'=>'user_id',
									'table_action'=>'Update',
									'table_ref_id'=> $id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);	
				return $id;		
		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

	function get_result( $where=array() ){


		if(!empty($where))
			$this->db->where($where);

		$this->db->order_by('user_name', 'asc');

		$query = $this->db->get('user');

		return $query->result();

	}


	function get_result_pagination( $parameters )
	{

		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


    	if(!empty($parameters['search']))
    	{
    		$this->db
    			 ->like('user_name',$parameters['search'])
    			 ->or_like('user_fullname',$parameters['search']); 
    	}

		$query   = $this->db->get('user');

		$total_rows = $query->num_rows();

		$query->free_result();


		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}



	if(!empty($parameters['search']))
    	{
    		$this->db
    			 ->like('user_name',$parameters['search'])
    			 ->or_like('user_fullname',$parameters['search']); 
    	}


    	 $query = $this->db->select("*,
										case user_level 
										when 0
										  then 'Operator'
										when 1
										  then 'Supervisor'
										when 2
										  then 'Support/Teamleader'
										when 3
										  then 'CCadmin'
										when 5
										  then 'Client'
										end as user_lvl,

										case user_sublevel 
										when 1
										  then 'Chaperone'
										when 2
										then  'Doctor'
										when 3
										then  'Admin'
										end as sub_level

								    ")
			 			   ->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset'])						   		         		   
		    	    	   ->get('user');		


	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 

 }

	public function get_chaperone_by_area( $area_id='')
	{
		try
		{
			if($area_id=='') throw new Exception("Error: Empty Area");

			$query=	$this->db
				->where('user_sublevel',1)
				->select(" 
					user_id,
					user_name,
					user_fullname,
					IF((select COUNT(*) as count  from areas_chaperone_link where area_id={$area_id} and user_id = user.user_id) > 0 , 1,0) as is_selected")
				->order_by('user_fullname' , 'asc')
				->get('user');

			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

 	function get_user_row($where, $return_type='row'	){

		if( is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		//$this->db->select("*");
	
		$query = $this->db->get('user');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}
}