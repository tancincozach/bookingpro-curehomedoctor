<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holidaymodel extends CI_Model{ 

	/**
	 * Areas
	 */
	function add( $set )
	{

		$user_id = '';
		$agent_name = ''; 
		$area_id = '';

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}		

		if(isset($set['area_id']))
		{	
			$areas= $set['area_id'];
			unset($set['area_id']);
		}

		if(empty($user_id) && $user_id=='')

		throw new Exception("Error: No User found ");	

		if(empty($agent_name)  && $agent_name=='')

		throw new Exception("Error: No Agent found ");	


		$row  = $this->get_row( array('public_holiday.holiday_name'=>"'".$set['holiday_name']."'"),'array');



		if(count($row) > 0 )
		{

			throw new Exception("Error: Holiday Already Exist");	
		}

		
		if($this->db->insert('public_holiday', $set))
		{
			$id = $this->db->insert_id();

		
				unset($set['holiday_name']);
				unset($set['effective_date']);
				$this->remove_area_holiday_links($id,$areas);
		

			$this->insert_audit_trail(
								array(
								'target_table'=>'public_holiday,areas_holidays_link',
								'table_ref_col'=>'holiday_id,area_id',
								'table_action'=>'Add',
								'table_ref_id'=>$id,
								'user_id'=>$user_id,
								'agent_name'=>$agent_name,
								'message'=> json_encode($set)
								)
			);			


			return $id;
		}
		else
		{
			 return 0;
		}
	}


	function update( $id, $set )
	{

	
		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
		if(isset($set['area_id']))
		{	
			$areas= $set['area_id'];
			unset($set['area_id']);
		}


     	try {
			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	


			$this->db->where('holiday_id', $id);

			if($this->db->update('public_holiday',$set ))
			{


				unset($set['holiday_name']);
				unset($set['effective_date']);
				$this->remove_area_holiday_links($id,$areas);


				$this->insert_audit_trail(
								array(
								'target_table'=>'public_holiday,areas_holidays_link',
								'table_ref_col'=>'holiday_id,area_id',
								'table_action'=>'Add',
								'table_ref_id'=>$id,
								'user_id'=>$user_id,
								'agent_name'=>$agent_name,
								'message'=> json_encode($set)
								)
				);			
				return $id;
			}
			else
			{
			return 0;
			}			
		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

	function remove_area_holiday_links($holiday_id,$set)
	{

		if(count($set)){
			
				foreach ($set as $area_key =>$area_val) {

						$area_holiday_link[] =  array(
													'area_id'=>$area_val,
													'holiday_id'=>$holiday_id
												);							

				}			
				$this->db->delete('areas_holidays_link',array('holiday_id'=>$holiday_id));					
				$this->db->insert_batch('areas_holidays_link',$area_holiday_link);
		}		
				
	}

	public function remove_holiday($set)
	{
		
  		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}


	  	try {
			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  || $agent_name=='')

			throw new Exception("Error: No Agent found ");


 			$row  = $this->get_row( array('areas_holidays_link.holiday_id'=>$set['holiday_id']),'array');

			$this->db->where('holiday_id',$set['holiday_id']);
			$this->db->delete('public_holiday',array('holiday_id'=>$set['holiday_id']));

			$this->db->where('holiday_id',$set['holiday_id']);
			$this->db->delete('areas_holidays_link',array('holiday_id'=>$set['holiday_id']));

			$this->insert_audit_trail(
									array(
									'target_table'=>'public_holiday,areas_holidays_link',
									'table_ref_col'=>'holiday_id,area_id',
									'table_action'=>'Delete',
									'table_ref_id'=> $id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($row)
									)
			  				);	


	  	} catch (Exception $e) {
	  		return array('error'=>$e->getMessage());
	  	}
	}
	function get_result( $where=array() ){


		if(!empty($where))
			$this->db->where($where);

		$this->db->order_by('area_name', 'asc');

		$query = $this->db->get('areas');

		return $query->result();

	}


	function get_array( $where=array() ){

		if(!empty($where))
			$this->db->where($where);

		$this->db->order_by('area_name', 'asc');

		$result = $this->db->get('areas')->result();

		$result_array = array();
		foreach( $result as $row ){
			$result_array[$row->area_id] = $row->area_name;
		}

		return $result_array;
	}

	function get_areas_availability_row($where , $return_type='row'	){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		
		 $this->db->select("*");	
	
	
		$query = $this->db->get('areas_availability');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function get_filtered_areas_availability($where ,$white_listed_column){

		$return_array= array();

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		
		 $this->db->select('*');	
	
	
		$query = $this->db->get('areas_availability');

		foreach ($query->row_array() as $field=>$row)
		{
			if(!in_array($field, $white_listed_column)) 
			{
				$return_array[$field] = $row;
			}		
		} 
		return $return_array;
	}

	function get_row($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}


	   $this->db 
   		 	 ->select("
   		 	 	public_holiday.*
   		 	 	,GROUP_CONCAT(areas.area_id SEPARATOR ', ') AS area_ids")
    		 ->group_by('areas_holidays_link.holiday_id')		 
    		 ->join('areas_holidays_link','areas_holidays_link.holiday_id=public_holiday.holiday_id','left',false)
    		 ->join('areas','areas.area_id=areas_holidays_link.area_id','left',false);

		$query   = $this->db->get('public_holiday');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}


	function get_concat_areas($parameters)
	{
		if(is_array($parameters['where']))
		{
			$this->db->where($parameters['where']);		
		}
		else
		{
			$this->db->where($parameters['where'], null, false);		
		}	

 	  	 $this->db->select("GROUP_CONCAT(area_name SEPARATOR ', ') AS areas,GROUP_CONCAT(areas.area_id SEPARATOR ', ') AS area_ids ")
    		 ->group_by('areas_holidays_link.holiday_id')		 
    		 ->join('areas_holidays_link','areas_holidays_link.holiday_id=public_holiday.holiday_id','left',false)
    		 ->join('areas','areas.area_id=areas_holidays_link.area_id','left',false);


    	$query = $this->db->get('public_holiday');

    	return $query->result();
	}

	function get_result_pagination( $parameters )
	{

		if(!empty($parameters['where']) && count($parameters['where']) > 0)
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
		}
		
/*

    	$this->db    	 
    		 ->select("*, GROUP_CONCAT(area_name SEPARATOR ', ') AS areas,GROUP_CONCAT(areas.area_id SEPARATOR ', ') AS area_ids ")
    		 ->group_by('areas_holidays_link.holiday_id')			         		   
    		 ->join('areas_holidays_link','areas_holidays_link.holiday_id=public_holiday.holiday_id','left')
    		 ->join('areas','areas.area_id=areas_holidays_link.area_id','left');

*/


		if(isset($parameters['search']) && $parameters['search']!='')
    	{
    		$this->db
    			 ->like('holiday.holiday_name',$parameters['search'])
    			 ->like('areas.area_name',$parameters['search']);

    	}


    	
		$query   = $this->db->get('public_holiday');

		$total_rows = $query->num_rows();

		$query->free_result();


		if(!empty($parameters['where']) && count($parameters['where']) > 0)
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


/*    	
$this->db    	         	
		     ->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset'])
 			 ->order_by('public_holiday.holiday_name','asc')  
 			 ->select("*, GROUP_CONCAT(area_name SEPARATOR ', ') AS areas,GROUP_CONCAT(areas.area_id SEPARATOR ', ') AS area_ids ")
    		 ->group_by('areas_holidays_link.holiday_id')			  	
    		 ->join('areas_holidays_link','areas_holidays_link.holiday_id=public_holiday.holiday_id','left')
    		 ->join('areas','areas.area_id=areas_holidays_link.area_id','left');



*/


		if(isset($parameters['search']) && $parameters['search']!='')
    	{
    		$this->db
    			 ->like('holiday.holiday_name',$parameters['search'])
    			 ->like('areas.area_name',$parameters['search']);

    	}

		$query  = $this->db
					   ->order_by('public_holiday.effective_date','desc')
					   ->get('public_holiday');		
/*
		echo $this->db->last_query();*/
		 

	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 

 }


	
	
	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}


	function get_schedules(){

		$this->db->select("*, IF(update_time_to IS NULL, '', CONVERT_TZ(CONCAT(CURRENT_DATE,' ',update_time_to), 'Australia/Sydney', '".$this->default_timezone."')) as update_time_to_tz");
		//$this->db->select(", IF(update_time_to IS NULL, '', CONVERT_TZ(CONCAT(CURRENT_DATE,' ',update_time_to), 'Australia/Sydney', '".$this->default_timezone."')) as update_time_to_tz");
		$query = $this->db->get('schedules');

		return $query->result();
	}

}