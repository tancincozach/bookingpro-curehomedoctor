<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patientmodel extends CI_Model{ 

	function add( $set )
	{

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}


		if($this->db->insert('patient', $set))
		{
			$id = $this->db->insert_id();

			$this->insert_audit_trail(
					array(
					'target_table'=>'patient',
					'table_ref_col'=>'patient_id',
					'table_action'=>'Add',
					'table_ref_id'=> $id,
					'user_id'=>$user_id,
					'agent_name'=>$agent_name,
					'message'=> json_encode($set)
					)
				);	
		}else{
			$id = 0;
		}

		return $id;
	}

 
	function update( $id, $set )
	{
		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}

		$this->db->where('patient_id', $id);

		if($this->db->update('patient', $set))
		{
			$this->insert_audit_trail(
					array(
					'target_table'=>'patient',
					'table_ref_col'=>'patient_id',
					'table_action'=>'Add',
					'table_ref_id'=> $id,
					'user_id'=>$user_id,
					'agent_name'=>$agent_name,
					'message'=> json_encode($set)
					)
				);	
		}

		return $id;
	}
	
	function delete( $id )
	{
		if( $id <= 0 OR $id=='' ) return 0;

		return ($this->db->delete('patient', array('patient_id' => $id))) ? $id:0; 
	}
	 
	function get_result( $params=array() )
	{

		if( !empty($params['where']) )
			$this->db->where($params['where']);

		if( !empty($params['where_str']) )
			$this->db->where($params['where_str'], null, false);

		$this->db->order_by('firstname', 'asc');

		$query = $this->db->get('patient');

		return $query->result();

	}

	function get_row($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		$this->db->select("*, TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age");

		$query = $this->db->get('patient');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}
	
	function get_result_pagination( $parameters =array() ){




		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}

		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by($parameters['order_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

	if(isset($parameters['search']))
    	{
    		$this->db
    			 ->like('firstname',$parameters['search'])
    			 ->or_like('lastname',$parameters['search'])
    			 ->or_like('medicare_number',$parameters['search'])
    			 ->or_like('medicare_ref',$parameters['search'])	
    			 ->or_like('latest_symptoms',$parameters['search'])
    			 ->or_like('street_addr',$parameters['search'])    			 
    			 ->or_like('email',$parameters['search']);    			 
    	}


		$query   = $this->db->get('patient');


		$total_rows = $query->num_rows();

		$query->free_result();


		
		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


    	$this->db->select('*,(patient_id + 1000)  as patient_ext_id');



    	if(isset($parameters['limit']) && !empty($parameters['limit']))
        {                   
           $this->db->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset']);
        }

		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by($parameters['order_by']);
    	}
    	
    	if(isset($parameters['group_by']))
		{
    	  $this->db->group_by($parameters['group_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

	if(isset($parameters['search']))
    	{
    		$this->db
    			 ->like('firstname',$parameters['search'])
    			 ->or_like('lastname',$parameters['search'])
    			 ->or_like('medicare_number',$parameters['search'])
    			 ->or_like('medicare_ref',$parameters['search'])	
    			 ->or_like('latest_symptoms',$parameters['search'])
    			 ->or_like('street_addr',$parameters['search'])    			 
    			 ->or_like('email',$parameters['search']); 
    	}


		$query   = $this->db->get('patient');
		
		
	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 

	}	


	function get_result_pagination_export( $parameters =array() ){




		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}

		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by($parameters['order_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

	if(isset($parameters['search']))
    	{
    		$this->db
    			 ->like('firstname',$parameters['search'])
    			 ->or_like('lastname',$parameters['search'])
    			 ->or_like('medicare_number',$parameters['search'])
    			 ->or_like('medicare_ref',$parameters['search'])	
    			 ->or_like('latest_symptoms',$parameters['search'])
    			 ->or_like('street_addr',$parameters['search'])    			 
    			 ->or_like('email',$parameters['search']);    			 
    	}


		$this->db->group_by('`patient`.`patient_id`');
		$this->db->join('suburb', '(`suburb`.`suburb_postcode` = `patient`.`postcode` AND `suburb_name` = `patient`.`suburb` AND `suburb`.`suburb_status` = 1)', 'LEFT OUTER JOIN ');
		$this->db->join('areas', '`areas`.`area_name` = `suburb`.`suburb_area`', 'LEFT');


		$query   = $this->db->get('patient');


		$total_rows = $query->num_rows();

		$query->free_result();


		
		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}

/*		if(isset($parameters['select']))
		{
    	  $this->db->select($parameters['select']);
    	}*/

    	$this->db->select('*,(patient_id + 1000)  as patient_ext_id');

    	if(isset($parameters['limit']) && !empty($parameters['limit']))
        {                   
           $this->db->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset']);
        }

		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by($parameters['order_by']);
    	}
    	
    	if(isset($parameters['group_by']))
		{
    	  $this->db->group_by($parameters['group_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

	if(isset($parameters['search']))
    	{
    		$this->db
    			 ->like('firstname',$parameters['search'])
    			 ->or_like('lastname',$parameters['search'])
    			 ->or_like('medicare_number',$parameters['search'])
    			 ->or_like('medicare_ref',$parameters['search'])	
    			 ->or_like('latest_symptoms',$parameters['search'])
    			 ->or_like('street_addr',$parameters['search'])    			 
    			 ->or_like('email',$parameters['search']); 
    	}

/*  
SELECT
  patient.postcode,
  suburb.suburb_postcode,
  suburb.suburb_name,
areas.*
FROM patient
  JOIN suburb
    ON suburb.suburb_name = patient.suburb
  JOIN areas
    ON areas.area_name = suburb.suburb_area
WHERE patient.patient_status = 1 AND areas.timezone = 'Australia/Sydney'

*/
		$this->db->group_by('`patient`.`patient_id`');
		$this->db->join('suburb', '(`suburb`.`suburb_postcode` = `patient`.`postcode` AND `suburb_name` = `patient`.`suburb` AND `suburb`.`suburb_status` = 1)', 'LEFT OUTER JOIN ');
		$this->db->join('areas', '`areas`.`area_name` = `suburb`.`suburb_area`', 'LEFT OUTER JOIN');
    	
		$query   = $this->db->get('patient');
		
	


	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 

	}


	function get_results(  $table_config = array())
	{
		
		try 
		{
			if(count($table_config)==0)
			{
			 throw new  Exception("Empty configuration", 1);
			}		

		    if(isset($table_config['where']))
		    {
		      if(is_array($table_config['where']))
		      {
		      	$this->db->where($table_config['where']);	
		      }
		      else{
      			$this->db->where($table_config['where'],NULL, FALSE);	
		      }		  	  
		    }

		    if(isset($table_config['order_by']))
		    {
			  $this->db->order_by($table_config['order_by']);
		    }	

		  
		    if(isset($table_config['limit']))
		    {  		    	
 	    	   $this->db->limit(@$table_config['limit']['limit'], @$table_config['limit']['offset']);
		    }

		    if(isset($table_config['table']))
		    {
	  		  $query = $this->db->get($table_config['table']);
	  			echo $this->db->last_query();
		    }

			if(isset($query))					 
			{
			  if ($query->num_rows() > 0)
			  {
		    	return $query->result();
				$query->free_result(); 
		      }	
			}			
		}
		catch(Exception $error)
		{
		  return  false;
		}

	}


	function get_duplicates_to_be_compared($id)
	{
	   $patient_array = array();



	   $patient_info = $this->db->query("SELECT * from patient where patient_id={$id}")->row_array();


	   if(count($patient_info) > 0 )
	   {
	   	 $patient_array['base'] = $patient_info;
		 $patient_array['duplicates'] =  $this->db->query("SELECT * from patient where  patient_status=1  and  patient_id!={$id} and firstname like '%".$patient_info['firstname']."%' AND lastname like '%".$patient_info['lastname']."%'")->result_array();
		 return $patient_array;
	   }
	   else{
	   	return 0;
	   }
	}

	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

}

