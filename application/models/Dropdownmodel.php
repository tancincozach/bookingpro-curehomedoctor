<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dropdownmodel extends CI_Model{ 

	function add($set){
		return ($this->db->insert('dropdown', $set))?$this->db->insert_id():0;
	}

	
	function update( $id, $set )
	{

		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
	
     	try {
			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	


			$this->db->where('dropdown_id', $id);
			

			if($this->db->update('dropdown',  $set))
			{
			

					$this->insert_audit_trail(
									array(
									'target_table'=>'dropdown',
									'table_ref_col'=>'dropdown_id',
									'table_action'=>'Update',
									'table_ref_id'=> $id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);	
				return $id;
			}
			else
			{
			return 0;
			}			
		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

	function sort_update($set){

		$sort_data = $set['sort_data'];

		foreach($sort_data as $order=>$id){
			//echo $order.' '.$id.PHP_EOL;
			$set_update['dropdown_order'] = $order;
			$this->db->where('dropdown_group', $set['dropdown_group']);
			$this->db->where('dropdown_id', $id);
			$this->db->update('dropdown', $set_update);
			//echo $this->db->last_query().PHP_EOL;
		}

	}

		function get_row($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

			$this->db->select("*");



		$query   = $this->db->get('dropdown');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function get_results($opt=array(), $result_type='result') {

		if( !empty($opt['where']) ){

			$this->db->where($opt['where']);

			if( !empty($where['order']) ){
				$this->db->order_by($where['order']);
			}else{
				$this->db->order_by('dropdown_order', 'asc');
				$this->db->order_by('dropdown_label', 'asc');
			}

			$result = $this->db->get('dropdown')->result();

			/*If array return*/
			if($result_type == 'array'){

				$result_array = array(''=>' --- select --- ');
				foreach ($result as $row) {
					$result_array[$row->dropdown_value] = $row->dropdown_label;
				}
				$result = $result_array;
			}

			return $result;
		}else{
			return false;
		}

	}

	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}
	
}