<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonmodel extends CI_Model{ 

	function pagination_config($set_config = array())
	{ 

		$config = array(
				'per_page'=>15,
				'uri_segment'=>3
		); 

		$config = $set_config;

        //config for bootstrap pagination class integration

        $config["enable_query_strings"] = TRUE; 
        $config["page_query_string"]    = TRUE; 

        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a >';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';

		return $config;
	}

    

    function get_csv_data( $file_input_name )
    {
        

        $this->load->library('csvimport');



        $data['error'] = '';   


        try
        {
        
         $file_path =  $_FILES[$file_input_name]['tmp_name'];       

        if ($file_path=='') 
        {
    
           throw new Exception('Error : Please select file to upload.');       
        }            

        $csv_array = $this->csvimport->get_array($file_path);



        if(count($csv_array) == 0)
        {
         throw new Exception( 'Unable to upload the file.' );
        }                                                                                       

        return $csv_array;

        } 
        catch (Exception $e)
        {                               
          $data['error'] =  $e->getMessage(); 

          return  $data;
        }
                   
    }


    function get_csv_data2($file_input_name ,$column_headers = array())
    {

        $row = 0;
        $result = array();


        $file_path =  $_FILES[$file_input_name]['tmp_name'];     

        $handle = fopen($file_path, "r");
        $delimiter = ",";
        $initial_line = 0;
        $header_exist = array();

        while (($data = fgetcsv( $handle, 0, $delimiter)) !== FALSE) 
        {     
            
                $column_num = count($data);

                foreach ($data as $key => $value){                
                    if(in_array($value, $column_headers))
                    {
                        $header_exist[] = $value;
                    }
                }

                if(count($header_exist) == $column_num)
                {

                    foreach($header_exist as $header_key=> $row_val)
                    {
                        foreach($data as $key =>$value)
                        {
                            
                            if($header_key==$key)
                            {
                                if($data[$key]!=$row_val)
                                {
                                  $result[$row_val][] = $data[$key];      
                                }                            
                            }
                            
                        }
                     $row++;
                    }
                }
                else
                {
                    foreach($data as $key=>$value)
                    {
                         $result[$key][] = $value;
                    }
                }
        }
        return $result;
    }
    function access($allowed){
        $allowed = explode(',', $allowed);
        $allowed[] = 99;
        /*print_r($allowed);
        echo $this->user_lvl.$this->user_sub_lvl;*/
        if( !in_array($this->user_lvl.$this->user_sub_lvl, $allowed) ){
            $this->session->set_flashdata('fmesg',"You don't have permissions from previous page.");
            
            if( $this->user_lvl == 5 AND  $this->user_sub_lvl == 1) { //chaperone
                redirect(base_url().'chaperone');
            }else if($this->user_lvl == 5 AND  $this->user_sub_lvl == 2){ //doctor
                redirect(base_url().'doctor'); 
            }else{
                redirect(base_url().'dashboard');
            }
        }

    } 

    function contact_header()
    {   
        $this->db->where('contact_status',1);

        $query = $this->db->get('contacts');

        if($query->num_rows()  > 0 )
        {
            $array = array();
            
            foreach( $query->result_array() as $row)    
            {
                $array[$row['id']] = $row['contact_title'];
            }
            return $array;
        }

        
    }

    function get_row_custom_contact($where){

        if( !empty($where) ){ 

            $this->db->where('contact_status',1);
            $this->db->where($where);
            
            $this->db->limit(1);
            $result = $this->db->get('contacts')->row();

            return $result;
        }
    }


    function getState($postcode){

        $state = '';

        if($postcode >= 2000){$state = 'NSW';}
        if($postcode >= 2600){$state = 'ACT';}
        if($postcode >= 2619){$state = 'NSW';}
        if($postcode >= 2900){$state = 'ACT';}
        if($postcode >= 2921){$state = 'NSW';}
        if($postcode >= 3000){$state = 'VIC';}
        if($postcode >= 4000){$state = 'QLD';}
        if($postcode >= 5000){$state = 'SA';}
        if($postcode >= 6000){$state = 'WA';}
        if($postcode >= 7000){$state = 'TAS';}

        return $state;
    }

    function insert_audit_trail($set){
        $set['created'] = strtotime('now');
        if( is_array($set) AND !empty($set) ){
            $this->db->insert('audit_trail', $set);
        }

    }

    function get_server_converted_date($timezone, $format='%Y-%m-%d'){
 
        $sql = "SELECT DATE_FORMAT(CURRENT_TIMESTAMP, '".$format."') as curr_date";

        if($timezone != 'Australia/Sydney'){
            $sql = "SELECT DATE_FORMAT(CONVERT_TZ(CURRENT_TIMESTAMP, 'Australia/Sydney', '$timezone'), '".$format."') as curr_date";
        }

        //$sql = "SELECT CONVERT_TZ(CURRENT_TIMESTAMP, 'Australia/Sydney', '$timezone') as curr_date";
        $query = $this->db->query($sql)->row();

        return $query->curr_date;
    }

    function medicare_form_item_numbers(){

        //$set['597'] = 'Urgent Attendances After hours (before 11pm)';
        
        $set['588'] = 'Urgent attendances after hours (before 11pm) for REGIONAL AREAS';
        $set['591'] = 'Urgent attendances after hours (before 11pm) for METROPOLITAN ATREAS';
        
        $set['599'] = 'Urgent Attendances After hours (after 11pm)';

        //$set['5023'] = 'Non urgent consultation (and multiple patients)';
        $set['5023'] = 'Non urgent consultation 24 hours (and multiple patients when seen after 11pm))';
        $set['594'] = 'Multiple patients when seen before 11pm';

        $set['73805'] = 'Urine Test';
        $set['73806'] = 'Pregnancy Test' ;
        $set['30216'] = 'Aspiration of Haematoma';
        $set['47915'] = 'Ingrown Toe Nail - Wedge resection ';
        $set['47916'] = 'Ingrown Toe Nail - Phenol to nail bed ';
        $set['36800'] = 'Insertion of catheters into bladder';
        $set['30061'] = 'Superficial Foreign Body Removal (includes from cornea or sclera) as an independent procedure';
        $set['30064'] = 'Subcutaneous Foreign Body Removal , requiring incision and exploration, including closure of wound if performed as an independent procedure';
        $set['45100'] = 'Remove “foreign bodies” from ear canals';
        $set['30026'] = 'Skin and subcutaneous tissue or mucous tissue or mucous membrane, repair of wound:, not on face or neck, small (not more than 7cm long)';
        $set['30032'] = 'Skin and subcutaneous tissue or mucous tissue or mucous membrane, repair of wound, on face or neck, small (not more than 7cm long), superficial';
        //$set['other'] = '';

        return $set;

    }

    function get_admin_email($type=''){

       /* $hcd_admin_email = "wayne@housecalldoctor.com.au; gin@housecalldoctor.com.au; gordon@housecalldoctor.com.au; laura@housecalldoctor.com.au; mani@housecalldoctor.com.au; kellie@welldone.com.au";*/

        $type = (trim($type)=='')?'ADMIN_EMAIL':$type;

        //if( $type != '' ){ 

        $this->db->where('set_name', $type);
        $query = $row = $this->db->get('site_setting');

        $record = $query->row();

        $admin_email = (isset($record->set_value) AND trim($record->set_value) != '')?$record->set_value:$hcd_admin_email;
        //}

        return $admin_email;
    }

    function get_schedules($date='', $ov_day='', $area_id){

        $date = '';

        $appt_created = date('Y-m-d H:i:s');
        
        /*if(empty($date)) $date=date('Y-m-d H:i:s');

        $day = strtolower(date('D', strtotime($date)));
        //$day = 'sat';

        if( !empty($ov_day) ){
            $day = $ov_day;
        }

        //check if there is holiday today
        
        if( !empty($area_id) ){

            $h_query = "SELECT * 
            FROM public_holiday
            LEFT OUTER JOIN areas_holidays_link ON areas_holidays_link.holiday_id = public_holiday.holiday_id
            WHERE areas_holidays_link.area_id = $area_id
            AND effective_date = '".$this->get_server_converted_date($this->default_timezone)."'";
        
            $h_result = $this->db->query($h_query);
           

            if($h_result->num_rows() > 0){
                $day = 'sun';
            } 
        }

        $this->db->where('day_'.$day, '1');
        $this->db->where('sched_status', '1');
        $this->db->limit(1);
        $this->db->order_by('specific_date', 'desc');
        $this->db->order_by('sched_id', 'desc');
        $sched = $this->db->get('schedules')->row();


        $now_unix = strtotime('now');
        //$now_unix = strtotime('2016-02-25 17:00:00');

        $return = array('isPeriod'=>TRUE, 'appt_created'=>$appt_created);

        if( !$sched->update_time_to == '' ){

            $sched_end = strtotime($sched->sched_end);

            if( $now_unix < $sched_end  AND (date('Y-m-d') == $this->get_server_converted_date($this->default_timezone)) ){
                $appt_created = date('Y-m-d').' '.$sched->update_time_to;
                $return['isPeriod'] = FALSE;
            }

        }

        $return['appt_created'] = $appt_created;*/

        $return = array('isPeriod'=>TRUE, 'appt_created'=>$appt_created);

        return $return;
    }

    function count_notice_open($notice_group){
        $this->db->where("notice_status=1 AND notice_group = {$notice_group}  AND ( expire_date >= '".date('Y-m-d h:i:s')."'  OR expire_date='0000-00-00 00:00:00') ", null, false);
        $this->db->select('count(*) as count');
        $row = $this->db->get('notices')->row();
        return $row->count;

    }

   public function check_schedule_tz($_state){
        
        $state = $this->client_tz_state($_state);        
        $now = strtotime('now');
        $day = strtolower(date('D'));       
        $arr = array('status'=>0, 'now'=>$now);

        if( $state->tz == '' ) $state->tz = SERVER_TZ;
        
        $sched = @$state->sched->$day;
        if( $sched->start == '' ) $sched->start = DEFAULT_SCHED_START;
        if( $sched->end == '' ) $sched->end = DEFAULT_SCHED_END;

        //date_default_timezone_set('Australia/Sydney');

        $start = strtotime($sched->start);
        $end = strtotime($sched->end);

        //convert to timezone
        $sched_tz = $this->convert_now_tz($now, $state->tz);
 
        $now = $sched_tz->now;

        $arr['tz']      = $state->tz;
        $arr['now']     = $now;
        $arr['start']   = $start;
        $arr['end']     = $end;

        $arr['hol']     = '';

        $hol = $this->is_holiday_today($now);
        if( $hol != false ){                     
            $arr['hol'] = $hol;
            $arr['status'] = 0;
            return $arr;
        }elseif( $now >= $start && $now <= $end){
            $arr['status'] = 1;
            return $arr;
        }else{
            $arr['status'] = 0;         
            return $arr;
        }
    }

    function check_out_of_schedule_old(){

        $now = time();
        //$now = '1470272400';
        $now_date = date('Y-m-d');
        //$now_date = '2016-05-07';
        $now_day = date('D');
        //$now_day = 'Sat';

        $return = array('status'=>0, 'msg'=>'','advice_caller'=>'');
   
        $ss_query = $this->db->get_where('site_setting', array('set_name'=>'OVERRIDE_SCHED_FILTER'));
        $ss_query_row = $ss_query->row();
        $is_override = @$ss_query_row->set_value;       
       
        if( !$is_override ){

            //check for holiday
            $h_query = "SELECT * 
                FROM public_holiday
                WHERE effective_date = '".$now_date."'";
            
            $h_result = $this->db->query($h_query);
               

            if($h_result->num_rows() > 0){
                $now_day = 'holiday';
            }

            /*//Mon-Frid 7am-8am
            if( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && ($now >= strtotime($now_date.' 07:00:00')  &&  $now <= strtotime($now_date.' 08:00:00')) ){

                $return['status']       = 1;
                $return['msg']          = 'Mon-Frid 7am-8am';

            //Mon-Frid 8am-4pm
            }elseif( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && ($now > strtotime($now_date.' 08:00:00')  &&  $now < strtotime($now_date.' 16:00:00')) ){
               
                $return['status']       = 1;
                $return['msg']          = 'Mon-Frid 8am-4pm';
                //$return['advice_caller_line']= 'We cannot take bookings until after 4pm, please call us back then if needed and we will be happy to make a booking and have a doctor out to see you';  
                //$return['advice_caller']= 'Advice caller to call back after  4pm';  

            //Saturday 8am-10am
            }elseif ( $now_day == 'Sat' && ($now > strtotime($now_date.' 08:00:00')  &&  $now < strtotime($now_date.' 10:00:00')) ) {
                 
                $return['status']       = 1;
                $return['msg']          = 'Saturday 8am-10am';
                //$return['advice_caller_line']= 'We cannot take bookings until after 10am, please call us back then if needed and we will be happy to make a booking and have a doctor out to see you';
                //$return['advice_caller']= 'Advice caller to call back after 10am';
                
            }*/

            //Mon-Frid 7am-8am
            if( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && ($now >= strtotime($now_date.' 07:00:00')  &&  $now <= strtotime($now_date.' 08:00:00')) ){

                $return['status']       = 1;
                $return['msg']          = 'Mon-Frid 7am-8am';

            //Mon-Frid 8am-4pm
            }elseif( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && ($now > strtotime($now_date.' 08:00:00')  &&  $now < strtotime($now_date.' 18:00:00')) ){
               
                $return['status']       = 1;
                $return['msg']          = 'Mon-Frid 8am-6pm';

            //Saturday 8am-10am
            }elseif ( $now_day == 'Sat' && ($now > strtotime($now_date.' 08:00:00')  &&  $now < strtotime($now_date.' 10:00:00')) ) {
                 
                $return['status']       = 1;
                $return['msg']          = 'Saturday 8am-10am';
                
            }

        }else{
            $return = array('status'=>0, 'msg'=>'','advice_caller'=>'');            
        }

        return $return; 

    }



    /**
     * 
     * @param  [type] $region_type METRO OR REGIONAL
     * @return status=1 if out of schedule
     */
    function check_out_of_schedule($region_type='REGIONAL', $custom_sched_closed=''){

        $now = time();
        //$now = '1470272400';
        

        $now_date = date('Y-m-d');
        $now_time = date('H:i:s');
        //$now_dt = $now_date.' '.$now_time;        
        //$now_day = date('D', strtotime($now_dt));

        //debugging overide
       /* $now_day = 'Sun';
        $now_date = '2018-02-11';
        $now_dt  = '2018-02-11 04:00:00';*/
        //echo '$this->test_override_current_time():'.$this->test_override_current_time().'<br />';
        
        $test_override_current_time = $this->test_override_current_time();
        if( $test_override_current_time != '' ){
            $debug_dt = explode(' ', $test_override_current_time);
            $now_date = $debug_dt[0];
            $now_time = $debug_dt[1];
        }
 
        $now_dt = $now_date.' '.$now_time;        
        $now_day = date('D', strtotime($now_dt));

        $return = array('status'=>0, 'msg'=>'','advice_caller'=>'');
   
        $ss_query = $this->db->get_where('site_setting', array('set_name'=>'OVERRIDE_SCHED_FILTER'));
        $ss_query_row = $ss_query->row();
        $is_override = @$ss_query_row->set_value;       
       

        //debugging display
        //echo date_default_timezone_get().'<br />';
        //echo $now_day.'<br />';
        //echo $now_dt.'<br />';
        //echo $now_date.' 08:00:00: '.strtotime($now_date.' 08:00:00').'<br />';
        //echo $now_dt.': '.strtotime($now_dt).'<br />';
        //echo $now_date.' 16:00:00: '.strtotime($now_date.' 16:00:00').'<br />';



        $default_sched_close_time = ($custom_sched_closed!='')?' '.$custom_sched_closed:' 05:00:00';
//		$default_sched_close_time = ($custom_sched_closed!='')?' '.$custom_sched_closed:' 00:00:01';
        //echo $default_sched_close_time;

        if( !$is_override ){

            //check for on app holiday or from common-api holiday
            if( $this->is_holiday_today($now_date) ){
                $now_day = 'holiday';
                //echo 'Holiday';
            }



            if($region_type == 'METRO'){

                //IF METRO (sydney) - bookings weekdays from 4pm

                //Sunday night - Thursday night -  NO BOOKINGS between midnight & 4pm
                if( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 16:00:00', $now_dt) ){
                    
                    $return['status']       = 1;
                    $return['msg']          = 'Sunday night - Thursday night -  NO BOOKINGS between midnight & 4pm';

                //Friday night - NO BOOKINGS between midnight & 12am Sat
                }elseif( $now_day == 'Sat' && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 12:00:00', $now_dt) ){
                    
                    $return['status']       = 1;
                    $return['msg']          = 'Friday night - NO BOOKINGS between midnight & 12pm Sat';

                //Saturday night - NO BOOKINGS between midnight & 6am Sun
                }elseif( $now_day == 'Sun' && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 06:00:00', $now_dt) ){
                    $return['status']       = 1;
                    $return['msg']          = 'Saturday night - NO BOOKINGS between midnight & 6am Sun';
                }

            //REGIONAL
            }else{

                //Sunday night - Thursday night -  NO BOOKINGS between midnight & 6pm
                if( in_array( $now_day, array('Mon','Tue','Wed','Thu','Fri')) && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 18:00:00', $now_dt) ){
                    
                    $return['status']       = 1;
                    $return['msg']          = 'Sunday night - Thursday night -  NO BOOKINGS between midnight & 6pm';

                //Friday night - NO BOOKINGS between midnight & 10am Sat
                }elseif( $now_day == 'Sat' && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 12:00:00', $now_dt) ){
                    
                    $return['status']       = 1;
                    $return['msg']          = 'Friday night - NO BOOKINGS between midnight & 12pm Sat';

                //Saturday night - NO BOOKINGS between midnight & 6am Sun
                }elseif( $now_day == 'Sun' && $this->check_date_is_within_range($now_date.$default_sched_close_time, $now_date.' 06:00:00', $now_dt) ){
                    $return['status']       = 1;
                    $return['msg']          = 'Saturday night - NO BOOKINGS between midnight & 6am Sun';
                }
            }

        }else{
            $return = array('status'=>0, 'msg'=>'','advice_caller'=>'');            
        }



        /*echo '<pre>';
        print_r($return);
        echo '</pre>';
        exit;*/


        return $return; 

    }

    function check_date_is_within_range($start_date, $end_date, $todays_date){

      $start_timestamp = strtotime($start_date);
      $end_timestamp = strtotime($end_date);
      $today_timestamp = strtotime($todays_date);

      return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));

    }

    function test_override_current_time() {

        if( in_array($_SERVER['SERVER_NAME'], array('127.0.0.1', 'localhost', 'bookingpro-curehomedoctor-test.welldone.net.au')) ){
  
            $this->db->where('set_name', 'TESTING_OVERRIDE_CURRENT_TIME');
            $site_setting = $this->db->get('site_setting')->row();
            $test_datetime = @$site_setting->set_value;

            return trim($test_datetime);

        }else{
            return '';
        }
        
    }

    public function is_holiday_today($now=''){
 
        if( $this->test_override_current_time() != '' ){
            $debug_dt = explode(' ', $this->test_override_current_time());
            $now = $debug_dt[0];
        }

        $today = ($now=='')?date('Y-m-d'):$now;

        $this->db->where('effective_date', $today);
        $query = $this->db->get('public_holiday');
        
        $str = '';

        if($query->num_rows() > 0){
            /*$result = $query->result();
            
            foreach ($result as $row) {
                $str .= $row->holiday_name.' ';
            }
            return $str;*/

            return 1;
        }else{

            $obj = $this->check_common_holiday($now);
            //print_r($obj);
            if( @$obj->status AND count(@$obj->result) > 0 ){

                /*foreach ($obj->result as $row) {
                    $str .= $row->holiday_name.' ';
                }

                return $str;*/

                return 1;

            }else{
                return 0;               
            }

        }
    }

    public function check_common_holiday($now=''){

        /*$this->db->where('set_name', 'HOLIDAY_REGION');
        $site_setting = $this->db->get('site_settings')->row();
        $region = @$site_setting->set_value;*/


        $region = 'NSW';

        //$hol_date = date('Y-m-d');
        $hol_date = ($now=='')?date('Y-m-d'):$now;
        //$hol_date = '2017-04-18';

        if( $_SERVER['SERVER_NAME'] == 'localhost' ){
            $url = "http://localhost/local/common-api/holiday/api/index/".$hol_date."/".$region;
            $url = "http://localhost/common-api/holiday/api/index/".$hol_date."/".$region;
        }else{
            $url = "https://common-api.welldone.net.au/holiday/api/index/".$hol_date."/".$region;
        } 
        //echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
     

        $obj = (object)json_decode($head);

        return $obj;

    }

    public function get_all_holiday_api( $parameters = array()){


        try {

            if(!isset($parameters['yr'])) throw new Exception("Empty Year");


            if( $_SERVER['SERVER_NAME'] == 'localhost' ){           
                $url = "http://localhost/common-api/holiday/api/all/".@$parameters['yr']."/".@$parameters['region'];
            }else{
                $url = "https://common-api.welldone.net.au/holiday/api/all/".@$parameters['yr']."/".@$parameters['region'];
            } 
    

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $head = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
         

            $obj = (object)json_decode($head);
                
            return $obj;                    
            
        } catch (Exception $e) {

            echo $e->getMessage();
            
        }

    }

    public function sendSms($phoneNumber, $messsage, $provider='messagenet') {
 

        switch ($provider) {
            case 'messagenet':
                $this->load->library('Mailer', 'mailer');

                $this->mailer->send_to = $phoneNumber;
                $this->mailer->Subject = '';
                $this->mailer->Body = $messsage;
                return $this->mailer->sendSms();

                break;
            case 'smsglobal':
                $this->load->library('Smsglobal');
               
                return $this->smsglobal->sendSms($phoneNumber, $messsage);

                break;
            
            default:
             
                break;
        }

 
    }


}