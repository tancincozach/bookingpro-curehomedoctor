<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitesettingsmodel extends CI_Model{

	var $table = 'site_setting';
	var $id = 'id';

	function add($set){
  
		return $this->db->insert($this->table, $set);
	}

	function update($id, $set){
		
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $set);

	}

	function update_by_name($name, $set){
		
		$query = $this->db->get_where($this->table, array('set_name'=>$name));

		if( $query->num_rows() > 0 ){

			$this->db->where('set_name', $name);
			return $this->db->update($this->table, $set);

		}else{
			$set['set_name'] = $name;
			return $this->add($set);
		}

	}	

	function delete($id){

		$this->db->where($this->id, $id);	
		return $this->db->delete($this->table);
	}

	function get_row($where,$result='row'){

		$this->db->where($where);
		$query = $this->db->get($this->table);
		
		if($result=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function get_results($where=array(),$result='row'){

		if(!empty($where))
			$this->db->where($where);


		$query = $this->db->get($this->table);


		echo $this->db->last_query();

		if($result=='row')
			return $query->result();		
		else
			return $query->result_array();		
	}


	function get_results_where_string($where='',$result='row'){

		if($where!='')
			$this->db->where($where, null, false);


		$query = $this->db->get($this->table);

	
		if($result=='row')
			return $query->result();		
		else
			return $query->result_array();		
	}
}