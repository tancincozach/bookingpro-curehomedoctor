<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Areamodel extends CI_Model{ 

	/**
	 * Areas
	 */
	function add( $set )
	{

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}

		try{

			if(empty($user_id) && $user_id=='')

			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			if(!$this->db->insert('areas', $set))

			throw new Exception("Error: Unable to add an area. ");	

				$area_id = $this->db->insert_id();

	 					$this->insert_audit_trail(
									array(
									'target_table'=>'areas',
									'table_ref_col'=>'area_id',
									'table_action'=>'Add',
									'table_ref_id'=> $area_id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);			

				return $area_id;
		}
		catch(Exception $error)
	    {	
	  	  return  array('error'=>$error->getMessage());
	    }	
	}


	function update( $id, $set )
	{

		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
	
     	try {

			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			$this->db->where('area_id', $id);

 			
			if(!$this->db->update('areas',  $set))
			
			throw new Exception("Error: Unable to update. ");	


			$this->insert_audit_trail(
									array(
									'target_table'=>'areas',
									'table_ref_col'=>'area_id',
									'table_action'=>'Update',
									'table_ref_id'=> $id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);	
			return $id;

		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

	function get_result( $where=array() ){


		if(!empty($where))
			$this->db->where($where);

		$this->db->order_by('area_name', 'asc');

		$query = $this->db->get('areas');


		return $query->result();

	}

	function get_row($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		$this->db->select("*");

		$query = $this->db->get('areas');

		if($return_type=='row')
			return $query->row();
		elseif($return_type=='result')
			return $query->result();
		else
			return $query->row_array();
	}



	function get_array( $where=array() ){

		if(!empty($where))
			$this->db->where($where);

		$this->db->order_by('area_name', 'asc');

		$result = $this->db->get('areas')->result();

		$result_array = array();
		foreach( $result as $row ){
			$result_array[$row->area_id] = $row->area_name;
		}

		return $result_array;
	}

	function get_areas_availability_row($where , $return_type='row', $order=''){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		if( $order != '' )
			$this->db->order_by($order);
		
		$this->db->select("*");	
	
	
		$query = $this->db->get('areas_availability');

		if($return_type=='row')
			return $query->row();
		elseif($return_type=='result')
			return $query->result();
		else
			return $query->row_array();
	}

	function get_filtered_areas_availability($where ,$white_listed_column){

		$return_array= array();

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		
		 $this->db->select('*');	
	
	
		$query = $this->db->get('areas_availability');

		foreach ($query->row_array() as $field=>$row)
		{
			if(!in_array($field, $white_listed_column)) 
			{
				$return_array[$field] = $row;
			}		
		} 
		return $return_array;
	}

	function get_by_suburb_id($suburb_id){
		
		$this->db->where('suburb_id', $suburb_id);
		$query = $this->db->get('suburb')->row();
		
		return $query->area_id;
	}
	

	function get_result_suburb( $where=array()){


		if(!empty($where))
		{
    	  $this->db->where($where);
    	}

    	$query = $this->db->get('suburb')->result();
   

		return $query;

	}


	function get_suburb_area($where){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

    	$this->db->join('areas', 'areas.area_id = suburb.area_id', 'LEFT OUER');
    	$query = $this->db->get('suburb');
   

		return $query->result();

	}


	/**
	 * This is use to get the availabitiy status of an areas by current date or by date defined on second params
	 * @param  string $suburb_partial partial suburb text to be search
	 * @param  date(Y-m-d) $effective_date if not set current date will be the default
	 * @return [type]                 [description]
	 */
	function get_suburb_available($suburb_partial, $effective_date='',$params=array()){
		
		try {
			
			if($suburb_partial == '') throw new Exception("Search of suburb must be atleast 2 chars", 1);

			if( $effective_date == '') {
				$effective_date = date('Y-m-d');
			}
			
			$sql = "`areas`.`area_id`, `areas`.`area_name`, `suburb_id`, `suburb_name`, `suburb_postcode`";
			//$sql .= ", IF((SELECT `is_open` FROM `areas_availability` WHERE `areas_availability`.`area_id` = `suburb`.`area_id` AND `areas_availability`.`effective_date`='$effective_date' LIMIT 1 )=0,0,1) AS open_status";
			$sql .= ", CASE 
				WHEN areas_availability.is_open=1 THEN 1
				WHEN areas_availability.is_open=0 AND (areas_availability.reopen_dt_end IS NULL OR areas_availability.reopen_dt_end = '0000-00-00 00:00:00') THEN IF( NOW() >= areas_availability.reopen_dt_start, 1, 0 )
				WHEN areas_availability.is_open=0 AND ('$effective_date' BETWEEN areas_availability.reopen_dt_start AND areas_availability.reopen_dt_end) THEN 0
				ELSE 1
			END AS open_status,
			area_avail_id,
			region_type
			";

		
 			
				
 			$this->db->where('`areas`.`area_status`', '1');
 			$this->db->where('`suburb`.`suburb_status`', '1');
 			$this->db->where(" (lower(suburb.suburb_name) LIKE '%".strtolower($suburb_partial)."%' OR  `suburb`.`suburb_postcode` LIKE '%".trim($suburb_partial)."%') ");

			if(isset($params['where_str']) && $params['where_str']!=''){

				$this->db->where($params['where_str'], null, false);			
					
			}

			if(isset($params['where']) && $params['where']!=''){

				$this->db->where($params['where']);			
					
			}	

			/*$this->db->like('lower(suburb.suburb_name)', strtolower($suburb_partial));
			$this->db->or_like('suburb.suburb_postcode', trim($suburb_partial));*/
 
			$this->db->select($sql);

			$this->db->join('areas', 'areas.area_id = suburb.area_id', 'LEFT OUTER');			
			$this->db->join('areas_availability',' areas_availability.area_avail_id = (SELECT area_avail_id FROM areas_availability WHERE areas_availability.area_id = areas.area_id ORDER BY area_avail_created DESC LIMIT 1)','LEFT OUTER', false);
			$query = $this->db->get('suburb');			 
			
			$result = $query->result();
			

			return $result;

		} catch (Exception $e) {
			echo 'Error: '. $e->getMessage();
		}
	}

	public function check_and_insert_areas_availability( $parameters )
	{
		try 
		{
		  if(!isset($parameters['area_id']) || $parameters['area_id']=='') 
		  	throw new Exception("Error : Cannot identify area");

		  if(!isset($parameters['effective_date']) || $parameters['effective_date']=='') 
		  	throw new Exception("Error : Empty Effective Date");

		  if(!isset($parameters['additional_info']['user_id']) || $parameters['additional_info']['user_id']=='')
		  	throw new Exception("Error : Cannot identify user");

	 	  if(!isset($parameters['additional_info']['agent_name']) || $parameters['additional_info']['agent_name']=='')
		  	throw new Exception("Error : Cannot identify agent");

		  	 $query = $this->db
		  	 	  		   ->select('areas_availability.area_avail_id,areas_availability.area_id,areas_availability.user_id,areas_availability.agent_name,areas_availability.effective_date,areas_availability.is_open')
		  	 	           ->where(" areas_availability.area_id",$parameters['area_id'])
		  	 	           ->where(" areas_availability.effective_date",$parameters['effective_date'])
		  	 	  		   ->join("areas","areas.area_id = areas_availability.area_id",'LEFT')
		  	 	  		   ->get("areas_availability");

			if($query->num_rows() > 0)
  		    {

			  $area_avail_info = $query->row();

		  	  if($area_avail_info->is_open==0) 

			 	throw new Exception("Error : The selected date is unavailable , please select another date.");
				
			  	if($return_id = $this->update_areas_availability( array('is_open'=>0) , $area_avail_info->area_avail_id )!= 0 )
			  	{
	  			 	$this->insert_audit_trail( array(
									'target_table'=>'areas_availability',
									'table_ref_col'=>'area_id',
									'table_ref_id'=>$area_avail_info->area_id,
									'table_action'=>'Update',
									'user_id'=>$parameters['additional_info']['user_id'],
									'agent_name'=>$parameters['additional_info']['agent_name'],
									'message'=> json_encode(
															array(
																  'area_id'=>$area_avail_info->area_id,
																  'user_id'=>$area_avail_info->user_id,
																  'agent_name'=>$area_avail_info->agent_name,
																  'effective_date'=>$area_avail_info->effective_date,
																  'is_open'=>0
																)
															)
									)	
							  );	
	  			 	return  $area_avail_info->area_avail_id;
			  	}
		    }
		    else
		    {
		    	$insert = array(
		    					'effective_date'=>date('Y-m-d',strtotime($parameters['effective_date'])),
		    					'area_id'=>$parameters['area_id'],
		    					'user_id'=>$parameters['additional_info']['user_id'],
		    					 'agent_name'=>$parameters['additional_info']['agent_name']
		    					); 

					$this->insert_audit_trail( 

			 								array(
												'target_table'=>'areas_availability',
												'table_ref_col'=>'area_id',
												'table_action'=>'Add',
												'table_ref_id'=>$parameters['area_id'],
												'user_id'=>$parameters['additional_info']['user_id'],
												'agent_name'=>$parameters['additional_info']['agent_name'],
		 									    'message'=> json_encode($insert)
												)
							 				 );


					$this->db->insert('areas_availability', $insert);

					
				   return $this->db->insert_id();	
			
					 
		    } 
		  
		} 
		catch (Exception $e) 
		{
			return array('error'=>$e->getMessage());
		}
	}	

	/**
	 * check_and_insert_areas_availabilityv2 version 2 for adding areas availability with date/time range
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function check_and_insert_areas_availability2( $params ) {
		
		try{

			if(!isset($params['area_id']) || $params['area_id']=='') throw new Exception("Error : Cannot identify area");
			if(!isset($params['user_id']) || $params['user_id']=='') throw new Exception("Error : User identity not set");
			if(!isset($params['agent_name']) || $params['agent_name']=='') throw new Exception("Error : Agent name identity not set ");
			//if(!isset($params['reopen_dt_start']) || $params['reopen_dt_start']=='') throw new Exception("Error : Date/Time start not set");


			if( $this->db->insert('areas_availability', $params) ){

				$area_avail_id = $this->db->insert_id();

				$this->insert_audit_trail( 
					array(
						'target_table'=>'areas_availability',
						'table_ref_col'=>'area_id',
						'table_action'=>'Add',
						'table_ref_id'=>$params['area_id'],
						'user_id'=>$params['user_id'],
						'agent_name'=>$params['agent_name'],
					    'message'=> json_encode($params)
					)
 				 );


				return $area_avail_id;
			}else{
				return 0;
			}

		}catch (Exception $e) {
			// /echo $e->getMessage();
			return 0;
		}
	}

	/**
	 * [get_open description]
	 * @param  integer $area_id        
	 * @param  string  $effective_date default CURRENT_DATE
	 * @return integer                 1 open or 0 closed
	 */
	function is_open($area_id=0, $effective_date=''){

		try {
			
			if($area_id==0 OR $area_id == '') throw new Exception("area_id must be set", 1);
			

			if( $effective_date == '') {
				$effective_date = date('Y-m-d H:i:s');
			}

			//$sql = "IF( (SELECT `is_open` FROM `areas_availability` WHERE `areas_availability`.`area_id` = `areas`.`area_id` AND `areas_availability`.`effective_date` = '$effective_date') = 0, 0, 1) AS availability";
			$sql = "CASE 
				WHEN areas_availability.is_open=1 THEN 1
				WHEN areas_availability.is_open=0 AND (areas_availability.reopen_dt_end IS NULL OR areas_availability.reopen_dt_end = '0000-00-00 00:00:00') THEN IF( NOW() >= areas_availability.reopen_dt_start, 1, 0 )
				WHEN areas_availability.is_open=0 AND (NOW() BETWEEN areas_availability.reopen_dt_start AND areas_availability.reopen_dt_end) THEN 0
				ELSE 1
			END AS availability,";
	 	
			$this->db->where('areas.area_id', $area_id);			
			$this->db->where('areas.area_status', 1);			

			$this->db->select($sql);

			$this->db->join('areas_availability',' areas_availability.area_avail_id = (SELECT area_avail_id FROM areas_availability WHERE areas_availability.area_id = areas.area_id ORDER BY area_avail_created DESC LIMIT 1)','LEFT OUTER', false);

			$query = $this->db->get('areas');
			
			$result = $query->row();		 

			return @$result->availability;

		} catch (Exception $e) {
			echo 'Error: '. $e->getMessage();
		}
 
	}




	function get_result_pagination( $parameters )
	{

		
		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


    		if($this->user_lvl.$this->user_sub_lvl == 53){

				$areas 	=  $this->get_areas_by_chaperone($this->user_id, 'array');

					$filter_area_client_admin = array();

					foreach ($areas as $i=>$value) {

						$filter_area_client_admin[] = $i;
										
					}


					if( isset($filter_area_client_admin) AND count($filter_area_client_admin)>0 ){ 
					 
					if( count($filter_area_client_admin) == 1 ){

						$this->db->where(array('areas.area_id'=>$filter_area_client_admin[0])) ;

					}else{
						
						$this->db->where(' `areas`.`area_id` IN ('.implode(',',array_values($filter_area_client_admin)).')', null, false);
					}
				}
	      	}



    	
    // 	$sql = "areas.area_id,areas.area_name,areas.area_status, areas.timezone,
	 		// count(DISTINCT(suburb.suburb_id)) as suburb_count ,
	 		// count(DISTINCT(areas_cars_link.car_id)) as car_count, 
	 		// count(DISTINCT(areas_chaperone_link.user_id)) as chaperone_count, 
	 		// areas_availability.area_avail_id ,
	 		// IF( areas_availability.is_open IS NULL OR areas_availability.is_open=1, 1, 0) AS open_status";

    	
    	$sql = "areas.area_id,areas.area_name,areas.area_status, areas.timezone, areas.region_type,
	 		count(DISTINCT(suburb.suburb_id)) as suburb_count ,
	 		count(DISTINCT(areas_cars_link.car_id)) as car_count, 
	 		count(DISTINCT(areas_chaperone_link.user_id)) as chaperone_count, 
	 		areas_availability.area_avail_id ,
	 		areas_availability.is_open,
			areas_availability.reopen_dt_start,
			areas_availability.reopen_dt_end, 
			CASE 
				WHEN areas_availability.is_open=1 THEN 1
				WHEN areas_availability.is_open=0 AND (areas_availability.reopen_dt_end IS NULL OR areas_availability.reopen_dt_end = '0000-00-00 00:00:00') THEN IF( NOW() >= areas_availability.reopen_dt_start, 1, 0 )
				WHEN areas_availability.is_open=0 AND (NOW() BETWEEN areas_availability.reopen_dt_start AND areas_availability.reopen_dt_end) THEN 0
				ELSE 1
			END AS open_status,
			areas.custom_sched_closed
			";

	 	$this->db->select($sql)
		   ->order_by('areas.area_name','asc')
 		   ->group_by('areas.area_id')			         		   
 		   ->join('suburb','suburb.area_id = areas.area_id and suburb.suburb_status=1','LEFT OUTER')		         		  
 		   ->join('areas_cars_link','areas_cars_link.area_id = areas.area_id','LEFT OUTER')
 		   ->join('areas_chaperone_link','areas_chaperone_link.area_id = areas.area_id','LEFT OUTER') 		   
    	   //->join('areas_availability','areas_availability.area_id = areas.area_id AND (areas_availability.effective_date IS NULL OR  areas_availability.effective_date = "'.$parameters['effective_date'].'")','left', false);		
    	   ->join('areas_availability',' areas_availability.area_avail_id = (SELECT area_avail_id FROM areas_availability WHERE areas_availability.area_id = areas.area_id ORDER BY area_avail_created DESC LIMIT 1)','LEFT OUTER', false);		

 
		if(isset($parameters['search']))
    	{
    		$this->db

    			 ->like('areas.area_name',$parameters['search']);    			 
    	}

		$query  = $this->db->get('areas');		

		 

	    return array(  'result'=> $query->result()) ; 

	    $query->free_result(); 

 }

function suburb_get_result_pagination( $parameters ){

	
	 if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by(@$parameters['order_by']['field'],@$parameters['order_by']['direction']);
    	}
    	
    	if(isset($parameters['group_by']))
		{
    	  $this->db->group_by(@$parameters['group_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

    	if(isset($parameters['join']))
		{
    	   $this->db->join($parameters['join']['table'],$parameters['join']['conditional'],$parameters['join']['type']);
    	}

		if(isset($parameters['search']) && !empty($parameters['search']	))
    	{
    		$this->db
    			 ->like('suburb_name',$parameters['search']);    			 
    	}

		$query   = $this->db->get('suburb');


		$total_rows = $query->num_rows();

		$query->free_result();


 
	 if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}


		if(isset($parameters['select']))
		{
    	  $this->db->select($parameters['select']);
    	}

    	if(isset($parameters['limit']) && !empty($parameters['limit']))
        {                   
           $this->db->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset']);
        }

		if(isset($parameters['order_by']))
		{
    	  $this->db->order_by(@$parameters['order_by']['field'],@$parameters['order_by']['direction']);
    	}
    	
    	if(isset($parameters['group_by']))
		{
    	  $this->db->group_by($parameters['group_by']);
    	}
    	
    	if(isset($parameters['having']))
		{
    	  $this->db->having($parameters['having']);
    	}

  		if(isset($parameters['join']))
		{
    	   $this->db->join($parameters['join']['table'],$parameters['join']['conditional'],$parameters['join']['type']);
    	}
 
	   if(isset($parameters['search']) && !empty($parameters['search']	))
    	{
    		$this->db
    			 ->like('suburb_name',$parameters['search']);    			 
    	}


		$query   = $this->db->get('suburb');		
		

		
	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 


	}


	function suburb_add( $insert_array )
	{

	  	$area = array()	;

	  	$ctr=0;


		$user_id = '';
		$agent_name = ''; 

		if( isset($insert_array['user_id']) ) {
			$user_id = $insert_array['user_id'];
			unset($insert_array['user_id']);
		}

		if( isset($insert_array['agent_name']) ) {
			$agent_name = $insert_array['agent_name']; 
			unset($insert_array['agent_name']);
		}

		try
		{
				if(empty($user_id))
				throw new Exception("Error: No User found ");	

				if(empty($agent_name))
					throw new Exception("Error: No Agent found ");	


		  		foreach($this->db->select('area_id,area_name')->get('areas')->result_array() as $id=>$value)
		  		{	
				  foreach ($value as $key => $value)
				  {
					$area[$key][]= $value;
				  }
		  		}

		     if(isset($insert_array['suburb_area']) && !empty($insert_array['suburb_area']) && $insert_array['suburb_area']!='')
		     {

		     	if(!in_array($insert_array['suburb_area'], $area['area_name']))
		  		{  					  			
		  			
		  			$insert_array['area_id'] = $this->add(array('area_name'=>$insert_array['suburb_area'],'user_id'=>$user_id,'agent_name'=>$agent_name));
		  		}
		  		else
		  		{  	 					
					if(isset($insert_array['area_id']))
					{
						$key  = array_search(@$insert_array['area_id'] ,@$area['area_id']); 
				    	$insert_array['area_id'] = $area['area_id'][$key];	
				    	$insert_array['suburb_area'] = $area['area_name'][$key];
					}
					else
					{
						$key  = array_search(@$insert_array['suburb_area'] ,@$area['area_name']); 
				    	$insert_array['area_id'] = $area['area_id'][$key];	
				    	$insert_array['suburb_area'] = $area['area_name'][$key];
					}

		  		    		  			

		  		}

		  					
		  		$query   = $this->db
						  		->select('suburb_id')
						  		->where(array('suburb_name'=>$insert_array['suburb_name'], 'area_id'=>$insert_array['area_id'], 'suburb_postcode'=>$insert_array['suburb_postcode']))
						  		->get('suburb');	

		     }	
		     else
		     {		     	
		     	$query_area   = $this->db
						  		->select('area_id,area_name')
						  		->where(array('area_id'=>$insert_array['area_id']))
						  		->get('areas');

				$area_result  = $query_area->result();

				if($query_area->num_rows()==0)
				{
					throw new Exception("Error: No area found ");			
				}

				$insert_array['area_id']    = $area_result[0]->area_id;
				$insert_array['suburb_area'] = $area_result[0]->area_name;

		  		$query   = $this->db
						  		->select('suburb_id')
						  		->where(array('suburb_name'=>$insert_array['suburb_name'], 'area_id'=>$insert_array['area_id'], 'suburb_postcode'=>$insert_array['suburb_postcode']))
						  		->get('suburb');


		     }
		  		

			//echo $this->db->last_query();exit();

				if($query->num_rows()==0)
				{						


					if(!$this->db->insert('suburb' , $insert_array) )
					{
						throw new  Exception("ERROR : Unable to add suburb");						
					}

					$suburb_id = $this->db->insert_id();
					$this->insert_audit_trail( 
								array(
								'target_table'=>'suburb',
								'table_ref_col'=>'suburb_id',
								'table_action'=>'Add',
								'table_ref_id'=>$suburb_id,
								'user_id'=>$user_id,
								'agent_name'=>$agent_name,
								    'message'=> json_encode($insert_array)
								)
								 );

				   return $suburb_id;			
				}
				else
				{		
				 	$existing_suburb  = $query->result();			


				  	if(count($existing_suburb) > 0 )
				  	{
						$update_result = $this->update_suburb($existing_suburb[0]->suburb_id,array_merge($insert_array,array('user_id'=>$user_id,'agent_name'=>$agent_name)));


				  		if(is_array($update_result) && array_key_exists('error', $update_result))
				  		{
				  				throw new Exception($update_result['error']);
				  		}

				  		return $update_result;
				  	}
				  	else
				  	{

				  		return  0 ;						
				  	}
				}
		}
		catch(Exception $error)
	    {	
	  	  return  array('error'=>$error->getMessage());
	    }		

	}


	function update_suburb( $id ,$set)
	{



		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}


		try
		{
			if(empty($user_id) && $user_id=='')

			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	


			if(!$this->db->where('suburb_id',$id )->update('suburb',$set))			
			 {
				throw new Exception("Error: Unable to update suburb ");			
			 }



		 	   $this->insert_audit_trail( 
				 								array(
													'target_table'=>'suburb',
													'table_ref_col'=>'suburb_id',
													'table_action'=>'Update',
													'table_ref_id'=>$id,
													'user_id'=>$user_id,
													'agent_name'=>$agent_name,
			 									    'message'=> json_encode($set)
													)
								 				 );	
		 	  return $id;
		}	
		catch(Exception $error)
		{
			return 	array('error'=>$error->getMessage());
		}		
	}

	function update_areas_availability( $set ,$id)
	{
	  return ($this->db->where('area_avail_id',$id )->update('areas_availability',$set ) ? $id:0);				
	}


	
	function get_area_availabilty_history($parameters)
	{
	  $return_array = array();

	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter");
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->join('areas','areas.area_id=table_audit_trail.table_ref_id','left',FALSE)
	  			 	  ->order_by('table_audit_trail.id','desc')
	  			 	  ->get('table_audit_trail');


	  	if($query->num_rows() > 0)
	  	{
	  		
			  foreach($query->result_array() as $row_key=>$row_val)
			  {	
			  	foreach ($row_val as $key => $value) {
			  		if($key!='message')
			  			$return_array[$key][]=$value;
			  		else
						$return_array[$key][]=json_decode($value,true);
			  	}
			  }
			  return $return_array;
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function car_add($set)
	{
 		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}	

		try{

			if(empty($user_id) && $user_id=='')

			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			if(!$this->db->insert('areas_cars_link', $set))

				throw new Exception("Error: Unable to add a car. ");	

			$car_id = $this->db->insert_id();
				

	 					$this->insert_audit_trail(
									array(
									'target_table'=>'areas_cars_link',
									'table_ref_col'=>'area_id',
									'table_action'=>'Add',
									'table_ref_id'=> $set['area_id'],
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);			

				return $car_id;
		}
		catch(Exception $error)
	    {	
	  	  return  array('error'=>$error->getMessage());
	    }	
	}

	function car_update($id,$set)
	{
		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['car_id']) ) {
			$car_id = $set['car_id'];
			unset($set['car_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}
			try {

			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			$this->db->where('car_id', $id);

 			
			if(!$this->db->update('areas_cars_link',  $set))
			
			throw new Exception("Error: Unable to update. ");	


			$this->insert_audit_trail(
									array(
									'target_table'=>'areas_cars_link',
									'table_ref_col'=>'car_id',
									'table_action'=>'Add',
									'table_ref_id'=> $car_id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);	
			return $id;

		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	

	}

	function get_row_by_car($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);	
		}else{
			$this->db->where($where, null, false);
		}

		$this->db->select("*");

		$query = $this->db->get('areas_cars_link');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function get_cars_per_area($parameters)
	{


		if(!empty($parameters['where']))
			{
				if(is_array($parameters['where']))
				{
					$this->db->where($parameters['where']);		
				}
				else
				{
					$this->db->where($parameters['where'], null, false);		
				}
	    	  
	    	}


			if(isset($parameters['order_by']))
			{
	    	  $this->db->order_by(@$parameters['order_by']['field'],@$parameters['order_by']['direction']);
	    	}
	    	
	    	if(isset($parameters['group_by']))
			{
	    	  $this->db->group_by(@$parameters['group_by']);
	    	}
	    	
	    	if(isset($parameters['having']))
			{
	    	  $this->db->having($parameters['having']);
	    	}

	    	if(isset($parameters['join']))
			{
	    	   $this->db->join($parameters['join']['table'],$parameters['join']['conditional'],$parameters['join']['type']);
	    	}

			if(isset($parameters['search']) && !empty($parameters['search']	))
	    	{
	    		$this->db
	    			 ->like('car_name',$parameters['search'])
	    			 ->or_like('car_plateno',$parameters['search']);	    			 
	    	}

			$query   = $this->db->get('areas_cars_link');


			$total_rows = $query->num_rows();

			$query->free_result();


			 if(!empty($parameters['where']))
			{
				if(is_array($parameters['where']))
				{
					$this->db->where($parameters['where']);		
				}
				else
				{
					$this->db->where($parameters['where'], null, false);		
				}
	    	  
	    	}


			if(isset($parameters['select']))
			{
	    	  $this->db->select($parameters['select']);
	    	}

	    	if(isset($parameters['limit']) && !empty($parameters['limit']))
	        {                   
	           $this->db->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset']);
	        }

			if(isset($parameters['order_by']))
			{
	    	  $this->db->order_by(@$parameters['order_by']['field'],@$parameters['order_by']['direction']);
	    	}
	    	
	    	if(isset($parameters['group_by']))
			{
	    	  $this->db->group_by($parameters['group_by']);
	    	}
	    	
	    	if(isset($parameters['having']))
			{
	    	  $this->db->having($parameters['having']);
	    	}

	  		
	    	   $this->db->join($parameters['join']['table'],$parameters['join']['conditional'],$parameters['join']['type']);
	    	

			if(isset($parameters['search']) && !empty($parameters['search']	))
	    	{
	    		$this->db
	    			 ->like('car_name',$parameters['search'])
	    			 ->or_like('car_plateno',$parameters['search']);	    			 
	    	}


			$query   = $this->db->get('areas_cars_link');		
			
		    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

		    $query->free_result(); 
	  	
	}

	function get_area_id_chaperone( $where=array()){

		if(!empty($where))

		$this->db->where($where);


		$this->db->select('ac_link_id,area_id');

		$query = $this->db->get('areas_chaperone_link');
		
		$return_array = array();
	   
	  	if($query->num_rows() > 0)
	  	{
	  	  foreach ($query->result() as $row)
		  {
		   $return_array['ac_link_id'][] = $row->ac_link_id;
		   $return_array['area_id'][] = $row->area_id;	
		  }	
	  	}
				
		return $return_array;
	}



	function insert_chaperone_by_area($parameters , $chaperone_id='')
	{

		try {
		 
		 	if( $chaperone_id == '' ) throw new Exception("Error: Chaperone ID must be provided", 1);



		 	if(!isset($parameters['area_id']) || count($parameters['area_id'])==0){
		  		//throw new Exception("Error: Fail to add chaperone by area ");
		  		
		  		$this->db->delete('areas_chaperone_link', array('user_id'=>$chaperone_id));

	 		}else{
 
				$insert = array();

				foreach($parameters['area_id'] as $id){

					$this->db->delete('areas_chaperone_link', array('user_id'=>$chaperone_id));

					$insert[] = array('area_id'=>$id,'user_id'=>$chaperone_id);

				}

				if(count($insert) > 0)
				$this->add_area_chaperone($insert);

			}
			
		}catch (Exception $e){

			return array('error'=>$e->getMessage());	

		}     
	}

	function insert_chaperone_by_user($parameters)
	{
	 	try {

	 		if(count($parameters['user_id'])==0 || !isset($parameters['area_id']))

	 			throw new Exception("Error: Fail to add chaperone by user");

		 	foreach ($parameters['user_id'] as $id)
		 	{
	 			   $insert[] = array('area_id'=>$parameters['area_id'],'user_id'=>$id);
	 		}

		  	if(count($insert) > 0)
			  $this->add_area_chaperone($insert);		

	 	} catch (Exception $e){
			return array('error'=>$error->getMessage());	 		
	 	}	  
	}

	function remove_chaperone_by_user($parameters)
	{
		try {

	 		if(count($parameters['user_id'])==0 || !isset($parameters['area_id']))

	 			throw new Exception("Error: fail to remove chaperone");

		 	foreach ($parameters['user_id'] as $id)
		 	{
	 			   	$this->db->delete('areas_chaperone_link', array('user_id'=>$id,'area_id'=>$parameters['area_id']));

	 		}	

	 	} catch (Exception $e){
			return array('error'=>$error->getMessage());	 		
	 	}	  
	}


	 function add_area_chaperone($set = array())
	{
	   try
	   {
	   	 if(empty($set)) throw new Exception("Error : Empty Chaperone ID");

	   	    $this->db->insert_batch('areas_chaperone_link', $set); 


	   	 return true;
	   }
	  catch(Exception $error)
	  {			

		return array('error'=>$error->getMessage());
	   }	   
	}
	
	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	/**
	 * Get areas assign to chaperones
	 * @param  [type] $user_id    [description]
	 * @param  string $resultType [description]
	 * @return result  or array
	 * use in dashboard/chaperone_switch_area_doctor
	 */
	function get_areas_by_chaperone($user_id, $resultType='result'){

		if( $resultType == 'array' ){
			$this->db->select('`areas`.`area_id`, `areas`.`area_name`');
		}

		$this->db->where('user_id', $user_id);

		$this->db->where("`areas`.`area_status`", 1);

		$this->db->join('areas', '`areas`.`area_id` = `areas_chaperone_link`.`area_id`', ' LEFT OUTER ' );
		$query = $this->db->get('areas_chaperone_link');		
		
		$result = '';

		if( $resultType == 'array' ){

			//$result = array(''=>'---select---');

			foreach ($query->result() as $row) {
				$result[$row->area_id] = $row->area_name;
			}
		}else{
			$result = $query->result();
		}

		return $result;
	}

	/**
	 * [get_cars_by_area_id description]
	 * @param  [type] $area_id    [description]
	 * @param  string $resultType [description]
	 * @return result  or array
	 * use in dashboard/chaperone_switch_area_doctor
	 */
	function get_cars($resultType='result', $select=''){

		//$this->db->where('`area_id`', $area_id);

		if(!empty($select)){
			$this->db->select($select);
		}

		$this->db->where('`car_deleted`', '0');

		$this->db->order_by('car_name', 'asc');

		$query = $this->db->get('areas_cars_link');
			 
		$result = '';

		if( $resultType == 'array' ){

			$result = array(''=>'---select---');

			foreach ($query->result() as $row) {
				$result[$row->car_id] = $row->car_name.' ('.$row->car_plateno.')';
			}
		}else{
			$result = $query->result();
		}

		return $result;
	}


	function get_cars_result($where){

 
		if( !is_array($where) ) return array();

		$this->db->where($where);

		$this->db->where('`car_deleted`', '0');
		$query = $this->db->get('areas_cars_link');

		return $query->result();	 
	}

	function get_charperones_by_area_id($area_id, $resultType='result'){

		$this->db->select('`areas_chaperone_link`.`user_id`, `user_fullname`');

		$this->db->where('`areas_chaperone_link`.`area_id`', $area_id);		

		$this->db->join('`user`', '`user`.`user_id` = `areas_chaperone_link`.`user_id`');

		$query = $this->db->get('areas_chaperone_link');

		$result = '';

		if( $resultType == 'array' ){

			$result = array(''=>'---select---');

			foreach ($query->result() as $row) {
				$result[$row->user_id] = $row->user_fullname;
			}
		}else{
			$result = $query->result();
		}

		return $result;

	}

	//temporary only
	function get_area_timezone($areas = array()){

		$timezones = array();

		if( count($areas) == 0 OR !is_array($areas) ) {

			$timezones[] = $this->default_timezone;
			return $timezones;

		}
 

		$this->db->where( 'area_id IN ('.implode(',', $areas).')' );
		$query = $this->db->get('areas');
 		//echo $this->db->last_query();
		
		if( $query->num_rows() > 0 ){

			$result = $query->result();

			foreach( $result as $row ){
				if( $row->timezone != '' ){
					if( !in_array($row->timezone, $timezones)  ){

						$timezones[] = $row->timezone;
						
					}
				}
			}
			
			if(count($timezones)>0){
				return $timezones;
			}else{
				$timezones[] = $this->default_timezone;
				return $timezones;
			}

		}else{
			$timezones[] = $this->default_timezone;
			return $timezones;
		}

	}

}