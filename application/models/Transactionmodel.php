<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactionmodel extends CI_Model{ 

	function add($set)
	{

		$ref_number = $this->generate_refno();

		$set['ref_number'] 			= $ref_number;
		$set['ref_number_formatted'] 	= 'HCD'.$ref_number;

		return ($this->db->insert('transaction', $set))?$this->db->insert_id():0;

	}

 	/**
 	 * This is function will generate a reference_number
 	 * @return int
 	 */
 	function generate_refno(){

 		$this->db->select('ref_number');
 		$this->db->order_by('ref_number', 'DESC');
 		$this->db->where('DATE_FORMAT(tran_created, "%Y%m") = ',date('Ym'));
 		$this->db->limit(1);
 		$result = $this->db->get('transaction')->row();
 		//echo $this->db->last_query();
 		if(isset($result->ref_number) AND $result->ref_number>0){
 			$ref_number = intval($result->ref_number);
 			$ref_number++;
 		}else{
 			$ref_number = date('ym').'0001';
 		}

 		return $ref_number;

 	}

 	function get_result_pagination($params){

		// general totals rows
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		$this->db->select('count(*) as total');
		$query = $this->db->get('transaction');
		$total_rows = $query->row()->total;
		$query->free_result(); //free results


		//generate query results
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		//limits
		if(isset($params['limits'])){
			$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
		}

		//sorting
		if( isset($params['sorting']) ){
			if( is_array($params['sorting']) ){
				$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
			}else{
				$this->db->order_by($params['sorting']);
			}

		}else{
			$this->db->order_by('`transaction`.`tran_id`', 'desc');
		}

		$select = "*";

		$this->db->select($select); 
		$query = $this->db->get('transaction');
		$result = $query->result();
		$query->free_result(); //free results

		return array('results'=>$result, 'total_rows'=>$total_rows);

 	}

 	function get_result_tran_app_pagination($params){

		// general totals rows
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		$this->db->select('count(*) as total');
		$this->db->join('`appointment`', '`appointment`.`tran_id` = `transaction`.`tran_id`', 'LEFT OUTER');
		$this->db->join('`patient`', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$query = $this->db->get('transaction');
		$total_rows = $query->row()->total;
		$query->free_result(); //free results


		//generate query results
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		//limits
		if(isset($params['limits'])){
			$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
		}

		//sorting
		if( isset($params['sorting']) ){
			if( is_array($params['sorting']) ){
				$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
			}else{
				$this->db->order_by($params['sorting']);
			}

		}else{
			$this->db->order_by('`transaction`.`tran_id`', 'desc');
		}

		$select = "*";
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."') AS appt_created_tz ";
		$select .= ", CONVERT_TZ(tran_created, 'Australia/Sydney', '".$this->default_timezone."') AS tran_created_tz ";

		$this->db->select($select); 

		$this->db->join('`appointment`', '`appointment`.`tran_id` = `transaction`.`tran_id`', 'LEFT OUTER');
		$this->db->join('`patient`', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$query = $this->db->get('transaction');
		$result = $query->result();
		$query->free_result(); //free results

		return array('results'=>$result, 'total_rows'=>$total_rows);

 	}

}