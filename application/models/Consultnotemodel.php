<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConsultnoteModel extends CI_Model
{ 
	/**
	 * Practice
	 */
	function add( $set )
	{

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}

		try{

			if(empty($user_id) && $user_id=='')

			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			if(!$this->db->insert('contacts', $set))

			throw new Exception("Error: Unable to add an area. ");	

				$area_id = $this->db->insert_id();

	 					$this->insert_audit_trail(
									array(
									'target_table'=>'contacts',
									'table_ref_col'=>'id',
									'table_action'=>'Add',
									'table_ref_id'=> $area_id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);			

				return $area_id;
		}
		catch(Exception $error)
	    {	
	  	  return  array('error'=>$error->getMessage());
	    }	
	}


	function update( $id, $set )
	{

		if( $id <= 0 OR $id=='' ) return 0;

		$user_id = '';
		$agent_name = ''; 

		if( isset($set['user_id']) ) {
			$user_id = $set['user_id'];
			unset($set['user_id']);
		}

		if( isset($set['agent_name']) ) {
			$agent_name = $set['agent_name']; 
			unset($set['agent_name']);
		}

     	try {

			if(empty($user_id) && $user_id=='')
						
			throw new Exception("Error: No User found ");	

			if(empty($agent_name)  && $agent_name=='')

			throw new Exception("Error: No Agent found ");	

			$this->db->where('id', $id);

 			
			if(!$this->db->update('contacts',  $set))

		
			
			throw new Exception("Error: Unable to update. ");	



			$this->insert_audit_trail(
									array(
									'target_table'=>'contacts',
									'table_ref_col'=>'id',
									'table_action'=>'Add',
									'table_ref_id'=> $id,
									'user_id'=>$user_id,
									'agent_name'=>$agent_name,
									'message'=> json_encode($set)
									)
			  				);	
			return $id;

		} catch (Exception $e) {

			return array('error'=>$e->getMessage());
		}	
		
	}

	function get_result( $params=array() ){


		if(!empty($params['where'])){			
			$this->db->where($params['where']);
		}

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}
			
		$this->db->order_by('contact_title', 'asc');

		$query = $this->db->get('contacts');

		echo $this->db->last_query();

		return $query->result();

	}

	function get_row($where , $return_type='row'){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		$this->db->select("*");

		$query = $this->db->get('contacts');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}


	function get_result_pagination( $parameters )
	{

		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}

		if(isset($parameters['search']) && !empty($parameters['search']))
    	{
    		$this->db
    			 ->or_like('contact_title',$parameters['search']);    			 
    	}
		$query   = $this->db->get('contacts');
		   
	
		$total_rows = $query->num_rows();

		$query->free_result();


		if(!empty($parameters['where']))
		{
			if(is_array($parameters['where']))
			{
				$this->db->where($parameters['where']);		
			}
			else
			{
				$this->db->where($parameters['where'], null, false);		
			}
    	  
    	}
    	
		if(isset($parameters['search']) && !empty($parameters['search']))
    	{
    		$this->db
    			 ->like('contact_title',$parameters['search']);    			 
    	}

    	 $query = $this->db->select("*")
			 			   ->limit(@$parameters['limit']['limit'],@$parameters['limit']['offset'])						   		         		   
		    	    	   ->get('contacts');		




	    return array(  'result'=> $query->result() ,'total_rows'=>$total_rows) ; 

	    $query->free_result(); 

 }
 	function get_user_row($where , $return_type='row'	){

		if( !is_array($where) ){
			$this->db->where($where);
		}else{
			$this->db->where($where, null, false);
		}

		
		 $this->db->select("*");	
	
	
		$query = $this->db->get('contacts');

		if($return_type=='row')
			return $query->row();
		else
			return $query->row_array();
	}

	function insert_audit_trail($parameters)
	{	
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);

	    if(!isset($parameters['target_table']))
	    	throw new Exception("Error : Table name must not be empty.");

	    if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
	    	throw new Exception("Error : Table index name and value  must not be empty.");
	    if(!isset($parameters['user_id']))
	    	throw new Exception("Error : User ID  must not be empty.");
	    if(!isset($parameters['agent_name']))
	    	throw new Exception("Error : Agent Name  must not be empty.");
	    if(!isset($parameters['message']))
	    	throw new Exception("Error : Message Activity  must not be empty.");	    		
		return ($this->db->insert('table_audit_trail', $parameters))?$this->db->insert_id():0;
	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}

	function get_audit_trail( $parameters  , $result = 'row')
	{
	  try
	  {
	  	if(count($parameters)==0) throw new Exception("Error : Empty Parameter", 1);
		if(!isset($parameters['target_table']))
			throw new Exception("Error : Table name must not be empty.");

		if(!isset($parameters['table_ref_col']) && !isset($parameters['table_ref_id']))
			throw new Exception("Error : Table index name and value  must not be empty.");
	  		
	  	$query = $this->db
	  			  	  ->where('target_table',$parameters['target_table'])
	  			 	  ->where('table_ref_col',$parameters['table_ref_col'])
	  			 	  ->where('table_ref_id',$parameters['table_ref_id'])
	  			 	  ->get('table_audit_trail');




	  	if($query->num_rows() > 0)
	  	{
	  		if($result=='row')	  		
	  		return $query->result();
	  		else
  			return $query->result_array();
	  	}

	  }	
	  catch(Exception $error)
	  {	
	  	return  $error->getMessage();
	  }
	}
}