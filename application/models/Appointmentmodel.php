<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appointmentmodel extends CI_Model{ 

	function add($set)
	{
		try {
			
			//insert to transaction
			$ref_number = $this->generate_refno();

			if( isset($set['tran_type']) ){
				$tran_type = $set['tran_type'];
			}else{
				$tran_type = ($set['booking_when']=='today')?'Booking Made':'Booked Out - Next Day Booked';				
			}


			$tran_set['ref_number'] 			= $ref_number;
			$tran_set['ref_number_formatted'] 	= ID_PREFIX.$ref_number;
			$tran_set['tran_type'] 				= $tran_type;
			$tran_set['tran_desc'] 				= '';
			$tran_set['agent_name'] 			= $set['book_by'];
			$tran_set['user_id'] 				= $set['book_by_user_id'];			
			
			$tran_set['caller_phone'] 			= $set['phone'];	
			$tran_set['caller_firstname'] 		= $set['firstname'];
			$tran_set['caller_lastname'] 		= $set['lastname'];
			$tran_set['caller_suburb'] 			= $set['suburb'];		

			unset($set['tran_type']);
			unset($set['booking_when']);
			
			unset($set['phone']);
			unset($set['firstname']);
			unset($set['lastname']);
			unset($set['suburb']);

			$tran_id = $this->add_transaction($tran_set);

			if( $tran_id == 0 ) throw new Exception("Error on creating new transaction", 1);

			$set['tran_id'] =  $tran_id;

			return ($this->db->insert('appointment', $set))?$this->db->insert_id():0;

		} catch (Exception $e) {
			return 0;
		}		
	}

	function set_parent_related_appt_id($related_appt_id){

		$set['related_appt_id'] = $related_appt_id;

		$this->db->where('appt_id', $related_appt_id);
		$this->db->update('appointment', $set);
	}

	function add_transaction($set){

		return ($this->db->insert('transaction', $set))?$this->db->insert_id():0;
	}

	function update_medicare($params){
		
		try {

			//if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			//update appointment
			$this->db->where('appt_id', $params['appt_id']); 
			$this->db->update('appointment', $params['set_appt']);
			//echo $this->db->last_query();

			//update patient
			$this->db->where('patient_id', $params['patient_id']); 
			$this->db->update('patient', $params['set_patient']);
			//echo $this->db->last_query();

			//save history here
			return 1;
		} catch (Exception $e) {

			return 0;

		}
	}	


	function update_appointment($params){
		
		try {

			//if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			//update appointment
			$this->db->where('appt_id', $params['appt_id']); 
			if( $this->db->update('appointment', $params['set_appt']) ){

				$this->insert_audit_trail(
					array(
						'target_table'=>'appointment',
						'table_ref_col'=>'appt_id',
						'table_action'=>'update',
						'table_ref_id'=> $params['appt_id'],
						'user_id'=>$this->user_id,
						'agent_name'=>$this->agent_name,
						'message'=> json_encode($params['set_appt'])
					)
				);	

			}
			//echo $this->db->last_query();
			

			//save history here
			return 1;
		} catch (Exception $e) {

			return 0;

		}
	}	
	
	function set_book($appt_id, $set){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);
			
			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
	}

	function set_unbook($appt_id, $set){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
	}

	function set_unseen($appt_id, $set){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
	}

	function set_visit($appt_id, $set){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
	}


	function set_cancel($appt_id, $set)
	{
		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
	}

	function flag_eta($appt_id){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			$set['flag_eta'] = 1;

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
		
	}

	function set_note($appt_id, $set){

		try {

			if( empty($appt_id) ) throw new Exception("Appt_id must not be empty", 1);
			
			$this->db->where('appt_id', $appt_id);

			if( $this->db->update('appointment', $set) ){
				return $appt_id;
			}
			
			//save history here

		} catch (Exception $e) {

			return 0;

		}
		
	}




 	/**
 	 * This is function will generate a reference_number
 	 * @return int
 	 */
 	function generate_refno(){

 		$this->db->select('ref_number');
 		$this->db->order_by('ref_number', 'DESC');
 		$this->db->where('DATE_FORMAT(tran_created, "%Y%m") = ',date('Ym'));
 		$this->db->limit(1);
 		$result = $this->db->get('transaction')->row();
 		//echo $this->db->last_query();
 		if(isset($result->ref_number) AND $result->ref_number>0){
 			$ref_number = intval($result->ref_number);
 			$ref_number++;
 		}else{
 			$ref_number = date('ym').'0001';
 		}

 		return $ref_number;

 	}

 	/**
 	 * Get appointments and patient
 	 * @param  array  $params where clause
 	 * @return array         (stdClass) results, (int) total_rows
 	 */
	function get_result_pagination( $params=array() )
	{

		// general totals rows
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		$this->db->select('count(*) as total');
		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`','LEFT OUTER');
		$query = $this->db->get('appointment');
		$total_rows = $query->row()->total;
		$query->free_result(); //free results


		//generate query results
		
		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}

		//limits
		if(isset($params['limits'])){
			$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
		}

		//sorting
		if( isset($params['sorting']) ){
			if( is_array($params['sorting']) ){
				$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
			}else{
				$this->db->order_by($params['sorting']);
			}

		}else{
			$this->db->order_by('`transaction`.`tran_id`', 'desc');
		}

		$select = "transaction.*, patient.*, appointment_consult_notes.consult_id,appointment.*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ";
		/*$select .= ", IF( appt_end_queue IS NULL, 
				CONCAT(TIMESTAMPDIFF(DAY, appt_created, NOW()),'-',TIMESTAMPDIFF(HOUR, appt_created, NOW())-TIMESTAMPDIFF(DAY, appt_created, NOW())*24,':',TIMESTAMPDIFF(MINUTE, appt_created, NOW())-TIMESTAMPDIFF(HOUR,appt_created, NOW())*60), 
				CONCAT(TIMESTAMPDIFF(DAY, appt_created, appt_end_queue),'-',TIMESTAMPDIFF(HOUR, appt_created, appt_end_queue)-TIMESTAMPDIFF(DAY, appt_created, appt_end_queue)*24,':',TIMESTAMPDIFF(MINUTE, appt_created, appt_end_queue)-TIMESTAMPDIFF(HOUR,appt_created, appt_end_queue)*60)  
			) AS in_queue,";*/
		//$select .= ", IF( appt_start_queue IS NULL, TIMEDIFF(NOW(),appt_created), TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
		
		//$select .= ", IF( appt_start_queue IS NULL AND appt_created >= NOW() , '---', TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
		$select .= ", (CASE 
					WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
					WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
					WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
					ELSE TIMEDIFF(appt_start_queue, appt_created) 
				END)  AS in_queue ";	
		$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
		$select .= ", `areas`.`area_name`";
		
		//$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', (SELECT timezone FROM areas AS sub_areas WHERE sub_areas.area_id =`areas`.area_id  LIMIT 1)) AS appt_created_tz";
		
		//$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', `areas`.`timezone`) AS appt_created_tz ";
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', '".$this->default_timezone."') AS appt_created_tz ";
		
		$select .= ", `areas_cars_link`.`car_name`";

		if( isset($params['consult_notes']) ){
			$select .= ', (SELECT `consult_id` FROM `appointment_consult_notes` WHERE appt_id = `appointment`.`appt_id` LIMIT 1) AS consult_id';
		}

		$this->db->select($select);

		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
		$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
		$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');
		$this->db->join('`areas_cars_link`', '`areas_cars_link`.`car_id` = `appointment`.`car_id`', 'LEFT OUTER');
		$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`','LEFT OUTER');
		$query = $this->db->get('appointment');


		$result = $query->result();
		$query->free_result(); //free results

		return array('results'=>$result, 'total_rows'=>$total_rows);
	}

/**
 	 * Get appointments and patient
 	 * @param  array  $params where clause
 	 * @return array         (stdClass) results, (int) total_rows
 	 */
	function get_result_pagination_ajax_notes_without_limit( $params=array() , $return )
	{

		if($return=='count')
			{
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			/*$this->db->where("sign_code!='' and book_status=".BOOK_STATUS_CLOSED, null, false);*/
			/*$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);*/


			$query  = $this->db->select('count(*) as total')
							    ->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`','LEFT OUTER')
					 		   ->get('appointment');

			$total_rows = $query->row()->total;		


			$query->free_result(); //free results			




			return array('total_rows'=>$total_rows);
		}
		else
		{
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			/*$this->db->where(" `appointment`.`sign_code`!='' and `appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);*/
/*			$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);*/

			$this->db->select('count(*) as total');
			$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
			$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
			$query = $this->db->get('appointment');
			$total_rows = $query->row()->total;		
			$query->free_result(); //free results


			//generate query results

			//where clause
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
			$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
			if( is_array($params['sorting']) ){
				$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
			}else{
				$this->db->order_by($params['sorting']);
			}

			}else{
			$this->db->order_by('`transaction`.`tran_id`', 'desc');
			}


			$select = "`transaction`.`ref_number_formatted`, patient.*, 
						`appointment_consult_notes`.`consult_id`,
						`appointment_consult_notes`.`consult_created`,
						`appointment_consult_notes`.`consult_updated`,
						`appointment_consult_notes`.`doctor_id`,
						`appointment_consult_notes`.`past_medical_history`,
						`appointment_consult_notes`.`immunisations`,
						`appointment_consult_notes`.`allergies`,
						`appointment_consult_notes`.`social_history`,
						`appointment_consult_notes`.`observation`,
						`appointment_consult_notes`.`diagnosis`,
						`appointment_consult_notes`.`diagnosis_ref_code`,
						`appointment_consult_notes`.`plan`,
						`appointment_consult_notes`.`cunsult_medication`,
						`appointment_consult_notes`.`current_medication`,	
						`appointment_consult_notes`.`medication_prescribed`,
						`appointment_consult_notes`.`medication_perscribed_sub`,
						`appointment_consult_notes`.`overall_notes`,
						`appointment_consult_notes`.`followup_gp`,
						`appointment_consult_notes`.`item_num`,
						`appointment_consult_notes`.`life_style_risk_factors`,
						 appointment.*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ";
			//$select .= ", IF( `appt_end_queue` IS NULL , TIMEDIFF(NOW(),`appt_created`), TIMEDIFF(`appt_end_queue`, `appt_created`)) AS in_queue";
			//$select .= ", IF( appt_start_queue IS NULL AND appt_created >= NOW() , '---', TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
			
			$select .= ", (CASE 
						WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
						WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
						WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
						ELSE TIMEDIFF(appt_start_queue, appt_created) 
					END)  AS in_queue ";
					
			$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
			$select .= ", `areas`.`area_name`";

			$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
			$select .= ", `areas`.`area_name`";
			$select .= ", `transaction`.`agent_name`";
			$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', `areas`.`timezone`) AS appt_created_tz ";
			$select .= ", `doctor`.`prov_num`";



	/*		$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);*/
			$this->db->select($select);
			$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`', 'LEFT OUTER');
			//$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`');
			$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
			$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
			$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
			$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
			$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');
			$query = $this->db->get('appointment');


			$result = $query->result();

			
			$query->free_result(); //free results




			return array('results'=>$result, 'total_rows'=>$total_rows);
		}

	}
	/**
 	 * Get appointments and patient
 	 * @param  array  $params where clause
 	 * @return array         (stdClass) results, (int) total_rows
 	 */
	function get_result_pagination_ajax_without_limit( $params=array() , $return )
	{

		if($return=='count')
			{
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			/*$this->db->where("sign_code!='' and book_status=".BOOK_STATUS_CLOSED, null, false);*/
			$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);


			$query  = $this->db->select('count(*) as total')
					 		   ->get('appointment');

			$total_rows = $query->row()->total;		




			$query->free_result(); //free results			




			return array('total_rows'=>$total_rows);
		}
		else
		{
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			/*$this->db->where(" `appointment`.`sign_code`!='' and `appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);*/
			$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);

			$this->db->select('count(*) as total');
			$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
			$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
			$query = $this->db->get('appointment');
			$total_rows = $query->row()->total;		
			$query->free_result(); //free results


			//generate query results

			//where clause
			if(isset($params['where'])){
			$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
			$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
			if( is_array($params['sorting']) ){
				$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
			}else{
				$this->db->order_by($params['sorting']);
			}

			}else{
			$this->db->order_by('`transaction`.`tran_id`', 'desc');
			}

			$select = "`transaction`.`ref_number_formatted`, patient.*, appointment.*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ,	appointment_consult_notes.*";
			//$select .= ", IF( `appt_end_queue` IS NULL , TIMEDIFF(NOW(),`appt_created`), TIMEDIFF(`appt_end_queue`, `appt_created`)) AS in_queue";
			//$select .= ", IF( appt_start_queue IS NULL AND appt_created >= NOW() , '---', TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
			
			$select .= ", (CASE 
						WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
						WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
						WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
						ELSE TIMEDIFF(appt_start_queue, appt_created) 
					END)  AS in_queue ";
					
			$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
			$select .= ", `areas`.`area_name`";

			$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name";
			$select .= ", `areas`.`area_name`";
			$select .= ", `transaction`.`agent_name`";
			$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', `areas`.`timezone`) AS appt_created_tz ";
			$select .= ", `doctor`.`prov_num`";


			$this->db->where("`appointment`.`book_status`=".BOOK_STATUS_CLOSED, null, false);
			$this->db->select($select);

			$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
			$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
			$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
			$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
			$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');			
			$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`', 'LEFT OUTER');
			$query = $this->db->get('appointment');


			$result = $query->result();


			//echo $this->db->lcfirst(str)ast_query();
			$query->free_result(); //free results




			return array('results'=>$result, 'total_rows'=>$total_rows);
		}

	}

	/**
	 * Appointment Transaction link
	 * @param  int $appt_id
	 * @return stdCass Object single row
	 */
	function get_row_plain($appt_id) {

		$this->db->where('appt_id', $appt_id);
		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');		
		$query = $this->db->get('appointment');

		return $query->row();
	}
 
	/**
	 * Appointment Transaction Patient link
	 * @param  int $appt_id
	 * @return stdCass Object single row
	 */
	function get_row_link($appt_id){

		/*$select = "*, TIMESTAMPDIFF(YEAR, '1984-03-18', CURDATE()) AS patient_age ";
		$select .= ", IF( appt_end_queue IS NULL, 
				CONCAT(TIMESTAMPDIFF(DAY, appt_created, NOW()),'-',TIMESTAMPDIFF(HOUR, appt_created, NOW())-TIMESTAMPDIFF(DAY, appt_created, NOW())*24,':',TIMESTAMPDIFF(MINUTE, appt_created, NOW())-TIMESTAMPDIFF(HOUR,appt_created, NOW())*60), 
				CONCAT(TIMESTAMPDIFF(DAY, appt_created, appt_end_queue),'-',TIMESTAMPDIFF(HOUR, appt_created, appt_end_queue)-TIMESTAMPDIFF(DAY, appt_created, appt_end_queue)*24,':',TIMESTAMPDIFF(MINUTE, appt_created, appt_end_queue)-TIMESTAMPDIFF(HOUR,appt_created, appt_end_queue)*60)  
			) AS in_queue";
		$this->db->select($select);*/

		$this->db->select("*, TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age");

		$this->db->where('appt_id', $appt_id);
		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$query = $this->db->get('appointment');

		return $query->row();
	}

	function get_row_link_full($appt_id){

		$select = "`transaction`.`ref_number_formatted`, patient.*, appointment.* ,
					`appointment_consult_notes`.`consult_id`,
					`appointment_consult_notes`.`consult_created`,
					`appointment_consult_notes`.`consult_updated`,
					`appointment_consult_notes`.`doctor_id`,
					`appointment_consult_notes`.`past_medical_history`,
					`appointment_consult_notes`.`immunisations`,
					`appointment_consult_notes`.`allergies`,
					`appointment_consult_notes`.`social_history`,
					`appointment_consult_notes`.`observation`,
					`appointment_consult_notes`.`diagnosis`,
					`appointment_consult_notes`.`diagnosis_ref_code`,
					`appointment_consult_notes`.`plan`,
					`appointment_consult_notes`.`cunsult_medication`,
					`appointment_consult_notes`.`medication_prescribed`,
					`appointment_consult_notes`.`medication_perscribed_sub`,
					`appointment_consult_notes`.`overall_notes`,
					`appointment_consult_notes`.`current_medication`,
					`appointment_consult_notes`.`followup_gp`,
					`appointment_consult_notes`.`med_cert_given`,
					`appointment_consult_notes`.`ref_form_given`,
					`appointment_consult_notes`.`item_num`,
					`appointment_consult_notes`.`life_style_risk_factors`,
					 TIMESTAMPDIFF(YEAR, `patient`.`dob`, CURDATE()) AS patient_age ";
		//$select .= ", IF( `appt_end_queue` IS NULL , TIMEDIFF(NOW(),`appt_created`), TIMEDIFF(`appt_end_queue`, `appt_created`)) AS in_queue";
		

		//$select .= ", IF( appt_start_queue IS NULL AND appt_created >= NOW() , '---', TIMEDIFF(appt_start_queue, appt_created)) AS in_queue";
		$select .= ", (CASE 
						WHEN appt_start_queue IS NULL AND appt_created >= NOW() THEN ''
						WHEN appt_start_queue IS NULL AND NOW() > appt_created THEN  TIMEDIFF(NOW(), appt_created)
						WHEN appt_start_queue IS NOT NULL AND appt_start_queue < appt_created THEN TIMEDIFF(appt_start_queue, prev_app_created)
						ELSE TIMEDIFF(appt_start_queue, appt_created) 
					END)  AS in_queue ";			

		$select .= ", chaperone.user_fullname as chaperone_name, doctor.user_fullname as doctor_name, doctor.user_id as dr_id";
		$select .= ", `areas`.`area_name`";
		$select .= ", `transaction`.`agent_name`";
		$select .= ", CONVERT_TZ(appt_created, 'Australia/Sydney', `areas`.`timezone`) AS appt_created_tz ";
		$select .= ", `doctor`.`prov_num`";

		$this->db->select($select);

		$this->db->where('appointment.appt_id', $appt_id);
		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('patient', '`patient`.`patient_id` = `appointment`.`patient_id`', 'LEFT OUTER');
		$this->db->join('`areas`', '`areas`.`area_id` = `appointment`.`area_id`', 'LEFT OUTER');
		$this->db->join('`user` as chaperone', '`chaperone`.`user_id` = `appointment`.`chaperone_id`', 'LEFT OUTER');
		$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');
		$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`', 'LEFT OUTER');
		$query = $this->db->get('appointment');

		return $query->row();
	}

	function get_row_consult_notes($appt_id){

		$this->db->where('appt_id', $appt_id);

		$query = $this->db->get('appointment_consult_notes');

		return $query->row();

	}

	function get_cunsult_notes($where_str){


		$select = "`transaction`.`ref_number_formatted`, 
					appointment.appt_start_queue, 
					appointment.patient_id,
					appointment.appt_id,
					appointment.symptoms,
					doctor.user_fullname AS doctor_name,
					`appointment_consult_notes`.`consult_id`,
					`appointment_consult_notes`.`consult_created`,
					`appointment_consult_notes`.`consult_updated`,
					`appointment_consult_notes`.`doctor_id`,
					`appointment_consult_notes`.`past_medical_history`,
					`appointment_consult_notes`.`immunisations`,
					`appointment_consult_notes`.`allergies`,
					`appointment_consult_notes`.`social_history`,
					`appointment_consult_notes`.`observation`,
					`appointment_consult_notes`.`diagnosis`,
					`appointment_consult_notes`.`diagnosis_ref_code`,
					`appointment_consult_notes`.`plan`,
					`appointment_consult_notes`.`cunsult_medication`,
					`appointment_consult_notes`.`medication_prescribed`,
					`appointment_consult_notes`.`medication_perscribed_sub`,
					`appointment_consult_notes`.`overall_notes`,
					`appointment_consult_notes`.`current_medication`,
					`appointment_consult_notes`.`followup_gp`,
					`appointment_consult_notes`.`item_num`,
					`appointment_consult_notes`.`life_style_risk_factors`,
					`appointment_consult_notes`.`call_center_triage`,
					`appointment_consult_notes`.`prognosis_id`";

		$this->db->select($select);

		$this->db->where($where_str, null, false);

		$this->db->join('transaction', '`transaction`.`tran_id` = `appointment`.`tran_id`', 'LEFT OUTER');
		$this->db->join('appointment_consult_notes', '`appointment_consult_notes`.`appt_id` = `appointment`.`appt_id`', 'LEFT OUTER');
		$this->db->join('`user` as doctor', '`doctor`.`user_id` = `appointment`.`doctor_id`', 'LEFT OUTER');

		$query = $this->db->get('appointment');

		return $query->result();
	}

	function add_consult($set){
		
		if($this->db->insert('appointment_consult_notes', $set)){

			$consult_id = $this->db->insert_id();

			$this->insert_audit_trail(
				array(
					'target_table'=>'appointment_consult_notes',
					'table_ref_col'=>'consult_id',
					'table_action'=>'Add',
					'table_ref_id'=> $consult_id,
					'user_id'=>$this->user_id,
					'agent_name'=>$this->agent_name,
					'message'=> json_encode($set)
				)
			);			

			return $consult_id;
		}else{
			return 0;
		}
	 
	}

	function add_consult_upload($consult_info,$upload_info){

		$error = false;
		

		if(empty($upload_info['consult_id']) || $upload_info['consult_id']==''){

				if($this->db->insert('appointment_consult_notes', $consult_info)){

				$consult_id = $this->db->insert_id();
				

				if($consult_id!=0){

					$upload_info['consult_id'] = $consult_id;

					 if($this->db->insert('appointment_consult_file', $upload_info)){

						$changes['file_path'] = $upload_info['file_path'];
						$changes['file_name'] = $upload_info['file_name'];
						$changes['consult_file_id'] = $this->db->insert_id();

						$this->insert_audit_trail(
							array(
								'target_table'=>'appointment_consult_notes',
								'table_ref_col'=>'consult_id',
								'table_action'=>'Create An Empty Consult Note And Upload',
								'table_ref_id'=> $consult_id,
								'user_id'=>$this->user_id,
								'agent_name'=>$this->agent_name,
								'message'=> json_encode($changes)
							)
						);		

					}else{
						$error = true;
					}

				}


			}else{
				$error = true;
			}

		}else{

			$this->add_consult_file($upload_info);

		}

		return $error;
	 
	}

	function update_consult($consult_id, $set){
		
		$this->db->where('consult_id', $consult_id);
		$query = $this->db->get('appointment_consult_notes');
		$row = $query->row();

		$changes = array();
		
		if( isset($set['symptoms']) ){			 
			$changes['symptoms'] = $set['symptoms'];
			unset($set['symptoms']);
		}

		$set2 = $set;
		unset($set2['consult_updated']);

		foreach ($set2 as $key => $value) {
			if (strcmp($value, $row->$key) !== 0) {
				$changes[$key] = $value;
			}
		}
		$changes['consult_updated'] = $set['consult_updated'];


		$this->db->where('consult_id', $consult_id);

		if($this->db->update('appointment_consult_notes', $set)){

			$this->insert_audit_trail(
				array(
					'target_table'=>'appointment_consult_notes',
					'table_ref_col'=>'consult_id',
					'table_action'=>'update',
					'table_ref_id'=> $consult_id,
					'user_id'=>$this->user_id,
					'agent_name'=>$this->agent_name,
					'message'=> json_encode($changes)
				)
			);			

			return $consult_id;
		}else{
			return 0;
		}
	 
	}


	function add_consult_file($set){
		
		if($this->db->insert('appointment_consult_file', $set)){

			$changes['file_path'] = $set['file_path'];
			$changes['file_name'] = $set['file_name'];
			$changes['consult_file_id'] = $this->db->insert_id();

			$this->insert_audit_trail(
				array(
					'target_table'=>'appointment_consult_notes',
					'table_ref_col'=>'consult_id',
					'table_action'=>'upload',
					'table_ref_id'=> $set['consult_id'],
					'user_id'=>$this->user_id,
					'agent_name'=>$this->agent_name,
					'message'=> json_encode($changes)
				)
			);		

			return $set['consult_id'];
		}else{
			return 0;
		}
	 
	}

	function insert_audit_trail($set)
	{	
	     		
		return ($this->db->insert('table_audit_trail', $set))?$this->db->insert_id():0;
	 	
	}


	function get_user_count_booking($chaperone_id, $appt_created_tz){

		$date = explode(' ', $appt_created_tz);

		$this->db->select('count(*) as totalbook');
		$this->db->where('chaperone_id', $chaperone_id);
		$this->db->where('book_status', BOOK_STATUS_BOOKED);
		$this->db->where(" DATE_FORMAT(CONVERT_TZ(appt_created, 'Australia/Sydney', 'Australia/Queensland'), '%Y-%m-%d') = ", $date[0]);
		$query = $this->db->get('appointment')->row();

		return $query->totalbook;
	}
}