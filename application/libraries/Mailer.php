<?php
include  'class.phpmailer.php';
class Mailer extends PHPMailer {
	
	public $From = "bookingpro@welldone.com.au";
	public $FromName = "BookingPro-CURE";

	public $Mailer = 'smtp';

	public $Host = 'mail.welldone.com.au'; 

	public $send_to = '';
	public $Subject = '';

	function __construct() {
       
	}
	
	public function sendMail() {
		
		$this->ClearAddresses();

		$this->send_to = $this->send_to; 	

		if(!is_array($this->send_to)) { 
			$this->send_to = str_replace(" ","",$this->send_to);
			$this->send_to = str_replace(";",",",$this->send_to);
			$this->send_to = explode(",",$this->send_to); 
		}
		
		foreach($this->send_to as $to){			 

			if(!is_array($to)){
				$to = array($to, '');
			}

			$this->AddAddress($to[0], @$to[1]);
		}
 		 
		$this->IsHTML(true);
 

	/*	if(!$this->Send()) return "Mailer Error: " . $this->ErrorInfo;
		else */
			return 'message_sent';
	}

	public function sendSMS() {
		
		$this->ClearAddresses();
		
		$this->From 	= 'bookingpro@welldone.com.au';		
		
		$this->ContentType = 'text/plain'; 
		$this->Encoding = '7bit';
		$this->AddReplyTo($this->From);		

		//$this->AddAddress($this->send_to);

		$this->send_to = $this->send_to;

		if(!is_array($this->send_to)) { 
			$this->send_to = str_replace(" ","",$this->send_to);
			$this->send_to = str_replace(";",",",$this->send_to);
			$this->send_to = explode(",",$this->send_to); 
		}

		foreach($this->send_to as $to){	

			$this->AddAddress($to.'@messagenet.com.au');
		}

	/*	if(!$this->Send()) return "Mailer Error: " . $this->ErrorInfo;
		else */
			return 'message_sent';
		//return 'message_sent';
	}	

}

?>