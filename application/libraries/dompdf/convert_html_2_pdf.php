<?php
require_once("dompdf_config.inc.php");
$html =
'<html><body>'.
'<h1>DOM PDF from HTML</h1>'.
'<p>Put your html here, or generate it with your favourite '.
'templating system.</p>'.
'</body></html>'; // Coded HTML --- OR ---

$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream("yourhtml.pdf");

// Read more: http://www.ehow.com/how_7261223_convert-html-pdf-using-php.html#ixzz30QvvfJxz

?>

