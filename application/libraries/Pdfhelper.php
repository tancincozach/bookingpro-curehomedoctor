<?php
require_once("dompdf/dompdf_config.inc.php");

class Pdfhelper {
    // Class for pushing html data to the DOMPDF classes for rendering as a PDF.

    function __construct(){
        $this->CI =& get_instance(); // Global CodeIgniter object for use in methods.
        
    }

    function renderPDF($html='',$filename='pdffile.pdf', $stream=TRUE){
        // $html = html to render as PDF
        // $filnam = the file name to create the PDF as.
        /*$dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($filname);*/

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        if ($stream) {
            //$dompdf->stream($filename, array("Attachment" => 0));
            //$dompdf->stream($filename, array("Attachment" => 0));
            $dompdf->stream($filename);
        } else {
            return $dompdf->output();
            //$dompdf->stream($filename);
        }        
    }


// Read more: http://www.ehow.com/how_7261223_convert-html-pdf-using-php.html#ixzz30QvvfJxz
}
?>
