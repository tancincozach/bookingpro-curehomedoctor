
$( document ).ready(function(){
    /*
    $('.datepicker').datetimepicker({
        locale: 'en-au',
        format: "DD/MM/YYYY"
    }); //YYYY-MM-DD*/
      
    /*$("#dob").inputmask("d/m/y", 
    {
        "placeholder": "dd/mm/yyyy",
        "onincomplete": function(){
            //console.log(this.value);
            
            if(!hcd.common.isDOB(this.value)){
                //alert('Date of Birth is invalid');

                $(this).popover({content:'<strong style="color:red">Date of birth is invalid</strong>', placement:'left', html:true}).popover('show');

                $(this).focus();
            }

            $('#patient_age').val('');  
        },
        "oncomplete": function(){

            if(!hcd.common.isDOB(this.value)){
                $(this).popover({content:'<strong style="color:red">Date of birth is invalid</strong>', placement:'left', html:true}).popover('show');
                $(this).focus();
            }else{
                $(this).popover('hide');
            }

            //var year = (this.value).split('/');
            //var years = moment().diff(year[2], 'years');
            //$('#patient_age').val(years);
            //console.log(moment(this.value, "DD/MM/YYYY").fromNow());
            
            //console.log(hcd.common.getAGE(this.value));
            var age = hcd.common.getAGE(this.value);
            $('#patient_age').val(age);
        }
    });*/



    /*$('#dob.datepicker').daterangepicker(
    	{
	        singleDatePicker: true,
	        showDropdowns: true,
	        locale: {
	            format: 'DD/MM/YYYY'
	        },
	        format: 'DD/MM/YYYY'
    	},
    	function(start, end, label) {
	        var years = moment().diff(start, 'years');
	        //alert("You are " + years + " years old.");
	        $('#patient_age').val(years);
   		}
    );*/

    if($('#dob').length){
            
            $('#dob').combodate({
               maxYear: new Date().getFullYear(),
               custom_dob_age_target: '#patient_age',
               on_error_popover: true
            }); 
        }

    //$('input[name=Mobile_Phone],input[name=Landline_Phone]').inputmask("9999999999");

    $('input[name=Medical_Number]')
      //                   .inputmask("9999999999")
                         .on('change',function()
                          {  

                            var medicard_value = $(this).val().replace(/_/g, '').length;


                            if(medicard_value < 10)
                            {
                                alert('Medicare number must be in 10 digits.');
                                $(this).focus();
                                return false;
                            }

                         });


      $('select[name=health_care_card_question]').on('change',function(){

        if($(this).val()=='Yes'){

                $('.expiry_date_container').show();                    
                $('input[name=expiry_date]').attr('required', 'required');


        }else{
                $('input[name=expiry_date]').val('');
                $('input[name=expiry_date]').removeAttr('required');
                $('.expiry_date_container').hide();                    

        }

              
      });                  

      var expiry_date = $('input[name=expiry_date]').val();

     //$('input[name=expiry_date]').datetimepicker({ format: 'DD/MM/YYYY'});    
        
    $('input[name="expiry_date"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
    });


    Booking.google_map_codeAddress();

});

var geocoder;
var map;


var Booking = {

    google_map_initialize: function() {

        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
            zoom: 17,
            center: latlng
        }
        if(document.getElementById('map-canvas')){
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        }
        
    },

    google_map_codeAddress:function() {

        var address = document.getElementById('Address');
        var suburb = document.getElementById('Suburb');
        var postcode = document.getElementById('Post_Code');
        var ret_addr = document.getElementById('returned_addr');
        var addr_chkbox = document.getElementById('addr_checkbox');
        var map_canv = document.getElementById('map-canvas');
        
        ret_addr.style.display = 'none';
        addr_chkbox.style.display = 'none';
        //map_canv.style.display = 'none';
        console.log(address.value);
        console.log(suburb.value);
        console.log(postcode.value);



        if(address.value.length > 0 && suburb.value.length > 0 && postcode.value.length > 0){

            //console.log('Map Initialized');
            Booking.google_map_initialize();
            
            /*
            Northern Territory              NT 0800—0899
            New South Wales                 NSW 2000—2599, 2619—2898, 2921—2999
            Australian Capital Territory    ACT 2600—2618, 2900—2920
            Victoria                        VIC 3000—3999
            Queensland                      QLD 4000—4999
            South Australia                 SA 5000—5799
            Western Australia               WA 6000—6797
            Tasmania                        TAS 7000—7799
            */
            var state = 'NT';
            if(postcode.value >= 2000){state = 'NSW';}
            if(postcode.value >= 2600){state = 'ACT';}
            if(postcode.value >= 2619){state = 'NSW';}
            if(postcode.value >= 2900){state = 'ACT';}
            if(postcode.value >= 2921){state = 'NSW';}
            if(postcode.value >= 3000){state = 'VIC';}
            if(postcode.value >= 4000){state = 'QLD';}
            if(postcode.value >= 5000){state = 'SA';}
            if(postcode.value >= 6000){state = 'WA';}
            if(postcode.value >= 7000){state = 'TAS';}

            var addr = address.value + ',' + suburb.value + ' ' + state + ' '+postcode.value;

            //console.log(addr);

            geocoder.geocode( { 'address': addr }, function(results, status) {


                ret_addr.style.display = 'block';
                addr_chkbox.style.display = 'block';

                
                //console.log(results[0].geometry.location);


                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    
                    ret_addr.innerHTML = results[0].formatted_address; // + ' (Latitude / Longitude:' + results[0].geometry.location + ')';
                    
                    document.getElementById('sm_text').value = results[0].formatted_address;
                    document.getElementById('sm_latlong').value = results[0].geometry.location;

                    // Looop through Geocoded data to build comparison string
                    for(zz=0;zz<results[0].address_components.length;zz++){
                        if(results[0].address_components[zz].types[0] == 'street_number'){
                            document.getElementById('sm_streetPrefix').value = results[0].address_components[zz].long_name;
                        }
                        if(results[0].address_components[zz].types[0] == 'route'){
                            document.getElementById('sm_street').value = results[0].address_components[zz].long_name;
                        }
                        if(results[0].address_components[zz].types[0] == 'locality'){
                            document.getElementById('sm_area').value = results[0].address_components[zz].long_name;
                        }
                        if(results[0].address_components[zz].types[0] == 'administrative_area_level_1'){
                            document.getElementById('sm_state').value = results[0].address_components[zz].long_name;
                        }
                    }


                    ret_addr.style.display = 'block';
                    addr_chkbox.style.display = 'block';
                    //map_canv.style.display = 'block';
                }


                /*
                 else {
                    //alert('Geocode was not successful for the following reason: ' + status);
                    //alert('Address not found!');
                     //   ret_addr.innerHTML = '<h2 style="color: red;">ADDRESS NOT FOUND!</h2>';

                     ret_addr.style.display = 'block';
                      addr_chkbox.style.display = 'block';
                }*/
            });

        }
    }

}