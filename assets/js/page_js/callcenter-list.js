
var generate_batch_consult_notes = {

        
        ajax_request:function(ajax_settings)
        {
             try{

                    if(ajax_settings=='') throw "Error : Ajax settings is empty";
                      
                      return  $.ajax(ajax_settings);
             } 
             catch(err)
              {
                 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
              } 
        }  
        ,    
        check_completed_bookings:function ( posted_data )
        {

             try{
                      

                        if(posted_data.selected_date == '') throw "Error : Date field is empty";

                          var ajax_settings = {   

                                    url: 'ajax/get_completed_batch_notes',
                                    type:'POST',
                                    dataType: 'json',
                                    beforeSend: function(jqXHR, settings) {
                                             
                                            if( settings.type == 'POST' ){ 
                                                settings.data = settings.data+'&'+hcd_globals.n+'='+hcd_globals.h;
                                            }
                                                    var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                    +'<img src="assets/images/ajax-loader.gif"/>&nbsp;&nbsp;Gathering data, Please wait....'
                                                                    +'</div>';
                                                                    
                                              $('.dl-med-container').remove();

                                            $('.date-picker-container').append($notification_html);                                    
                                        }                            
                                    ,
                                    error: function(request, status, error){                               
                                           hcd.common.alert({'title':'ERROR', 'message':error});
                                    },
                                    data:{download_date:posted_data.selected_date,area:posted_data.area}

                        }


                        result = generate_batch_consult_notes.ajax_request(ajax_settings);    



                        result.done(function( resultObject ){

                                if(resultObject.total_rows!=0)
                                {

                                var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                +'<span class>Found  <strong>'+resultObject.total_rows +'</strong> </span> Note(s) on '+posted_data.selected_date
                                                                +(resultObject.total_rows > 0 ? ' <button class="btn btn-info btn-sm dl-med">Download Consult Note(s)</button><div class="progress-icon"></div>':'')
                                                                +'</div>';

                                $('.generate').hide();

                                $('.dl-med-container').remove();



                                  $('.date-picker-container').append($notification_html);


                                        $('.dl-med').on('click',function(){                             


                                                if(!$.isEmptyObject(resultObject))
                                                {
                                                   
                                                    generate_batch_consult_notes.generate({selected_date:posted_data.selected_date,area:posted_data.area});
                                                }
                                        
                                        });
                                }
                                else{
                                            var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                +'<span class="label label-danger" style="font-size:14px;">No record found </span> '                                                
                                                                +'</div>';

                                $('.dl-med-container').remove();

                                  $('.date-picker-container').append($notification_html);
                                }
                      });
                  
             } 
             catch(err)
              {
                 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
                 $('.generate').show();

              } 
        }
        ,    
        view:function()
        {



                var default_date = $('#download_medicard_default_date').val();


 
                      $areas = $("<select/>", {
                                class: 'form-control input-sm batch_area '
                            });




                    $area_links = $('.areas').find("a.noclose");

                     $areas.append($("<option/>", {
                                value: '',
                                text: 'SELECT AREA'
                            }));

                    $.each($area_links ,function(){

                        var  area_name = $(this).attr('alt'),area   = $(this).find('input[type=checkbox]').val();
                        
                            $areas.append($("<option/>", {
                                value: area,
                                text: area_name
                            }));
                    });

                    $area_container  = $("<div/>",{class:'area_container'}).append($areas);

                    var cbox_object =   
                    common_plugin.modal( {
                            message:'<div class="form-inline"><div class="row date-picker-container">' 
                                        +'<div class="col-md-12">'                                
                                        +'<label>Date (QLD) : </label>'
                                            +'<input type="text" class="form-control input-sm"  name="consult_date_range" value="'+default_date+'">&nbsp;&nbsp;<label>Area:</label>'+$area_container.html()+'&nbsp;&nbsp;<button class="btn btn-primary generate btn-sm">Generate</button>'                                                 
                                    +'</div></div>'

                                +'</div>'
                                ,
                                title:'BookingPro Generate Batch Consult Notes'
                                } , '<button class="btn btn-default set-anyday" alt="no"  data-dismiss="modal">Close</button>' );


                    $('input[name="consult_date_range"]').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                        format: 'DD/MM/YYYY'
                        },
                        format: 'DD/MM/YYYY'
                    }, 
                    function(start, end, label) {
                    $('.generate').show();
                });

                    $('.generate').on('click',function()
                    {   

                      generate_batch_consult_notes.check_completed_bookings({selected_date:$('input[name="consult_date_range"]').val(),area:$('.batch_area').val()});    

                    });

                    $('.batch_area').on('change',function()
                    {
                        $('.generate').show();

                    });

             
        }
        ,
        generate:function( posted_data)
        {

                  var ajax_settings = {   

                            url: 'ajax/generate_notes',
                            type:'POST',
                            dataType: 'json',
                            beforeSend: function(jqXHR, settings) {
                                     
                                    if( settings.type == 'POST' ){ 
                                        settings.data = settings.data+'&'+hcd_globals.n+'='+hcd_globals.h;
                                    }
                                     $('.progress-icon').html('');
                                     $('.progress-icon').html('<img src="assets/images/ajax-loader.gif"/> Generating pdf....');                                    
                                }                            
                            ,
                            error: function(request, status, error){                               
                                   hcd.common.alert({'title':'ERROR', 'message':error});
                            }                             
                            ,
                            data:{download_date:posted_data.selected_date,area:posted_data.area}
                }

                ///console.log(ajax_settings);

                var ajax  = this.ajax_request(ajax_settings);

                ajax.done(function( resultObject ){

                $('.progress-icon').html('');   

                 if(!$.isEmptyObject(resultObject.result.url))
                    {

                      window.location='callcenter/download_consult_batch_zip?filename='+resultObject.result.name+'&url='+resultObject.result.url+'&unix='+resultObject.result.unix;


                    }
                })
        }
        
}



var generate_batch_medicare = {

        
        ajax_request:function(ajax_settings)
        {
             try{

                    if(ajax_settings=='') throw "Error : Ajax settings is empty";
                      
                      return  $.ajax(ajax_settings);
             } 
             catch(err)
              {
                 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
              } 
        }  
        ,    
        check_completed_bookings:function ( posted_data )
        {

             try{
                      

                        if(posted_data.selected_date == '') throw "Error : Date field is empty";

                          var ajax_settings = {   

                                    url: 'ajax/get_completed_batch',
                                    type:'POST',
                                    dataType: 'json',
                                    beforeSend: function(jqXHR, settings) {
                                             
                                            if( settings.type == 'POST' ){ 
                                                settings.data = settings.data+'&'+hcd_globals.n+'='+hcd_globals.h;
                                            }
                                                    var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                    +'<img src="assets/images/ajax-loader.gif"/>&nbsp;&nbsp;Gathering data, Please wait....'
                                                                    +'</div>';
                                                                    
                                              $('.dl-med-container').remove();

                                            $('.date-picker-container').append($notification_html);                                    
                                        }                            
                                    ,
                                    error: function(request, status, error){                               
                                           hcd.common.alert({'title':'ERROR', 'message':error});
                                    },
                                    data:{download_date:posted_data.selected_date,area:posted_data.area}

                        }


                        result = generate_batch_medicare.ajax_request(ajax_settings);    



                        result.done(function( resultObject ){

                                if(resultObject.total_rows!=0)
                                {

                                var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                +'<span class>Found  <strong>'+resultObject.total_rows +'</strong> </span> Completed Bookings on '+posted_data.selected_date
                                                                +(resultObject.total_rows > 0 ? ' <button class="btn btn-info btn-sm dl-med">Download Medicare</button><div class="progress-icon"></div>':'')
                                                                +'</div>';

                                $('.generate').hide();

                                $('.dl-med-container').remove();



                                  $('.date-picker-container').append($notification_html);


                                        $('.dl-med').on('click',function(){                             


                                                if(!$.isEmptyObject(resultObject))
                                                {
                                                   
                                                    generate_batch_medicare.generate({selected_date:posted_data.selected_date,area:posted_data.area});
                                                }
                                        
                                        });
                                }
                                else{
                                            var  $notification_html = '<div class="col-md-12 dl-med-container" style="padding:10px">'
                                                                +'<span class="label label-danger" style="font-size:14px;">No record found </span> '                                                
                                                                +'</div>';

                                $('.dl-med-container').remove();

                                  $('.date-picker-container').append($notification_html);
                                }
                      });
                  
             } 
             catch(err)
              {
                 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
                 $('.generate').show();

              } 
        }
        ,    
        view:function()
        {



                var default_date = $('#download_medicard_default_date').val();


 
                      $areas = $("<select/>", {
                                class: 'form-control input-sm batch_area '
                            });




                    $area_links = $('.areas').find("a.noclose");

                     $areas.append($("<option/>", {
                                value: '',
                                text: 'SELECT AREA'
                            }));

                    $.each($area_links ,function(){

                        var  area_name = $(this).attr('alt'),area   = $(this).find('input[type=checkbox]').val();
                        
                            $areas.append($("<option/>", {
                                value: area,
                                text: area_name
                            }));
                    });

                    $area_container  = $("<div/>",{class:'area_container'}).append($areas);

                    var cbox_object =   
                    common_plugin.modal( {
                            message:'<div class="form-inline"><div class="row date-picker-container">' 
                                        +'<div class="col-md-12">'                                
                                        +'<label>Date (QLD) : </label>'
                                            +'<input type="text" class="form-control input-sm"  name="medicard_date_range" value="'+default_date+'">&nbsp;&nbsp;<label>Area:</label>'+$area_container.html()+'&nbsp;&nbsp;<button class="btn btn-primary generate btn-sm">Generate</button>'                                                 
                                    +'</div></div>'

                                +'</div>'
                                ,title:'BookingPro Generate Batch Medicare'} , '<button class="btn btn-default set-anyday" alt="no"  data-dismiss="modal">Close</button>' );


                    $('input[name="medicard_date_range"]').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                        format: 'DD/MM/YYYY'
                        },
                        format: 'DD/MM/YYYY'
                    }, 
                    function(start, end, label) {
                    $('.generate').show();
                });

                    $('.generate').on('click',function()
                    {   

                      generate_batch_medicare.check_completed_bookings({selected_date:$('input[name="medicard_date_range"]').val(),area:$('.batch_area').val()});    

                    });

                    $('.batch_area').on('change',function()
                    {
                        $('.generate').show();

                    });

             
        }
        ,
        generate:function( posted_data)
        {

                  var ajax_settings = {   

                            url: 'ajax/generate_medical_form',
                            type:'POST',
                            dataType: 'json',
                            beforeSend: function(jqXHR, settings) {
                                     
                                    if( settings.type == 'POST' ){ 
                                        settings.data = settings.data+'&'+hcd_globals.n+'='+hcd_globals.h;
                                    }
                                     $('.progress-icon').html('');
                                     $('.progress-icon').html('<img src="assets/images/ajax-loader.gif"/> Generating pdf....');                                    
                                }                            
                            ,
                            error: function(request, status, error){                               
                                   hcd.common.alert({'title':'ERROR', 'message':error});
                            }                             
                            ,
                            data:{download_date:posted_data.selected_date,area:posted_data.area}
                }

                ///console.log(ajax_settings);

                var ajax  = this.ajax_request(ajax_settings);

                ajax.done(function( resultObject ){

                $('.progress-icon').html('');   

                 if(!$.isEmptyObject(resultObject.result.url))
                    {

                        window.location='callcenter/download_batch_zip?filename='+resultObject.result.name+'&url='+resultObject.result.url+'&unix='+resultObject.result.unix;


                    }
                })
        }
        
}


$( document ).ready(function(){
    /*
    $('.datepicker').datetimepicker({
        locale: 'en-au',
        format: "DD/MM/YYYY"
    }); //YYYY-MM-DD*/
      
    $('#filter_appt_start').daterangepicker(
        {
            //singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
            format: 'DD/MM/YYYY'
        }
    ); 



    $('.download-medicard').click(function(event){

       generate_batch_medicare.view();

        event.preventDefault();

    });


    $('.download-consult-notes').click(function(event){

       generate_batch_consult_notes.view();

        event.preventDefault();

    });


    $('.clear-date').click(function(event){

       $('#filter_appt_start').val('');

        event.preventDefault();

    });

    generate_batch_consult_notes

 
});
 