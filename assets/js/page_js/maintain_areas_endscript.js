
 

$( document ).ready(function(){



	var style_formats = [
			 
			{title : 'Header 2', block : 'h2' },
			{title : 'Header 3', block : 'h3' },
			{title : 'Header 4', block : 'h4' }			 
		];
	var plugins = [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	         "save table contextmenu directionality emoticons paste textcolor",
	         "template"
	    ];

	//Initialize tinyMCE for Greeting
	tinymce.init({
	    selector: "#summernote",
	    document_base_url: hcd_globals.base_url,
	    theme: "modern",
	   /* width: 300,*/
	    height: 250,
	    cleanup : true,
	    plugins: plugins,
	    convert_urls: false,
	    content_css: "assets/css/bootstrap.min.css",
	    /**/
	    toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons", 
		style_formats : style_formats,

	   /* external_filemanager_path : 'filemanager/show', */

    	/*templates: '<?php echo base_url(); ?>ajax/tinymcetemplate',*/

	    template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}

	});	

	

});