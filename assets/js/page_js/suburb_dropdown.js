
$( document ).ready(function(){
    
    $( ".suburb" ).mcautocomplete({
        // These next two options are what this plugin adds to the autocomplete widget.
        showHeader: true,
        columns: [
            {
                name: 'Suburb',
                width: '200px',
                valueField: 'suburb'
            },
            {
                name: ' Postcode',
                width: '70px',
                valueField: 'postcode'
            },
            {
                name: 'Area',
                width: '200px',
                valueField: 'area'
            },
            {
                name: 'Status',
                width: '50px',
                valueField: 'open_status'
            }
        ],
        autoFocus: true,
        // Event handler for when a list item is selected.
        open: function(event, ui) {
            //var me = $(this);

            //var adata = $(me).attr('data');

            /*if(adata == 'verify-suburb'){
                $('#incident_suburb_id').val('');
            }*/
            
            $('#dashboard_form').find('.btn-service-cls').hide();

            $('#suburb-not-display-note').show();
            $('.overnight-staff-options').hide();

            console.log('open');
            return false;
        },
        select: function(event, ui) {

            var me = $(this);

            console.log('ui.item.id: '+ui.item.id);
            console.log('ui.item.area_id: '+ui.item.area_id);

            if( typeof ui.item.id !== "undefined" ){
                //$(this).val(ui.item.suburb+' '+ui.item.postcode+((ui.item.council!='')?' '+ui.item.area:''));
                
                me.val(ui.item.suburb);

                $("input[name='collection[Suburb][id]']").val(ui.item.id);
                $("input[name='collection[Suburb][area]']").val(ui.item.area);
                $("input[name='collection[Suburb][postcode]']").val(ui.item.postcode);
                $("input[name='collection[Suburb][area_id]']").val(ui.item.area_id);
                
                $("input[name='collection[area_open_status]']").val(ui.item.open_status);
                $("input[name='collection[area_avail_id]']").val(ui.item.area_avail_id);
                

                $.get('ajax/check_schedule/'+ui.item.region_type, {area_id: ui.item.area_id}, function(response){

                    if(response.status == '1'){
                        //alert(response.msg)

                        var form_data = $('#dashboard_form').serialize();

                        window.location = 'dashboard/out_of_booking_time?'+response.uri+'&'+form_data;

                    }else{

                        $('#suburb-not-display-note').hide();

                         $('.overnight-staff-options').show();
                         
                        if( ui.item.open_status == 'Open' ){
                            $("#btn-service-1").show();
                        }
                        
                        if( ui.item.open_status == 'Closed' ){
                            $("#btn-service-2").show();
                        }

                    }

                }, 'json');

 

            }else{
                $("input[name='collection[Suburb][id]']").val('');
                $("input[name='collection[Suburb][area]']").val('');
                $("input[name='collection[Suburb][postcode]']").val('');
                $("input[name='collection[Suburb][area_id]']").val('');
                
                $("input[name='collection[area_open_status]'").val('');
                $("input[name='collection[area_avail_id]'").val('');

                $("#btn-service-3").show();
                //$('#btn-service-3').find('button').show();
                $('#suburb-not-display-note').hide();
            }



            return false;
        },
        focus: function(event, ui) {
            //var me = $(this);
            
            
            //console.log(me);

            return false;
        },        
        // The rest of the options are for configuring the ajax webservice call.
        minLength: 2,
        source: function(request, response) {
            
           // $('#incident_suburb_id').val('');

            var searchtxt = (request.term).trim();

            if( searchtxt.length >= 2 ){

                $.ajax({
                    url: 'ajax/suburb_availability/',
                    dataType: 'json',
                    data: {
                        featureClass: 'P',
                        style: 'full',
                        maxRows: 12,
                        name_startsWith: searchtxt
                    },
                    // The success event handler will display "No match found" if no items are returned.
                    success: function(data) {
                         
                        var result;
                        
                        if (!data || data.length === 0 ) {
                            result = [
                                {suburb:'No match found. Press tab or hit', postcode:'enter', area: 'to show Service No button', open_status: ''}
                                //{suburb:'', postcode: 'No match found.'}
                            ];
                        }
                        else {
                            result = data;
                        }
                        response(result);
                    },

                    error: function(request, status, error){
                        //alert(request.responseText);
                        alert('Session expired.');
                        //window.location = document.URL;
                    }

                });

            }
        }
    });


    $( ".suburb" ).keydown(function(event){
        if(event.keyCode == 13) {
           // if($(".suburb").val().length==0) {
              event.preventDefault();
              return false;
           // }
        }
     });
    
});