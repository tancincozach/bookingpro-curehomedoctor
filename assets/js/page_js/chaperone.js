var booked_out_area = {

        booked_out:function( area ){
            $.ajax({
                    url:'ajax/booked_out_area',
                    type:'post',
                    data:{agent_id:hcd_globals.user_id,agent_name:hcd_globals.agent_name,area_id:area},
                    success:function( result ){

                    
                        if(!$.isEmptyObject(result.message)){
                            alert(result.message);
                            window.location=document.URL;
                        }

                    },
                    dataType:'json'

            });

        },
        reopen:function(area){

            $.ajax({
                    url:'ajax/booked_reopen_area',
                    type:'post',
                    data:{agent_id:hcd_globals.user_id,agent_name:hcd_globals.agent_name,area_id:area},
                    success:function(result){

                        if(!$.isEmptyObject(result.message)){
                            alert(result.message);
                            window.location=document.URL;
                        }

                    },
                    dataType:'json'
            });

        },
        init:function(href_object){

            var  $href_link = $(href_object),
                 $dropdown_list =   $href_link.parent().parent(),
                 $btn = $dropdown_list.parent().find('button'),
                 area = $href_link.attr('alt');

              if($href_link.hasClass( "book_out" )){

                    if( confirm('Are you sure  you want to book area out ?') ){

                        $btn.removeClass( "btn-success" );
                        $btn.addClass( "btn-danger" );    
                        $dropdown_list.find('.bg-danger').hide();                                                                                           
                        $dropdown_list.find('.bg-success').show();  
                        this.booked_out(area);    
                     }   

              }else{

                  if( confirm('Are you sure  you want to re-open area out ?') ){
                        

                        $btn.removeClass( "btn-danger" );
                        $btn.addClass( "btn-success" );                                                                                                            
                        $dropdown_list.find('.bg-success').hide();  
                        $dropdown_list.find('.bg-danger').show();  
                        this.reopen(area);
                     }   

              }

        }
}


$(function(){



    $('.book_out,.reopen_booking').on('click',function(event){

  
        booked_out_area.init($(this));

        event.preventDefault();
    

    });

    

});