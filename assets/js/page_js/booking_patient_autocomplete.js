
$( document ).ready(function(){
    
     
     $( "#search_patient_match" ).autocomplete({
        minLength: 2,
        //source: 'ajax/search_patient_match',
        source: function(request, response) {

            var searchtxt = (request.term).trim();

            if( searchtxt.length >= 2 ){

                $.ajax({
                    url: 'ajax/search_patient_match/',
                    dataType: 'json',
                    data: {
                        featureClass: 'P',
                        style: 'full',
                        maxRows: 12,
                        term: searchtxt
                    },
                    // The success event handler will display "No match found" if no items are returned.
                    success: function(data) {
                         
                        var result;
                        
                        if (!data || data.length === 0 ) {
                            result = [
                                {value:'', label:'No match found.', desc:'Press down arrow key and enter or click me to create new record'}
                            ];
                        }
                        else {
                            result = data;
                        }
                        response(result);
                    },

                    error: function(request, status, error){
                        //alert(request.responseText);
                        alert('Session expired.');
                        //window.location = document.URL;
                    }

                });

            }
        },
        open: function(event, ui) {
            $('#booking_patient_search_form').find('button').hide();
        },
        focus: function( event, ui ) {
            var me = $(this);
            
            console.log(ui.item.id);

            if( typeof ui.item.id !== "undefined"){
                me.val(ui.item.label);
            }

            return false;
        },
        select: function( event, ui ) {
            /*$( "#project" ).val( ui.item.label );
            $( "#project-id" ).val( ui.item.value );
            $( "#project-description" ).html( ui.item.desc );
            $( "#project-icon" ).attr( "src", "images/" + ui.item.icon );*/
            
            var me = $(this);

            //exisintg
            if( typeof ui.item.id !== "undefined"){
                
                me.val(ui.item.label);

                $('#booking_patient_search_form').find('input[name="collection[patient_id]"]').val(ui.item.id);
                $('#btn-patient_type-1').show();

            //new
            }else{
                //alert('New Record will be created');
                $('#btn-patient_type-2').show();

                //$('#booking_patient_form').find('input[name="First_Name"]').focus();
            }

            console.log(ui.item);

            return false;
        }
    })
    .data("uiAutocomplete")._renderItem = function( ul, item ) {
      return $( '<li style="cursor: pointer">' )
        .append( "<a><strong>" + item.label + "</strong><br>" + item.desc + "</a>" )
        .appendTo( ul );
    };       

    $('#search_patient_match').focus();
    $("#search_patient_match").trigger($.Event("keypress", { keyCode: 40 }));
});