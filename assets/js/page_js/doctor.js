
$( document ).ready(function(){
 

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( ".consult_dropdowns" )
        // don't navigate away from the field on tab when selecting an item
        .on( "keydown", function( event ) {
            /*if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }*/
        })
        .autocomplete({
            minLength: 0,
            source: function( request, response ) {
              //console.log(request);
                var el_id = $($(this)[0].element).attr('name');

                $.getJSON( "ajax/get_consult_dropdown", {
                    term: extractLast( request.term ),
                    group: el_id
                }, response );
            },
            search: function() {
              // custom minLength
              /*var term = extractLast( this.value );
              if ( term.length < 2 ) {
                return false;
              }*/
            },
            focus: function(event, ui) {
              // prevent value inserted on focus              
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
              this.value = terms.join( ", " );
              return false;
            }
        });


    $('#consult_file').on('change', function(event){
        $('#upload_consult_form').submit();
    });


    $('select[name=consult_sel]').on('change',function(){


        var cid = $(this).val();

        $.ajax({
                url:'ajax/get_consult_info/',
                data:{cid:cid},
                success:function( result ){




                  //$('textarea[name=symptoms_backup],textarea[name=symptoms]').val();
                //$('textarea[name=symptoms_backup],textarea[name=symptoms]').val();
            
                  if(!$.isEmptyObject(result[0])){

                var res_obj = result[0];

                    
                    console.log(res_obj);


                        $('textarea[name=symptoms]').val(res_obj.present_complain);
                        $('textarea[name=call_center_triage]').val(res_obj.call_center_triage);
                        $('#past_medical_history').val(res_obj.past_med_history);
                        $('textarea[name=current_medication]').val(res_obj.current_medication);
                        $('textarea[name=immunisations]').val(res_obj.immunisations);
                        $('textarea[name=allergies]').val(res_obj.allergies);
                        $('textarea[name=social_history]').val(res_obj.family_history);
                        $('textarea[name=observation]').val(res_obj.observations);
                        $('textarea[name=diagnosis]').val(res_obj.diagnosis);
                        $('textarea[name=plan]').val(res_obj.plan);
                        $('textarea[name=cunsult_medication]').val(res_obj.current_medication);
                        $('textarea[name=medication_prescribed]').val(res_obj.medication_administired);
                        $('textarea[name=followup_gp]').val(res_obj.follow_up_with_gp);
                        
                    
                  }



                },
                type:'GET',
                dataType:'json'

            });

    });
//med-perc-container
    $('.add-med').on('click',function(event){

            var tbl_cnt = $('table.med-prec-frm').length;    

           $tbl_template =  '<table class="table table-condensed med-prec-frm" style="width:100%">'+
                                '<tr>'+
                                    '<td><label>Name</label></td>'+
                                    '<td><input type="text" name="medication_perscribed_sub['+tbl_cnt+'][name]" class="form-control input-sm" value=""></td>'+
                                    '<td><label>Strength</label></td>'+
                                    '<td><input type="text" name="medication_perscribed_sub['+tbl_cnt+'][strength]" class="form-control input-sm" value=""></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td><label>Frequency</label></td>'+
                                    '<td><input type="text" name="medication_perscribed_sub['+tbl_cnt+'][frequency]" class="form-control input-sm" value=""></td>'+
                                    '<td><label>Quantity</label></td>'+
                                    '<td><input type="text" name="medication_perscribed_sub['+tbl_cnt+'][quantity]" class="form-control input-sm" value=""></td>'+
                                '</tr>'+           
                                '<tr>'+
                                    '<td><label>Repeats</label></td>'+
                                    '<td><input type="text" name="medication_perscribed_sub['+tbl_cnt+'][repeats]" class="form-control input-sm" value=""></td>'+
                                    '<td><a class="btn btn-danger remove-med" style="margin-bottom:10px;"  href="#" title="remove"><span class="glyphicon glyphicon-remove "></span></a></td>'+

                                '</tr>'+
                            '</table>';


            $('#med-perc-container').append($tbl_template);

        event.preventDefault();
    });

    Doctor.remove_med_perscribed();
});


var Doctor = {

    nexttab: function(tab) {
        $('#myTabs li:eq('+tab+') a').tab('show');
    },

    copy_consult_note: function(fld){
    	var td = $(fld).closest('tr').find('td');
    	
    	console.log(td);
    	console.log($('textarea[name="past_medical_history"]').text());

    	$('textarea[name="past_medical_history"]').val(td[3].innerHTML);
    	$('textarea[name="current_medication"]').val(td[4].innerHTML);
    	$('textarea[name="immunisations"]').val(td[5].innerHTML);
    	$('textarea[name="allergies"]').val(td[6].innerHTML);
    	$('textarea[name="social_history"]').val(td[7].innerHTML);
    	$('textarea[name="observation"]').val(td[8].innerHTML);
    	$('textarea[name="diagnosis"]').val(td[9].innerHTML);
    	$('textarea[name="plan"]').val(td[10].innerHTML);
    	$('textarea[name="cunsult_medication"]').val(td[11].innerHTML);
    	$('textarea[name="medication_prescribed"]').val(td[12].innerHTML);
    	$('textarea[name="followup_gp"]').val(td[13].innerHTML);
    },

    on_select_change: function(fld){

        console.log(fld.value);

        var me = $(fld);

        console.log(me.attr('name'));
        console.log( me.next('input').length);

        if( me.next('input').length > 0 ){
            me.next('input').hide();
            me.next('input').attr('disabled', 'disabled');
        }

        if(  (fld.value).toLowerCase() == 'other' ){
            //me.after('<input type="text" class="form-control" name="'+me.attr('name')+'_other" value="" placeholder="other" >');
            me.next('input').show();
            me.next('input').removeAttr('disabled');
        }

    },
    remove_med_perscribed:function(){


        $(document).on('click', '.remove-med', function(event) {
        
                var tbl_cnt = $('table.med-prec-frm').length;   

                 //if(confirm('Are you sure to remove this medication')){

                        if(tbl_cnt == 1){
                            alert('Must have at least 1 medication perscribed.');
                            return false;
                        }

                        $(this).parent().closest('table').remove();
                //}

        


            event.preventDefault();
        });

    }

}