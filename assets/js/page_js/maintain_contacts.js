
$( document ).ready(function(){

	
	var style_formats = [
			 
			{title : 'Header 2', block : 'h2' },
			{title : 'Header 3', block : 'h3' },
			{title : 'Header 4', block : 'h4' }			 
		];
	var plugins = [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	         "save table contextmenu directionality emoticons paste textcolor",
	         "filemanager template"
	    ];

	//Initialize tinyMCE for Greeting
	tinymce.init({
	    selector: "#contact-msg",
	    document_base_url: hcd_globals.base_url,
	    theme: "modern",
	   /* width: 300,*/
	    height: 250,
	    cleanup : true,
	    plugins: plugins,
	    convert_urls: false,
	    content_css: "assets/css/bootstrap.min.css",
	    /**/
	    toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons", 
		style_formats : style_formats,

	    external_filemanager_path : 'filemanager/show', 

    	/*templates: '<?php echo base_url(); ?>ajax/tinymcetemplate',*/

	    template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}

	});	

var FilemanagerDialogue = {

				click: function(URL){

					parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);
					parent.tinymce.activeEditor.windowManager.close();
				}
			}

});

var  common_plugin = 
				{
				  modal :function( modal_config , modal_button )
				  {
				  	
				  	var default_button = '<button type="button" class="btn btn-primary">Save changes</button>'
									 +'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';

				  	$('.modal').remove();



	            var config = $.extend({                
		                title: 'House Call Doctor',
		                message: 'No message to display.'
	            }, modal_config );

				  	$modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
								  +'<div class="modal-dialog" role="document">'
								    +'<div class="modal-content">'
								      +'<div class="modal-header">'
								        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
								      +'</div>'
								      +'<div class="modal-body">'+ config.message +'</div>'
								      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
								    +'</div>'
								  +'</div>'
								+'</div>').appendTo('body');


			 $modal_object.modal({keyboard:false, backdrop:false})
			            .on('hidden.bs.modal', function (e) {
			                $(this).remove();
			            }); 
				  }
				}




	var contact = 
			     {
			     	view_contact:function( contact_id)
			     	{
			     		 try
							 {
								if(contact_id =='' ) throw "Error:  Please select contact.";

	
									$.ajax({
											url: 'ajax/get_contact_by_id',
											type:'POST',
											data:{id:contact_id},
											dataType: 'json',											
											success: function(data)
											{												
												if(!$.isEmptyObject(data.error))
												{
													hcd.common.alert({'title':'Alert Box', 'message':data.error});
												}
												else
												{

													common_plugin.modal( {
														message:'<div class="row">'	
																	+'<div class="col-md-12">'								  
										  						 +data.msg
															+'</div>'
													}, '<button class="btn btn-primary" data-dismiss="modal">Ok</button>');	
												}		                           
											},

											error: function(request, status, error){											

									}
								});


								}
								catch( err )
								{
									hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
								}
			     	}
				 }	




		$('.search').click(function(){			
			
				$('#contactSearchForm').submit(function(event){ 
			
				   	var search = $('input[name=search_contact]').val();
			        if ( search != "" ) {
		              $('#formtype').val('search');
			          $(this).attr('action','maintenance/contacts/?&search='+search);
			          $(this).submit();
			        }

			    }); 
		})