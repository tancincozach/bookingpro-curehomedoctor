
$( document ).ready(function(){

	/*$('.edit-item').on('click', function(event){
		event.preventDefault();


	})*/

});


var consult = {



	add: function(elem, event){
		event.preventDefault();
		
		var add_dropdown_item = $(elem).closest('.panel').find('.panel-body').find('.add_dropdown_item');

		//console.log(add_dropdown_item.length);
		
		if(add_dropdown_item.length < 1 ){

			var html = '<div class="add_dropdown_item">'
                    +'	<div class="input-group input-group-sm">'
                    +'        <input type="text" class="form-control">'
                    +'        <span class="input-group-btn">'
                    +'            <button type="button" class="btn btn-success btn-flat" onclick="consult.save(this, event)" title="Add Dropdown"><i class="glyphicon glyphicon-save"></i></button>'
                    +'            <button type="button" class="btn btn-danger btn-flat" onclick="consult.cancel(this, event)" title="Cancel"><i class="glyphicon glyphicon-triangle-right"></i></button>'
                    +'        </span>'
                    +'    </div>'
                    +'</div>';
           
            $(elem).closest('.panel').find('.panel-body .consult-list').before(html);
		}

	},

	save: function(elem, event){
		event.preventDefault();
		
		var itemv = ($(elem).closest('.input-group').find('input').val()).trim(),
			dropdown = $(elem).closest('.box'),
			me = this;
		
		
		if(itemv == '') {
			alert('Empty value is not allowed');
			return false;
		}else{
 
			if( !hcd.processing ){

				hcd.processing = true;

				$.post('ajax/dropdown_item_action', {type:'add', text: itemv, group:dropdown.data('id')}, function(res){

					if( typeof res.error == 'undefined' ){
						
						dropdown.find('.consult-list').append(res.item);

						me.cancel(elem, event);					
					}else{
						alert(res.error_msg);
					}

				},'json')
				.always(function(){
					hcd.processing = false;
				});

				
			}
		}

		return false;
	},

	cancel: function(elem, event){
		event.preventDefault();

		$(elem).closest('.add_dropdown_item').remove();

		return false;
	},

	opt_item: function(elem){

			var me = $(elem),
			parent = me.closest('.list-group-item');

		return {
			parent: parent,
			id: parent.data('id'),
			c_item: parent.find('.item'),
			c_edit: parent.find('.edit'),
			text: parent.find('.item').find('.text').html()
		}

	},	

	edit_item: function(elem, event){
		event.preventDefault();
		
		/*var me = $(elem),
			parent = me.closest('.list-group-item'),
			id = parent.data('id'),
			c_item = parent.find('.item'),
			c_edit = parent.find('.edit');*/
		
		var opt = this.opt_item(elem);
		opt.c_item.hide();
		opt.c_edit.show();		

		$(opt.c_edit).find('input').val(opt.text);

		//console.log(opt.id);
		//console.log(opt.text);

		return false;
	},

	save_item: function(elem, event){
		event.preventDefault();
		
		var opt = this.opt_item(elem);		
		opt.c_item.hide();
		opt.c_edit.show();

		var me = this;
				
		if(opt.text == '') {
			alert('Empty value is not allowed');
			return false;
		}else{
 
			if( !hcd.processing ){

				hcd.processing = true;

				var in_txt = opt.c_edit.find('input').val();

				$.post('ajax/dropdown_item_action', {type:'update', text: in_txt, id:opt.id}, function(res){

					if( typeof res.error == 'undefined' ){
						
						opt.c_item.find('.text').html(in_txt)
						
						alert(res.msg);

						me.cancel_item(elem, event);					
					}else{
						alert(res.error_msg);
					}

				},'json')
				.always(function(){
					hcd.processing = false;
				});

				
			}
		}

		return false;
	},

	delete_item: function(elem, event){
		event.preventDefault();

		var opt = this.opt_item(elem);

		var li_item = $(elem).closest('.list-group-item');

		//console.log(li_item);
		
		if( confirm('Delete: '+opt.text+' ?') ){

			if( !hcd.processing ){

				hcd.processing = true;

				$.post('ajax/dropdown_item_action', {type:'delete', id: opt.id, text: opt.text}, function(res){
					
					if( typeof res.error == 'undefined' ){
						
						alert(res.msg);

						li_item.remove();

					}else{

						alert(res.error_msg);
					}

				},'json')
				.always(function(){
					hcd.processing = false;
				});
			}
		}

		return false;
	},

	cancel_item: function(elem, event){
		event.preventDefault();

		opt = this.opt_item(elem);
		opt.c_item.show();
		opt.c_edit.hide();
	}


}