

    var general = {

        hcd_sms_email_settings:function ( )
        {
                $.ajax({
                        url: 'ajax/get_email_sms_configuration',
                        type:'get',
                        dataType: 'json',                                           
                        success: function(response)
                        {                                               
                                                        
                           if(!$.isEmptyObject(response.email_sms_config))
                            {
                                $.each(response.email_sms_config,function(i,value){                            

                                        if(value.set_name=='SEND_SMS')
                                        {              
                                        console.log('sms');
                                        console.log(parseInt(value.set_value));
                                           if(parseInt(value.set_value)==1)
                                            {
                                              $('#send_sms ').bootstrapToggle('on');       
                                            }
                                            else
                                            {
                                                $('#send_sms ').bootstrapToggle('off');  
                                            }
                                            
                                        }

                                        if(value.set_name=='SEND_EMAIL')
                                        {                                             
                                          if(parseInt(value.set_value)==1)
                                            {
                                              $('#send_email').bootstrapToggle('on');       
                                            }
                                            else
                                            {
                                                $('#send_email ').bootstrapToggle('off');  
                                            }
                                           
                                        }
                                });
                            }  

                        },

                        error: function(request, status, error){                                            

                      }
                });         
        }
        ,
        email_configuration:function( object ) 
        {
             $.ajax({
                        url: 'ajax/post_email_settings',
                        type:'POST',
                        dataType: 'json',   
                        data:object,                                        
                        success: function(response)
                        {                                               
                                if(!$.isEmptyObject(response.error))
                                {
                                    hcd.common.alert({'title':'House Call Doctor', 'message':response.error});
                   
                                }
                                else
                                {

                                    hcd.common.alert( {'title':'House Call Doctor',
                                        'message':response.message
                                    }); 
                                        
                                }   
                        },

                        error: function(request, status, error){                                            

                      }
                });         
        },

        set_chaperone_max_booking: function(field){

            if( confirm('Confirm Update') ){

                var max = $('#general_chap_max_booking').val();

                if( max != '' ){

                    $.post('ajax/ajax_chap_max_booking', {maxno:max, user_id: hcd_globals.user_id, agent_name: hcd_globals.agent_name }, function(response){

                         hcd.common.alert({'title':'MAX BOOKING', 'message':response.message}); 

                    },'json')                    
                    .always(function(){

                    });

                }else{
                    hcd.common.alert({'title':'Error Input', 'message':'Input must be number'});
                }
            }

            return false;
        }
        
  }



$( document ).ready(function(){

    
    var style_formats = [
             
            {title : 'Header 2', block : 'h2' },
            {title : 'Header 3', block : 'h3' },
            {title : 'Header 4', block : 'h4' }          
        ];
    var plugins = [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons paste textcolor",
             "template"
        ];

    //Initialize tinyMCE for Greeting
    tinymce.init({
        selector: "#summernote",
        document_base_url: hcd_globals.base_url,
        theme: "modern",
       /* width: 300,*/
        height: 250,
        cleanup : true,
        plugins: plugins,
        convert_urls: false,
        content_css: "assets/css/bootstrap.min.css",
        /**/
        toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons", 
        style_formats : style_formats,

        /*external_filemanager_path : 'filemanager/show', */

        /*templates: '<?php echo base_url(); ?>ajax/tinymcetemplate',*/

        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }

    }); 

      $('#send_email,#send_sms').change(function() {

                   $.ajax({
                        url: 'ajax/post_sms_email_configuration',
                        type:'post',
                        dataType: 'json',                                           
                        data:{option:$(this).attr('id'),set_value:($(this).prop('checked')==true ? 1:0)},
                        success: function(response){

                        }});
                })

        $('.submit-email').click(function(){                

                var email    = $(this).parent().prev().children('input').val(),
                    postData = {set_name:$(this).attr('alt'),set_value:email,user_id:hcd_globals.user_id,agent_name:hcd_globals.agent_name};

                general.email_configuration(postData);

        });

      $('#override_sched_filter').change(function() {

           $.ajax({
                url: 'ajax/post_override_filter_schedule',
                type:'post',
                dataType: 'json',                                           
                data:{option:$(this).attr('id'),set_value:($(this).prop('checked')==true ? 1:0)},
                success: function(response){

            }});
        })        
});
    


        general.hcd_sms_email_settings();