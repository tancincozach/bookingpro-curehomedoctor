   
     var  common_plugin = 
   					{
   					  modal :function( modal_config , modal_button )
   					  {
   					  	
   					  	var default_button = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
											 +'<button type="button" class="btn btn-primary">Save changes</button>';

   					  	$('.modal').remove();



			            var config = $.extend({                
				                title: 'House Call Doctor',
				                message: 'No message to display.'
			            }, modal_config );

   					  	$modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
										  +'<div class="modal-dialog" role="document">'
										    +'<div class="modal-content">'
										      +'<div class="modal-header">'
										        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
										        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
										      +'</div>'
										      +'<div class="modal-body">'+ config.message +'</div>'
										      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
										    +'</div>'
										  +'</div>'
										+'</div>').appendTo('body');


					 $modal_object.modal({keyboard:false, backdrop:false})
					            .on('hidden.bs.modal', function (e) {
					                $(this).remove();
					            }); 
   					  }
   					}


$(function(){

		$('.action-button').click(function()
		{
		  $('#formtype').val($(this).attr('alt'));
		});

		$('.cancel').click(function(event){
			window.location="maintenance/practices";
			event.preventDefault();			
		});


		$('.search').click(function(){			
			
				$('#PracticesListForm').submit(function(event){ 
			
				   	var search = $('input[name=search_practice]').val();
			        if ( search != "" ) {
		              $('#formtype').val('search');
			          $(this).attr('action','maintenance/practices/?&search='+search);
			          $(this).submit();
			        }

			    }); 
		})


		$('select[name=user_level]').change(function(){
			
			if($(this).val()==5)
			{		      
			 $('.sub_level').removeClass('hide');
			}
			else
			{
		 	  $('.sub_level').addClass('hide');	
			}
		});
});