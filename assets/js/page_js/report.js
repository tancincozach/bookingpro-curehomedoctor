
$( document ).ready(function(){
    /*
    $('.datepicker').datetimepicker({
        locale: 'en-au',
        format: "DD/MM/YYYY"
    }); //YYYY-MM-DD*/
      
    $('.singleDatePicker').daterangepicker(
        {
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
            format: 'DD/MM/YYYY'
        }
    ); 

    $('.rangeDatePicker').daterangepicker(
        {
            //singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
            format: 'DD/MM/YYYY'
        }
    ); 
 
});
 