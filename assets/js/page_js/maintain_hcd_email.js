
var  common_plugin = 
				{
				  modal :function( modal_config , modal_button )
				  {
				  	
				  	var default_button = '<button type="button" class="btn btn-primary">Save changes</button>'
									 +'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';

				  	$('.modal').remove();



	            var config = $.extend({                
		                title: 'House Call Doctor',
		                message: 'No message to display.'
	            }, modal_config );

				  	$modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
								  +'<div class="modal-dialog" role="document">'
								    +'<div class="modal-content">'
								      +'<div class="modal-header">'
								        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
								      +'</div>'
								      +'<div class="modal-body">'+ config.message +'</div>'
								      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
								    +'</div>'
								  +'</div>'
								+'</div>').appendTo('body');


			 $modal_object.modal({keyboard:false, backdrop:false})
			            .on('hidden.bs.modal', function (e) {
			                $(this).remove();
			            }); 
				  }
				}


	$(function(){
			$('.search').click(function(){			
				
					$('#contactSearchForm').submit(function(event){ 
				
					   	var search = $('input[name=search_contact]').val();
				        if ( search != "" ) {
			              $('#formtype').val('search');
				          $(this).attr('action','maintenance/contacts/?&search='+search);
				          $(this).submit();
				        }

				    }); 
			})
	});

	