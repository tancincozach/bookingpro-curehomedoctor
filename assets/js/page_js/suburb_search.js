
$( document ).ready(function(){
 
});


var Suburb = {

    search: function(field){

        var searchtxt = field.value;
        searchtxt = searchtxt.replace(/\s+/g, '');

        if( !hcd.processing && searchtxt.length > 2 ){ 

            var form_fields = $(field).parents('form').serialize();

            hcd.processing = true;

            $("#search_results").html('<p>Please wait ... processing your required</p>');
            $.post('ajax/suburb_search', form_fields, function(res){

                if( res.error ){
                    $("#search_results").html('<p style="padding: 20px; color: red">'+res.error+'</p>');
                }else{
                    if( res.total > 0 ){
                        $("#search_results").html(res.records);
                    }else{
                        $("#search_results").html('<p>No records found</p>');
                    }
                }
                
            },'json')
            .always(function(){
                hcd.processing = false;
            });
        }

        return false;
    }

}