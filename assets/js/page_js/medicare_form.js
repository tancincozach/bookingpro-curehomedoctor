
$( document ).ready(function(){
    
     jsSignature  =  $('.js-signature').jqSignature();

       
    /*$("#dob").inputmask("d/m/y", 
    {
        "placeholder": "dd/mm/yyyy",
        "onincomplete": function(){
            //console.log(this.value);
            if(!hcd.common.isDOB(this.value)){
                $(this).popover({content:'<strong style="color:red">Date of birth is invalid</strong>', placement:'left', html:true}).popover('show');
                $(this).focus();
            }
        },
        "oncomplete": function(){
            if(!hcd.common.isDOB(this.value)){
                $(this).popover({content:'<strong style="color:red">Date of birth is invalid</strong>', placement:'left', html:true}).popover('show');
                $(this).focus();
            }else{
                $(this).popover('hide');
            }
        }
    });*/

    $('#dob').combodate({
       maxYear: new Date().getFullYear(),
       //custom_dob_age_target: '#patient_age',
       on_error_popover: true
    }); 


   if($('.signature-image').length > 0)
    {
          jsSignature.hide();
    }
    else
    {
           jsSignature.show();              
    }


    $('.clear_sign').click(function(event){

      
        Medicare.clear_signature($(this).attr('alt'));
         event.preventDefault();
    });


});


var Medicare = {

    confirm: function(){

        //$('#signature').empty();
        var dataUrl = $('.js-signature').jqSignature('getDataURL'); 
        //console.log(dataUrl);

        if(dataUrl=='')
        {
            alert('Please sign on the signature');
            return false;
        }

        $('#hidden_sig').val(dataUrl);

        return true;
    },

    sig_required: function(field){

        if( $(field).is(":checked") ){

            $('#hidden_sig').removeAttr('required');
            $('.cls-sign').hide();
        }else{
            $('#hidden_sig').attr('required', 'required');
            $('.cls-sign').show();
        }
    },
    clear_signature:function( appt_id )
    {
         if($('.signature-image').length > 0)
         {
                $('.signature-image').hide();
                  jsSignature  =  $('.js-signature').jqSignature();
                    jsSignature.show(); 
                   // this.update_signature(appt_id);
           }
           else
           {
              jsSignature.show();           
              jsSignature.jqSignature('clearCanvas');
           }
    }
    ,
    update_signature:function( appt_id)
    {
            try{

            if(appt_id=='' || appt_id==0)
                    throw "Error: Appt ID is empty."; 


                $.ajax({
                    url:'ajax/clear_signature/',
                    data:{appt_id:appt_id,agent_name:hcd_globals.agent_name},
                    type:'post',
                    dataType: 'json', 
                    success:function(response)
                    {
                       if(!$.isEmptyObject(response.error))
                        {                        
                            hcd.common.alert({'title':'Alert Box', 'message':response.error});
                        }
                    }
                    ,error:function( response )
                    {
                     hcd.common.alert({'title':'Alert Box', 'message':response});               
                    }
                })
            return true;
            }
            catch( err )
            {
            hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
            return false;
            } 
    }

} 