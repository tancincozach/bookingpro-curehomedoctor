   




var areas_availability = {

	ajax_request_set_availability:function( posted_data ) 
	{   	
		if(!$.isEmptyObject(posted_data))
		{
			$.ajax({
				url: 'ajax/set_availability',
				type:'POST',
				dataType: 'json',
				data:posted_data,
				success: function(data)
				{
					if(!$.isEmptyObject(data.error))
					{
						hcd.common.alert({'title':'Alert Box', 'message':data.error});
					}
					else
					{
						hcd.common.alert({'title':'House Call Doctor', 'message':data.msg});

						common_plugin.modal( {
							message:'<div class="row">'	
										+'<div class="col-md-12">'								  
			  						 +data.msg
								+'</div>'
						}, '<button class="btn btn-primary" data-dismiss="modal">Ok</button>');	
					}		                           
				},

				error: function(request, status, error){											

				}
			});
		}				   						
	},

	setAvailability:function( area_id )
	{
	  try
	  {
	    if(area_id=='') throw "Error : Area is empty";

	    common_plugin.modal(   {
							     message:'<div class="row">'
										+'<div class="col-md-12">'
										+'<label> Set Closed availability today ? If not please select another date. ? </label>'									
									+'</div>'
						 		+'</div>'
						   } ,'<button class="btn btn-success  set-today " alt="yes">Yes</button><button class="btn btn-primary set-today" alt="no">No</button><button class="btn btn-default" data-dismiss="modal">Close</button>' 
						);

		$('.set-today').click(function(){


			var effective_date = $('#current_date_formatted').val();

			if($(this).attr('alt')=='no')
			{
				common_plugin.modal( {
											message:'<div class="row">'	
														+'<div class="col-md-12">'								  
							  							+'<label>Effective Date : </label>'
					  			     						+'<input name="effective-date" type="text" class="form-control" data-provide="datepicker" data-date-format="DD/MM/YYYY" data-date-autoClose="true"/>'
													+'</div>'
												+'</div>'
												} , '<button class="btn btn-primary set-anyday" alt="yes">Save</button><button class="btn btn-default set-anyday" alt="no"  data-dismiss="modal">Cancel</button>' );
		

			 var $date_picker = $('input[name=effective-date]');

				  // $date_picker.daterangepicker({ singleDatePicker: true, locale: { format: 'DD/MM/YYYY' }, format: 'DD/MM/YYYY' });

				  $date_picker.datetimepicker();	


				   	$('.set-anyday').click(function()
				   	{   				   		
			   			if($(this).attr('alt')=='yes')
				   		{	   						   		
					  if( $date_picker.val()!='')
				   		  {   	

				   		   effective_date = $date_picker.val();					   		   

				   		   var posted_data = {
										effective_date:effective_date,
										area_id :area_id,
										user_id:misc_data.user_id,
										agent_name:misc_data.agent_name
				   						  }  						   		
				   		    areas_availability.ajax_request_set_availability(posted_data);

	   		      			$('.modal').modal('hide');

	   		      				setTimeout(function() { 
										window.location='maintenance/areas';
	   		      				},2000);
	   		      				

				   		  }
				   		  else
				   		  {

				   		  	hcd.common.alert({'title':'ERROR', 'message':'Please select availability date'});
				   		  	$('.modal').modal('hide');
				   		  } 
				   		  
				   		}
			 
				   	});   											
			}
			else
			{
			var posted_data = {
								effective_date:effective_date,
								area_id :area_id,
								user_id:misc_data.user_id,
								agent_name:misc_data.agent_name
						 	 }  						   		
				areas_availability.ajax_request_set_availability(posted_data);  
				$('.modal').modal('hide');

				setTimeout(function() { 
					window.location='maintenance/areas';
  				},1000);
	   		      				

			}   							

		});

	  }
 	   catch(err)
	  {
	  	 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
	  }						   											
	},

	setAvailability2: function(area_id, agent_name, user_id){


		$.get('ajax/set_availability_modal/',{area_id:area_id, agent_name:agent_name, user_id:user_id}, function(modallayout){
		 
			$(modallayout).modal({keyboard:false, backdrop:false})
				.on('shown.bs.modal', function (event) {
					
				 	$('input[name=reopen_dt_start]').datetimepicker({
				 		'format': 'DD/MM/YYYY HH:mm',
				 		useCurrent: true,				 		
				 		keepInvalid: true, 
				 		keyBinds: {
					 		up: null,
					 		down: null,
					 		left: null,
					 		right: null
					 	}
				 	});

				 	$('input[name=reopen_dt_end]').datetimepicker({
				 		'format': 'DD/MM/YYYY HH:mm',
				 		 useCurrent: false, //Important! See issue #1075				 		 
				 		 keepInvalid: true,
				 		 keyBinds: {
					 		up: null,
					 		down: null,
					 		left: null,
					 		right: null
					 	}
				 	});

			        $('input[name=reopen_dt_start]').on("dp.change", function (e) {
			            $('input[name=reopen_dt_end]').data("DateTimePicker").minDate(e.date);
			        });
			        $('input[name=reopen_dt_end]').on("dp.change", function (e) {
			            $('input[name=reopen_dt_start]').data("DateTimePicker").maxDate(e.date);
			        });


					$('#closed_reopen_dt').find('input, textarea').each(function(index){
						$(this).attr('disabled', 'disabled');
					});


			        $('#closed_reopen_dt .nav-tabs a').on('shown.bs.tab', function(event){
					    var x = $(event.target).text();         // active tab
					    //var y = $(event.relatedTarget).text();  // previous tab

					    console.log(x);

					    if( x == 'CLOSE FOR EVENING' ){

							$('#closed_reopen_dt #close_for_evening').find('input').each(function(index){
								$(this).removeAttr('disabled');
								//console.log('remove close_for_evening')
							});

							$('#closed_reopen_dt #close_for_period').find('input, textarea').each(function(index){
								$(this).attr('disabled','disabled');
								//console.log('remove close_for_evening')
							});

					    }else if(x == 'CLOSE FOR PERIOD & SCRIPT CHANGE'){

							$('#closed_reopen_dt #close_for_period').find('input, textarea').each(function(index){
								$(this).removeAttr('disabled');
								//console.log('remove close_for_period')
							});
							
							$('#closed_reopen_dt #close_for_evening').find('input').each(function(index){
								$(this).attr('disabled','disabled');
								//console.log('remove close_for_evening')
							});

					    }
					}); 




					//var modal = $(this);  
		 			//modal.find('.modal-body').html(responseHtml); 
		 			//modal.find('.modal-body').find('.category_options_div').find('.breadcrumb').hide()
				})
				.on('hidden.bs.modal', function (e) {
					$(this).remove();
					window.location = 'maintenance/areas';
				});
 
		});

 

	},

	on_submit_setAvailability2: function(form){

		var form = $(form).serialize();

		if( confirm('Please confirm on updating the availability status?') ){			

			$.post('ajax/set_availability2', form, function(response){

				if(response.status) {
	 
					$('.area_availability_history_div table > tbody > tr:first').before(response.new_sechedule);
					$('.area_availability_history_div table > tbody > tr:first').fadeOut('slow', function(){
						$(this).fadeIn('slow');

						hcd.common.alert({'title':'SUCCESS', 'message':response.status_msg});
					});

				}else{
					hcd.common.alert({'title':'ERROR', 'message':response.status_msg});
				}

			}, 'json');

		}

		return false
	},

	on_click_radio_reopen_dt_range: function(radio){

		$('#closed_reopen_dt').hide();

		if( radio == 0 ){			
			$('#closed_reopen_dt').show();

			/*$('#closed_reopen_dt').on('shown.bs.tab', function (e) {
				//console.log(e);
				console.log(e.target);
			});*/

			$('#closed_reopen_dt .nav-tabs a[href="#close_for_evening"]').tab('show');

		}else{
	 
		}
	},



   	reopen_area_activity:function( area_avail_id )
   	{
   	 try
   	 {
   	 	if(area_avail_id=='')  throw "Cannot change or reopen  availability";			   			   	 		

	    common_plugin.modal(   {
				     message:'<div class="row">'
							+'<div class="col-md-12">'
							+'<label> Are you sure to reopen the  availability of this area ? </label>'									
						+'</div>'
			 		+'</div>'
			   } ,'<button class="btn btn-success reopen-availability" alt="yes"  data-dismiss="modal">Yes</button><button class="btn btn-default" data-dismiss="modal">No</button>' 
			);

	    $('.reopen-availability').click(function()
	      {	
	    	$.ajax({
				url: 'ajax/reopen_availability',
				type:'POST',
				dataType: 'json',
				data:{ 
					avail_id:area_avail_id,
					user_id:misc_data.user_id,
					agent_name:misc_data.agent_name
				},
				success: function(data)
				{
					if(!$.isEmptyObject(data.error))
					{
						hcd.common.alert({'title':'Alert Box', 'message':data.error});
						setTimeout(function() { 
							window.location='maintenance/areas';
  							},1000);
					}
					else
					{
						hcd.common.alert({'title':'House Call Doctor', 'message':data.msg});

						common_plugin.modal( {
							message:'<div class="row">'	
										+'<div class="col-md-12">'								  
			  						 +data.msg
								+'</div>'
						}, '<button class="btn btn-primary" data-dismiss="modal">Ok</button>');	
									setTimeout(function() { 
							window.location='maintenance/areas';
  							},1000);
					}		                           
				},

				error: function(request, status, error){											

				}
			});	    	
	      }
	     );

   	 }
 	  catch(err)
	  {
	  	 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
	  }
		
   },

   set_area_timezone:function( area_id )
   {
	try
   	 {
   	 	if(area_id=='')  throw "Cannot set timezone ";			   			   	 		

	    common_plugin.modal(   {
				     message:'<div class="row">'
							+'<div class="col-md-12">'
							+'<label> Timezone </label>'									
							+'<select name="timezone" class="form-control timezone">'
								+'<option value="" selected="selected">Select Timezone</option>'
								+'<option value="Australia/Adelaide">Australia/Adelaide</option>'
								+'<option value="Australia/Broken_Hill">Australia/Broken_Hill</option>'
								+'<option value="Australia/Hobart">Australia/Hobart</option>'
								+'<option value="Australia/Lord_Howe">Australia/Lord_Howe</option>'
								+'<option value="Australia/Melbourne">Australia/Melbourne</option>'
								+'<option value="Australia/NSW">Australia/NSW</option>'
								+'<option value="Australia/Perth">Australia/Perth</option>'
								+'<option value="Australia/Queensland">Australia/Queensland</option>'
								+'<option value="Australia/Sydney">Australia/Sydney</option>'
								+'<option value="Australia/Victoria">Australia/Victoria</option>'
								+'<option value="Asia/Manila">Asia/Manila</option>'
							+'</select>'
						+'</div>'
			 		+'</div>'
			   } ,'<button class="btn btn-success change-timezone" alt="yes"  data-dismiss="modal">Yes</button><button class="btn btn-default" data-dismiss="modal">No</button>' 
			);

	    $('.change-timezone').click(function()
	      {	
	    	$.ajax({
				url: 'ajax/set_area_timezone',
				type:'POST',
				dataType: 'json',
				data:{ 
					timezone:$('.timezone').val(),
					area_id:area_id ,
					user_id:misc_data.user_id,
					agent_name:misc_data.agent_name
				},
				success: function(data)
				{
					if(!$.isEmptyObject(data.error))
					{
						hcd.common.alert({'title':'Alert Box', 'message':data.error});
						setTimeout(function() { 
							window.location='maintenance/areas';
  							},1000);
					}
					else
					{
						hcd.common.alert({'title':'House Call Doctor', 'message':data.msg});

						common_plugin.modal( {
							message:'<div class="row">'	
										+'<div class="col-md-12">'								  
			  						 +data.msg
								+'</div>'
						}, '<button class="btn btn-primary" data-dismiss="modal">Ok</button>');	
									setTimeout(function() { 
							window.location='maintenance/areas';
  							},1000);
					}		                           
				},

				error: function(request, status, error){											

				}
			});	    	
	      }
	     );

   	 }
 	  catch(err)
	  {
	  	 hcd.common.alert({'title':'ERROR', 'message':err.message||err});
	  }
		
   }
}


var areas = {

	set_region_type: function(area_id, region_type, agent_name, user_id){

		if( confirm('Please confirm change to '+region_type+'?') ){
  	

			$.post('ajax/set_region_type', {id: area_id, region:region_type, agent_name:agent_name, user_id:user_id}, function(response){

				if(response.status) {
	 
					hcd.common.alert({'title':'SUCCESS', 'message':response.msg});
					 
				}else{
					hcd.common.alert({'title':'ERROR', 'message':response.msg});
				}

				setTimeout(function(){ 
					window.location = document.URL;
				},1500 );
				

			}, 'json'); 
 

		}

		return false
	},

 	view_chaperone :function( area_id )
   	{
   	 	try{

			if(area_id=='')  throw "Cannot view chaperone ";	



			 	$.ajax({
						url: 'ajax/get_chaperone',
						type:'POST',
						dataType: 'json',
						data:{ area_id:area_id },
						success: function(data)
						{
							if(!$.isEmptyObject(data.error))
							{
								hcd.common.alert({'title':'Alert Box', 'message':data.error});
								setTimeout(function() { 
									window.location='maintenance/areas';
		  							},1000);
							}
							else
							{
										hcd.common.alert({'title':'House Call Doctor ', 'message':data.msg});

									    common_plugin.modal(   {
									    		'title':'House Call Doctor - Assign Chaperone ',
											     message:'<form id="chaperone-form" method="POST"><input type="hidden" name="area_id" value="'+area_id+'"><div class="row">'											     
														+'<div class="col-md-12">'
														+'<label> Chaperone :</label>'									
														+'<div class="chaperone-container"></div>'
													+'</div>'
										 		+'</div></form>'
										   } ,'<button class="btn btn-success asign-chaperone" alt="yes"  data-dismiss="modal">Save</button><button class="btn btn-default" data-dismiss="modal">No</button>' 
										);

									 $('.chaperone-container').html('');

									 if(!$.isEmptyObject(data.result))
						              {
						              	  $.each(data.result,function(key,value)
						              	  {
						              		$('<div class="checkbox" ><label><input type="checkbox"  class="chaperone-cbox" name="chaperone[]" value="'+value.user_id+'" '+(value.is_selected==1  ? ' checked ':' ')+'>'+value.user_fullname+'</label></div>')
			                        		.appendTo('.chaperone-container');  	
						              	  });				              	

					              	 	  

						              	  $('.asign-chaperone').on('click',function()
						              	  {								              	  		

						              	  	var unselected_chaperone = new Array() , selected_chap = new Array();

						              	  	var ctr = 0;
						              	  	$(".chaperone-cbox").each(function(){

			              	  			    	if($(this).is(":checked"))
			              	  			    	{						              	  			    	
 														  selected_chap[ctr] = $(this).val();
 														  ctr++;
						             			}
						              	  	});

						              	  	ctr=0;

				              	  	     	$(".chaperone-cbox").each(function(){

				              	  			    if(!$(this).is(":checked"))
				              	  			    {						              	  			    	
												  	  unselected_chaperone[ctr] = $(this).val();
													  ctr++;
				      							}
						              	  	});	
						              	  	/*console.log('selected:'+selected_chap);
						              	  	console.log('unselected:'+unselected_chaperone);*/

						              	  		$.ajax({
														url: 'ajax/assign_chaperone_per_area',
														type:'POST',
														dataType: 'json',
														data:{
															area_id:area_id,
															selected:selected_chap,
															unselected:unselected_chaperone
														},
														success:function(result)
														{

															if(result)
															{
															 hcd.common.alert({'title':'House Call Doctor', 'message':'You have successfully assigned a chaperone.'});

																common_plugin.modal( {
																	message:'<div class="row">'	
																				+'<div class="col-md-12">'								  
													  						 +result.msg
																		+'</div>'
																}, '<button class="btn btn-primary" data-dismiss="modal">Ok</button>');	
																			setTimeout(function() { 
																	window.location='maintenance/areas';
										  							},1000);	
															}
															
														},
														error:function()
														{

														}
						              	  			  });
						              	  });

						              }							 

							}		                           
						},

						error: function(request, status, error){											

						}
					});	 
		

		}
		catch(err)
		{
			hcd.common.alert({'title':'ERROR', 'message':err.message||err});
		}

   	},

	add_suburb :function(  action , area_id )
	{
	 try
	 {
	   if(action =='' ) throw "Error:  Please select action.";

	    if(area_id=='' || area_id==0) throw "Error:  Area is empty.";

	    switch( action )
	    {
	  		case 'upload':

		  	var uploadfrm   ='<form id="suburb-form" accept-charset="utf-8" method="post" role="form" action="maintenance/import_suburb"  enctype="multipart/form-data">'
				  					+'<input type="hidden" style="display:none;" value="'+hcd_globals.h+'" name="'+hcd_globals.n+'">'
				  					+'<input type="hidden" value="upload" name="formtype" id="formtype">'
				  					+'<input type="hidden" value="'+area_id+'" name="area_id" id="formtype">'
				  					+'<div class="input-group">'
				  					+'<span>'
				  					+'Must be on CSV Format : Post Code,Suburb,Area'
				  					+'</span>'
				  					+'</div>'
				  					+'<div class="input-group">'
				                +'<span class="input-group-btn">'
				                    +'<span class="btn btn-primary btn-file">'
				                        +'Browse… <input type="file" multiple="" name="suburb" class="upload-file">'
				                    +'</span>'
				                +'</span>'
				                +'<input type="text" placeholder="Upload Suburb" class="form-control" readonly="">'
				            +'</div></form>',

	  		 		button  = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
						 +'<button type="button"  class="btn btn-primary upload-suburb">Upload Suburb</button>';

				

				 	 common_plugin.modal({message:uploadfrm} , button);	

	 					 $file  = $('.btn-file :file');

						$file.on('change', function() {
								var input = $(this),
								numFiles = input.get(0).files ? input.get(0).files.length : 1,
								label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
								input.trigger('fileselect', [numFiles, label]);
								})
							 .on('fileselect', function(event, numFiles, label){

									var input = $(this).parents('.input-group').find(':text'),
									    log = numFiles > 1 ? numFiles + ' files selected' : label;

									if( input.length ) {
									    input.val(log);
									} else {
									    if( log ) alert(log);
									}
								});

									$('.upload-suburb').on('click',function()
									{
									  areas.upload_suburb($file.val());
									});

			  	break;
			  	case 'view':

			  	 		window.location="maintenance/suburbs/?area="+area_id;

			  	break;
			  	case 'add':
			  			 window.location="maintenance/suburb_form/?area="+area_id;
			  	break;
	  	}   					  
	  
	}
	catch( err )
	{
		hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
	}
 },
 upload_suburb:function(file)
 {
	try
	{
	 if(file=='') throw "Error:  Please select file.";
	 	$('#suburb-form').submit();
	}
	catch(err)
	{
		hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
	}
 }
  ,   				
 initAreas :function()
 {
	$('.add-suburb').click(function(event)
	{

		var area_id  = $(this).parent().parent().attr('alt'),
		    act   = $(this).attr('alt');
		    console.log(act);
		areas.add_suburb(act,area_id);
		event.preventDefault();
	});	
	$('.action-button').click(function()
	{
	 $(	'#formtype').val($(this).attr('alt'));
	});	

			$('.search').click(function(){			
				var search = $('input[name=search_suburb]').val();
				var area_id = $('input[name=area_id]').val();
				$('#suburbForm').submit(function(event){ 
			


			        if ( search != "" ) {
		              $('#formtype').val('search');
			          $(this).attr('action','maintenance/suburbs/?area='+area_id+'&search='+search);
			          $(this).submit();
			        }

			    }); 
		})
  }
}				

$(function()
{
  areas.initAreas();
});
