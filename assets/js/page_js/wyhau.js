
$( document ).ready(function(){
     
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function(e, ui) {    

        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;  
    };

    //Make diagnosis table sortable     
    $(".table-row-order tbody").sortable({ 
        helper: fixHelperModified,
        placeholder: "ui-state-highlight",
        cursor: 'move',
        containment: "parent",
        stop: function(event,ui) {
            renumber_table('#table-wyhau');
        }    
    }).disableSelection(); 

    function renumber_table(tableID) {

        try {

            if( hcd.processing ) throw "Please wait... still processing the previous request";

            hcd.processing = true;

            var tr_ids = [];

            $(tableID + " tr").each(function() {
                                 
                var trid = $(this).attr('id');

                if(  typeof trid != 'undefined'){
                    trid = trid.split('-');
                    tr_ids.push(trid[1]);
                } 
                
            });

            if(tr_ids.length == 0) throw "Zero rows found";

            //console.log(tr_ids);
            $.post('maintenance/wyhau/sort', {'formtype':'sort', 'sort_data':tr_ids}, function(res){
                hcd.processing = false;
            })
            .always(function(){
                hcd.processing = false;
            });

        }catch( err ){

            hcd.common.alert({'title':'Alert Box', 'message':err.message||err});

        }


    }

});