   
     var  common_plugin = 
   					{
   					  modal :function( modal_config , modal_button )
   					  {
   					  	
   					  	var default_button = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
											 +'<button type="button" class="btn btn-primary">Save changes</button>';

   					  	$('.modal').remove();



			            var config = $.extend({                
				                title: 'House Call Doctor',
				                message: 'No message to display.'
			            }, modal_config );

   					  	$modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
										  +'<div class="modal-dialog" role="document">'
										    +'<div class="modal-content">'
										      +'<div class="modal-header">'
										        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
										        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
										      +'</div>'
										      +'<div class="modal-body">'+ config.message +'</div>'
										      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
										    +'</div>'
										  +'</div>'
										+'</div>').appendTo('body');


					 $modal_object.modal({keyboard:false, backdrop:false})
					            .on('hidden.bs.modal', function (e) {
					                $(this).remove();
					            }); 
   					  }
   					}



 var car_plugin = {


 		set_email_or_sms : function( post_data){

 					$.ajax({
 							url:'ajax/set_car_sms_or_email',
 							type:'POST',
 							data:post_data,
 							success:function( result ){


 							 hcd.common.alert({'title':'House Call Doctor','message':result.message});  

								setTimeout(function(){window.location=document.URL }, 2000);
 							},
 							dataType:'json'
 					})

 		},
 		 check_all_cbox:function( object,checked ){

 		 			$(object).each(function(){


 		 						$(this).prop('checked', checked);

 		 			})
 		 },


 		gather_checked_cbox_values:function(cbox_elem,checked){

 				var ids = Array();

			    var post_data = {};

				post_data.agent_id = hcd_globals.user_id;

				post_data.agent_name = hcd_globals.agent_name;

 				$(cbox_elem).each(function(){

 						ids.push($(this).val()+':'+$(this).prop('checked'));
 				});

				post_data.data = ids;

 				return post_data;

 		},

 		init:function(){

				$('.send-btn').on('click',function(){

						var post_data = {};

						//var temp_var = {};

						if($(this).attr('alt')=='email'){
						  

						  	post_data    = car_plugin.gather_checked_cbox_values('input[name=email]');

							post_data.email = post_data.data;

						  	delete(post_data.data);

					    	if(hcd.common.confirm('Are you sure you want to set Send Email(s)?')){

								car_plugin.set_email_or_sms(post_data);

							}


						}else{

							post_data = car_plugin.gather_checked_cbox_values('input[name=sms]');

							post_data.sms = post_data.data;

							delete(post_data.data);

						   	if(hcd.common.confirm('Are you sure you want to set Send SMS(s)?')){

								car_plugin.set_email_or_sms(post_data);

							}

						}

						
					
						

						console.log('sdfsdfsfsf');
						console.log(post_data);

				});

 				$('input[name=check_all_email],input[name=check_all_sms]').click(function(){

 				    if($(this).attr('name')=='check_all_sms'){

				    	
				   	 	car_plugin.check_all_cbox('input[name=sms]',$(this).prop('checked'));					    

				    
				    }else{
				    	
			    		car_plugin.check_all_cbox('input[name=email]',$(this).prop('checked'));	
				    	

				    }


 			});



		/*	$('input[name=email],input[name=sms]').click(function(){

				var check_obj = $(this) , 

				    car_id = check_obj.val(),

				    post_data = {};

				    if(check_obj.attr('name')=='sms'){

				    	post_data.sms  = check_obj.prop('checked');
				    	
				    }else{
				    	
				    	post_data.email  = check_obj.prop('checked');

				    }

				     post_data.agent_id = hcd_globals.user_id;

				     post_data.agent_name = hcd_globals.agent_name;
				     
					 post_data.car_id = check_obj.val();

					if(hcd.common.confirm()){
					 	car_plugin.set_email_or_sms(post_data);
					 }else{
					 	check_obj.prop('checked', false);
					 }
				    

			});*/

 		}


 }  					


$(function(){

		$('.action-button').click(function()
		{
		  $('#formtype').val($(this).attr('alt'));
		});

		$('.cancel').click(function(event){
			window.location='maintenance/cars/';
			event.preventDefault();			
		});


		$('.search').click(function(){			
			
				$('#carListForm').submit(function(event){ 
			
				   	var search = $('input[name=search_car]').val();

		              $('#formtype').val('search');
			          $(this).attr('action','maintenance/cars/?search='+search);
			          $(this).submit();


			    }); 
		});

		car_plugin.init();

});