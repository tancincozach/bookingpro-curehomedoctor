   
     var  common_plugin = 
   					{
   					  modal :function( modal_config , modal_button )
   					  {
   					  	
   					  	var default_button = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
											 +'<button type="button" class="btn btn-primary">Save changes</button>';

   					  	$('.modal').remove();



			            var config = $.extend({                
				                title: 'House Call Doctor',
				                message: 'No message to display.'
			            }, modal_config );

   					  	$modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
										  +'<div class="modal-dialog" role="document">'
										    +'<div class="modal-content">'
										      +'<div class="modal-header">'
										        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
										        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
										      +'</div>'
										      +'<div class="modal-body">'+ config.message +'</div>'
										      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
										    +'</div>'
										  +'</div>'
										+'</div>').appendTo('body');


					 $modal_object.modal({keyboard:false, backdrop:false})
					            .on('hidden.bs.modal', function (e) {
					                $(this).remove();
					            }); 
   					  }
   					}


	   	var patient  = {

                     	import_patients : function ()
                     	{

				   	    }
				    }	


$(function(){
     
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified	 = function(e, ui) {    

        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;  
    };

		$(document).on('change', '.btn-file :file', function() {
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});

		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {

			var input = $(this).parents('.input-group').find(':text'),
			    log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {
			    input.val(log);
			} else {
			    if( log ) alert(log);
			}

		});

		$('.action-button').click(function()
		{
		  $(	'#formtype').val($(this).attr('alt'));
		});

		$('.cancel').click(function(){
			window.location="maintenance/patientlist";
		});

		$('.delete').on('click',function()
		{
		  	$('#patient-form').validator('destroy').submit();
		});		

		/*$('.datepicker').daterangepicker({
		    singleDatePicker: true,
		    showDropdowns: true,
		    locale: {
            format: 'YYYY/MM/DD'
        	}
		});	*/	

		$('.datepicker').daterangepicker({
		    singleDatePicker: true,
		    showDropdowns: true,
	        locale: {
	            format: 'DD/MM/YYYY'
	        },
	        format: 'DD/MM/YYYY'
		});

		$('.search').click(function(){			
			
				$('#PatientListForm').submit(function(event){ 
			
				   	var search = $('input[name=search_patient]').val();
			        if ( search != "" ) {
		              $('#formtype').val('search');
			          $(this).attr('action','maintenance/patientlist/?&search='+search);
			          $(this).submit();
			        }

			    }); 
		})

	

});