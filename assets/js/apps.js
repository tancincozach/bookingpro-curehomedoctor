 //Self-Executing Anonymous Function
 //Encapsulate by hcd
 ;(function(hcd, $, undefined) {
  
    hcd.processing = false;

    hcd.common = {

        confirm: function(msg){
            
            var str = msg || 'Confirm Submit';

            if( confirm(str) ){
                return true;
            }

            return false;
        },

        alert: function(opts){

            var defaults = $.extend({                
                title: 'Alert',
                message: "Alert"
            }, opts );

            var tpl = '<div class="modal fade">'
            +'    <div class="modal-dialog modal-md">'
            +'        <div class="modal-content">'
            +'            <div class="modal-header">'
            +'                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            +'                <h4 class="modal-title">'+defaults.title+'</h4>'
            +'            </div>'
            +'            <div class="modal-body">'
            +'                <p>'+defaults.message+'</p>'
            +'            </div>'
            +'            <div class="modal-footer">'
            +'                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
            +'            </div>'
            +'        </div><!-- /.modal-content -->'
            +'    </div><!-- /.modal-dialog -->'
            +'</div><!-- /.modal -->';

            $(tpl).modal({keyboard:false, backdrop:false})
            .on('hidden.bs.modal', function (e) {
                $(this).remove();
            }); 

        },

        //use on toggling ref number
        ref_num_toggle: function(field, str1, str2){
            var fld_d = $(field).html();

            if( fld_d == str1 ){
                $(field).html(str2);
            }else{
                $(field).html(str1);
            }
        },

        //format: dd/mm/yyyy
        isDOB: function(xdate){
            var currVal = xdate;
            if(currVal == '')
                return false;
            
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern);
            
            if (dtArray == null) 
                return false;
            
            //dd/mm/yyyy format
            dtDay = dtArray[1];
            dtMonth= dtArray[3];
            dtYear = dtArray[5];
            
            if (dtMonth < 1 || dtMonth > 12){
                
                return false;

            }else if (dtDay < 1 || dtDay> 31){

                return false;

            }else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31){
                
                return false;

            }else if (dtMonth == 2){

                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                        return false;
            }
            return true;
        },

        //dd/mm/yyyy
        getAGE: function(xdate){

            var currVal = xdate;
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern);

            if (dtArray == null) 
                return 0;
             
            dtDay = dtArray[1];
            dtMonth= dtArray[3]-1; //-1 month since js month start at 0
            dtYear = dtArray[5];

            var birthday = new Date();
            birthday.setDate(dtDay); 
            birthday.setMonth(dtMonth); 
            birthday.setFullYear(dtYear); 
      
            var dob = new Date(birthday);
            var currentDate = new Date();
            var currentYear = currentDate.getFullYear();
            var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
            var age = currentYear - dob.getFullYear();

            if(birthdayThisYear > currentDate) {
                age--;
            }

            if(age < 1) age = 0;

            return age;     

        }
    }

    hcd.area = {


    }

    hcd.wyhau = {
        

    }

   common_plugin = 
                {
                  modal :function( modal_config , modal_button )
                  {
                    
                    var default_button = '<button type="button" class="btn btn-primary">Save changes</button>'
                                     +'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';

                    $('.modal').remove();



                var config = $.extend({                
                        title: 'BookingPRO',
                        message: 'No message to display.'
                }, modal_config );

                    $modal_object = $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
                                  +'<div class="modal-dialog" role="document">'
                                    +'<div class="modal-content">'
                                      +'<div class="modal-header">'
                                        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                                        +'<h4 class="modal-title" id="myModalLabel">'+config.title +'</h4>'
                                      +'</div>'
                                      +'<div class="modal-body">'+ config.message +'</div>'
                                      +'<div class="modal-footer">'+ (modal_button ? modal_button: default_button)+'</div>'
                                    +'</div>'
                                  +'</div>'
                                +'</div>').appendTo('body');


             $modal_object.modal({keyboard:false, backdrop:false})
                        .on('hidden.bs.modal', function (e) {
                            $(this).remove();
                        }); 
                  }
                }


    hcd.notice_group = '';

    hcd.notification = {           



              update_bell: function(response){

        /*        var obj = '';
                if( response.notice_group == 1 ) obj = $('#a_notice_gen'); //general;
                if( response.notice_group == 2 ) obj = $('#a_notice_ros'); //roster*/

                obj = $('#general_notices_li > a');

                var content_html = '<div class="myblink">'
                +((response.total>0)?'<span class="badge notice-count" >'+response.total+'</span>':'')
                +'    <span class="glyphicon glyphicon-bell '+(response.notice_group==2 && response.total > 0?'myblink':'')+'"></span>'
                +'</div>';

                obj.html(content_html);

            },
            init_modal_events:function(date_time_picker)
            {

                                $('#new_expiry_date').datetimepicker( {format: "YYYY-MM-DD HH:mm"} );

                                if(date_time_picker.length > 0)
                                {
                                    $(date_time_picker.join(',')).datetimepicker( {format: "YYYY-MM-DD HH:mm"} );

                                }
                                
               
                                $('.update').on('click',function(event){
                                    var 
                                        $container   = $(this).parent().parent().parent().children('.notif-details') ,
                                         note_id = $(this).attr('alt'),                            
                                         html        = $container.find('textarea[name=content_html]').val(),
                                         expiry      = $container.find('input[name=expiry_date]').val() ,                              
                                         post        = {'content':html,expire:expiry,group:hcd.notice_group,agent_name:hcd_globals.agent_name,user_id:hcd_globals.user_id};
                                         hcd.notification.update(post,note_id);
                                       event.preventDefault();
                                });
             
                              $('.add').on('click',function(event){
                                    var 
                                        $container   = $('#new_notice') ,                             
                                         html        = $container.find('textarea[name=content_html]').val(),
                                         expiry      = $container.find('input[name=expiry_date]').val() ,                              
                                         post        = {'content':html,expire:expiry,group:hcd.notice_group,agent_name:hcd_globals.agent_name,user_id:hcd_globals.user_id};                             
                                         hcd.notification.add(post);
                                         event.preventDefault();
                                });


                             $('.delete').on('click',function(event){

                              
                                if( confirm('Are you sure  you want to delete this record ?') ){
                                    if(hcd.notification.delete($(this).attr('alt'),hcd.notice_group))
                                    {
                                        $(this).parent().parent().remove();
                                    }
                                }   
                                event.preventDefault();
                                return false;
                            });


            },
            append_by_row:function(notice_id)
            {
                $.ajax({
                url:'ajax/get_notification_by_row',
                type:'POST',
                data:{id:notice_id},
                dataType: 'json',       
                success:function( response )
                {

                      hcd.notification.update_bell(response);

                    if(!$.isEmptyObject(response.error))
                    {
                      hcd.common.alert({'title':title,'message':response.error});     
                    }
                    else
                    {
                             var  date_obj = [] ;

                             var date_created  = ($.isEmptyObject(response.date_created) ? 'N/A':response.date_created),
                                agent_name    = ($.isEmptyObject(response.agent_name) ? 'N/A':response.agent_name),
                                expiry_date    = ($.isEmptyObject(response.expire_date) ? '':response.expire_date),
                                html          = ($.isEmptyObject(response.content_html) ? 'N/A':response.content_html);

                        date_obj.push('#expiry_datetimepicker_'+response.id);

                        $('.notice-list').append(
                                '<li class="list-group-item"><p><span>'+date_created+'</span>&nbsp;<span>'+agent_name+'</span><div class="pull-right"><button class="btn btn-xs btn-primary  show-notice" data-toggle="collapse" data-target="#note_'+response.id+'" aria-expanded="false" aria-controls="'+response.id+'" >Update</button>&nbsp;<button class="btn btn-xs btn-danger delete" alt="'+response.id+'">Delete</button></p></div><p>'+html+
                                '</p>'+
                                '<div id="note_'+response.id+'" class="notif-details collapse">'+    
                                '<div class="form-group">'+
                                    '<label for="text_notes">Notice</label>'+
                                    '<textarea name="content_html" rows="3" class="form-control">'+html+'</textarea>'+                                            
                                '</div>'+
                                 '<div class="form-group">'+
                                    '<label for="text_notes">Expiry</label>'+
                                     '<div class="input-group date"  id="expiry_datetimepicker_'+response.id+'">'+
                                     '<input type="text" class="form-control" name="expiry_date"  value="'+expiry_date+'"/>'+
                                     '<span class="input-group-addon">'+
                                     '<span class="glyphicon glyphicon-calendar"></span>'+
                                    '</span>'+
                                '</div>'+
                                '</div>'+                                             
                                  '<div class="form-group text-right">'+
                                    '<button class="btn btn-success update btn-sm" alt="'+response.id+'">Save</button>'+
                                '</div>'+   
                                '</div>'+
                                '</li>'
                        );
                        $('.content_html,.new_expire_date').val('');
                          hcd.notification.init_modal_events(date_obj);
                    }

                }
             });
        
            },
            update_list:function()
            {
                  $.ajax({
                        url:'ajax/get_notification',
                        type:'POST',
                        data:{'group':hcd.notice_group},
                        dataType: 'json',       
                        success:function( request )
                        {

                          var  list = '' ,
                               date_obj = [];
                                        

                                                $.each(request,function( i ,value){
                                                    
                                                    var date_created  = ($.isEmptyObject(value.date_created) ? 'N/A':value.date_created),
                                                        agent_name    = ($.isEmptyObject(value.agent_name) ? 'N/A':value.agent_name),
                                                        expiry_date    = ($.isEmptyObject(value.expire_date) ? '':value.expire_date),
                                                        html          = ($.isEmptyObject(value.content_html) ? 'N/A':value.content_html);

                                                        date_obj.push('#expiry_datetimepicker_'+value.id);

                                                       list+=   '<li class="list-group-item"><p><span>'+date_created+'</span>&nbsp;<span>'+agent_name+'</span><div class="pull-right"><button class="btn btn-xs btn-primary  show-notice" data-toggle="collapse" data-target="#note_'+value.id+'" aria-expanded="false" aria-controls="'+value.id+'" >Update</button>&nbsp;<button class="btn btn-xs btn-danger delete" alt="'+value.id+'">Delete</button></p></div><p>'+html+
                                                            '</p>'+
                                                            '<div id="note_'+value.id+'" class="notif-details collapse">'+    
                                                            '<div class="form-group">'+
                                                                '<label for="text_notes">Notice</label>'+
                                                                '<textarea name="content_html" rows="3" class="form-control">'+html+'</textarea>'+                                            
                                                            '</div>'+
                                                             '<div class="form-group">'+
                                                                '<label for="text_notes">Expiry</label>'+
                                                                 '<div class="input-group date"  id="expiry_datetimepicker_'+value.id+'">'+
                                                                 '<input type="text" class="form-control" name="expiry_date"  value="'+expiry_date+'"/>'+
                                                                 '<span class="input-group-addon">'+
                                                                 '<span class="glyphicon glyphicon-calendar"></span>'+
                                                                '</span>'+
                                                            '</div>'+
                                                            '</div>'+                                             
                                                              '<div class="form-group text-right">'+
                                                                '<button class="btn btn-success update btn-sm" alt="'+value.id+'">Save</button>'+
                                                            '</div>'+   
                                                            '</div>'+
                                                            '</li>'
                                                            
                                                });



                                $('.notice-list').html(list);

                                hcd.notification.init_modal_events(date_obj);



                        }
                        ,error:function(response)
                        {
                               console.log(response);
                        }

                     })

            }
            ,
            view:function(notice_group)
            {
             var title = (notice_group==1 ? 'General Notice':'Roster Notice');
            hcd.notice_group  = notice_group;
             $.ajax({
                url:'ajax/get_notification',
                type:'POST',
                data:{'group': hcd.notice_group},
                dataType: 'json',       
                success:function( request )
                {
                var  date_obj = [] ,
                     add_form = '<button class="new btn btn-info  btn-sm  pull_right" data-toggle="collapse" data-target="#new_notice" aria-expanded="false" aria-controls="new_notice">New Notice</button>'+
                                '<div id="new_notice" class="collapse">'+ 
                                '<input type="hidden" class="group" value="'+hcd.notice_group+'">'+    
                                            '<div class="form-group">'+
                                                '<label for="text_notes">Notice</label>'+
                                                '<textarea name="content_html" rows="3" class="form-control"></textarea>'+                                            
                                            '</div>'+
                                            '<div class="form-group">'+
                                                '<label for="text_notes">Expiry</label>'+
                                                 '<div class="input-group date"  id="new_expiry_date">'+
                                                 '<input type="text" class="form-control" name="expiry_date"  />'+
                                                 '<span class="input-group-addon">'+
                                                 '<span class="glyphicon glyphicon-calendar"></span>'+
                                                '</span>'+
                                            '</div>'+
                                            '</div>'+                                        
                                              '<div class="form-group text-right">'+
                                                '<button class="btn btn-success add btn-sm" >Submit</button>&nbsp;&nbsp;'+ 
                                                '<button class="btn btn-danger  btn-sm" data-toggle="collapse" data-target="#new_notice" aria-expanded="false" aria-controls="new_notice" >Cancel</button>'+
                                            '</div>'+   
                                        '</div>';

                if(!$.isEmptyObject(request.error))
                {                        
                    hcd.common.alert({'title':title,'new_expire_date':request.error});  
                }
                else
                {          
                     if($.isEmptyObject(request)){
                        list='<ul class="notice-list list-group"></ul>'+add_form;
                     }
                     else
                     {
                          var  list = '<ul class="notice-list list-group">';
                    

                            $.each(request,function( i ,value){
                                
                                var date_created  = ($.isEmptyObject(value.date_created) ? 'N/A':value.date_created),
                                    agent_name    = ($.isEmptyObject(value.agent_name) ? 'N/A':value.agent_name),
                                    expiry_date    = ($.isEmptyObject(value.expire_date) ? '':value.expire_date),
                                    html          = ($.isEmptyObject(value.content_html) ? 'N/A':value.content_html);

                                    date_obj.push('#expiry_datetimepicker_'+value.id);

                                    list +='<li class="list-group-item"><p><span>'+date_created+'</span>&nbsp;<span>'+agent_name+'</span><div class="pull-right"><button class="btn btn-xs btn-primary  show-notice" data-toggle="collapse" data-target="#note_'+value.id+'" aria-expanded="false" aria-controls="'+value.id+'" >Update</button>&nbsp;<button class="btn btn-xs btn-danger delete" alt="'+value.id+'">Delete</button></p></div><p>'+html+
                                        '</p>'+
                                        '<div id="note_'+value.id+'" class="notif-details collapse">'+    
                                            '<div class="form-group">'+
                                                '<label for="text_notes">Notice</label>'+
                                                '<textarea name="content_html" rows="3" class="form-control">'+html+'</textarea>'+                                            
                                            '</div>'+
                                             '<div class="form-group">'+
                                                '<label for="text_notes">Expiry</label>'+
                                                 '<div class="input-group date new_expire_date"  id="expiry_datetimepicker_'+value.id+'">'+
                                                 '<input type="text" class="form-control" name="expiry_date"  value="'+expiry_date+'"/>'+
                                                 '<span class="input-group-addon">'+
                                                 '<span class="glyphicon glyphicon-calendar"></span>'+
                                                '</span>'+
                                            '</div>'+
                                            '</div>'+                                             
                                              '<div class="form-group text-right">'+
                                                '<button class="btn btn-success update btn-sm" alt="'+value.id+'">Save</button>'+
                                            '</div>'+   
                                        '</div>'+
                                        '</li>';
             
                                        
                            });

                            list+='</ul>'+add_form;
                            
 
                     }

                        
                                
                    hcd.common.alert({'title':title,'message':list});       

                    $('#new_expiry_date').datetimepicker( {format: "YYYY-MM-DD HH:mm"} );

                    if(date_obj.length > 0)
                    {
                        $(date_obj.join(',')).datetimepicker( {format: "YYYY-MM-DD HH:mm"} );

                    }
                    
    
                     hcd.notification.init_modal_events(date_obj);


                }
                }
                ,error:function(response)
                {
                       console.log(response);
                }

             })
            },
              delete:function(notice_id)
            {
              var title  = (hcd.notice_group==1 ? 'General Notice':'Roster Notice');

                 try{
                        if(notice_id=='' || notice_id==0)
                                throw "Error: Note ID is empty."; 


                            $.ajax({
                                url:'ajax/delete_notice/?id='+notice_id,
                                data:{id:notice_id,agent_name:hcd_globals.agent_name,'notice_group':hcd.notice_group},
                                type:'post',
                                dataType: 'json', 
                                success:function(response)
                                {
                                       if(!$.isEmptyObject(response.error))
                                        {                        
                                            hcd.common.alert({'title':title,'message':response.error});  
                                        }
                                        else
                                        {
                                             hcd.common.alert({'title':title,'message':response.message});  
                                             hcd.notification.update_bell(response);
                                        }
                                }
                                ,error:function( response )
                                {
                                 hcd.common.alert({'title':'Alert Box', 'message':response});               
                                }
                            })
                       return true;
                    }
                     catch( err )
                    {
                        hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
                        return false;
                    }
            }
            ,
            add:function( posted_data)
            {
                try{

                    if($.isEmptyObject(posted_data))
                            throw "Error: Input fields are emtpy."; 

                  if(hcd.notice_group == 1)
                        title = "General  Notices";
                    else
                        title = "Roster  Notices";

                        $.ajax({
                            url:'ajax/add_notice',
                            data:posted_data,
                            type:'post',
                            dataType: 'json', 
                            success:function(response)
                            {
                               if(!$.isEmptyObject(response.error))
                                {                        
                                    hcd.common.alert({'title':title,'message':response.error});  
                                }
                                else
                                {                                         
                                     hcd.notification.append_by_row(response.last_id);

                                }
                            }
                            ,error:function( response )
                            {
                             hcd.common.alert({'title':'Alert Box', 'message':response});               
                            }
                        })
                }
                 catch( err )
                {
                    hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
                }

            },

            update:function( posted_data ,id)
            {
                try{
                    if(id=='' || id==0)
                            throw "Error: Note ID is empty."; 
                    if($.isEmptyObject(posted_data))
                            throw "Error: Input fields are emtpy."; 

                    if(hcd.notice_group == 1)
                        title = "General  Notices";
                    else
                        title = "Roster  Notices";

                        $.ajax({
                            url:'ajax/update_notice/?id='+id,
                            data:posted_data,
                            type:'post',
                            dataType: 'json', 
                            success:function(response)
                            {
                                   if(!$.isEmptyObject(response.error))
                                    {                        
                                        hcd.common.alert({'title':title,'message':response.error});  
                                    }
                                    else
                                    {
                                         hcd.common.alert({'title':title,'message':response.message});  
                                         hcd.notification.update_list();
                                    }
                            }
                            ,error:function( response )
                            {
                             hcd.common.alert({'title':'Alert Box', 'message':response});               
                            }
                        })
                }
                 catch( err )
                {
                    hcd.common.alert({'title':'Alert Box', 'message':err.message||err});
                }

            }
    }




          

}(window.hcd = window.hcd || {}, jQuery));


   var StickyNotes = {
                ck_name: hcd_globals.cookie_name+'-note',
                cls: 'mynote',
                _init: function() {


                    var coo = this._parse();
                  
                    var div = '<div class="'+this.cls+' '+coo.s+'">'
                        +'<a href="javascript:void(0)" onclick="StickyNotes.activate(\'\')" title="Hide Note" class="close-mynote"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>'
                        +'<form>'
                        +'<strong>My Note</strong>'
                        +'<textarea rows="10" id="mynote-input" onkeyup="StickyNotes.onWriteText(this)" >'+coo.n+'</textarea>'
                        +'</form>'
                        + '</div>';

                    $('#ribbon').html('');
                    $('#ribbon').append(div);
                },

                _parse: function(){                    
                    var coo = $.cookie(this.ck_name);
           
                   if($.isEmptyObject(coo))
                    {
                                        

                        $.cookie(this.ck_name,  JSON.stringify({'s':'','n':''}) , { expires: 7, path: '/' });                

                            coo = $.cookie(this.ck_name);

                           return coo = JSON.parse(coo);    
                                                                      
                    }   
                    else
                    {
                            coo = JSON.parse(coo);
                    }
                        return coo;

                },

                activate: function(show) {

                    var coo = this._parse();
                    
                    var arr_data = {};
                    arr_data["s"] = show;
                    arr_data["n"] = coo.n;

                    arr_data = JSON.stringify(arr_data);

                    $.cookie(this.ck_name, arr_data, { path: '/',  json: true });

                    $('.'+this.cls).toggleClass("activate");
                },

                onWriteText: function(field) {

                    var coo = this._parse();

                    var noteTxt = $(field).val();
             
                    var arr_data = {};
                    arr_data["s"] = coo.s;
                    arr_data["n"] = noteTxt;

                    arr_data = JSON.stringify(arr_data);

                    $.cookie(this.ck_name, arr_data, { path: '/' });

                }

            }

     var Notes = 
        {   
            getRootUrl: function () {

              var defaultPorts = {"http:":80,"https:":443};

              var url  = window.location.protocol + "//" + window.location.hostname + (((window.location.port)  && (window.location.port != defaultPorts[window.location.protocol])) ? (":"+window.location.port) : "");


              url = url.replace('.welldone.net.au','').replace(/^https?:\/\//,'').replace(/^http?:\/\//,'');


              return url ;
            }
            ,
            createDummyNoteList:function() 
            {
                var new_note =  {notes:[{title:'note1',content:'content 1'},{title:'note2',content:'content 2'},{title:'note3',content:'content 3'}]}  
                localStorage.setItem(this.getRootUrl()+"_notes", JSON.stringify(new_note));      
            }
            ,
             setLocalStorage:function(title,content){
                
                var  new_note = {title:title,content:content}

                localStorage.setItem("notes", JSON.stringify(new_note));      
              
             }
             ,
             createNoteLists:function(notes)
             {      
                    $('#note_list').remove();

                    $note_div_container = $('.right-menu');             
                    
                    if(!$.isEmptyObject(notes))
                    {

                        $note_div_container.append($('<li>',{id:'note_list',class:'dropdown note_dropdown'}).html('<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="" class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;Notes<strong class="caret"></strong></a>'));
                        $note_list = $('<ul/>',{class:'dropdown-menu','style':'text-transform:uppercase'});

      
                $note_list.append($('<li>').html('<a href="#" class="select_note" ><span style="" class="glyphicon glyphicon-plus"></span>&nbsp;Add New</a>'));
                        $.each(notes , function(index,value){   
                                $.each(value,function(i,val){                                                                      
                                      $note_list.append($('<li>').html('<a href="#" class="select_note" >'+val.title+'</a>'));

                                });
                                  
                        });
     
                      $('#note_list').append($note_list);
                    }

             }
             ,checkLocalStorage:function(notes) {

                
                    var notes = localStorage.getItem(this.getRootUrl()+"_notes");

                   console.log(this.getRootUrl());

                    if(notes != null || notes != undefined)
                    {

                         note_list  = JSON.parse(notes);
                         this.createNoteLists(note_list);

                    }
                    else
                    {
                        this.createDummyNoteList();
                        console.log('empty!');
                    }

             },
             init:function()
             {
                this.checkLocalStorage();
             }
       }



$( document ).ready(function(){



    //console.log( "ready!" );


    $.ajaxSetup({
        beforeSend: function(jqXHR, settings) {
             
            if( settings.type == 'POST' ){ 
                settings.data = settings.data+'&'+hcd_globals.n+'='+hcd_globals.h;
            }
            return true;
        }
    });

    //Initialize tooltip
    $('[data-toggle="tooltip"]').tooltip();

    //Initialize popover
    $('[data-toggle="popover"]').popover();



    /*//Set .datetimepicker so that can be access even from ajax rendered html
    $('body').on('focus',".datetimepicker", function(){
        $(this).datetimepicker({format: "YYYY-MM-DD HH:mm",pickTime:true}); 
    }); 

    $('body').on('focus',".datepicker", function(){
        $(this).datetimepicker({format: "YYYY-MM-DD",pickTime:false});  
    }); */

    /*$(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    }); 

    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function(e, ui) {    

        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;  
    };

    //Make diagnosis table sortable     
    $(".table-row-order tbody").sortable({ 
        helper: fixHelperModified,
        placeholder: "ui-state-highlight",
        cursor: 'move',
        containment: "parent"        
    }).disableSelection();  */


    /*//Initialize tinyMCE
    tinymce.init({
        selector: "#tinymce-textarea",
        document_base_url: JS_base_url,
        theme: "modern",
        //width: 300,
        height: 250,
        cleanup : true,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons paste textcolor",
             "filemanager template"
        ],
        convert_urls: false,
        //ontent_css: "css/style.css",
        content_css: "assets/css/custom.css",        
        toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons | template", 
        style_formats : [
            {title : 'Orange Bold', inline : 'span', styles : {color : '#DAA520', fontWeight : 'bold'}},
            {title : 'Green Bold', inline : 'span', styles : {color : '#008000', fontWeight : 'bold'}},
            {title : 'Red Bold', inline : 'span', styles : {color : '#ff0000', fontWeight : 'bold'}},
            {title : 'Indigo Bold', inline : 'span', styles : {color : '#4B0082', fontWeight : 'bold'}},
            {title : 'Dark Blue Bold', inline : 'span', styles : {color : '#00008B', fontWeight : 'bold'}},
            {title : 'Yellow highlight', inline : 'span', styles : {backgroundColor : 'yellow'}},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Header styles'},
            {title : 'Red header 1', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Green header 1', block : 'h1', styles : {color : '#008000'}},
            {title : 'Indigo header 1', block : 'h1', styles : {color : '#4B0082'}},
            {title : 'Float styles'},
            {title : 'Float Right', block : 'div', styles :{border : '1px solid #1E90FF', backgroundColor : 'lightskyblue', width : '250px', padding : '10px', float : 'right', margin : '5px'}},
            {title : 'Float Left', block : 'div', styles :{border : '1px solid #1E90FF', backgroundColor : 'lightskyblue', width : '250px', padding : '10px', float : 'left', margin : '5px'}},
            {title : 'Box styles'},
            {title : 'Yellow BG Red Border', block : 'div', styles :{border : '1px solid #ff0000', backgroundColor : '#EEE8AA', padding : '10px', margin : '5px', marginLeft : '25px',marginRight : '25px', textAlign : 'center'}},
            {title : 'Green BG Red Border', block : 'div', styles :{border : '1px solid #ff0000', backgroundColor : '#98FB98', padding : '10px', margin : '5px', marginLeft : '25px', marginRight : '25px', textAlign : 'center'}},
            {title : 'Blue BG Red Border', block : 'div', styles :{border : '1px solid #ff0000', backgroundColor : '#AFEEEE', padding : '10px', margin : '5px', marginLeft : '25px', marginRight : '25px', textAlign : 'center'}},
            {title : 'Pink BG Red Border', block : 'div', styles :{border : '1px solid #ff0000', backgroundColor : '#FFC0CB', padding : '10px', margin : '5px', marginLeft : '25px', marginRight : '25px', textAlign : 'center'}},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }

    }); */

           StickyNotes._init();
          /*Notes.init();*/

            var stickyTop = $('.mynote').offset().top; // returns number
            $(window).scroll(function(){ // scroll event
                var windowTop = $(window).scrollTop()+stickyTop; // returns number 
                $('.mynote').css('top', windowTop);
                 
            });

});